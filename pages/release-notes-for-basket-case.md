---
guid: http://www.benjaminrose.com/?page_id=3299
layout: page
title: Release notes for Basket Case
author: Ben
date: 2020-02-20T04:03:23.000Z
aside:
  toc: true
id: 3299
sidebar:
  nav: basket-case-menu
---
These are the release notes and change logs for Basket Case.

{% include _reusedContent/warningBasketCase2Upgrade.md %}


## v2.1.109

**Date**: 15 May 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)\
**Primal Lib version**: 1.1.60

### Changes

* Now requires Primal Lib 1.1.60.
* Updated basket models so they look better in the hand, on the ground, etc.
* You can now allow baskets in chests and other inventories while still excluding them from the player's main inventory. This is configurable.
* Baskets are now always allowed in the offhand slot.
* Fixed a compatibility issue where baskets could be duplicated when put in the offhand slot using Quark hotkeys.
* Fixed a bug where right clicking on a basket while holding a block would delete one block from the stack. It now opens the basket GUI, or places the block if you're sneaking.

## v2.1.98

**Date**: 20 March 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)\
**Primal Lib version**: 1.1.57

### Changes

* Fix issue with basket contents being lost when exiting and reopening a world.

## v2.1.95

**Date**: 5 March 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)\
**Primal Lib version**: 1.1.57

### Changes

#### General changes

* Now requires Primal Lib 1.1.57.
* When viewing a basket's tool tip on an inventory screen, press Shift to see a preview of the basket's contents.
* Baskets now have normal vanilla behavior when broken (the basket drops and all its contents spill out, just like a chest). You can enable the "hardcore basket breakage" configuration option to restore the old behavior (the basket is destroyed when broken and all its contents drop). Either way, you can still pick up a basket by sneak-right-clicking on it.
* Baskets no longer insta-break and have a more appropriate sound when being placed and broken.
* Generic baskets can only be made from vines when wood-specific baskets are disabled (I'm assuming you are not interested in generic baskets if you are enabling wood-specific baskets).

#### Compatibility with other mods

* Basket Case now has better compatibility with several popular and thematically similar mods, including:

    * Better With Mods
    * Biomes O' Plenty
    * No Tree Punching

See the [Primal Lib release notes]({% link pages/release-notes-for-primal-lib.md %}) for most of the details, since most of the changes occurred in Primal Lib. See below for some changes that were specific to Basket Case itself.
  
#### Compatibility with Biomes O' Plenty

* Ivy and willow vines from Biomes O' Plenty can now serve as cordage in basket recipes and are also registered as vines in the OreDictionary. This makes it so they can be crafted into generic baskets if wood-specific baskets are disabled.
* Bushes and shrubs from Biomes O' Plenty will now drop oak twigs as long as twigs and wood-specific baskets are enabled. Otherwise they will drop generic twigs.

#### Bug fixes

* There were some bugs where you could effectively duplicate items in a basket. For example, dropping the basket when its inventory was open would allow you to move the items into your inventory while still keeping the items in the dropped basket. You could also move a basket to another hotbar slot while its inventory was open and achieve the same result. I've fixed this so now you can't move or drop a basket from its current hotbar slot as long as its inventory screen is open.

### Known issues

* When opening the inventory in creative mode, all contents of a basket will be deleted. This is a [bug with Forge](https://github.com/MinecraftForge/MinecraftForge/issues/7359) that has been around for years. I can't fix it.

## v2.1.93

**Date**: 24 February 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)\
**Primal Lib version**: 1.1.56

### Changes

* This release had everything listed under v2.1.95, but it had a bug that prevented plant fibers from being crafted into twine. I pulled this release from CurseForge. If you downloaded it, please upgrade to v2.1.95.

## v2.0.79

**Date**: 14 February 2021\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)\
**Primal Lib version**: 1.0.43

### Changes

  * Now requires Primal Lib 1.0.43, which has a fixed config file.
  * You can now use Better With Mods bark to craft Basket Case bark strips.
  * Made config text translatable.

## v2.0.75

**Date**: 24 January 2021\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)\
**Primal Lib version**: 1.0.39

### Changes

  * Now requires Primal Lib 1.0.39, which has a minor bug fix.
  * Updated mod info and logo.
  * Removed annoying output in the Minecraft console.

### Bug fixes

  * Fixed the recipe advancement for bark strips.

## v2.0.72

**Date**: 20 September 2020\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)\
**Primal Lib version**: 1.0.36

{: .warning}
**ATTENTION:** Basket Case 2.0 is a major upgrade for Basket Case. Make a backup of your world before upgrading Basket Case from 1.0.

### Major changes

  * **Basket Case now requires the mod Primal Lib.** [Primal Lib]({% link pages/primal-lib.md %}) is my own library mod, which I created to help speed up my ability to create mods. I will be releasing some of these other mods soon.
  * **Most items and blocks in the mod were given new IDs** under the hood to follow better coding standards, because when I first started the mod, I didn’t really know what I was doing. This is why you should make a backup of your world before upgrading Basket Case.
  * **Basket Case is now built on Forge 14.23.5.2768.** Other versions of Forge may work, but be aware that you may run into buggy behavior. Note that 14.23.5.2768 is NOT the latest recommended build of Forge. The latest recommended build is 2854. Unfortunately, Forge dropped support for that version after only a few weeks of releasing it. I ran into too many problems trying to update and couldn't get any support to resolve the problems.
  * I refactored a lot of old, crappy code under the hood. Basket Case is now more stable and easier to change, and it's less embarrassing to have other modders look at my code.
  * All recipes have been changed to be a little more intuitive and, in some cases, simpler.

### New features

  * **Twelve new baskets.** You can now create baskets out of different kinds of wood. Each type of vanilla tree will drop their own kind of twigs and bark, and the baskets made out of these specific materials will be colored like the bark or wood of each vanilla tree. I think the new baskets look pretty nice. However, as with most things in my mod, you can turn off this feature and have only the old, generic baskets if you wish.
  * **The default stack size for basket inventories has been increased.** A small basket can now hold 64 total items (4 slots, 16 items per slot), so it’s at least as much as a regular stack of items now. Medium and large basket inventories have been increased, as well. You can tweak these values in the config file.
  * **Unused basket ingredients are now furnace fuel.** They have very short burn times, but if you have a lot of extra twigs, you could cook yourself a steak with them. This can be helpful if you find yourself collecting tons of excess items. (Note that you can already use the config file to control drop rates. You can also require players to sneak to get the drops, which puts the power into the hands of the players if they want the drops or not.)
  * All items in the creative inventory are now in a nice order, instead of being all mixed up and random.
  * The basket inventory GUI got a slight upgrade, with the basket slots now appearing centered instead of on the far left.
  * Stripped logs now use the vanilla textures from 1.13.

### Bug fixes

  * Baskets were causing suffocation if you walked into them at head height or jumped up into them. I fixed that problem.
  * The default behavior in Basket Case is that baskets can only be placed in the hot bar. I fixed a bug with this behavior where you could get baskets into your regular inventory by shift-clicking the basket while in your hot bar. I fixed another bug where baskets could get into your regular inventory if they were in the crafting table when you closed the crafting table screen. Note that this behavior can be turned off in the config file.
  * In Basket Case, you’re supposed to be able to break a vine block, and all vine blocks below it would automatically break and drop vines. This wasn’t happening, so I fixed it. Note that you can turn this behavior on or off in the config file.
  * Baskets had really heavy shadow around them, especially if you put them next to each other. I've fixed this lighting issue.

## v1.0.31

**Date**: 21 April 2019\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.4.2705

### Bug fixes

  * Fixed bug where sneaking while breaking basket would duplicate the items inside. Now this will drop the basket and the inventory separately.

## v1.0.29

**Date**: 11 October 2018\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.4.2705

### Enhancements

  * Added/fixed recipes for obtaining bark from the crafting grid.
  * Added config value for whether drop chance of twigs and plant fibers should vary based on tool being used.

## v1.0.25

**Date**: 8 October 2018\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.4.2705

  * Initial release.
