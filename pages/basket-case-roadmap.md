---
layout: page
title: Basket Case roadmap
author: Ben
date: 2021-01-25T03:33:14.287Z
aside:
  toc: false
sidebar:
  nav: basket-case-menu
---
This is less of a roadmap for Basket Case and more of a list of ideas I want to pursue, if I have time.

Most likely:

* Support Biomes O Plenty reeds
* Make it so the player looks like they're holding small basket by its handle, and all baskets look larger when held
* Make EntityItem baskets larger—they are too tiny right now
* Make baskets require a solid block below them, like doors and carpet—baskets will break if block below is broken
* Make baskets have facing, so you can control the direction of the handles
* Better fix for this bug: Can put baskets in main inventory by shift + left/right click.
* Set up auto-deployment to CurseForge

Somewhat likely:

* Make baskets optionally weak/breakable
   * Baskets break when you stand on them (and drop wicker/cordage?)
   * Baskets have durability, maybe varies depending on size/material - make optional
* Make number of slots configurable
* Put lid on basket when it has contents

Unlikely:

* Add more kinds of storage containers, like barrels
* Extra large baskets, 2+ blocks
* Baskets can be dyed
* Make contents visible in-world so they don't look empty