---
guid: http://www.benjaminrose.com/?page_id=2801
layout: page
title: Stripped logs
author: Ben
date: 2020-01-14T23:35:55-08:00
aside:
  toc: false
id: 2801
sidebar:
  nav: primal-lib-menu
---
In ancient times, tree bark was stripped from trees and separated into long strips for use as [cordage](https://www.primitiveways.com/cordage.html). Because stripping the bark from a tree can kill the tree, only small sections of the bark was stripped off any given tree.

In Primal Lib, I have chosen to follow the precedent set by Minecraft 1.13+ of stripping the bark from the entire log. However, stripped logs will not support leaves. This means if you strip bark off every log in the tree, all the leaves will die, leaving you with only a trunk.

{%
  include figure.html
  image-name="primalLib_strippedLogs.png"
  caption="All stripped logs in Primal Lib"
%}

There are six stripped log blocks. This page covers all six blocks.

## Obtaining

Stripped logs are obtained in one of two ways:

{% include _reusedContent/obtainingBarkAndStrippedLogs.md %}

## Using

**Crafting ingredient**. Stripped logs can be used in place of regular logs to make planks. If No Tree Punching compatibility is enabled in the Primal Lib config settings, you can put No Tree Punching saws and stripped logs in the crafting grid to make planks. If you're using Better With Mods, note that this recipe does not require an axe and does not produce sawdust.

The number of planks produced is configurable. The default is four. The max is 64. (Prior to 1.1.56, the recipe always produced 4 planks.) Set to 3 for better compatibility with Better With Mods and No Tree Punching. 

**Fuel**. Stripped logs can be burned in a furnace. How long they burn is configurable. The default is 300 ticks (15 seconds). For compatibility with Better With Mods, set the value somewhere between 1200 and 1600. (Prior to 1.1.56, the burn time was not configurable and stripped logs always burned for 300 ticks.)

## Configuring

  * You can disable stripped logs completely along with [bark]({% link pages/bark.md %}). As a consequence, there will be no [bark strips]({% link pages/bark-strips.md %}).
  * You can toggle whether stripped logs can be used to craft planks.
  * You can configure how many planks are produced from a stripped log. The default is four. The max is 64. (Prior to 1.1.56, the amount was not configurable and the recipe always produced 4 planks.)
  * You can configure how long stripped logs will burn in a furnace. For compatibility with Better With Mods, set the value somewhere between 1200 and 1600. (Prior to 1.1.56, the burn time was not configurable and stripped logs always burned for 300 ticks.)

## Data

|----
|Block|Block ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Stripped acacia log|primallib:log_stripped_acacia|item.primallib:log_stripped_acacia.name|logWood
|Stripped birch log|primallib:log_stripped_birch|item.primallib:log_stripped_birch.name|logWood
|Stripped dark oak log|primallib:log_stripped_dark_oak|item.primallib:log_stripped_dark_oak.name|logWood
|Stripped jungle log|primallib:log_stripped_jungle|item.primallib:log_stripped_jungle.name|logWood
|Stripped oak log|primallib:log_stripped_oak|item.primallib:log_stripped_oak.name|logWood
|Stripped spruce log|primallib:log_stripped_spruce|item.primallib:log_stripped_spruce.name|logWood
