---
id: 1350
title: Primalcraft
date: 2017-10-16T20:14:55-08:00
author: Ben
layout: page
guid: http://www.benjaminrose.com/?page_id=1350
sidebar:
  nav: primalcraft-menu
---
{: .warning}
**ATTENTION:** Primalcraft is no longer under development. I have decided to split it into multiple mods. I'm leaving this page here for historical purposes to show what features I may be working on for future mods. If you're interested in some of these features, let me know!

![Primalcraft logo](\assets\images\primalcraft_logo.png)

Primalcraft is a mod about extreme survival in the stone age. Do you think our primal ancestors punched trees to get logs and used wooden pickaxes to mine stone? Haha, no. Your first tools will be rocks and sticks. Your first meal will be a dead insect.

I created Primalcraft to do a few things. Mainly, I wanted Minecraft to be harder. Each achievement should come at a cost so you can truly be proud of it. I also wanted to encourage exploration and caving by making it a little harder to get basic needs without traveling and taking a few risks. Finally, I wanted to make it a little more believable and model strategies that our ancestors used, and adventurous folks today still use, to survive out in the wild.

## Primary features

  * **Stone age tools**. Your first tools—and weapons—are rocks and sticks. You can throw rocks and dig with sticks. To get better stone tools, you have to find hammer stones and core stones and use knapping to make stone tool heads, then bind them to a haft with cordage made from grass.
  * **Basket weaving**. You can use grass, bark, vines, and reeds to weave wicker baskets for your first form of storage.
  * **Hunger and thirst**. You get hungry faster. Your first meals will be grubs and locusts. You will also get thirsty. You can quench thirst by drinking from rivers and ponds with an empty hand.
  * **Hunting**. Animals run away from you and cannot be tamed (although taming will eventually be allowed). Kill them by throwing rocks or catching them in the water and beating them with a stick or stabbing them with a stone knife.
  * **Hostile wolves**. Wolves are your primary enemy. During the day they will avoid you, but at night they will hunt you down and eat you for dinner unless you have fire to keep them at bay. All the fantasy monsters are gone.
  * **Campfires**. To make a campfire, you have to rub sticks together to get a burning ember, then put it on tinder and blow to create flames.
  * **Dark nights**. It gets really dark at night. You did make a fire, right?
  * **Death with consequences**. When you die, you respawn in a random location far away from your original spawn point.
  * **Gravity**. Trees require tools to chop down, and they fall instead of hovering in the air. Every block in the game will fall if not properly supported.

## Secondary features

You start with a full 3x3 crafting grid in your inventory. There are no crafting tables. There are dirt slabs. Punching logs and stone hurts you. Baskets can be picked up by shift-right-clicking. Grass drops straw, leaves drop sticks, dirt drops grubs and stones, logs give bark, and broken tools sometimes yield parts that can be reused. There are hickory trees. Movement speed is based on terrain. You can move through leaves.

## Future features

Spears. Animal butchering. Hide armor. Dynamic lights. Detailed, individual wounds. Encumbrance. Copper tools.
