---
layout: page
title: Bark strips from specific trees
author: Ben
date: 2020-05-16T21:58:52.540Z
sidebar:
  nav: basket-case-2-0-menu
---
Basket Case's bark strips are based on [bark strips from Primal Lib]({% link pages/bark-strips.md %}). In Basket Case, bark strips are specific to the tree they come from, so there are six kinds of bark strips.

{%
  include figure.html
  image-name="basketCase2_barkStripsAcacia.png"
  alt="Acacia bark strip"
  caption="Acacia bark strip"
%}

{%
  include figure.html
  image-name="basketCase2_barkStripsBirch.png"
  alt="Birch bark strip"
  caption="Birch bark strip"
%}

{%
  include figure.html
  image-name="basketCase2_barkStripsDarkOak.png"
  alt="Dark oak bark strip"
  caption="Dark oak bark strip"
%}

{%
  include figure.html
  image-name="basketCase2_barkStripsJungle.png"
  alt="Jungle bark strip"
  caption="Jungle bark strip"
%}

{%
  include figure.html
  image-name="basketCase2_barkStripsOak.png"
  alt="Oak bark strip"
  caption="Oak bark strip"
%}

{%
  include figure.html
  image-name="basketCase2_barkStripsSpruce.png"
  alt="Spruce bark strip"
  caption="Spruce bark strip"
%}

Bark strips are the flexible *inner* bark of the tree, so their color is the same as that of the wood.

## Obtaining

Basket Case bark strips are obtained using the same recipe as generic [bark strips from Primal Lib]({% link pages/bark-strips.md %}). The only difference is that the resulting bark strips will be specific to the type of wood used.

## Using

Basket Case bark strips can be crafted into wicker—see the [wicker]({% link pages/wicker.md %}) page for the recipes.

In all other ways, Basket Case bark strips are identical to [bark strips from Primal Lib]({% link pages/bark-strips.md %}), including being registered as "cordage" in Forge's OreDictionary and serving as furnace fuel.

## Configuring

Configuration settings for [bark strips from Primal Lib]({% link pages/bark-strips.md %}) will also apply to Basket Case bark strips, including whether bark strips count as [cordage]({% link pages/cordage.md %}) and whether they are enabled at all.

In the Basket Case config settings, bark strips can be tweaked in the following ways:

  * You can choose to prevent wood-specific baskets. In that case, only the generic bark strips from Primal Lib will be present (if configured to be turned on in the Primal Lib config). Generic bark strips can still be crafted into [wicker]({% link pages/wicker.md %}), but it will be generic wicker that looks like oak wicker. If you _do_ allow wood-specific baskets, then generic bark strips will _not_ be available.

## Data

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Acacia bark strips|basketcase:bark_strips_acacia|item.basketcase:bark_strips_acacia.name|barkStrips, cordage, cordageStrong
|Birch bark strips|basketcase:bark_strips_birch|item.basketcase:bark_strips_birch.name|barkStrips, cordage, cordageStrong
|Dark oak bark strips|basketcase:bark_strips_dark_oak|item.basketcase:bark_strips_dark_oak.name|barkStrips, cordage, cordageStrong
|Jungle bark strips|basketcase:bark_strips_jungle|item.basketcase:bark_strips_jungle.name|barkStrips, cordage, cordageStrong
|Oak bark strips|basketcase:bark_strips_oak|item.basketcase:bark_strips_oak.name|barkStrips, cordage, cordageStrong
|Spruce bark strips|basketcase:bark_strips_spruce|item.basketcase:bark_strips_spruce.name|barkStrips, cordage, cordageStrong
