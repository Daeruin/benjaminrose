---
id: 1609
title: Basket Case 1.0 wiki
date: 2018-09-18T20:03:00-08:00
author: Ben
layout: page
guid: http://www.benjaminrose.com/?page_id=1609
aside:
  toc: true
sidebar:
  nav: basket-case-menu
---
## Wicker

Baskets are made with wicker. Wicker comes in three sizes—small wicker makes small baskets, medium wicker makes medium baskets, and large wicker makes large baskets. You'll need a crafting table to make medium and large wicker. (All the baskets themselves also require a crafting table to make.)

### Binding materials

Wicker can be made from a variety of materials. The first thing you need is something to bind your wicker together. Basket Case has four types of binding—two from vanilla and two that are new in Basket Case.

  * Vines
  * Reeds (sugar cane)
  * Bark strips
  * Cordage from plant fibers

Here's how to get bark strips and cordage from plant fibers.

#### Cordage from plant fibers

To get **plant fibers**, break tall grass (not regular grass blocks) or any double-tall plant except sugar cane. The chance of getting plant fibers depends on the tool you are using:

  * Bare hands: 50% chance
  * Shovel: 70% chance
  * Axe: 100% chance

Two or three plant fibers can then be braided into one or two pieces of cordage.

{%
  include figure.html
  image-name="primalLib_cordageRecipe.gif"
  width="224"
  height="136"
  alt="Cordage recipe"
  caption="Recipe for cordage from plant fibers"
%}

#### Bark strips

First get some **bark** from logs by right-clicking with an axe. This will drop four pieces of bark and turn the logs into stripped logs. Stripped logs can be used in place of vanilla logs in crafting recipes. (These are custom stripped logs in 1.12. When Basket Case is updated to 1.13, it will use vanilla stripped logs instead.) Place the bark in the crafting grid to turn it into bark strips.

{%
  include figure.html
  image-name="primalLib_barkStrips.gif"
  caption="Recipe for bark strips"
%}

Now that you have something to bind the wicker together, you can make the wicker itself. There are two basic recipes:

  * Wicker from twigs
  * Wicker from bark strips or vines

### Wicker from twigs

Gather **twigs** by breaking leaves. The chance of getting twigs depends on the tool you are using:

  * Bare hands: 50% chance
  * Axe: 100% chance

Bind the twigs together using the vines, reeds, cordage, or bark strips you gathered.

{%
  include figure.html
  image-name="basketCase1_smallWickerFromTwigsRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for small wicker from twigs"
  caption="Recipe for small wicker from twigs"
%}

{%
  include figure.html
  image-name="basketCase1_mediumWickerFromTwigsRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for medium wicker from twigs"
  caption="Recipe for medium wicker from twigs"
%}

{%
  include figure.html
  image-name="basketCase1_largeWickerFromTwigsRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for large wicker from twigs"
  caption="Recipe for large wicker from twigs"
%}

### Wicker from vines or bark strips

Baskets can be made directly from bark strips and vines, because they are much stronger than reeds and cordage. They don't need twigs to provide structure.

In Basket Case, you can gather **vines** using an axe instead of shears. When you break a vine block with an axe, all the vines directly beneath it will automatically break and drop vines as well.

See the directions above on how to get [bark strips](#bark-strips).

{%
  include figure.html
  image-name="basketCase1_smallWickerFromCordageRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for small wicker from vines or bark strips"
  caption="Recipe for small wicker from vines or bark strips"
%}

{%
  include figure.html
  image-name="basketCase1_mediumWickerFromCordageRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for medium wicker from vines or bark strips"
  caption="Recipe for medium wicker from vines or bark strips"
%}

{%
  include figure.html
  image-name="basketCase1_largeWickerFromCordageRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for large wicker from vines or bark strips"
  caption="Recipe for large wicker from vines or bark strips"
%}

## Baskets

Every basket is made with five pieces of wicker of the appropriate size. You'll need some extra cordage, sugar cane, bark strips, or vines to bind the five pieces of wicker together. All baskets take a full 3x3 crafting grid, so you will need a crafting table.

### Basket recipes

{%
  include figure.html
  image-name="basketCase1_smallBasketRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for small basket"
  caption="Recipe for small basket"
%}

{%
  include figure.html
  image-name="basketCase1_mediumBasketRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for medium basket"
  caption="Recipe for medium basket"
%}

{%
  include figure.html
  image-name="basketCase1_largeBasketRecipe.gif"
  width="224"
  height="136"
  alt="Recipe for large basket"
  caption="Recipe for large basket"
%}

### Using baskets

There are three sizes of basket:

  * Small—four inventory slots; each slot holds four items
  * Medium—eight inventory slots; each slot holds eight items
  * Large—twelve inventory slots; each slot holds twelve items

![Large basket inventory](/assets/images/basketCase1_largeBasketInventory.png){: width="400"}

Baskets behave differently than chests and regular blocks.

  * **Opening a basket from your inventory**. While holding a basket in your main hand, use it (right click) without targeting a block. Looking up at the sky is a good way to make sure you're not targeting a block.
  * **Placing a basket**. Baskets can only be placed on solid flat surfaces. Hold a basket in your main hand and right click while targeting a block. The basket will be placed. You can also place a basket from your off-hand, as long as you aren't holding a placeable block in your main hand.
  * **Picking up a basket**. Use (right click) the basket with an empty hand while sneaking. The basket will drop into the world as an item.
  * **Emptying a basket**. Break the basket while sneaking. The basket will drop as an item, and its contents will spill out into the world. (This behavior didn't work in v1.0.29 and earlier.)
  * **Destroying a basket**. Break the basket without sneaking. The basket will be destroyed, and its contents will spill out into the world.

## Configuration

Basket Case has both a config file and an in-game config menu accessible through the Mods menu. Basket Case is ridiculously configurable for such a small mod. Almost everything described above can be turned off or adjusted if desired. For example:

  * If you feel like gathering all the materials for a basket is too grindy, you can adjust the drop rate for the materials.
  * If you only want baskets for decoration, you can turn off their inventory capabilities.
  * If for some reason cordage or bark strips break your mod pack balance, they can be disabled.
  * You can disable certain sizes of baskets if you so desire.
  * Etc.
