---
guid: http://www.benjaminrose.com/?page_id=1603
layout: page
title: About my Minecraft mods
author: Ben
date: 2018-09-19T02:53:37.000Z
aside:
  toc: false
id: 1603
sidebar:
  nav: minecraft-mods-menu
---
Here are my mod projects:

  * [**Basket Case**]({% link pages/basket-case.md %})—*Released.* Make wicker baskets from bark strips and twigs. Wicker baskets are a convenient form of portable storage and add a rustic aesthetic to your world.
  * [**Primal Lib**]({% link pages/primal-lib.md %})—*Released.* A library mod to help me release mods faster. Future releases of Basket Case and any other mods I create will rely on Primal Lib.
  * [**Darkest Before Dawn**]({% link pages/darkest-before-dawn.md %})—*Alpha release.* A mod that adds more difficulty and realism around making fire.
  * [**Primalcraft**]({% link pages/primalcraft.md %})—*Abandoned.* Primalcraft was a mod about extreme survival in the stone age. I have abandoned this mod but am planning to release some of its features as separate mods. Basket Case is the first of these.

I've been modding Minecraft since 2014. It started with my son. We bought Minecraft for him, and one day he convinced me to play. I was instantly hooked. Soon I became bored with the vanilla game and started using mods. Soon after that, I became frustrated that none of the mods did things the way I wanted. So I started learning Java so I could make my own mod. I called it Primalcraft.

After a couple of years working on Primalcraft, I realized that it would never be finished enough to release. I was trying to do too much. Many features were incomplete or poorly done, and I didn't have enough time to finish it all. So I picked out one idea and finished it. That was Basket Case.

I would like to release many of the other ideas I initially tried to implement in Primalcraft. We'll see how it goes.
