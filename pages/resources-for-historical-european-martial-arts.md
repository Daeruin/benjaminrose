---
id: 1235
title: HEMA
date: 2013-08-18T22:04:53-08:00
author: Ben
layout: page
guid: http://www.benjaminrose.com/?page_id=1235
---
**Resources for historical European martial arts**

Since discovering that historical European martial arts (HEMA) is an active field of study and practice and joining my own local club, I've discovered a lot of great information about HEMA online. Here are some of the best resources I've found so far. There are dozens of other blogs and probably hundreds of videos, but I'm only listing the ones that I feel are the best and most instructive. If you have other recommendations, please leave a comment!

**HEMA organizations**

  * [The HEMA Alliance](http://hemaalliance.com/ "The HEMA Alliance")
  * [The Association for Renaissance Martial Arts](http://www.thehaca.com/ "The Association for Renaissance Martial Arts")
  * [HEMA Group Finder](http://www.communitywalk.com/user/view/81443 "HEMA group finder")

**HEMA blogs**

  * [HROARR](http://www.hroarr.com/ "HROARR") (more than just a blog, there are many resources here)
  * [Encased in Steel](http://historical-academy.co.uk/blog/ "Encased in Steel")
  * [Chivalric Fighting Arts Association](http://chivalricfighting.wordpress.com/ "Chivalric Fighting Arts Association")

**HEMA and HEMA-related forums**

  * [HEMA Alliance forums](http://hemaalliance.com/discussion/ "HEMA Alliance forums")
  * [Schola Gladiatoria](http://www.fioredeiliberi.org/ "Schola Gladiatoria")
  * [Sword Forum International](http://www.swordforum.com/forums/content.php?s=bdf31e894b1258e5089421450240fd47 "Sword Forum International")
  * [MyArmoury.com](http://www.myarmoury.com/home.php "MyArmoury - a resource for historic arms and armour collectors")
  * [Sword Buyer's Guide](http://www.sword-buyers-guide.com/ "Sword Buyer's Guide") (tons of great information about how to buy quality swords at decent prices)

**HEMA libraries**

  * [Wiktenauer](http://www.wiktenauer.com/wiki/Main_Page "Wiktenauer, the world's largest library of historical European martial arts resources") (the biggest library of HEMA manuscripts on the web)

**HEMA videos**

  * [Sword School Syllabus Videos](http://www.swordschool.com/wiki/index.php/Main_Page "Sword school") (video reference library for the Fiore tradition)
  * [Nova Fechtbuch](http://novafechtbuch.com/ "Nova Fechtbuch video library") (video library)
  * [Learn Sword Fighting](http://learn-sword-fighting.com/#welcome "Learn Sword Fighting") (paid online instructional videos)
