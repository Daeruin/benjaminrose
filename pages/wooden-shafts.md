---
guid: http://www.benjaminrose.com/?page_id=2732
layout: page
title: Wooden shafts
author: Ben
date: 2020-01-12T23:18:38-08:00
aside:
  toc: false
id: 2732
sidebar:
  nav: primal-lib-menu
---
A wooden shaft is made from a tree [branch]({% link pages/branches.md %}) that has been stripped of its extraneous leaves and twigs. It can be used as a basic digging tool, a simple club, and a replacement for sticks in recipes.

Wooden shafts are meant for some of my future mods and may not be desirable or appropriate for your worlds or modpacks. They are disabled by default, but you can enable them in the config settings if you want.

![Primal Lib wooden shaft](/assets/images/primalLib_woodenShaft.png)

## Obtaining

Place a [branch]({% link pages/branches.md %}) in the crafting grid with an axe.

{%
  include figure.html
  image-name="primalLib_woodenShaftRecipe.png"
  caption="Recipe for wooden shaft"
%}

## Using

Durability: 18

**Attacking**. Press attack with the shaft to inflict damage on mobs and other players. Upon damaging a mob or player, the shaft's durability decreases by one.

  * Attack damage: 1 (half a heart)
  * Attack speed: 1
  * Damage per second: 1

**Breaking blocks**. Wooden shafts are effective at breaking the same blocks as shovels (dirt, clay, sand, etc.). They can break such blocks faster than with your hands, but slower than with a wooden shovel. Upon breaking a block, the shaft's durability decreases by one.

**Crafting ingredient**. Shafts can be used in place of sticks in all crafting recipes.

**Fire making**. In Darkest Before Dawn, you can use a wooden shaft on a fireboard to create a glowing ember, which is the starting point for making a fire. Every attempt to create a glowing ember decreases its durability by one.

**Fuel**. Wooden shafts can be burned in a furnace for five seconds (100 ticks)—the same as a stick.

Wooden shafts cannot be repaired.

## Configuring

In the config settings, wooden shafts can be tweaked in the following ways:

  * You can enable and disable wooden shafts. If disabled, they cannot be crafted from branches. This will prevent you from making fires in Darkest Before Dawn.
  * You can decide to let vanilla sticks be used in place of wooden shafts in crafting recipes (this ability is turned off by default).

## Data

**Item ID:** primallib:wooden_shaft

**Translation ID:** item.primallib:wooden_shaft.name

**OreDictionary terms:** shaftWood, stickWood
