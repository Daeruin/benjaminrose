---
guid: http://www.benjaminrose.com/?page_id=2886
layout: page
title: Wicker
author: Ben, Henry
date: 2020-02-21T02:15:00.000Z
aside:
  toc: true
id: 2886
sidebar:
  nav: basket-case-2-0-menu
---
Wicker has been used to make baskets for thousands of years. It's woven from flexible plant materials. In Basket Case, those materials are [twigs]({% link pages/twigs.md %}), [bark strips]({% link pages/bark-strips.md %}), and vines.

{%
  include figure.html   image-name="basketCase2_wickerAllColors.png"
  alt="Basket Case wicker types"
  caption="All types of wicker in Basket Case"
%}

{% include _reusedContent/basketColoring.md %}

Wicker comes in three sizes—small, medium, and large.

{%
  include figure.html   image-name="basketCase2_wickerSizes.png"
  alt="Basket Case wicker sizes"
  caption="Small, medium, and large wicker"
%}

There are three basic types of wicker:

* Twig wicker (made from wood-specific twigs)
* Bark wicker (made from wood-specific bark strips)
* Generic wicker (made from vines, generic bark strips, or generic twigs)

## Obtaining

Large and medium wicker require a crafting table. Small wicker can be crafted without a crafting table.

### Recipes for twig wicker

To make wicker from twigs, you must have cordage to bind the twigs together. The recipes shown here use [twine]({% link pages/twine.md %}) as cordage. See the [cordage]({% link pages/cordage.md %}) page for a full list of options.

{%
  include figure.html
  image-name="basketCase2_recipeWickerSmallTwigAllTwine.gif"
  alt="Small wicker from twigs using twine"
  caption="Small twig wicker using twine"
%}

{%
  include figure.html
  image-name="basketCase2_recipeWickerMediumTwigAllTwine.gif"
  alt="Medium wicker from twigs using twine"
  caption="Medium twig wicker using twine"
%}

{%
  include figure.html
  image-name="basketCase2_recipeWickerLargeTwigAllTwine.gif"
  alt="Large wicker from twigs using twine"
  caption="Large twig wicker using twine"
%}

### Recipes for bark wicker

{%
  include figure.html
  image-name="basketcase2_recipeWickerSmallBarkStripsAll.gif"
  alt="Small wicker from bark strips - all wood types"
  caption="Small bark wicker"
%}

{%
  include figure.html
  image-name="basketcase2_recipeWickerMediumBarkStripsAll.gif"
  alt="Medium wicker from bark strips - all wood types"
  caption="Medium bark wicker"
%}

{%
  include figure.html
  image-name="basketcase2_recipeWickerLargeBarkStripsAll.gif"
  alt="Large wicker from bark strips - all wood types"
  caption="Large bark wicker"
%}

### Recipe for generic wicker

Generic wicker looks identical to oak wicker, but it's crafted from different ingredients. Generic bark strips and twigs are only available if you've configured Basket Case to not allow wood-specific baskets. Generic wicker can be crafted from vines, including ivy and willow vines if compatibility with Biomes O' Plenty is enabled in the Primal Lib config options.

The recipes shown here use [twine]({% link pages/twine.md %}) as cordage. See above for more items that could be used as cordage.

{%
  include figure.html
  image-name="basketcase2_recipeWickerSmallGeneric.gif"
  alt="Small generic wicker"
  caption="Small generic wicker"
%}

{%
  include figure.html
  image-name="basketcase2_recipeWickerMediumGeneric.gif"
  alt="Medium generic wicker"
  caption="Medium generic wicker"
%}

{%
  include figure.html
  image-name="basketcase2_recipeWickerLargeGeneric.gif"
  alt="Large generic wicker"
  caption="Large generic wicker"
%}

## Using

**Crafting ingredient**. Wicker can be crafted into [baskets]({% link pages/baskets.md %}). Small wicker makes small baskets, medium wicker makes medium baskets, and large wicker makes large baskets.

**Fuel**. Wicker can be burned in a furnace. Small wicker burns for four seconds, medium wicker for five seconds, and large wicker for six seconds.

## Configuring

In the config settings, wicker can be tweaked in the following ways:

* You can choose to prevent wood-specific [baskets]({% link pages/baskets.md %}).
  * If you prevent wood-specific baskets, only generic twigs and bark strips from Primal Lib will be present (if configured to be turned on in the Primal Lib config). Generic twigs and bark strips can still be crafted into wicker, but it will be generic wicker that looks like oak wicker.
  * If you allow wood-specific baskets, then generic twigs and bark strips will not be available. Instead, wood-specific twigs and bark strips will be present for crafting into wood-specific wicker.
* You can choose to disallow small, medium, or large baskets. Doing so also prevents that size of wicker from being crafted. For example, if you disallow large baskets, then large wicker cannot be crafted.

## Data

### Small wicker

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Small generic wicker|basketcase:<br>wicker_small_generic|item.basketcase:<br>wicker_small_generic.name|wicker, wickerSmall
|Small acacia bark wicker|basketcase:<br>wicker_small_bark_acacia|item.basketcase:<br>wicker_small_bark_acacia.name|wicker, wickerSmall
|Small birch bark wicker|basketcase:<br>wicker_small_bark_birch|item.basketcase:<br>wicker_small_bark_birch.name|wicker, wickerSmall
|Small dark oak bark wicker|basketcase:<br>wicker_small_bark_dark_oak|item.basketcase:<br>wicker_small_bark_dark_oak.name|wicker, wickerSmall
|Small jungle bark wicker|basketcase:<br>wicker_small_bark_jungle|item.basketcase:<br>wicker_small_bark_jungle.name|wicker, wickerSmall
|Small oak bark wicker|basketcase:<br>wicker_small_bark_oak|item.basketcase:<br>wicker_small_bark_oak.name|wicker, wickerSmall
|Small spruce bark wicker|basketcase:<br>wicker_small_bark_spruce|item.basketcase:<br>wicker_small_bark_spruce.name|wicker, wickerSmall
|Small acacia twig wicker|basketcase:<br>wicker_small_twig_acacia|item.basketcase:<br>wicker_small_twig_acacia.name|wicker, wickerSmall
|Small birch twig wicker|basketcase:<br>wicker_small_twig_birch|item.basketcase:<br>wicker_small_twig_birch.name|wicker, wickerSmall
|Small dark oak twig wicker|basketcase:<br>wicker_small_twig_dark_oak|item.basketcase:<br>wicker_small_twig_dark_oak.name|wicker, wickerSmall
|Small jungle twig wicker|basketcase:<br>wicker_small_twig_jungle|item.basketcase:<br>wicker_small_twig_jungle.name|wicker, wickerSmall
|Small oak twig wicker|basketcase:<br>wicker_small_twig_oak|item.basketcase:<br>wicker_small_twig_oak.name|wicker, wickerSmall
|Small spruce twig wicker|basketcase:<br>wicker_small_twig_spruce|item.basketcase:<br>wicker_small_twig_spruce.name|wicker, wickerSmall

### Medium wicker

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Medium generic wicker|basketcase:<br>wicker_medium_generic|item.basketcase:<br>wicker_medium_generic.name|wicker, wickerMedium
|Medium acacia bark wicker|basketcase:<br>wicker_medium_bark_acacia|item.basketcase:<br>wicker_medium_bark_acacia.name|wicker, wickerMedium
|Medium birch bark wicker|basketcase:<br>wicker_medium_bark_birch|item.basketcase:<br>wicker_medium_bark_birch.name|wicker, wickerMedium
|Medium dark oak bark wicker|basketcase:<br>wicker_medium_bark_dark_oak|item.basketcase:<br>wicker_medium_bark_dark_oak.name|wicker, wickerMedium
|Medium jungle bark wicker|basketcase:<br>wicker_medium_bark_jungle|item.basketcase:<br>wicker_medium_bark_jungle.name|wicker, wickerMedium
|Medium oak bark wicker|basketcase:<br>wicker_medium_bark_oak|item.basketcase:<br>wicker_medium_bark_oak.name|wicker, wickerMedium
|Medium spruce bark wicker|basketcase:<br>wicker_medium_bark_spruce|item.basketcase:<br>wicker_medium_bark_spruce.name|wicker, wickerMedium
|Medium acacia twig wicker|basketcase:<br>wicker_medium_twig_acacia|item.basketcase:<br>wicker_medium_twig_acacia.name|wicker, wickerMedium
|Medium birch twig wicker|basketcase:<br>wicker_medium_twig_birch|item.basketcase:<br>wicker_medium_twig_birch.name|wicker, wickerMedium
|Medium dark oak twig wicker|basketcase:<br>wicker_medium_twig_dark_oak|item.basketcase:<br>wicker_medium_twig_dark_oak.name|wicker, wickerMedium
|Medium jungle twig wicker|basketcase:<br>wicker_medium_twig_jungle|item.basketcase:<br>wicker_medium_twig_jungle.name|wicker, wickerMedium
|Medium oak twig wicker|basketcase:<br>wicker_medium_twig_oak|item.basketcase:<br>wicker_medium_twig_oak.name|wicker, wickerMedium
|Medium spruce twig wicker|basketcase:<br>wicker_medium_twig_spruce|item.basketcase:<br>wicker_medium_twig_spruce.name|wicker, wickerMedium

### Large wicker

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Large generic wicker|basketcase:<br>wicker_large_generic|item.basketcase:<br>wicker_large_generic.name|wicker, wickerLarge
|Large acacia bark wicker|basketcase:<br>wicker_large_bark_acacia|item.basketcase:<br>wicker_large_bark_acacia.name|wicker, wickerLarge
|Large birch bark wicker|basketcase:<br>wicker_large_bark_birch|item.basketcase:<br>wicker_large_bark_birch.name|wicker, wickerLarge
|Large dark oak bark wicker|basketcase:<br>wicker_large_bark_dark_oak|item.basketcase:<br>wicker_large_bark_dark_oak.name|wicker, wickerLarge
|Large jungle bark wicker|basketcase:<br>wicker_large_bark_jungle|item.basketcase:<br>wicker_large_bark_jungle.name|wicker, wickerLarge
|Large oak bark wicker|basketcase:<br>wicker_large_bark_oak|item.basketcase:<br>wicker_large_bark_oak.name|wicker, wickerLarge
|Large spruce bark wicker|basketcase:<br>wicker_large_bark_spruce|item.basketcase:<br>wicker_large_bark_spruce.name|wicker, wickerLarge
|Large acacia twig wicker|basketcase:<br>wicker_large_twig_acacia|item.basketcase:<br>wicker_large_twig_acacia.name|wicker, wickerLarge
|Large birch twig wicker|basketcase:<br>wicker_large_twig_birch|item.basketcase:<br>wicker_large_twig_birch.name|wicker, wickerLarge
|Large dark oak twig wicker|basketcase:<br>wicker_large_twig_dark_oak|item.basketcase:<br>wicker_large_twig_dark_oak.name|wicker, wickerLarge
|Large jungle twig wicker|basketcase:<br>wicker_large_twig_jungle|item.basketcase:<br>wicker_large_twig_jungle.name|wicker, wickerLarge
|Large oak twig wicker|basketcase:<br>wicker_large_twig_oak|item.basketcase:<br>wicker_large_twig_oak.name|wicker, wickerLarge
|Large spruce twig wicker|basketcase:<br>wicker_large_twig_spruce|item.basketcase:<br>wicker_large_twig_spruce.name|wicker, wickerLarge
