---
guid: http://www.benjaminrose.com/?page_id=1681
layout: page
title: Primal Lib
author: Ben
date: 2018-10-23T21:21:22-08:00
aside:
  toc: true
id: 1681
sidebar:
  nav: primal-lib-menu
---
_Library mod for the Primalcraft family of mods._

![Primal Lib logo](/assets/images/primal-lib-logo.png){: height="150px"}

Primal Lib is a library mod that all my other Minecraft mods will require in order to run. It has some basic items and functionality that provide a foundation for other mods to build on. Primal Lib is not meant to be used on its own.

Primal Lib includes:

  * **Basic items and blocks that are used in multiple mods**. For example, twine is a basic material that's used in crafting recipes in both [Basket Case]({% link pages/basket-case.md %}) and Darkest Before Dawn (in progress).
  * **Basic behaviors related to its items**. For example, you can configure vines and sugar cane to count as [cordage]({% link pages/cordage.md %}).
  * **Common utility code that all my mods rely on**. For example, code that registers blocks and textures, functions that spawn entities and particles, and code that adds drops when breaking a block.

Primal Lib's basic items, blocks, and behaviors are described in more detail below.

## Basic items and blocks

The following table gives a brief overview of all the items and blocks in Primal Lib. Click the links in the first column for more details on each item or block.

|---
|Item/Block|Obtained By|Use
|-|-|-
|[Bark]({% link pages/bark.md %})|Right clicking a log with an axe|Making bark strips
|[Bark strips]({% link pages/bark-strips.md %})|Crafting bark|Making wicker and baskets (in Basket Case)
|[Branches]({% link pages/branches.md %})|Breaking leaf blocks|Making wooden shafts
|[Plant fibers]({% link pages/plant-fibers.md %})|Breaking tall grass|Making wicker and baskets (in Basket Case)
|[Stripped logs]({% link pages/stripped-logs.md %})|Right clicking a log with an axe|Byproduct of obtaining bark
|[Twigs]({% link pages/twigs.md %})|Breaking leaf blocks|Making baskets (in Basket Case)
|[Twine]({% link pages/twine.md %})|Crafting plant fiber|Making wicker and baskets (in Basket Case)
|[Wooden shafts]({% link pages/wooden-shafts.md %})|Crafting branches|Basic weapon and digging tool

## Basic behaviors

Primal Lib adds or changes the following behaviors:

  * Primal Lib makes vines and sugar cane count as [cordage]({% link pages/cordage.md %}). This affects recipes in mods that use the "cordage" OreDict term. You can also allow string to count as cordage (turned off by default).
  * If you break a vine block that's connected to other vines below it, all the vines below will automatically be broken as well. This behavior can be turned off in the config file.
  * You can configure Primal Lib so that players are required to sneak in order to get any of the drops in this mod. This lets players choose when to get the drops. You may find this useful if some items are piling up without getting used.
  * Stripped logs do not support leaves, so if you strip the bark from every log in the tree, the leaves will die, leaving you with a leafless trunk of stripped logs.

Primal Lib has some built-in compatibility with Better With Mods, Biomes O' Plenty, and No Tree Punching. Compatibility with Biomes O' Plenty and No Tree Punching must be enabled in the config options.

## Download

<iframe src="https://www.cfwidget.com/409199?version=1.12.2" width="100%"></iframe>
