---
id: 2703
layout: page
title: Twine
author: Ben
date: 2020-01-13T04:33:29.000Z
guid: http://www.benjaminrose.com/?page_id=2703
sidebar:
  nav: primal-lib-menu
---
Twine is a type of [cordage]({% link pages/cordage.md %}) (string) made from grass and similar plant fibers.

![Primal Lib twine](/assets/images/primalLib_twine.png)

## Obtaining

Craft from two or three [plant fibers]({% link pages/plant-fibers.md %}).

{%
  include figure.html
  image-name="primalLib_cordageRecipe.gif"
  caption="Recipe for twine"
%}

Stating with 1.1.56, any item registered as "plantFiber" to Forge's OreDictionary can be used. Sugar cane is registered as "plantFiber." If Biomes O' Plenty integration is enabled in Primal Lib, the following items will also be registered as "plantFiber": cattail, river cane, reed, root, flax, and tall cattail.

## Using

**Crafting ingredient**. Twine can be used in place of vanilla string in all crafting recipes. Twine is also used by other mods that are built on PrimalLib.

  * Basket Case—Twine can be used to make [wicker]({% link pages/wicker.md %}) and [baskets]({% link pages/baskets.md %}).
  * Darkest Before Dawn (in progress)—Twine can be used to make fireboards.

**Fuel**. Twine can be burned in a furnace for one second (20 ticks).

## Configuring

In the config settings, twine can be tweaked in the following ways:

  * You can prevent twine from being crafted. This will also disable certain basket recipes in Basket Case and prevent you from making fires in Darkest Before Dawn.

## Data

**Item ID:** primallib:twine

**Translation ID:** item.primallib:twine.name

**OreDictionary terms:** cordage, cordageWeak, string
