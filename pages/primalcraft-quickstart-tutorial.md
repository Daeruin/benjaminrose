---
id: 1420
title: Primalcraft quickstart tutorial
date: 2017-10-16T22:27:24-08:00
author: Ben
layout: page
guid: http://www.benjaminrose.com/?page_id=1420
sidebar:
  nav: primalcraft-menu
---
{: .warning}
**ATTENTION:** Primalcraft is no longer under development. I have decided to split it into multiple mods. I'm leaving this page here for historical purposes to show what features I may be working on for future mods. If you're interested in some of these features, let me know!

Your main goal upon spawning in Primalcraft is to protect yourself from predators—wolves. Come night, they will eat you unless you stay near a fire. So you need to get logs to make your fire last until dawn. And to do that, you need a stone hatchet. That means your first order of business is gathering the needed materials.

## Gather survival materials

Start by looking around for any rocks lying on the ground and pick them up. The most precious rocks to you right now are **hammerstones** (medium oval-shaped rocks). **Core stones** (large teardrop-shaped rocks) are used to generate **stone flakes** (small triangular rocks), which are then turned into tool heads. You can also find the occasional stone flake on the ground. If you can find a few stone flakes, the need for a core stone is less urgent. **Throwing stones** (small round rocks) will help you kill some animals for lunch.

Not surprisingly, rocks are more plentiful around other stone, so if you see bare stone or gravel nearby you are more likely to find rocks. Rivers and mountains are good places to look. You can also get rocks from digging in the dirt, but you have other reasons for running around and getting to know the lay of the land while you look for rocks. If you haven't found a hammerstone and a core stone or some stone flakes by noon, you might want to start digging in grass and dirt to make sure you can get yourself a stone hatchet before dark.

Besides rocks, you also need to start gathering other materials needed for making your first tools and starting a campfire. So harvest any tall grass you see to get **plant fibers**, and break any leaves you find to get some **branches** and **twigs**. You'll need at least twelve plant fibers, five branches, and three twigs. Some will be for making tools and others for making a fire.

## Make tools

Before you get too far, your first order of business is to make some cordage to fasten tool heads to their handles. Put three plant fibers in the crafting grid in a column to make cordage. You'll want four pieces of cordage—two for tools, and two for making a fire later on. You can also use vines and reeds instead of cordage, if you happen across them.

The ideal situation for making your first tools is to get a hammerstone and a core stone. Put them in the crafting grid (hammerstone on top of core stone) and make at least three stone flakes. These will become your first tools. If you haven't found a core stone, but you managed to find some stone flakes and a hammerstone, you can still make your first set of tools.

Your first tool will be a stone scraper—a jagged piece of stone for scraping fur from hide and, more importantly at the moment, turning a branch into a smooth wooden shaft. To make a scraper, just put your hammerstone above a stone flake in the crafting grid. Then put your scraper in the crafting grid with a branch to create a wooden shaft.

The wooden shaft will be the handle for your next tool. Ideally that will be a stone knife. It's much more durable than the stone scraper, so it's ideal for making your next wooden shaft. However, if it's getting dark, or if you only have one stone flake left, you might want to skip straight to making a stone hatchet so you can get some firewood.

To create your knife blade or hatchet head, hold a stone flake in one hand and your hammerstone in the other and right-click to open the knapping interface.

//TODO: ADD KNAPPING INSTRUCTIONS

Now put the tool head, cordage, and wooden shaft diagonally in the crafting grid. Voila. You finally have your first stone tool.

## Find food and water

While you are searching for rocks and such, you'll probably get hungry and thirsty. Your first meal will likely be a dead locust. They'll drop from tall grass occasionally as you harvest plant fibers. Don't turn your nose up at this delicacy ([reference](https://www.livestrong.com/article/549444-the-nutritional-value-of-locusts/)), because it will likely be the main thing keeping you from starving to death.

While you are gathering your other survival materials, keep an eye out for nearby animals. Wolves will steer clear of you while it's daytime, but it's probably wise to keep away from them (unless they happen to be clustered around the only tree or water source in sight—then you might want to try chasing them away before dark). You will want to spend the night well away from them, in case your campfire goes out.

Any other animals can be killed and eaten. They'll run away from you before you can get very close. If you find any throwing stones, you might be able to get close enough to get a lucky shot (you can even pick the throwing stone back up if you miss). Otherwise, try to catch them on a hillside or in the water where it's harder for them to run away. Because it's so hard to make a kill, focus on pigs and cows since they provide the most saturating meat.

Chickens are a great find. Pick up their eggs and cook them in your campfire at night.

You will likely get thirsty before the day is over. Right-click and hold with an open hand on any water source to refill your thirst bar. Don't miss opportunities to drink as you wander about.

## Identify a good camping spot

While you are running around gathering materials and food, you need to identify a good place to spend the night—preferably somewhere that meets the following criteria:

  * Well away from wolf packs.
  * Relatively flat land (purely for convenience).
  * Water source nearby.
  * Trees nearby. You may need to be chopping wood in the night to keep your fire alive.
  * To save a bit of time, look for a spot with a non-grass block on which to build your fire.

## Make a fire

Eventually it will start to get dark. To protect yourself from wolves and provide light, you'll want a campfire. Before you start, make sure you have all the right materials at hand. If you don't have the right material at the right time, your campfire will fail. And that could mean the difference between life or death as darkness falls.

Needed materials:

  * **Two wooden shafts** and **two pieces of cordage** to make a fireboard ([reference](https://books.google.com/books?id=MR8-HoKl3_cC&pg=PA23&lpg=PA23&dq=fireboard+two+sticks&source=bl&ots=LAvJP5GWLH&sig=7ovxkXeA6FWGhvB4pPrJtKdiQ5U&hl=en&sa=X&ved=0ahUKEwin59rPuvnWAhVR-GMKHUnWBUsQ6AEIYTAM#v=onepage&q=fireboard%20two%20sticks&f=false)).
  * **Another wooden shaft** to use on your fireboard to make a glowing ember.
  * **One plant fiber** to make tinder.
  * **Three twigs** for kindling.
  * **At least one log**. Ideally you'll have **four logs** to keep the fire going all night.

To make a campfire ([reference](https://www.youtube.com/watch?v=z9n9rqb-lvY)):

  1. Choose a suitable spot for the fire. Stone, dirt, gravel, or sand will work, but grass will not—turn it into dirt by mining it with your hands or a wooden shaft first.
  2. Craft some tinder using plant fibers. Place the tinder on the block where you want to build the fire.
  3. Craft a fireboard using two wooden shafts and two pieces of cordage. Place the fireboard on the ground near your tinder.
  4. While holding a wooden shaft, hold down right-click on the fireboard for a few seconds to create a glowing ember. You may have to try several times, and the fireboard or the shaft may break and need to be replaced before you finally get your glowing ember.
  5. The glowing ember will pop into your hot bar as long as you have an empty hand to hold it with. Right click with the glowing ember on the tinder. The glowing ember will disappear.
  6. Now hold right click on the tinder to blow on it for a while. When you see a little puff of smoke, quickly right-click with some twigs to add them to the tinder.
  7. Hold right click to blow on it for a while longer. When you see a little flame, quickly add a log.

Congratulations, you have a campfire! Right click on the campfire to open the campfire interface. Add something flammable to keep it going. Straw, bark, sticks, and logs will all work, although some will burn longer than others. The campfire provides light as long as something is burning in it. Four logs should last you the night.
