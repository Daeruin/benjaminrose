---
guid: http://www.benjaminrose.com/?page_id=2726
layout: page
title: Branches
author: Ben
date: 2020-01-12T21:23:34-08:00
aside:
  toc: true
id: 2726
sidebar:
  nav: primal-lib-menu
---
Branches are a tree's larger, stronger limbs that are suitable for making tools with.

Branches are meant for some of my future mods and may not be desirable or appropriate for your worlds or modpacks. They are disabled by default, but you can enable them in the config settings if you want.


![Primal Lib branch](/assets/images/primalLib_branch.png)

## Obtaining

Break leaf blocks that are on the innermost layer of a tree, right next to the trunk. The outer leaves won't drop branches. The chance of getting branches depends on the tool you are using:

  * **Bare hands**: 20% chance
  * **Axe**: 100% chance

Only the above tools will yield branch drops. You can't just go around hitting leaves with any random object you happen to be holding!

## Using

**Crafting ingredient**. Put a branch in the crafting grid with an axe to craft a [wooden shaft]({% link pages/wooden-shafts.md %}).

{%
  include figure.html
  image-name="primalLib_woodenShaftRecipe.png"
  alt="Wooden shaft recipe"
  caption="Recipe for wooden shaft"
%}

**Fuel**. Branches can be burned in a furnace for five seconds (100 ticks).

## Configuring

In the config settings, branches can be tweaked in the following ways:

  * You can enable and disable branches. If disabled, branches will not drop from leaves. This will prevent you from making [wooden shafts]({% link pages/wooden-shafts.md %}) and fires in Darkest Before Dawn.
  * You can prevent branches from being crafted into [wooden shafts]({% link pages/wooden-shafts.md %}). This will prevent you from making fires in Darkest Before Dawn.
  * You can toggle whether branches should drop from leaves that aren't next to a log block.
  * You can control how many branches drop when breaking leaves. The default is one.
  * You can decide whether the chance of dropping branches should depend on what tool is being used.
  * If the prior option is turned on, you can specify the percentage chance of branches dropping for bare hands and axe.

## Data

**Item ID:** primallib:branch

**Translation ID:** item.primallib:branch.name

**OreDictionary terms:** n/a
