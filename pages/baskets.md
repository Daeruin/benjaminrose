---
layout: page
title: Baskets
author: Henry, Ben
date: 2020-04-26T20:18:21.880Z
aside:
  toc: false
sidebar:
  nav: basket-case-2-0-menu
---
Baskets are the earliest form of storage known to humankind, predating even pottery, and are still used today. Baskets can be made from many materials that are readily available around the world.

In Basket Case, baskets are used for early game item storage and transportation. The smallest baskets can be made without tools or crafting table. You can also use baskets decoratively.

Here are a variety of Basket Case's baskets in different colors and sizes:

![Small oak bark basket](/assets/images/basketCase2_basketBarkSmallOak.png)
![Medium birch bark basket](/assets/images/basketCase2_basketBarkMediumBirch.png)
![Large acacia bark basket](/assets/images/basketCase2_basketBarkLargeAcacia.png)
![Small jungle bark basket](/assets/images/basketCase2_basketBarkSmallJungle.png)
![Medium dark oak bark basket](/assets/images/basketCase2_basketBarkMediumDarkOak.png)
![Large spruce bark basket](/assets/images/basketCase2_basketBarkLargeSpruce.png)
![Small birch twig basket](/assets/images/basketCase2_basketTwigSmallBirch.png)
![Medium spruce twig basket](/assets/images/basketCase2_basketTwigMediumSpruce.png)
![Large dark oak twig basket](/assets/images/basketCase2_basketTwigLargeDarkOak.png)
![Small oak twig basket](/assets/images/basketCase2_basketTwigSmallOak.png)
![Medium jungle twig basket](/assets/images/basketCase2_basketTwigMediumJungle.png)
![Large acacia twig basket](/assets/images/basketCase2_basketTwigLargeAcacia.png)

{% include _reusedContent/basketColoring.md %}

Basket Case offers three sizes of basket—small, medium, and large—made from the appropriate size of wicker.

There are three basic types of basket, each made from a different type of [wicker]({% link pages/wicker.md %}):

* Twig baskets (made from twig wicker)
* Bark baskets (made from bark wicker)
* Generic baskets (made from generic wicker)

Baskets can be made from each of the six vanilla tree types. All these varieties add up to 39 total baskets, depending on how you have configured Basket Case.

## Obtaining

Small baskets can be crafted without a crafting table. Medium and large baskets require a crafting table.

Medium and large baskets require cordage to bind the wicker together. Twig and bark baskets must use the appropriate kind of [bark strips]({% link pages/bark-strips.md %}).

### Twig baskets

{%
  include figure.html
  image-name="basketCase2_recipeBasketSmallTwigAll.gif"
  alt="Recipe for small twig baskets"
  caption="Recipe for small twig baskets"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketMediumTwigAll.gif"
  alt="Recipe for medium twig baskets"
  caption="Recipe for medium twig baskets"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketLargeTwigAll.gif"
  alt="Recipe for large twig baskets"
  caption="Recipe for large twig baskets"
%}

### Bark baskets

{%
  include figure.html
  image-name="basketCase2_recipeBasketSmallBarkAll.gif"
  alt="Recipe for small baskets from bark strips"
  caption="Recipe for small baskets from bark strips"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketMediumBarkAll.gif"
  alt="Recipe for medium baskets from bark strips"
  caption="Recipe for medium baskets from bark strips"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketLargeBarkAll.gif"
  alt="Recipe for large baskets from bark strips"
  caption="Recipe for large baskets from bark strips"
%}

### Generic baskets

Generic baskets of medium and large size require cordage to bind the wicker together. The images here show only [twine]({% link pages/twine.md %}) as cordage, but Basket Case also supports vines and bark strips by default. See the [cordage]({% link pages/cordage.md %}) page for a full list of options.

{%
  include figure.html
  image-name="basketCase2_recipeBasketSmallGeneric.png"
  alt="Recipe for small generic basket"
  caption="Recipe for small generic basket"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketMediumGeneric.png"
  alt="Recipe for medium generic basket"
  caption="Recipe for medium generic basket using twine as cordage"
%}

{%
  include figure.html
  image-name="basketCase2_recipeBasketLargeGeneric.png"
  alt="Recipe for large generic basket"
  caption="Recipe for large generic basket using twine as cordage"
%}

## Using

Baskets behave differently than chests and regular blocks.

**Placing**. Baskets can only be placed on solid flat surfaces. Hold a basket in your main hand and right click while targeting a block. The basket will be placed. You can also place a basket from your off-hand, as long as you aren't holding a placeable block in your main hand.

**Picking up**. While sneaking, use (right click) the basket with an empty hand. The basket will drop into the world as an item.

**Breaking or destroying**. Break the basket without sneaking. The effects of doing this changed in v2.1.95.
  * Before v2.1.95, breaking the basket destroys the basket itself, and its contents drop. Breaking it while sneaking makes the basket and its contents to drop.
  * After v2.1.95, breaking the basket makes the basket and its contents to drop wether you are sneaking or not. You can enable the old "hardcore" basket destruction behavior in the config settings.

**Opening a basket from your inventory**. While holding a basket in your main hand, use it (right click) without targeting a block. Looking up at the sky is a good way to make sure you're not targeting a block.

**Previewing basket contents**. With your inventory open, hover over any basket and press Shift. The contents of the basket will display as part of the tool tip.

{%
  include figure.html
  image-name="basketCase2_toolTip.png"
  alt="Basket contents in tool tip when holding shift"
  caption="Basket contents in tool tip when holding shift"
%}

**Storage capacity**. Baskets have a limited number of storage slots, and the slots have smaller stack sizes. (The stack sizes can be configured.)

|---
|Basket|# of Slots|Max Stack Size|Max # of Items Held
|-|-|-
|Small basket|4|16|64
|Medium basket|8|32|256
|Large basket|12|48|576

**Decoration**. Baskets are a great piece of rustic decor. If desired, you can turn off their storage capacity completely, making them only useful for decoration.

## Configuring

In the config settings, baskets can be tweaked in the following ways:

* You can choose to prevent wood-specific baskets.
  * If you prevent wood-specific baskets, only generic [twigs]({% link pages/twigs.md %}) and [bark strips]({% link pages/bark-strips.md %}) from Primal Lib will be present (if configured to be turned on in the Primal Lib config). Generic twigs and bark strips can still be crafted into [wicker]({% link pages/wicker.md %}) and baskets, but they will be generic wicker and baskets that look like oak.
  * If you allow wood-specific baskets, then generic twigs, bark strips, and wicker will not be available. Instead, wood-specific twigs and bark strips will be present, which can be crafted into wood-specific baskets.
* You can choose to disallow small, medium, or large baskets. Doing so also prevents that size of [wicker]({% link pages/wicker.md %}) from being crafted. For example, if you disallow large baskets, then large wicker cannot be crafted.
* You can make baskets decorative only, so they can't function as containers.
* You can specify the maximum stack size for items in each size of basket. The defaults are 16 for small baskets, 32 for medium, and 48 for large. The highest you can set the max stack size is 64.
* You can make it so baskets can be carried in your regular inventory. By default, baskets can only go in the hot bar.
* You can make it so baskets can be put in other containers, such as chests (starting with 2.1.109).
* You can make it so baskets are destroyed when broken unless you are sneaking. (Before 2.1.95, this behavior could not be turned off.)

## Data

### Small baskets

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Small generic basket|basketcase:<br>basket_small_generic|item.basketcase:<br>basket_small_generic.name|n/a
|Small acacia bark basket|basketcase:<br>basket_small_bark_acacia|item.basketcase:<br>basket_small_bark_acacia.name|n/a
|Small birch bark basket|basketcase:<br>basket_small_bark_birch|item.basketcase:<br>basket_small_bark_birch.name|n/a
|Small dark oak bark basket|basketcase:<br>basket_small_bark_dark_oak|item.basketcase:<br>basket_small_bark_dark_oak.name|n/a
|Small jungle bark basket|basketcase:<br>basket_small_bark_jungle|item.basketcase:<br>basket_small_bark_jungle.name|n/a
|Small oak bark basket|basketcase:<br>basket_small_bark_oak|item.basketcase:<br>basket_small_bark_oak.name|n/a
|Small spruce bark basket|basketcase:<br>basket_small_bark_spruce|item.basketcase:<br>basket_small_bark_spruce.name|n/a
|Small acacia twig basket|basketcase:<br>basket_small_twig_acacia|item.basketcase:<br>basket_small_twig_acacia.name|n/a
|Small birch twig basket|basketcase:<br>basket_small_twig_birch|item.basketcase:<br>basket_small_twig_birch.name|n/a
|Small dark oak twig basket|basketcase:<br>basket_small_twig_dark_oak|item.basketcase:<br>basket_small_twig_dark_oak.name|n/a
|Small jungle twig basket|basketcase:<br>basket_small_twig_jungle|item.basketcase:<br>basket_small_twig_jungle.name|n/a
|Small oak twig basket|basketcase:<br>basket_small_twig_oak|item.basketcase:<br>basket_small_twig_oak.name|n/a
|Small spruce twig basket|basketcase:<br>basket_small_twig_spruce|item.basketcase:<br>basket_small_twig_spruce.name|n/a

### Medium baskets

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Medium generic basket|basketcase:<br>basket_medium_generic|item.basketcase:<br>basket_medium_generic.name|n/a
|Medium acacia bark basket|basketcase:<br>basket_medium_bark_acacia|item.basketcase:<br>basket_medium_bark_acacia.name|n/a
|Medium birch bark basket|basketcase:<br>basket_medium_bark_birch|item.basketcase:<br>basket_medium_bark_birch.name|n/a
|Medium dark oak bark basket|basketcase:<br>basket_medium_bark_dark_oak|item.basketcase:<br>basket_medium_bark_dark_oak.name|n/a
|Medium jungle bark basket|basketcase:<br>basket_medium_bark_jungle|item.basketcase:<br>basket_medium_bark_jungle.name|n/a
|Medium oak bark basket|basketcase:<br>basket_medium_bark_oak|item.basketcase:<br>basket_medium_bark_oak.name|n/a
|Medium spruce bark basket|basketcase:<br>basket_medium_bark_spruce|item.basketcase:<br>basket_medium_bark_spruce.name|n/a
|Medium acacia twig basket|basketcase:<br>basket_medium_twig_acacia|item.basketcase:<br>basket_medium_twig_acacia.name|n/a
|Medium birch twig basket|basketcase:<br>basket_medium_twig_birch|item.basketcase:<br>basket_medium_twig_birch.name|n/a
|Medium dark oak twig basket|basketcase:<br>basket_medium_twig_dark_oak|item.basketcase:<br>basket_medium_twig_dark_oak.name|n/a
|Medium jungle twig basket|basketcase:<br>basket_medium_twig_jungle|item.basketcase:<br>basket_medium_twig_jungle.name|n/a
|Medium oak twig basket|basketcase:<br>basket_medium_twig_oak|item.basketcase:<br>basket_medium_twig_oak.name|n/a
|Medium spruce twig basket|basketcase:<br>basket_medium_twig_spruce|item.basketcase:<br>basket_medium_twig_spruce.name|n/a

### Large baskets

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Large generic basket|basketcase:<br>basket_large_generic|item.basketcase:<br>basket_large_generic.name|n/a
|Large acacia bark basket|basketcase:<br>basket_large_bark_acacia|item.basketcase:<br>basket_large_bark_acacia.name|n/a
|Large birch bark basket|basketcase:<br>basket_large_bark_birch|item.basketcase:<br>basket_large_bark_birch.name|n/a
|Large dark oak bark basket|basketcase:<br>basket_large_bark_dark_oak|item.basketcase:<br>basket_large_bark_dark_oak.name|n/a
|Large jungle bark basket|basketcase:<br>basket_large_bark_jungle|item.basketcase:<br>basket_large_bark_jungle.name|n/a
|Large oak bark basket|basketcase:<br>basket_large_bark_oak|item.basketcase:<br>basket_large_bark_oak.name|n/a
|Large spruce bark basket|basketcase:<br>basket_large_bark_spruce|item.basketcase:<br>basket_large_bark_spruce.name|n/a
|Large acacia twig basket|basketcase:<br>basket_large_twig_acacia|item.basketcase:<br>basket_large_twig_acacia.name|n/a
|Large birch twig basket|basketcase:<br>basket_large_twig_birch|item.basketcase:<br>basket_large_twig_birch.name|n/a
|Large dark oak twig basket|basketcase:<br>basket_large_twig_dark_oak|item.basketcase:<br>basket_large_twig_dark_oak.name|n/a
|Large jungle twig basket|basketcase:<br>basket_large_twig_jungle|item.basketcase:<br>basket_large_twig_jungle.name|n/a
|Large oak twig basket|basketcase:<br>basket_large_twig_oak|item.basketcase:<br>basket_large_twig_oak.name|n/a
|Large spruce twig basket|basketcase:<br>basket_large_twig_spruce|item.basketcase:<br>basket_large_twig_spruce.name|n/a
