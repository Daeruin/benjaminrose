---
id: 1360
layout: page
title: Primalcraft recipes
author: Ben
date: 2017-10-17T06:28:03.000Z
guid: http://www.benjaminrose.com/?page_id=1360
sidebar:
  nav: primalcraft-menu
---
{: .warning}
**ATTENTION:** Primalcraft is no longer under development. I have decided to split it into multiple mods. I'm leaving this page here for historical purposes to show what features I may be working on for future mods. If you're interested in some of these features, [let me know](mailto:daeruin@benjaminrose.com)!

## Fire making

### Fireboard

Ingredients:

* Wooden Shaft x2
* Cordage, Bark Strips, Vines, or Reeds x2

![](/assets/images/primalcraft_recipeFireboard.gif)

### Tinder

Ingredients:

* Plant fiber

![](/assets/images/primalcraft_recipeTinder.png)

### Torch

Ingredients:

* Pine Sap
* Cordage, Bark Strips, Vines, or Reeds x2
* Birch Bark
* Wooden Shaft

![](/assets/images/primalcraft_recipeTorch.png)

## Cordage, wicker, and baskets

### Cordage

Ingredients:

* Plant fiber x3

![](/assets/images/primalcraft_recipeCordage.png)

### Bark strips

Ingredients:

* Bark

![](/assets/images/primalcraft_recipeBarkStrips.gif)

### Small wicker

Ingredients:

* Twigs x2
* Cordage or Reeds x2

![](/assets/images/primalcraft_recipeWickerSmallWeak.gif)

Ingredients:

* Bark strips or vines x4

![](/assets/images/primalcraft_recipeWickerSmallStrong.gif)

Ingredients:

* Stone Knife
* Medium Wicker

![](/assets/images/primalcraft_recipeWickerSmallFromMedium.png)

### Medium wicker

Ingredients:

* Small Wicker x2
* Cordage, Bark Strips, Vines, or Reeds

![](/assets/images/primalcraft_recipeWickerMediumFromSmall.gif)

Ingredients:

* Stone Knife
* Large Wicker

![](/assets/images/primalcraft_recipeWickerMediumFromLarge.png)

### Large wicker

Ingredients:

* Medium Wicker x2
* Cordage, Bark Strips, Vines, or Reeds

![](/assets/images/primalcraft_recipeWickerLargeFromMedium.gif)

### Small wicker basket

Ingredients:

* Small Wicker x5
* Cordage, Bark Strips, Vines, or Reeds x4

![](/assets/images/primalcraft_recipeWickerBasketSmall.gif)

### Medium wicker basket

Ingredients:

* Medium Wicker x5
* Cordage, Bark Strips, Vines, or Reeds x4

![](/assets/images/primalcraft_recipeWickerBasketMedium.gif)

### Large wicker basket

Ingredients:

* Large Wicker x5
* Cordage, Bark Strips, Vines, or Reeds x4

![](/assets/images/primalcraft_recipeWickerBasketLarge.gif)

## Tools

### Stone flake

Ingredients:

* Hammerstone
* Core Stone

![](/assets/images/primalcraft_recipeStoneFlake.png)

### Stone scraper

Ingredients:

* Hammerstone
* Stone Flake

![](/assets/images/primalcraft_recipeStoneScraper.png)

### Wooden shaft

Ingredients:

* Stone Knife or Stone Scraper
* Stone Flake

![](/assets/images/primalcraft_recipeWoodenShaft.gif)

### Stone knife

Ingredients:

* Stone Knife Tool Head
* Cordage
* Wooden Shaft

![](/assets/images/primalcraft_recipeStoneKnifeAngled.gif)

Ingredients:

* Stone Knife Tool Head
* Cordage
* Wooden Shaft

![](/assets/images/primalcraft_recipeStoneKnifeStraight.gif)

### Stone hatchet

Ingredients:

* Stone Hatchet Tool Head
* Cordage
* Wooden Shaft

![](/assets/images/primalcraft_recipeStoneHatchetAngled.gif)

Ingredients:

* Stone Hatchet Tool Head
* Cordage
* Wooden Shaft

![](/assets/images/primalcraft_recipeStoneHatchetStraight.gif)

### Wooden shovel

Ingredients:

* Stone Hatchet
* Log (any type)

![](/assets/images/primalcraft_recipeWoodenShovel.png)

### Bone needle

Ingredients:

* Stone Knife
* Bone

![](/assets/images/primalcraft_recipeBoneNeedle.png)

### Wooden pegs

Ingredients:

* Stone Knife
* Wooden Shaft

![](/assets/images/primalcraft_recipeWoodenPegs.png)

## Dirt

### Dirt slab

Ingredients:

* Pile of Dirt x2

![](/assets/images/primalcraft_recipeDirtSlabsFromPiles.png)

Ingredients:

* Wooden Shovel
* Dirt Block

![](/assets/images/primalcraft_recipeDirtSlabsFromBlock.png)

### Loose dirt block

Ingredients:

* Pile of Dirt x4

![](/assets/images/primalcraft_recipeDirtBlockFromPiles.png)

Ingredients:

* Dirt Slab x2

![](/assets/images/primalcraft_recipeDirtBlockFromSlabs.png)

### Piles of dirt

Ingredients:

* Wooden shovel
* Dirt Slab

![](/assets/images/primalcraft_recipeDirtPiles.png)
