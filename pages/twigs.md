---
guid: http://www.benjaminrose.com/?page_id=2723
layout: page
title: Twigs (generic)
author: Ben
date: 2020-01-12T21:20:55-08:00
aside:
  toc: true
id: 2723
sidebar:
  nav: primal-lib-menu
---
Twigs are a tree's smaller, younger limbs, thinner than your finger. Twigs are suitable for weaving into wicker or serving as kindling for a fire.

![Primal Lib twig](/assets/images/primalLib_twig.png)

## Obtaining

Break leaf blocks that are on the outer layers of the tree. Leaves that are right next to the trunk will _not_ drop twigs. Starting with 1.1.56, if the Biomes O' Plenty compatibility is turned on, bushes and shrubs will also drop twigs.

The chance of getting twigs depends on the tool you are using:

  * **Bare hands**: 50% chance
  * **Axe**: 100% chance

Only the above tools will yield twig drops. You can't just go around hitting leaves with any random object you happen to be holding! This behavior can be turned on and off in the config options.

The number of twigs dropped is configurable. The default is one.

## Using

**Crafting ingredient**. Generic twigs are used by other mods that are built on PrimalLib.

  * Basket Case—Twigs are used to make baskets. In Basket Case, there are different types of twig for each vanilla tree.
  * Darkest Before Dawn (in progress)—Twigs are used as kindling to make a fire.

**Fuel**. Twigs can be burned in a furnace for 30 seconds.

## Configuring

In the config settings, generic twigs can be tweaked in the following ways:

  * You can disable twigs completely (will not drop from leaves). In Basket Case, in order to craft wood-specific colored baskets, you must enable either twigs or [bark strips]({% link pages/bark-strips.md %}).
  * You can toggle whether twigs should drop from leaves that are next to a log block.
  * You can control how many twigs drop when breaking leaves. The default is one.
  * You can decide whether the chance of dropping twigs should depend on what tool is being used. When enabled, only bare hands, shovel, or axe will yield twigs. When disabled, twigs always drop regardless of what you're holding.
    * If the prior option is turned on, you can specify the percentage chance of twigs dropping for bare hands and axe.

## Data

**Item ID:** primallib:twig_generic

**Translation ID:** item.primallib:twig_generic.name

**OreDictionary terms:** twig
