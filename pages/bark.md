---
id: 2716
layout: page
title: Bark
author: Ben
date: 2020-01-13T05:12:51.000Z
guid: http://www.benjaminrose.com/?page_id=2716
sidebar:
  nav: primal-lib-menu
---
Bark is one of the starting points for making baskets and general purpose [cordage]({% link pages/cordage.md %}).

{%
  include figure.html
  image-name="primalLib_barkAll.png"
  caption="All types of bark in Primal Lib"
%}

There are six kinds of bark. Each is technically its own item. This page covers all six items.

## Obtaining

Bark comes from the logs of a tree. Each type of tree produces its own specific type of bark. Only vanilla logs will give bark—no modded trees.

There are two ways to obtain bark:

{% include _reusedContent/obtainingBarkAndStrippedLogs.md %}

The amount of bark obtained is configurable. The default is four. The max is 64. Set to one for better compatibility with Better With Mods. (Prior to 1.1.56, the amount obtained from the crafting grid was always four.)

## Using

**Crafting ingredient**. Bark can be crafted into [bark strips]({% link pages/bark-strips.md %}).

{%
  include figure.html
  image-name="primalLib_barkStrips.gif"
  caption="Recipe for bark strips"
%}

**Fuel**. Bark can be burned in a furnace for two seconds (40 ticks).

The mod Darkest Before Dawn (in progress) uses birch bark as an ingredient for making torches.

## Configuring

In the config settings, bark can be tweaked in the following ways:

  * You can disable bark completely (won't drop by right-clicking logs and can't be crafted). As a consequence, there will be no stripped logs and no [bark strips]({% link pages/bark-strips.md %}). In Basket Case, bark strips are one of four items that can be used to bind wicker together to craft baskets. Bark strips are required in order to craft wood-specific colored baskets. In Darkest Before Dawn, birch bark is one way to make torches.
  * You can specify how many pieces of bark are produced from each log. The default is four. The max is 64. (Before 1.1.56, only the drops from right-clicking logs could be configured, and the max was 100.)
  * Starting with 1.1.56, you can enable compatibility with No Tree Punching, which allows you to obtain bark using a flint hatchet and any mattock.

## Data

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Acacia bark|primallib:bark_acacia|item.primallib:bark_acacia.name|barkAcacia, barkWood
|Birch bark|primallib:bark_birch|item.primallib:bark_birch.name|barkBirch, barkWood
|Dark oak bark|primallib:bark_dark_oak|item.primallib:bark_dark_oak.name|barkDarkOak, barkWood
|Jungle bark|primallib:bark_jungle|item.primallib:bark_jungle.name|barkJungle, barkWood
|Oak bark|primallib:bark_oak|item.primallib:bark_oak.name|barkOak, barkWood
|Spruce bark|primallib:bark_spruce|item.primallib:bark_spruce.name|barkSpruce, barkWood
