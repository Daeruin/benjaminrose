---
layout: page
title: Release notes for Primal Lib
author: Ben
date: 2021-02-21T00:48:25.317Z
aside:
  toc: true
sidebar:
  nav: primal-lib-menu
---
These are the release notes (change logs) for Primal Lib.

## v1.1.60

**Date**: 3 May 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)

### Changes

* Supports Basket Case 2.1.109.
* Added a utility method for determining if an item is in the player's hotbar.
* Added a utility method for determining if an item is in the player's main hand.

## v1.1.57

**Date**: 5 March 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)

### Changes

Basket Case now has better compatibility with several popular and thematically similar mods, including:

* Better With Mods
* Biomes O' Plenty
* No Tree Punching

Read on for details about the compatibility improvements and other changes.

#### General changes

* Supports Basket Case 2.1.95 and Darkest Before Dawn 0.0.76.
* The twine recipe has been updated to require the generic OreDictionary ingredient "plantFiber" for better compatibility with other mods. Along with this, plant fiber and sugar cane have been added to the OreDictionary as "plantFiber." This means sugar cane must now be crafted into twine to serve as cordage.
* The max number of bark drops has been lowered to 64.

#### Compatibility with Better With Mods

* The amount of bark that logs produce is now configurable. For compatibility with Better With Mods, set the amount to 1. This applies both to the bark recipe and to bark drops when right-clicking on logs.
* The amount of planks that stripped logs produce is now configurable. For compatibility with Better With Mods, set the amount to 3. Note that the recipe does not require an axe and does not produce sawdust.
* Stripped logs now have a configurable burn time. For compatibility with Better With Mods, set the value somewhere between 1200 and 1600.
  
#### Compatibility with Biomes O' Plenty

* Compatibility with Biomes O' Plenty must be turned on in the Primal Lib config.
* Ivy and willow vines from Biomes O' Plenty can now serve as cordage and are also registered to the OreDictionary as vines.
* Cattail, river cane, reed, root, flax, and tall cattail from Biomes O' Plenty are registered as "plantFiber" and can now be crafted into twine.
* Many Biomes O' Plenty grasses and plants will now drop plant fiber. This includes dead grass, desert grass, dune grass, medium grass, wheat grass, barley, devilweed, sea oats, spectral fern, and wild rice.
* Bushes and shrubs from Biomes O' Plenty will now drop twigs. They will be generic twigs unless wood-specific baskets are enabled in Basket Case, in which case they will be oak twigs.
  
#### Compatibility with No Tree Punching

* Compatibility with No Tree Punching must be turned on in the Primal Lib config.
* The amount of planks that stripped logs produce is now configurable. For compatibility with No Tree Punching, set the amount to 3. Note that the recipe does not require an axe and does not produce sawdust.
* You can now get bark from logs using flint hatchets and any mattock. This includes both the crafting grid and right-clicking on placed logs. You must right click on the sides of the log to get bark.
* You can now craft planks from stripped logs using No Tree Punching saws. You still can't get planks by right clicking on a stripped log with a hatchet or mattock.
* If you configure Primal Lib to vary the drop chance of Primal Lib items harvested with an axe, the No Tree Punching hatchet and mattock will honor those settings.

#### Bug fixes

* Fixed some config tool tips in the lang file.

## v1.1.56

**Date**: 24 February 2022\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 should work)

### Changes

* This release had everything listed under v1.1.57, but it had a bug that prevented plant fibers from being crafted into twine. I pulled this release from CurseForge. If you downloaded it, please upgrade to v1.1.57.

## v1.0.43

**Date**: 20 February 2021\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)

This release of Primal Lib supports version 2.0.79 of Basket Case.

### Fixes

  * The config menu now functions again.

## v1.0.39

**Date**: 24 January 2021\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)

This release of Primal Lib supports version 2.0.75 of Basket Case.

### Changes

  * Updated mod info and logo.

### Fixes

  * Fixed a bug with the wooden shaft recipe advancement.

## v1.0.36

**Date**: 20 September 2020\
**Minecraft version**: 1.12.2\
**Forge version**: 14.23.5.2768 (other versions for 1.12.2 may work)

This release of Primal Lib supports version 2.0.72 of Basket Case.

### Changes

  * This is the initial alpha release of Primal Lib. Please see the [Primal Lib home page]({% link pages/primal-lib.md %}) for detailed information.