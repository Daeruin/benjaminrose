---
guid: http://www.benjaminrose.com/?page_id=2852
layout: page
title: Basket Case 2.0 wiki
author: Ben
date: 2020-02-20T04:28:24.000Z
aside:
  toc: true
id: 2852
sidebar:
  nav: basket-case-2-0-menu
---
Basket Case 2.0 is a major upgrade that adds 36 new baskets with different textures based on vanilla trees. See the [release notes]({% link pages/release-notes-for-basket-case.md %}) for more details on what changed with 2.0.

{: .warning}
**ATTENTION:** Make a backup of your world before upgrading Basket Case.

## Wiki contents

* [Twigs from specific trees]({% link pages/twigs-from-specific-trees.md %})
* [Bark strips from specific trees]({% link pages/bark-strips-from-specific-trees.md %})
* [Wicker]({% link pages/wicker.md %})
* [Baskets]({% link pages/baskets.md %})
