---
layout: page
title: Darkest Before Dawn
author: Ben
date: 2022-3-14
aside:
  toc: true
sidebar:
  nav: minecraft-mods-menu
---
_A mod that adds realistic fire-making and torches. Just remember, the night is always darkest before the dawn._

![Darkest Before Dawn banner](/assets/images/darkestBeforeDawn_banner.png)

{: .warning}
Darkest Before Dawn is incomplete and is guaranteed to have bugs. I'm releasing an early version to gauge interest and gather feedback. If you use Darkest Before Dawn, I kindly ask that you report any bugs in my [issue tracker ](https://www.curseforge.com/minecraft/mc-mods/darkest-before-dawn/issues) and take a few minutes to leave feedback on [CurseForge](https://www.curseforge.com/minecraft/mc-mods/darkest-before-dawn).

The goal of Darkest Before Dawn is to add more realism and difficulty around making fire, light, and torches. It will take a significant amount of time before you finally get a furnace, so you'll need to rely on campfires for a while. Torches will burn out, so make sure you have protection at night and watch out for monsters in your base! Ultimately you'll want to rely on more permanent forms of light like glowstone, redstone lamps, or burning netherrack.

## Overview of features

Darkest Before Dawn has the following features:

* **Torches don't last forever.** They go out after a while. You can't put lit torches in your inventory, and you can only place them on soft blocks like dirt. (Eventually I may add a torch bracket block.)
* **Torches have more realistic recipes.** You must have spruce resin, pitch, or animal fat as a fuel source. Pine cones, birch bark, cattails, and sugar cane are also used.
* **Your first light source will be a campfire.** You make it by rubbing sticks together to get a glowing ember, putting the ember on some tinder, and blowing on it. Add some sticks and logs and you will have a campfire. See below for exact instructions.
* **Furnaces are now made with [clay bricks](https://www.youtube.com/watch?v=ShvAN9bLwnw)**, which must be hardened in your campfire. It will take a long time to make your furnace.
* **When it's dark, it's really dark.** The world gets darker based on sunlight, moonlight, the weather, and your distance underground. You won't be able to explore caves without a light source.

## Prerequisites for Darkest Before Dawn

* You must install [Primal Lib 1.1.57](https://www.curseforge.com/minecraft/mc-mods/primal-lib/files).
* You should install [JEI](https://www.curseforge.com/minecraft/mc-mods/jei) so you have easy access to all the recipes.

## Download

<iframe src="https://www.cfwidget.com/606516?version=1.12.2" width="100%"></iframe>

## Darker darkness

Darkest Before Dawn makes it darker at night, in caves, and when it's raining. It also takes sunlight and moonlight into account.

The mod controls light by taking direct control of your brightness settings. If you change your brightness setting manually, Darkest Before Dawn will change it back as soon as you exit the settings page.

This feature has some drawbacks. If you're in a cave, when you are near a light source, the entire cave is relatively brighter. As you move away from the light source, everything gets relatively darker. So the closer you are to the light source, the more you can see in general. This isn't ideal, but it's one of the limitations of using the brightness settings to control light.

This feature can be resource intensive. This feature is probably not compatible with shader packs. Use the config settings to disable it or reduce resource usage.

There are some good alternatives to using this feature, such as the [Hardcore Darkness](https://www.curseforge.com/minecraft/mc-mods/hardcore-darkness) mod, which I highly recommend.

## Campfire

Campfires will be  your first source of light and are required in order to make torches and make the ingredients for a furnace. Making a campfire is difficult and takes time, so make sure you collect the ingredients quickly and start making one well before it gets dark.

### Making a campfire

Needed materials:

* **Tinder.** You can make this by putting plant fiber in the crafting grid. You only need one.
* **Fireboard.** You'll need two wooden shafts and two twine (or other cordage) to craft it.
* **Wooden shaft.** This is made by putting a branch and an axe in the crafting grid. Make sure you enable branches in the Primal Lib config settings.
* **Twigs.** You need at least three.
* **Logs.** You need at least one. If you want the fire to last through the night, get at least four logs.

To make a campfire:

1. Place tinder on the ground. It can't be placed on grass blocks—only dirt, gravel, clay, sand, farmland, grass paths, or stone.
2. Place a fireboard nearby. Right click and hold with a wooden shaft on the fireboard. After a few seconds, if you are lucky a glowing ember will appear. If not, keep trying.
3. Glowing embers can only be held with your main hand. Free up your main hand and the glowing ember will pop into your hot bar. Glowing embers also go out very quickly.
4. Right click on the tinder with the glowing ember. You should see small smoke particles appearing.
5. Right click on the smoking tinder with an empty hand. You should hear a blowing sound.
6. Wait three seconds and right click on the stoked tinder with an empty hand to blow again. Keep doing this until you see a tiny flame appear. This means the fire is ready for twigs.
7. Right click on the tinder while holding at least three twigs. This adds kindling to your baby fire.
8. Immediately blow on the kindling by right clicking on it with an empty hand. Wait three seconds and do it again. Keep doing this until  you see a big puff of smoke appear with a flame.
9. Right click on the kindling with a log. Your campfire will appear!

Some tips:

* Make sure it's not raining or you won't get very far.
* Remember tinder can't be placed on grass blocks.
* When blowing on your tinder or kindling (steps 6 and 8), you can hold right click so you don't have to count the seconds. Just watch for the flame and smoke so you know when to add twigs or logs.
* If you have flint and steel, you don't need a glowing ember. You can right click on the tinder with flint and steel. Skip straight to step 5.
* If you already have a burning torch, put down your tinder and add twigs, then right click on that with the torch. Skip straight to step 8.
* If you make a mistake, your baby fire will go out. Don't wait too long between steps, and don't add twigs or logs too soon.
* If you mess up or want to start over, you can sneak-right-click to pick up your twigs and kindling.
* The log should burn for over three minutes, so you'll need about three or four logs to last through an entire night.

### Using a campfire

**Fuel capacity.** Campfires can only have four items in the fuel slot at a time. If you're adding things that burn quickly, your campfire will quickly go out unless you're watching it and adding fuel as it burns. Logs have the longest burn time. You can turn any item into campfire fuel using the config settings.

**Adding fuel and items to cook.** Right click on the campfire with any item that can be cooked or serve as fuel. It will automatically be added into the appropriate slot in the campfire. Logs can only be placed in the fuel slot this way, not hte cooking slot.

**Lighting torches.** Sneak-right-click on a burning campfire with an unlit torch. The torch will light.

**Relighting from coals.** When the fire runs out of fuel, it turns into coals. The coals will stay hot for a while. You can add fuel to the coals, and the fire will automatically relight. If you don't add fuel in time, the coals turn to ashes.

**Putting the fire out.** Right click on the campfire with a water bucket to put it out, or break the campfire. Or just wait for it to run out of fuel. Campfire fuel is used up faster when it's raining.

**Getting charcoal dust.** Breaking the campfire when it's burning, coals, or ashes will produce some charcoal dust. The amount is configurable.

#### Campfire recipes

**Food.** You can cook normal vanilla food in your campfire. That includes pork chops, beef, chicken, rabbit, mutton, fish, and potatoes.

**Charcoal dust.** Put a log in the cooking slot, and after a long time it will produce charcoal dust. When a campfire goes out, it leaves ashes on the ground. Break the ashes to get more charcoal dust. Charcoal dust is used to make pitch for torches.

**Pottery.** First make an unfired clay bowl by putting three clay balls in the crafting grid in the shape of a bucket. Put the unfired clay bowl in the cooking slot. After a long time it will become a hardened clay bowl. This can be used to melt resin and animal fat in the campfire. Melted animal fat is called *rendered* animal fat.

**Torch fuel.** Place spruce resin or animal fat in the crafting grid with a hardened clay bowl. This puts the resin or fat into the bowl. Then put that into the cooling slot. Soon you'll have some melted fat or resin that you can use to make a torch.

**Miscellaneous.** You can cook cactus, wet sponge, and chorus fruit in a campfire just like you would in a furnace.

## Fireboard

When making a fire using sticks, the stick or piece of wood on the ground is called a "fireboard" or sometimes a "fire hearth." 

In Darkest Before Dawn, you can craft a fireboard with two wooden shafts and two pieces of twine or other cordage. (Use [JEI](https://www.curseforge.com/minecraft/mc-mods/jei) for the exact recipe.)

Fireboards have a 10% chance of breaking each time you attempt to use them to create a burning ember.

If you're interested, here's some geeky background to my design choice for Darkest Before Dawn fireboards. Modern survivalists often use a flat plank of wood as a fireboard, and they cut a notch in it where the spinning stick goes. But obviously our Stone Age ancestors didn't have planks, and they often didn't have effective knives to cut a notch with. You might not either, if you get stuck unexpectedly in the wilderness. Another method for making a fireboard is to tie two sticks together and use the crevice between the sticks as the receptacle for the spinning stick. This is an extremely low-tech method that doesn't require any kind of knife or tool. See this [YouTube video](https://www.youtube.com/watch?v=IL9PGHWWIbQ) for an example of how the two-stick fireboard works.

## Torches

One of the main goals of Darkest Before Dawn is to make it more challenging to have sustained lighting sources. To this end, torches will now go out after a certain amount of time. By default, torches are less bright than in vanilla Minecraft. Torches are also harder to make in Darkest Before Dawn.

In Darkest Before Dawn, torches only last so long before going out. By default, they last 2.5 minutes of gameplay time (3,000 ticks). The length can be changed in the config settings. Torches continue to burn down in your inventory, when dropped on the ground, and when placed as a block. Torches do not give light when held in your inventory.

When a torch is about to go out, it will start smoldering—its light dims, it starts emitting extra smoke, and it displays fewer fire particles. By default, torches smolder for about 30 seconds before they go out. This length can be changed in the config settings.

When a torch goes out, it will sometimes drop a wooden shaft. The percentage chance of this occurring is 85% by default, and this value is configurable.

I have not figured out how to make torches give light when held. This may be possible with OptiFine or shaders, but I have not tested it.

### Obtaining

All torch recipes create unlit torches. There are multiple recipes for torches. Please use [JEI](https://www.curseforge.com/minecraft/mc-mods/jei) to see all the recipes.

Nearly all torches will require melted resin or rendered animal fat as a fuel source. To get animal fat and resin:

* Animal fat drops from many animals when they are killed. Some animals drop more than others. You can use the config settings to define which mobs drop animal fat and how much they drop.
* To obtain resin, first strip the bark from a spruce tree. It works best to strip a single block near the base of the tree, leaving the bottom block intact. After a while, some resin will drop down onto the block below the stripped block. Right click on the resin, and it will go directly into your inventory.

To melt resin or render animal fat, you will need a clay bowl. See **Campfire recipes** above for directions on making a clay bowl. Once you have a hardened clay bowl, put it in the crafting grid with some animal fat or resin. You will get a clay bowl filled with animal fat or resin. Then place this in the cooking slot of a campfire. The animal fat or resin will quickly melt. After you take it out of the campfire, it will eventually harden, so you have to make your torch soon or you may have to melt your animal fat or resin again.

Torches also require something to hold your melted resin or rendered animal fat. There are three items that can do this:

* Birch bark—obtained by stripping birch logs
* Spruce cones—obtained by breaking spruce leaves
* Cattails (if you use Biomes O' Plenty)

Once you have one of the above items along with rendered animal fat or melted resin, you can make a torch with a wooden shaft and some twine or other cordage. Use JEI for the exact recipe.

You can also use a piece of frayed cane with melted resin or rendered animal fat. To get frayed cane, right click with sugar cane on a hard block like a log or stone block. This is by far the easiest way to make a torch. You can disable it in the config settings.

Another way to make a torch is to make pitch by combining melted resin with charcoal dust. You can get charcoal dust from the ashes of a campfire, by cooking logs in a campfire, or by right clicking with charcoal on a hard surface. Once you have pitch, place it in the crafting grid with a wooden shaft.

### Using

**Lighting.** You can use existing sources of fire to light an unlit torch. The blocks and items that can be used to light a torch is configurable. You can't light a torch when it's raining.

* While holding an unlit torch, right click on any fire source to light the held torch. When clicking on a campfire, you'll have to sneak-right-click to light the torch (regular right click will open the campfire GUI).
* When holding a lit torch, right click on a placed unlit torch to light the placed torch.

**Holding.** By default, lit torches can only be held in your main hand or off hand. Dropped torches will only go into your inventory if one of your hands is empty. Attempting to place a lit torch into any other slot will cause you to drop the torch. This behavior can be disabled in the config settings.

**Placing.** Torches can only be placed on soft blocks: grass, dirt, sand, clay, and snow. Eventually I plan to create a torch bracket block that will allow torches to be placed on wood and stone.

**Picking up.** You can sneak-right-click with an empty hand on a placed torch to pick it up. Dropped torches will only go into an empty main or off hand slot.
