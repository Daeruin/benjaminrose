---
id: 3301
title: Basket Case FAQ
date: 2020-02-19T19:04:16-08:00
author: Ben
layout: page
guid: http://www.benjaminrose.com/?page_id=3301
sidebar:
  nav: basket-case-menu
---
_**Where can I download Basket Case?**_

Here:

<iframe src="https://www.cfwidget.com/minecraft/mc-mods/basketcase?version=1.12.2" width="100%"></iframe>

**_Can I use Basket Case in my mod pack?_**

Yes. Please credit me.

**_Will you back port Basket Case?_**

No. I am not the slightest bit interested in back porting. Please don't ask.

**_Will you make Basket Case compatible with some other mod?_**

Maybe. In general, I want Basket Case to be usable with other mods, but it depends on how difficult the compatibility will be and how much I feel like the other mod matches Basket Case thematically.

**_I found a bug. Help!_**

You can report bugs in the [Basket Case issue tracker](https://bitbucket.org/Daeruin/basketcase/issues).

_**How do I install Basket Case?**_

Follow the instructions for [installing Forge mods](https://minecraft.gamepedia.com/Mods/Installing_Forge_mods).

**_Where is the source code?_**

I host the Basket Case [repository in BitBucket](https://bitbucket.org/Daeruin/basketcase/src/master/).
