---
layout: page
title: How to install and configure mods
author: Ben
date: 2021-02-08T04:17:51.889Z
aside:
  toc: false
sidebar:
  nav: minecraft-mods-menu
---
Minecraft by itself is a really fun game. Minecraft with mods is even funner. Mods are add-ons that change how the game is played.

If you've never played Minecraft with mods before, it can be hard to know where to start. Here are a few resources to help you get started.

## Installing mods

I recommend the following articles for help installing Forge and your first mod.

* [How to Install Minecraft Mods to Customize Your Game](https://www.howtogeek.com/202633/how-to-install-minecraft-mods-to-customize-your-game/)—This article explains what mods are, why you might want to use them, how to stay safe with mods, and how to do a basic installation using the vanilla game launcher. However, once you get to the heading titled "Installing Your First Mod," you should jump to the following article for the actual installation.
* [How to Manage Minecraft Instances and Mods with MultiMC](https://www.howtogeek.com/202661/how-to-manage-minecraft-instances-and-mods-with-multimc/)—This article shows you how to install and run a modded Minecraft game in an organized and sustainable way using MultiMC. I highly recommend using MultiMC instead of the vanilla Minecraft launcher if you are using mods.

## Configuring mods

Most mods can be configured, or tweaked, to work how you want them to work. I always include configuration options in my mods, because if you're using mods, chances are you want more control over your Minecraft experience.

Once you've installed a mod based on Forge for Minecraft 1.12, you can configure the mod from inside Minecraft—if the mod author has included configuration options in the mod.

1. On the main Minecraft menu, click the *Mods* button to get a list of all the mods you have installed.
2. Select the mod you want to configure.
3. Click the *Config* button. If there's no button, the mod doesn't have configuration options.
4. In the mod's configuration menu, set the options you want. Every mod's configuration menu is different.