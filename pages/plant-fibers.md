---
guid: http://www.benjaminrose.com/?page_id=2690
layout: page
title: Plant fibers
author: Ben
date: 2020-01-13T04:28:20.000Z
aside:
  toc: true
id: 2690
sidebar:
  nav: primal-lib-menu
---
Plant fibers are the fibrous stalks of grass and other plants, used to make [cordage]({% link pages/cordage.md %}) (twine) in ancient times.

![Plant fibers](/assets/images/primalLib_plantFibers.png)

## Obtaining

Break tall grass or any double-tall plant except sugar cane. Starting with 1.1.56, if the Biomes O' Plenty compatibility is turned on, the following will also drop plant fibers: dead grass, desert grass, dune grass, medium grass, wheat grass, barley, devilweed, sea oats, spectral fern, and wild rice.

By default, the chance of getting plant fibers depends on the tool you are using:

  * **Bare hands**: 50% chance
  * **Shovel**: 70% chance
  * **Axe**: 100% chance

Only the above tools will yield plant fiber drops. You can't just go around hitting tall grass with any random object you happen to be holding! This behavior can be turned on and off in the config options.

The amount of plant fiber dropped is configurable. The default is one.

## Using

**Crafting ingredient**. Two or three plant fibers can be braided into one or two pieces of [twine]({% link pages/twine.md %}).

{%
  include figure.html
  image-name="primalLib_cordageRecipe.gif"
  caption="Recipe for twine from plant fibers"
%}

**Fuel**. Plant fibers can be burned in a furnace for half a second (10 ticks).

## Configuring

In the config settings, plant fibers can tweaked in the following ways:

  * You can disable plant fibers completely (will not drop from tall grass).
  * You can prevent plant fibers from being crafted into [twine]({% link pages/twine.md %}).
  * You can control how many plant fibers drop when breaking tall grass. The default is one.
  * You can decide whether the chance of dropping plant fibers should depend on what tool is being used.
    * If set to **true**, only bare hands, shovel, or axe will yield plant fibers. You can specify the percentage chance of plant fibers dropping with each of those tools. 
    * If set to **false**, plant fibers always drop regardless of what you're holding.

## Data

**Item ID:** primallib:plant_fiber

**Translation ID:** item.primallib:plant_fiber.name

**OreDictionary terms:** plantFiber (starting in 1.1.56)
