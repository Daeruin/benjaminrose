---
layout: page
title: Compatibility with other mods
author: Ben
date: 2021-02-15T02:00:46.495Z
aside:
  toc: true
sidebar:
  nav: minecraft-mods-menu
---
I've spent some time building features to improve compatibility between Primal Lib and Basket Case and some other popular mods that fit thematically.

* Better With Mods
* Biomes O' Plenty
* No Tree Punching

## Better With Mods

[Better With Mods](https://www.curseforge.com/minecraft/mc-mods/bwm-suite) is a fantastic Minecraft mod that essentially recreates the functionality of the well-loved [Better Than Wolves](http://www.sargunster.com/btw/index.php?title=Main_Page) mod, except it works with newer versions of Minecraft and it's built with more mod compatibility in mind. Better With Mods keeps relatively close to vanilla Minecraft, adding some difficulty and cool elements like mechanical power (windmills, sawmills, etc.). Thematically, Better With Mods fits really well with Primal Lib and Basket Case.

Initial compatibility features were added in Primal Lib 1.0.43 and Basket Case 2.0.79. Further compatibility improvements were added in Primal Lib 1.1.57 and Basket Case 2.1.95.

You have to enable Better With Mods compatibility  in the Primal Lib config settings.

### Bark

* Bark from Primal Lib and Better With Mods are interchangeable in recipes. Better With Mods bark can be used to make bark strips, and therefore Basket Case baskets. Primal Lib bark can be used in Better With Mods cauldrons to make tanned leather.
* The bark items themselves can't be combined in your inventory, so if you leave bark enabled in both mods, you will have to juggle multiple stacks of bark.
* Better With Mods only gives 3 pieces of bark when using an axe. Primal Lib gives 4 pieces of bark by default when using an axe. In addition, because Better With Mods for 1.12 did not account for the possibility of stripped logs, it gives you bark when crafting stripped logs into planks. With Primal Lib's default values, you'll be getting more bark than Better With Mods intended. In Primal Lib's config settings, you can reduce the amount of bark it provides or disable bark (and therefore stripped logs) completely if desired.
* If you're using Basket Case with Primal Lib, it takes quite a bit of bark to make a basket. However, you can still make baskets from twigs and vines.

### Planks

* In Better With Mods, crafting planks with an axe only provides 3 planks. You can configure how many planks are produced by an axe and stripped logs to match.
* Because Better With Mods for 1.12 did not account for the possibility of stripped logs, it gives you bark when crafting stripped logs into planks. You may want to disable stripped logs or the planks recipe because of this.

### Stripped logs

* As already mentioned above, Better With Mods for 1.12 did not account for the possibility of stripped logs, so it gives you bark when crafting stripped logs into planks. You may want to disable stripped logs because of this.
* If you keep stripped logs, you can configure their burn time in Primal Lib's config settings. In Better With Mods, logs have a burn time between 1200 and 1600 ticks, depending on what kind of log it is. Primal Lib's stripped logs will all have the same burn time. I recommend setting it to 1400 for a good balance.

### Shafts and branches

* Both Primal Lib and Better With Mods have wooden shafts. In Primal Lib, shafts are made from branches and can be used as primitive digging tools and weapons. In Better With Mods, shafts are made from sticks and can be placed as a vertical block, which is useful as a marker or a way to make torches taller.
* Branches and shafts are disabled in Primal Lib by default, so there's nothing to worry about here unless you want to enable them for some reason.
* If you do enable branches and shafts, be aware that Primal Lib shafts have the same texture as Better With Mods shafts (they are both borrowed from Better Than Wolves). This may be confusing to players.

## Biomes O' Plenty

Compatibility improvements were added in Primal Lib 1.1.57 and Basket Case 2.1.95.

### Plant fiber

* Starting in Primal Lib 1.1.57 and Basket Case 2.1.95, many Biomes O' Plenty plants will drop plant fiber, which can be used to craft cordage and make baskets.
* Biomes O' Plenty plants that will drop plant fiber: dead grass, desert grass, dune grass, medium grass, wheat grass, barley, devilweed, sea oats, spectral fern, and wild rice.
* Cattail, river cane, reed, root, flax, and tall cattail from Biomes O' Plenty also count as plant fiber and can be crafted into cordage.

### Cordage

* Ivy and willow vines from Biomes O' Plenty can serve as cordage.

### Twigs

* Bushes and shrubs from Biomes O' Plenty will drop twigs. They will be generic twigs unless wood-specific baskets are enabled in Basket Case, in which case they will be oak twigs.

### Vines and baskets

* Ivy and willow vines from Biomes O' Plenty count as vines, so they can be crafted into generic baskets in Basket Case (if wood-specific baskets are disabled).


## No Tree Punching

[No Tree Punching](https://www.curseforge.com/minecraft/mc-mods/no-tree-punching) aims to extend the early game by making it so  you can't punch trees—you have to get flint to make a stone hatchet first. It also adds some other fun and interesting detail for the early to mid game, such as pottery, saws, and mattocks. It's a great fit thematically for Primal Lib and Basket Case, so I've built in some features that make our mods more compatible.

Compatibility improvements were added in Primal Lib 1.1.57 and Basket Case 2.1.95.

* Compatibility with No Tree Punching must be turned on in the Primal Lib config.
* You can get bark from logs using flint hatchets and any mattock. This includes both the crafting grid and right-clicking on placed logs. You must right click on the sides of the log to get bark.
* If you configure Primal Lib to vary the drop chance of Primal Lib items harvested with an axe, the No Tree Punching hatchet and mattock will honor those settings.
* You can craft planks from stripped logs using No Tree Punching saws. You still can't get planks by right clicking on a stripped log with a hatchet or mattock.
* The amount of planks that stripped logs produce is configurable. For compatibility with No Tree Punching, set the amount to 3.
