---
id: 3268
layout: page
title: Cordage
author: Ben
date: 2020-02-18T04:46:53.000Z
guid: http://www.benjaminrose.com/?page_id=3268
sidebar:
  nav: primal-lib-menu
---
Cordage is a strong, naturally crafted string used by our ancient ancestors, aboriginal people, and wilderness survival enthusiasts today.

## Cordage in my mods

In my mods, I use cordage as a general term for naturally crafted string, regardless of what material it comes from or how it's created. Most recipes in my mods require cordage instead of string.

A number of items count as cordage in Primal Lib, as shown below. (Primal Lib does this by registering items as "cordage" in Forge's [OreDictionary](https://documentation-tterrag.readthedocs.io/en/latest/utilities/oredictionary/).)

Vanilla items that count as cordage:

* Sugar cane, before 1.1.56
* Vines

Primal Lib items that count as cordage:

* [Twine]({% link pages/twine.md %})
* [Bark strips]({% link pages/bark-strips.md %})

Biomes O' Plenty items that count as cordage (if enabled, starting with 1.1.56):

* Ivy
* Willow vines

## Configuring

In the config settings, cordage can be tweaked in the following ways:

* You can choose not to let Primal Lib register bark strips or vines as cordage. Twine is always registered as cordage.
* You can enable compatibility with Biomes O' Plenty. This will register ivy and willow vines as cordage. (Prior to 1.1.56, this was not possible.)

## Real-life cordage

Cordage is often made by braiding or twining together the fibrous stalks of grass and other plants, but also various other plant materials, such as bark strips, roots, and vines.

The general technique for making cordage is to cut grass or plant stalks, then slit them open and sometimes beat them against a rock or tree to loosen up the individual fibers. Fibers should be cut when dry, otherwise they need to be dried out afterward. Then you twist the fibers together with your fingers or roll them along your thigh so they twine together in thick strands.

Here are some additional details and resources about cordage:

  * [Primitive Ways](https://www.primitiveways.com/cordage.html)
  * [Wildwood Survival](https://www.wildwoodsurvival.com/survival/cordage/trm/trm1-1pg04.html)
  * [Mother Earth News](https://www.motherearthnews.com/diy/making-cordage-natural-materials-zmaz83jfzraw)
  * [Jon's Bushcraft](http://www.jonsbushcraft.com/cordage%20making.htm)
