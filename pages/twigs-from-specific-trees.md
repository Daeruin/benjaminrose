---
id: 2853
layout: page
title: Twigs from specific trees
author: Ben
date: 2020-02-20T04:30:14.000Z
guid: http://www.benjaminrose.com/?page_id=2853
sidebar:
  nav: basket-case-2-0-menu
---
Basket Case twigs are based on [twigs from Primal Lib]({% link pages/twigs.md %}). In Basket Case, twigs are specific to the tree they come from, so there are six kinds of twig.

{%
  include figure.html
  image-name="basketCase2_twigAcacia.png"
  alt="Acacia twig"
  caption="Acacia twig"
%}

{%
  include figure.html
  image-name="basketCase2_twigBirch.png"
  alt="Birch twig"
  caption="Birch twig"
%}

{%
  include figure.html
  image-name="basketCase2_twigDarkOak.png"
  alt="Dark oak twig"
  caption="Dark oak twig"
%}

{%
  include figure.html
  image-name="basketCase2_twigJungle.png"
  alt="Jungle twig"
  caption="Jungle twig"
%}

{%
  include figure.html
  image-name="basketCase2_twigOak.png"
  alt="Oak twig"
  caption="Oak twig"
%}

{%
  include figure.html
  image-name="basketCase2_twigSpruce.png"
  alt="Spruce twig"
  caption="Spruce twig"
%}

## Obtaining

Basket Case twigs are obtained in the same way as generic [twigs from Primal Lib]({% link pages/twigs.md %}). The only difference is that the resulting twigs will be specific to the type of wood. If compatibility with Biomes O' Plenty is enabled in Primal Lib 1.1.56 and later, bushes and shrubs will drop oak twigs.

## Using

Basket Case twigs can be crafted into wicker—see the [wicker]({% link pages/wicker.md %}) page for the recipes.

In all other ways, Basket Case twigs are identical to [twigs from Primal Lib]({% link pages/twigs.md %}), including serving as furnace fuel.

## Configuring

Configuration settings for [twigs from Primal Lib]({% link pages/twigs.md %}) will also apply to Basket Case twigs.

In the Basket Case config settings, twigs can be tweaked in the following ways:

  * You can choose to prevent wood-specific baskets. In that case, only the generic twigs from Primal Lib will be present (if configured to be turned on in the Primal Lib config). Generic twigs can still be crafted into [wicker]({% link pages/wicker.md %}), but it will be generic wicker that looks like oak wicker. If you _do_ allow wood-specific baskets, then generic twigs will _not_ be available.

## Data

|----
|Item|Item ID|Translation ID|OreDictionary Terms
|-|-|-|-
|Acacia twigs|basketcase:twigs_acacia|item.basketcase:twigs_acacia.name|twig
|Birch twigs|basketcase:twigs_birch|item.basketcase:twigs_birch.name|twig
|Dark oak twigs|basketcase:twigs_dark_oak|item.basketcase:twigs_dark_oak.name|twig
|Jungle twigs|basketcase:twigs_jungle|item.basketcase:twigs_jungle.name|twig
|Oak twigs|basketcase:twigs_oak|item.basketcase:twigs_oak.name|twig
|Spruce twigs|basketcase:twigs_spruce|item.basketcase:twigs_spruce.name|twig
