---
guid: http://www.benjaminrose.com/?page_id=3289
layout: page
title: Basket Case
author: Ben
date: 2020-02-20T03:03:43.000Z
aside:
  toc: true
id: 3289
sidebar:
  nav: basket-case-menu
---
_Just in case you need baskets in your Minecraft world._

![Basket Case banner](/assets/images/basketCase2_banner.png)

## Introduction

![Basket Case logo](/assets/images/basketcase-logo.png){: height="150px"}

Basket Case is a small Minecraft mod that adds wicker baskets as a new form of storage. Baskets can be used decoratively, but they are also a handy form of portable storage. This convenience comes with a few minor tradeoffs. They take a little work to make, and they only hold so much.

Baskets come in three sizes—small, medium, and large—and hold less than a chest. The following screenshots show the basket inventories with some items and blocks from Basket Case and Primal Lib.

![Small basket inventory](/assets/images/basketCase2_basketInventorySmall.png)
![Medium basket inventory](/assets/images/basketCase2_basketInventoryMedium.png)
![Large basket inventory](/assets/images/basketCase2_basketInventoryLarge.png)

Baskets are made from wicker, which is made from plant materials like twigs, bark strips, vines, reeds, and grass. Baskets can carried in your hot bar, and they can be opened while you carry them. Baskets are convenient to pick up with a sneak-right-click, which saves you time and durability on your tools.

## Download

<iframe src="https://www.cfwidget.com/minecraft/mc-mods/basketcase?version=1.12.2" width="100%"></iframe>

## Dependencies

Basket Case was built for the Java edition of Minecraft using the [Forge](http://files.minecraftforge.net/) modding API. In addition to Forge, version 2 of Basket Case also requires you to install the [Primal Lib]({% link pages/primal-lib.md %}) mod. Check the [Release Notes]({% link pages/release-notes-for-basket-case.md %}) page for specifics on what version of Minecraft, Forge, and Primal Lib you will need to run a given version of Basket Case.

## Documentation

  * [Release Notes]({% link pages/release-notes-for-basket-case.md %})—Release notes and version dependencies for every version of Basket Case
  * [Basket Case 1.0 Wiki]({% link pages/basket-case-1-0-wiki.md %})
  * [Video walkthrough for 1.0](https://www.youtube.com/watch?v=W0pJKOnMDNw)
  * [Basket Case 2.0 Wiki]({% link pages/basket-case-2-0-wiki.md %})
  * [FAQ]({% link pages/basket-case-faq.md %})

## Links

  * [Curseforge page](https://www.curseforge.com/minecraft/mc-mods/basketcase)
  * [Minecraftforum post](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2929111-basketcase-just-in-case-you-need-baskets-in-your)
  * [Issue tracker](https://www.curseforge.com/minecraft/mc-mods/basketcase/issues) (Curseforge)
