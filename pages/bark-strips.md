---
id: 2719
layout: page
title: Bark strips (generic)
author: Ben
date: 2020-01-13T05:16:30.000Z
guid: http://www.benjaminrose.com/?page_id=2719
sidebar:
  nav: primal-lib-menu
---
After stripping [bark]({% link pages/bark.md %}) from a tree, you can separate it into thin strips suitable for using as [cordage]({% link pages/cordage.md %}). You can also use bark strips to make [baskets]({% link pages/baskets.md %}) in Basket Case.

In ancient times, the [process](http://www.jonsbushcraft.com/willow-bark-cordage.htm) of preparing bark strips could be very intensive. You would strip the bark from the tree with an axe, then strip off the brittle outer bark with a knife, cut or tear the flexible inner bark into strips, boil the inner bark strips with ashes in water, let them dry, and finally shave the strips down with a knife. I have chosen to drastically simplify the process for the game.

![Primal Lib bark strips](/assets/images/primalLib_barkStrips.png)

## Obtaining

Craft from any [bark]({% link pages/bark.md %}). You get four generic bark strips for each piece of bark. This means each log block can produce 16 bark strips by default.

{%   include figure.html
  image-name="primalLib_barkStrips.gif"
  caption="Recipe for bark strips"
%}

## Using

**Crafting ingredient**. Bark strips are registered as "cordage" in Forge's OreDictionary. They are used as crafting ingredients by other mods that are built on PrimalLib.

* **Basket Case**—Bark strips are used to make [wicker]({% link pages/wicker.md %}) and [baskets]({% link pages/baskets.md %}). In Basket Case, there are different types of bark strips for each vanilla tree.
* **Darkest Before Dawn** (in progress)—Bark strips can be used to make fireboards.

**Fuel**. Bark strips can be burned in a furnace for one and a half seconds (30 ticks).

## Configuring

In the config settings, bark strips can be tweaked in the following ways:

* You can disable bark strips completely (cannot be crafted from bark). In Basket Case, bark strips are one of four items that can be used to bind [wicker]({% link pages/wicker.md %}) together to craft [baskets]({% link pages/baskets.md %}). In order to craft wood-specific colored baskets, you must enable either bark strips or [twigs]({% link pages/twigs.md %}).
* You can prevent bark strips from counting as [cordage]({% link pages/cordage.md %}) for crafting recipes.

## Data

**Item ID:** primallib:bark_strips_generic

**Translation ID:** item.primallib:bark_strips_generic.name

**OreDictionary terms:** barkStrips, cordage, cordageStrong
