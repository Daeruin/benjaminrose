{: .warning}
**ATTENTION:** Basket Case 2.0 is a major upgrade for Basket Case. Make a backup of your world before upgrading Basket Case from 1.0.