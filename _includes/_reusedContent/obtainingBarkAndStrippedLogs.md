  * Use (right click) an axe on a log.
  * Place a log in the crafting grid with an axe.

{%
  include figure.html
  image-name="primalLib_oakBarkRecipe.png"
  caption="Recipe for oak bark"
%}

After crafting, the log will become a stripped log.

{%
  include figure.html
  image-name="primalLib_oakBarkRecipeAfterCrafting.png"
  caption="After crafting bark"
%}

If compatibility with No Tree Punching is enabled (starting with 1.1.56), you can also use a flint hatchet and any mattock in place of an axe. You must click on the side of the log to get bark.