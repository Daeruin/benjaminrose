---
from_wordpress: true
comments_id: 761
title: Dragon Age II Screen Shots (Not for the Faint of Heart)
date: 2012-02-23T20:02:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=761
permalink: /post/dragon-age-ii-screen-shots-not-for-the-faint-of-heart/
categories:
  - gaming
tags:
  - dragon age
---
A while back I started playing Dragon Age II, a dark fantasy game that merges action and drama. It's very bloody—excessively so, I would say, since giant gouts of blood accompany pretty much every blow in the game. But what can I say? I'm a violence monger, and I've really been enjoying the game.

One of the interesting mechanics it has is the ability to rotate the action in three dimensions during play, combined with the ability to pause combat at any point in time. So you can pause the game, then rotate and pan the view to get a close look at what's going on from pretty much any conceivable angle. This makes for some pretty sweet screen shots.

So, I thought I'd share some of my screen shots today. These are just thumbnails. Click on them to get a much bigger version.

The pause, pan, and zoom ability allowed me to zoom in on this nice shot of my character getting impaled by a long spear:

[![Impaled](/assets/images/dragonAge2_impaled.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_impaled.jpg)

And getting stabbed in the back:

[![Backstab2](/assets/images/dragonAge2_backstab2.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_backstab2.jpg)

Here's one of my party members getting stabbed in the back also:

[![Backstab](/assets/images/dragonAge2_backstab1.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_backstab1.jpg)

I get to do some pretty cool things, too. This is my character sending a mage flying through the air (notice the nice mist of blood left where he used to be standing):

[![Mighty Blow](/assets/images/dragonAge2_mightyBlow.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_mightyBlow.jpg)

And this is my party's rogue doing a sweet jump attack on an unsuspecting enemy:

[![Jump Attack](/assets/images/dragonAge2_jumpAttack.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_jumpAttack.jpg)

And this is my character in the middle of my party mage's Firestorm spell. Notice the burning enemies and dead bodies littered about. When a mage casts Firestorm, you just wait it out and see who's left over.

[![Firestorm](/assets/images/dragonAge2_firestorm.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_firestorm.jpg)

On the other hand, there's more of my character getting mauled, this time by a giant spider fang:

[![Spider Bite](/assets/images/dragonAge2_spiderBite.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_spiderBite.jpg)

Here comes a dragon. Be careful!

[![Dragon Wings](/assets/images/dragonAge2_dragonWings.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_dragonWings.jpg)

I wasn't careful.

[![Dragon Bite](/assets/images/dragonAge2_dragonBite.jpg){: width="150px" height="150px"}](/assets/images/dragonAge2_dragonBite.jpg)

I've really enjoyed Dragon Age II quite a bit. I recommend it heartily!
