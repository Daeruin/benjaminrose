---
from_wordpress: true
comments_id: 25
title: Blogging is publishing
date: 2007-07-14T15:14:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=25
permalink: /post/blogging-is-publishing/
categories:
  - metablogging
  - writing and publishing
---
I have found out that posting something on your blog may prevent you from publishing it elsewhere. I didn't know that! Here's a quote from the submission guidelines for _Strange Horizons_ magazine.

> Like most magazines, we consider material that has appeared on publicly-accessible websites to be published, and therefore cannot consider it.

Best consider that before posting your new short story on the Web.