---
from_wordpress: true
comments_id: 116
title: Brandon Sanderson Book Signing
date: 2008-11-01T09:20:23-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=116
permalink: /post/brandon-sanderson-book-signing/
categories:
  - fiction
---
I mentioned a while back that I attended a book signing by Brandon Sanderson. It was a new experience for me. I've never attended a con and never heard of an author I liked doing a book signing here in Utah. Luckily for me, Sanderson lives here, just about 45 minutes from my house. I skipped the actual release party for The Hero of Ages because it was just a tad too far for me to make it after work. Plus, I didn't want to wait in a gigantic line.

So I waited for Sanderson's appearance at Sam Weller's Zion Bookstore the day after, and I'm glad I did. It was a small little gathering, maybe 20 or 30 people tops. I've listened to a live podcast recording done by Sanderson's Writing Excuses cohosts at the release party, and it sounded way too busy. Some people had been waiting in line all day just to be at the front and get the low numbered copies (Sanderson numbers his signatures with each release). I got number 496, but I'm OK with that. I'm really not that into getting signatures. It's just kind of fun to have it, since I can.

The crowd was small enough that Sanderson was able to do a Q&A and a reading without using a microphone. There were maybe 50 chairs set up just down the stairs in the Sam Weller's basement level, and less than half of them were full. I enjoyed the small setting. You could see him quite well, and it had a relaxed, fun feel.

I actually saw Sanderson hanging out by the fantasy/sci-fi shelves with a friend as I went down the stairs. I thought about wandering over to say hi, but didn't want to bug him. Plus, I'm just not that outgoing. After a few minutes, he made his way downstairs and they got started. A few people asked if they could get signatures right away, since they had to leave early. The bookstore people decided to go ahead and do the signing first. I was second in line. Again, I was too introverted to really think of anything to say, so he just signed it and I went back to sit down.

This signing was interesting because Sanderson brought the guy who did the internal illustrations—the maps and all the little symbols. He signed the maps and passed out some business cards for some person who's making Mistborn jewelry. A good portion of the Q&A focused on the illustrations. I think people's curiosity had been piqued. I know mine was. I asked Sanderson how he managed to snag his own internal illustrator, since authors typically have no say on the cover illustrations.

He said that internal illustrations are a bit different. They're not always present, for one thing. A book editor might decide to include a map, but the author is more involved since the world setting belongs to him or her. In Sanderson's case, he'd had a bad experience working with the publisher on the map for one of his earlier books. So this time he just commissioned the artwork himself and gave it to his publisher, basically saying "Here. Use this or nothing." It also worked out because he now owns that artwork and can do with it as he pleases, unlike the cover art, which is owned by the artist.

Several other questions were directed at the artist. He said parts of the coastline for the Mistborn map came from a tracing of a rust stain on the side of a building. Apparently natural phenomena like rust stains and puddles have fractal boundaries, whatever that means, just like real coastlines. I guess it makes sense. What is the ocean but a monstrous puddle? He also said he got the inspiration for the Allomantic symbols from a picture of a bent, rusted nail he found online. There's one symbol for each Allomantic metal, and they also do double duty as the people's alphabet. There are also three versions of each symbol. The versions used in the first book are the alphabet as it was in Alendi's log book from a thousand years previous. The versions used in the second book are the current symbols from Vin's day. The versions used in the third book are from way before even Alendi; they're the symbols in their rough, primal state. (I could be remembering that slightly wrong.)

I had a chance to talk to the artist for a few minutes afterward. He had said that although he and Sanderson never actually discussed the language, he'd imagined that the alphabet was like Tagalog, where each consonant is always paired with a default vowel. The vowel can be changed by adding an additional marking to the letter, like a dot. I thought that was interesting, so we talked about it for a few minutes before some other people came over wanting him to sign stuff.

Someone also asked if there was anything behind the character names, and I'm glad they asked. I had been a little bothered by the names, because they didn't seem coherent as a group. Some seemed French, some German, some just fantasy-like, and others were nicknames. Sanderson said he based most of the names on French, including some that I just wasn't educated enough to recognize. For example, the name Kelsier, which I thought was just a random fantasy-sounding name, would actually have been pronounced the French way, Kels-ee-ey (or something like that). Vin actually means "wine" in French, if I'm not mistaken (I haven't actually looked it up). There are a few German-based names, because Sanderson said there was a portion of the population with Germanic-based roots.

Sanderson also did a reading, which I was really looking forward to. Unfortunately, he chose to read a chapter that he had already posted on his Web site. I thought that was lame. I felt that most people who showed up were probably already big enough fans that they would have already read that chapter online, as I had. This is was a minor blemish in an otherwise fun evening.

If you've gotten this far and you haven't read any of the Mistborn books yet—what the heck? Go get one and read it. They're good.