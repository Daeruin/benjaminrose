---
from_wordpress: true
comments_id: 965
title: A Red Thing by Joe Zieja
date: 2012-07-06T12:38:53-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=965
permalink: /post/a-red-thing-by-joe-zieja/
categories:
  - fiction
---
Joe Zieja is a fellow blogger and member of my current writing group over at David Farland's Writing Groups. Joe has won an Honorable Mention in the Writers of the Future contest, which is no mean feat, and he has recently decided to take a step into the darkness that is self publishing. He's released his short story A Red Thing on [Smashwords](https://www.smashwords.com/books/view/179136). Here's a little about the book. If it sounds intriguing to you, then you should definitely check it out.

[![A Red Thing by Joe Zieja](https://dwtr67e3ikfml.cloudfront.net/bookCovers/5f5cc6e9ed0a6060dbd6f6e3cb3f297e93c795f5__300x0)](https://www.smashwords.com/books/view/179136)

> A red evil has fallen upon the world. The Maji Benkara, demons who crave sensation and pleasure not accessible to them in their ethereal forms, must possess humans to achieve a vicarious life. They rule with magic, with fear, and with cruelty—but they need a willing host to do it.
>
> "A Red Thing" follows one man's journey through the dissolution of his humanity as a Maji Benkara takes control of him and uses his body to wreak havoc. The lines become blurred between a conscience lost and a power gained, and he must fight every moment to retain some shred of the man he once was.
