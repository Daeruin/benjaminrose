---
from_wordpress: true
comments_id: 667
title: I Dig Gothic-Symphonic Metal
date: 2011-08-17T21:37:10-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=667
permalink: /post/i-dig-gothic-symphonic-metal/
categories:
  - music
---
These days I really enjoy me some gothic-symphonic metal. It's hugely popular in Europe, with the iconic bands being Nightwish from Finland and Within Temptation from The Netherlands. Lacuna Coil is sometimes called a gothic metal band, but I think that's just because their primary vocalist is female. For some reason, all gothic metal bands seem to have female singers. That's fine by me, but it's kind of interesting that the defining characteristic of an entire subgenre of metal is the gender of the lead vocalist.

I totally dig the combination of grand, soaring orchestral backgrounds, and grinding, punch-to-the-face metal guitar riffs, and powerful, beautiful female vocals. So I figured I'd share a few of my favorite tracks here for your enjoyment.

The song below is called Fata Morgana by the band Imperia, which hails from The Netherlands, Belgium, or Finland depending on who you ask or which band member you're talking about. This song is one of my current favorites. I'm listening to it multiple times a day on Spotify right now. I love the operatic vocals and drum work on this song. Unfortunately, I couldn't find a decent YouTube video of this song that actually had, you know, video. So you'll have to content yourself by gazing upon the ethereal beauty of vocalist Helena Michaelson with her creepy, dark bandmates in the background.

<iframe src="https://www.youtube.com/embed/ELKrBGhqJvY" width="480" height="390" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

This next piece is called Ghost Love Score by the band Nightwish, a band from Finland. This epic song sounds like a movie soundtrack. I chose this particular video because it shows the lyrics, which can be difficult to understand in part due to the vocalist's accent. The lyrics are well done and tell an interesting story.

<iframe width="560" height="315" src="https://www.youtube.com/embed/YODCM26JXOY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

The next one is Hand of Sorrow by Within Temptation from The Netherlands, chosen for two reasons. First, it demonstrates the incredible artistry and drama of their live shows, with a live orchestra, elaborate costumes, and amazing live performances. Second, the lyrics of this song were inspired by one of my favorite epic fantasy stories of all time, the Farseer Trilogy by Robin Hobb. Some of the shots in the video are a little odd, but overall it's a pretty well done video.

<iframe src="https://www.youtube.com/embed/TDhbewTR_vg" width="560" height="345" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Now we have Unleashed by the band Epica, also from The Netherlands (are you seeing a trend here?). I have no particular reason for choosing this song; I'm not familiar enough with Epica's work yet to have a favorite, but this song is pretty sweet.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rxd6sxLxdys" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Finally comes a band with a male singer: Kamelot, performing Ghost Opera. It follows in a very similar vein to the songs above and is a stellar example of the mixture of the symphonic and the metallic. I love this song!

<iframe src="https://www.youtube.com/embed/S9hGHnXPEi4" width="480" height="390" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
