---
layout: article
title: Migration from Wordpress to Jekyll, Part 1
author: Ben
date: 2020-04-27T05:48:51.605Z
permalink: /post/migration-from-wordpress-to-jekyll-part-1
categories:
  - metablogging
  - software
---
It took me two months, but the migration of my website from Wordpress to Jekyll is finally done. Things look different now, but virtually all of my content made it through unscathed.

I wanted to talk about what the process was like, problems I faced, and lessons I learned. There are [several](https://blog.webjeda.com/wordpress-to-jekyll-migration/) [other](https://www.davidebarranca.com/2019/03/migrating-from-wordpress-to-jekyll-on-netlify/) places on the Wide, Wide World o' Web that provide more of a step-by-step or tutorial. I don't want to recreate that. Instead, this will be a little bit more of a personal narrative, with many details about my decision-making process and challenges I overcame. Many of these details will fill in gaps that are glossed over by other explanations you will find. Hopefully my experience will make someone else's process a little easier.

This is part 1 of my migration process. This will cover backing up my Wordpress site, getting all the content out of it, installing Jekyll, and getting everything into Netlify.

## Backing everything up

Before starting, I backed up my Wordpress site. I had some kind of backup plugin that I used to do a backup of my Wordpress database. I can't remember what it was called, but there are many to choose from.

Then I used Wordpress's built-in functions to [export](https://wordpress.com/support/export/) all my posts. This turned out to be both a blessing and a curse later on. The blessing: I was able to use the same export file to get my blog comments onto my new Jekyll-based site. The curse: This method of importing comments was really meant for importing into another Wordpress site, which resulted in some weirdness and frustration.

## Downloading images

I first used [Media Cleaner](https://meowapps.com/media-cleaner-tutorial/) from Meow Apps to purge all my unused images. Be careful with this. I ended up accidentally deleting the banner image from my home page. I was fine with this, because my banner image was from an artist on Deviant Art, and I had not asked permission to use it. I had always felt guilty about that, so deleting it was fine. The downside was that my site went without a banner image for two months.

After deleting unused images, I downloaded the remaining images to my hard drive. If I remember right, I ended up doing this from my cPanel's file manager. I ended up with a bunch of nested folders for each year, month, and day. In a later step, I eventually decided to move all images to one folder, giving them all names that made sense, and using a grep search in BBEdit to fix all the links.

Dealing with images was a major pain and one of my least favorite parts of the whole process. I would recommend doing more research to see if you can find a way to get your images finalized without as much pain, or just reconcile yourself to the job ahead.

## Exporting content from Wordpress

![Jekyll Exporter Wordpress Plugin](https://ps.w.org/jekyll-exporter/assets/banner-772x250.png?rev=1043570)

The next order of business was to export my blog posts and pages. Before doing that, I deactivated virtually all my Wordpress plugins, hoping that would make the exported content as clean as possible. I think there was only one or two plugins I left active. 

I think one plugin I left active was a plugin for tables. I was hoping the tables would come through intact, but they didn't. I ended up having to recreate all my tables from scratch, which was not fun. Some tables I just gave up on and ended up presenting the contents in a more linear form.

I had a few drafts sitting around that I had never finished. I quickly finished and published one of them and deleted several others. I left one that I was unsure about finishing.

I then installed and used the [Jekyll Exporter](https://wordpress.org/plugins/jekyll-exporter/) plugin to export all my content into a Jekyll-based format. This gave me the following:

* A `_posts` folder with all my blog posts in Markdown format
* A `_drafts` folder with all my drafts in Markdown format
* All my pages in Markdown, placed in folders using the menu structure I had defined in Wordpress
* A `_config.yml` file with the URL and description of my site—I copied the description into the config file I got from my theme later on

You should be aware that the Jekyll Exporter plugin hasn't been updated for a while. Some people have been unable to get it to work at all. Others, like myself, got mixed results, as you'll see later on.

## Installing Jekyll

If you've never used Jekyll before, you'll have to install it first. I had installed Jekyll before at work, back when I was studying it for potential use for a documentation website as part of my job. So I was familiar with the process. Back when I installed Jekyll on my work laptop, it was a major pain and took me almost an entire day. I can't remember exactly what went wrong that time. But I came away knowing a few tricks.

To run Jekyll, you will need [Ruby](https://www.ruby-lang.org/en/) first. I use Mac computers, which come with Ruby already installed, although not always the latest version. I chose to install the latest version this time.

Ruby packages are called "gems." Most gems are libraries, but some are full-on applications. Jekyll itself is a gem. However, before doing anything else, I immediately installed `rbenv`, a Ruby gem that lets you manage and install different versions of Ruby. 

From there, I moved on to [installing](https://jekyllrb.com/docs/installation/) Jekyll. Thankfully, I didn't run into any problems this time. Installing Jekyll on my personal iMac was a breeze.

## Choosing a Jekyll theme

There are many ways to get a new Jekyll-based blog started. I think with Netlify, you can start one with just the click of a few buttons. I'll explain how I did it. It may not have been the best way to do it.

My first step was to choose a Jekyll theme. Jekyll is probably the most popular static site generator, so you can find themes all over the web. I used <https://jekyll-themes.com/>. 

Before choosing any tool, it helps to make a list of requirements up front, so you know what you're looking for and don't get sucked into features you don't really need. Some of the most important features to me were:

* responsive design
* built-in search
* built-in sidebars
* support for social media sharing
* support for third-party commenting systems
* support for Google Analytics

Some of those would be impossible for me to do on my own, and others would take me forever. So I wanted a very robust theme out of the box.

I chose the theme [TeXt](https://jekyll-themes.com/text/), because it looked very polished and came with all the features above.

![TeXt theme for Jekyll](https://raw.githubusercontent.com/kitian616/jekyll-TeXt-theme/master/screenshots/TeXt-home.jpg)

## Putting together the Jekyll site

There are different ways to install Jekyll themes. Some are easier than others. There's an explanation of the most common ways on the TeXt theme's [quick start page](https://tianqi.name/jekyll-TeXt-theme/docs/en/quick-start). Which method you use depends on your use case.

I'm using Bitbucket instead of GitHub, so forking the theme was not an option for me. (My primary reason for using Bitbucket is that back when I was starting to learn programming, GitHub did not offer free private repositories, but Bitbucket did. I wanted to keep my blog source in the same place as my other programming projects. I think today you can host private repositories on GitHub for free.) I already knew I wanted to make changes to the theme, so I chose to download it. This gives me the freedom to change the theme however I want, but makes it harder to incorporate future improvements the original author may make to the theme.

After downloading the theme, I unzipped it to the directory I was planning to use. Then I copied the Markdown files I had exported from Wordpress into the corresponding folders in the theme directory, replacing the placeholder content from the TeXt theme. My posts and pages were now in a Jekyll site directory.

I had already installed git long ago. If you haven't [installed](https://www.atlassian.com/git/tutorials/install-git) git yet, do that now. Since I already had git, I [initialized](https://www.atlassian.com/git/tutorials/setting-up-a-repository/git-init) git in the site directory on my computer, started a [new repository](https://confluence.atlassian.com/bitbucket/create-a-repository-800695642.html) in BitBucket, and [pushed the site up to the empty repo](https://confluence.atlassian.com/bitbucket/push-versioned-code-to-an-empty-repository-877177145.html). I now had source control on my site.

## Hosting my site on Netlify

![Netlify logo](https://www.netlify.com/img/press/logos/full-logo-light.png)

I already knew I wanted to use [Netlify](https://www.netlify.com/). I had researched them as part of a potential project at work. They offer free hosting for small websites. On their free plan, you only get one team member, limited bandwidth, limited build time, etc. But for a personal blog that doesn't get major traffic, it's perfectly sufficient. And Netlify is amazing for hosting static sites. There are other great hosting services you could use. Do the research and pick one. Or just go with Netlify.

Once I created my Netlify account, I told them I was using Jekyll to build the site and pointed Netlify at the Bitbucket repository, and they hooked everything up almost instantly. It was almost disturbing to me how easy it was. Within a few minutes, my site was live on Netlify, pulling from Bitbucket and building automatically.

Netlify gives your site a random identifier and uses that as a subdomain (randomIdentifier.netlify.com), so you can reach your site using that URL immediately. Most people will want a custom URL, and Netlify has everything you need to set that up.

## What's next

Theoretically, my site was now live and potentially visible, but I had a lot of work to do to get my site into shape before truly unleashing it upon the world. I left my old Wordpress site active while I continued to work on my new, static site.

The next post will discuss everything I did to clean up my old Wordpress content and modify my chosen Jekyll theme. That was the longest and hardest part of the process by far.

* [Migrating from Wordpress to Jekyll, Part 2](https://www.benjaminrose.com/post/migration-from-wordpress-to-jekyll-part-2-the-lost-draft)

Historical note: Due to a computer problem, described in [another blog post](https://www.benjaminrose.com/post/migration-from-wordpress-to-jekyll-part-x), I thought I lost part 2 and gave up on it for a time. I eventually found the draft of part 2 and published it.