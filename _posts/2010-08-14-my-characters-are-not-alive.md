---
from_wordpress: true
comments_id: 218
title: My Characters Are Not Alive
date: 2010-08-14T09:53:08-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=218
permalink: /post/my-characters-are-not-alive/
categories:
  - writing and publishing
---
I often hear authors talk about how their characters talk to them, or their characters surprise them, or their characters tell them who they are and how they want to be. I don't get it. In fact, I've come to loathe it when I hear that kind of talk. I guess it's kind of like listening to someone talk about how much they love eating seafood when I think it's disgusting. I can't imagine why anyone would engage in that kind of behavior by choice.

I'm not trying to say the "voices in your head" method is bad, or doesn't produce good stories, because I know it works for an awful lot of people. It just doesn't make sense to me. It sounds like superstitious mumbo-jumbo. So when I see a blog post or listen to an interview, and an author starts talking like that, I just move on. Deleted. Stop/Eject. Alt-F4.

I know of a few authors who aren't like that. Their characters do not surprise them, because their characters are carefully designed exactly how they need to be for the story. If they have an idea that seems surprising with regard to a character, that idea is carefully considered to see whether it fits the story. If not, it's discarded. The only two authors I know who work like this are very good: Connie Willis and Brandon Sanderson.

Maybe it's just a choice of language. Maybe when people say their characters talk to them, it's just a way of describing them using their imagination or having a daydream. Or maybe authors just like to romanticize the idea of the characters being something separate from them, of the story having a life of its own.

Or maybe it's a kind of [altered level of consciousness](http://drawright.com/theory.htm), where your creative side takes more control. I know for a fact that I'm a left-brainer, an analytical thinker. All of those little tests say so, and all of my family members would definitely agree. Maybe that's why I don't understand.

Do your characters talk to you or surprise you? Have you ever really, truly felt like your characters were alive, separate? Am I really missing out? How does it work for you?