---
from_wordpress: true
comments_id: 999
title: People of the Bookshelf
date: 2012-10-11T18:20:22-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=999
permalink: /post/people-of-the-bookshelf/
categories:
  - fiction
  - 'life &amp; stuff'
---
**How do you shelve your books?**

Today I ran across an amusing article that provides a funny point of view on the question. It's called [People of the Bookshelf](http://www.theglobalmail.org/feature/people-of-the-bookshelf/413/ "People of the Bookshelf"), and it opens with a couple who are having an argument about how to organize their books. It's worth the read—go ahead and take a minute to read it!

![Picture of a bookshelf by Stewart Butterfield](https://upload.wikimedia.org/wikipedia/commons/2/2b/Bookshelf.jpg)

I admit, I'm tickled by the idea of shelving books based on how the authors would get along in real life. I couldn't do it, personally, since I'm often insensitive to such issues and could never keep it straight anyway. But it's interesting, and I can relate to the desire. After all, I'm the one who shelves his books based on how well he liked them. My least favorite books hang out on the bottom shelf gathering dust.

My wife doesn't object to this scheme but she does object to my secondary level of organization (yes, I have another level), which is to group them by size and shape. Given two authors that I like roughly the same who reside on the same shelf, I'll put their similarly sized books together, so the spines and tops are as even as possible or at least ascend or descend in pleasing lines. To my lovely wife, that screams wrongness. To her, the natural state of a bookshelf is uneven. They could be organized by author or subject or whatever, as long as they don't look so evenly _unnatural_ like my shelves.

We agree to disagree on the matter. I keep my books on one shelf, and she keeps hers on another. It works for us.

How do you organize your books?
