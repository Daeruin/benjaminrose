---
from_wordpress: true
comments_id: 29
title: Evanescence-esque
date: 2007-07-29T14:41:36-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=29
permalink: /post/evanescence-esque/
categories:
  - music
---
When I first heard Wake Me Up Inside, the hit single by Evanescence, I loved it. They had me completely hooked. Their sound was something I'd never quite heard before, a mix of heavy, pounding guitars, piano, and symphonic strings fronted by Amy Lee's pure, soaring vocals, and it spoke to me. After they lost their founding guitar player, I felt a little apprehensive about the recent release of their second album, but I found it just as good as the first. I'm a fan.

However, I'm not a music critic. For one thing, I don't have the vocabulary for it—I'm an amateur vocalist and musician, but I haven't taken any kind of lessons or training since high school. Also, my musical tastes are very narrow. I just don't appreciate the wide spectrum of music out there. I'm quite picky about what I like, and the rest just doesn't interest me.

So I was surprised recently to discover that there are several other female-fronted goth/symphonic metal bands out there, and some of them have been around longer than Evanescence, which formed in 1998. Here are the bands I've found, in no particular order:

[**Lacuna Coil**](http://www.lacunacoil.it/) (1996). An Italian band with a big following and a unique sound. Not as heavy as I like it, but definitely some high-quality music. Their female-male co-vocalist mix is really cool. Of all the bands I'm listing here, Cristina's voice comes closest to matching Amy Lee's in excellence, though it has a very different quality to it. I've found several of their songs stuck in my head for days since I heard them, a good sign. I'm definitely planning on buying their latest album.

[**Allyptic**](http://www.allyptic.com/) (2004). I just discovered this band last week from a link on Within Temptation's MySpace page. I was surprised to find out that they are based here in my home state of Utah. Their bassist takes lessons from the same guy my brother used to take lessons from. Sadly, Allyptic isn't nearly as good as any of these other bands, despite their frequently-used quote from some DJ saying they are better than Evanescence. The instrumental parts are good, but poor Milica's voice doesn't hold a candle up to Amy Lee. Despite her claim to opera training in the bel canto technique, I found Milica's voice to have a somewhat shrieky tone to it. Still, I like them well enough, and I hope they continue to improve. With some higher-quality production behind them, I think they could make a great record.

[**Nightwish**](http://www.nightwish.com/) (1996). I don't really know much about this band at all, except that they hail from Finland. I'm already inclined to like them just for that fact—I've always been intrigued by the Finnish language, partly because Tolkien based his Elvish language on it; but Finnish also has an inherent rhythmic, musical quality due to its interesting stress pattern. Sadly, all of Nightwish's lyrics are in English. I've only heard a few short clips of their music, but I liked what I heard. I do know that they recently fired their long time vocalist and hired a new lady to front the band. I really wish I could find a few full-length songs to listen to, since all they have on both their official Web page and their MySpace page are short one-minute segments.

[**Within Temptation**](http://www.within-temptation.com/) (1996). This is another band that I have a soft spot for, based as they are in the Netherlands, where I lived for two years. Although I think their name is kind of strange (what is "within temptation" supposed to mean, anyway?), their music is very good. They seem to have a penchant for costumes and big, dramatic shows, which makes me want to see them in concert sometime. I'm a sucker for dramatic music, as long as it doesn't get too melodramatic or cheesy like too many progressive metal bands these days. Within Temptation seems to be right along my line.

So there they are. Look them up and let me know what you think. Are you with me on the coolness of these femme-fatale-fronted metal bands?
