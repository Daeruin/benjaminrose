---
from_wordpress: true
comments_id: 749
title: Off the NaNo Rails
date: 2011-11-17T11:09:45-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=749
permalink: /post/off-the-nano-rails/
categories:
  - writing and publishing
---
In my last post, I whined in a rather pathetic manner about all the things that had been keeping me from meeting my NaNoWriMo word count goals. Well, I've definitely gone off the rails by now. I'd need to write almost double the original daily word counts every day for the rest of the month in order to catch up. And that just isn't going to happen.

What seemed like an exciting and achievable goal on November first has turned into something that's a bit embarrassing and disappointing.

So I'm trying to see the positive. NaNoWriMo got me to pull together a story that, while I wasn't completely comfortable with, at least was in enough shape for me to begin writing. And I started writing. And I now have five chapters of a novel I didn't have before.

And best of all, I am going to keep writing. I'm going to get as many words done as I can every night until November is over—and beyond.

That's all for now. I've got to get back to work.