---
from_wordpress: true
comments_id: 1135
title: Wheel of Time Cover Art for the US Hardback Editions
date: 2013-02-28T21:46:06-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1135
permalink: /post/wheel-of-time-cover-art-for-the-us-hardback-editions/
categories:
  - art
  - fiction
---
Some time ago, I posted the [cover art for the ebook editions of The Wheel of Time series](/post/the-wheel-of-time-e-book-covers-image-heavy-post/ "The Wheel of Time E-Book Covers (image-heavy post)") by Robert Jordan. For the sake of comparison, here is the art for the US hardback editions. All of these covers were done by [Darrell K. Sweet](http://www.sweetartwork.com/) except for the last book, _A Memory of Light_, which was done by [Michael Whelan](http://www.michaelwhelan.com/ "Michael Whelan").

Click each image to see a larger version.

&nbsp;

**Prequel — _New Spring_ cover art**

[![00 New Spring by Darrell K Sweet](/assets/images/00-New-Spring.jpg){: width="500px" height="368px"}](/assets/images/00-New-Spring.jpg)

&nbsp;

**1. _The Eye of the World_ outside cover art**

[![02 Eye of the World outside by Darrell K Sweet](/assets/images/01-The-Eye-of-the-World-outside.jpg){: width="500px" height="367px"}](/assets/images/01-The-Eye-of-the-World-outside.jpg)

&nbsp;

**_The Eye of the World_ inside front cover art**

[![01 The Eye of the World inside](/assets/images/01-The-Eye-of-the-World-inside.jpg){: width="500px" height="357px"}](/assets/images/01-The-Eye-of-the-World-inside.jpg)

&nbsp;

**2. _The Great Hunt_ cover art**

[![02 The Great Hung by Darrell K Sweet](/assets/images/02-The-Great-Hunt.jpg){: width="500px" height="369px"}](/assets/images/02-The-Great-Hunt.jpg)

&nbsp;

**3. _The Dragon Reborn_ cover art**

[![03 The Dragon Reborn by Darrell K Sweet](/assets/images/03-The-Dragon-Reborn.jpg){: width="500px" height="375px"}](/assets/images/03-The-Dragon-Reborn.jpg)

&nbsp;

**4. _The Shadow Rising_ cover art**

[![04 The Shadow Rising by Darrell K Sweet](/assets/images/04-The-Shadow-Rising.jpg){: width="500px" height="375px"}](/assets/images/04-The-Shadow-Rising.jpg)

&nbsp;

**5. _The Fires of Heaven_ cover art**

[![05 The Fires of Heaven](/assets/images/05-The-Fires-of-Heaven.jpg){: width="500px" height="379px"}](/assets/images/05-The-Fires-of-Heaven.jpg)

&nbsp;

**6. _Lord of Chaos_ cover art**

[![06 Lord of Chaos by Darrell K Sweet](/assets/images/06-Lord-of-Chaos-wrap.jpg){: width="500px" height="375px"}](/assets/images/06-Lord-of-Chaos-wrap.jpg)

&nbsp;

**7. _A Crown of Swords_ cover art**

[![07 A Crown of Swords](/assets/images/07-A-Crown-of-Swords.jpg){: width="500px" height="373px"}](/assets/images/07-A-Crown-of-Swords.jpg)

&nbsp;

**8. _Path of Daggers_ cover art**

[![08 Path of Daggers by Darrell K Sweet](/assets/images/08-Path-of-Daggers.jpg){: width="500px" height="375px"}](/assets/images/08-Path-of-Daggers.jpg)

&nbsp;

**9. _Winter's Heart_ cover art**

[![09 Winter's Heart by Darrell K Sweet](/assets/images/09-Winters-Heart.jpg){: width="500px" height="365px"}](/assets/images/09-Winters-Heart.jpg)

&nbsp;

**10. _Crossroads of Twilight_ cover art**

[![10 Crossroads of Twilight by Darrell K Sweet](/assets/images/10-Crossroads-of-Twilight.jpg){: width="500px" height="373px"}](/assets/images/10-Crossroads-of-Twilight.jpg)

&nbsp;

**11. _Knife of Dreams_ cover art**

[![11 Knife of Dreams cover by Darrell K Sweet](/assets/images/11-Knife-of-Dreams.jpg){: width="500px" height="374px"}](/assets/images/11-Knife-of-Dreams.jpg)

&nbsp;

**12. _The Gathering Storm_ cover art**

[![12 The Gathering Storm cover by Darrell K Sweet](/assets/images/12-The-Gathering-Storm.jpg){: width="500px" height="372px"}](/assets/images/12-The-Gathering-Storm.jpg)

&nbsp;

**13. _The Towers of Midnight_ cover art**

[![13 The Towers of Midnight cover by Darrell K Sweet](/assets/images/13-The-Towers-of-Midnight.jpg){: width="500px" height="372px"}](/assets/images/13-The-Towers-of-Midnight.jpg)

&nbsp;

**14. _A Memory of Light_ — Darrell K. Sweet unfinished concept art**

Mr. Sweet passed away before finishing the cover.

[![14 A Memory of Light cover by Darrell K Sweet](/assets/images/14-A-Memory-of-Light-Sweet.jpg){: width="500px" height="333px"}](/assets/images/14-A-Memory-of-Light-Sweet.jpg)

&nbsp;

**_A Memory of Light_ cover art — Michael Whelan**

[![14 A Memory of Light cover by Michael Whelan](/assets/images/14-A-Memory-of-Light.jpg){: width="500px" height="331px"}](/assets/images/14-A-Memory-of-Light.jpg)
