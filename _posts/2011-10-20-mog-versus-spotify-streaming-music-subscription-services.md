---
from_wordpress: true
comments_id: 716
title: "MOG versus Spotify: Review of Streaming Music Services"
date: 2011-10-20T10:39:42-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=716
permalink: /post/mog-versus-spotify-streaming-music-subscription-services/
categories:
  - music
  - software
---
![Mog versus Spotify](/assets/images/mogVsSpotify.jpg){: width="172px" height="73px"}

A while back I had my first serious run in with streaming music services. I had heard about them before, but never seriously considered them. After all, I have a pretty large music library full of music that I love, and I like owning my music. The problem is that for a number of years I haven't had the extra cash to buy all the music I wanted. At the same time, I've had the opportunity to listen to more music than ever before, thanks to working from home and never needing to spend significant amounts of time on the phone.

So when Spotify, a streaming music service that has been very popular in Europe, made a rather large splash on this side of the pond, I was intrigued enough to give it a try. After all, it was free to try. What was there to lose?

It turned out that I loved it! The free version had ads that were repellent to me, but it was awesome to listen to all my favorite music, plus newer music I have been unable to buy, while discovering many new artists at the same time. I quickly paid the measly $5 monthly fee for the ad-free version.

Then I started to hear about other music services, like MOG, Rhapsody, and Grooveshark. I tried a few of them out, and quickly discovered that MOG and Spotify were the top two for me. Honestly, I didn't give Rhapsody much of a chance. Grooveshark is cool because it has the biggest music library of them all—but it's all user uploaded, so quality isn't always the best, plus it's a web app only. And I just can't reconcile myself to that. I demand a dedicated desktop application for my music.

So here follows a fairly detailed review of MOG and Spotify, divided into categories, with a winner for each category.

## ![Spotify Icon](/assets/images/spotifyIcon.png){: width="25px" height="25px"} User Interface: Spotify

I am a sucker for software that is both functional AND beautiful, so this category is a big one for me. When I first tried MOG, I found their web app so awful that I could only stand to use it for about five minutes before giving up. Luckily, they released a completely redesigned web app along with a brand new desktop application soon after I signed up. Otherwise they would have been lost to me forever. As it is, with their new application, MOG has a slight edge in the category of beauty. Spotify's user interface is nice and clean but nothing spectacular; it's very reminiscent of iTunes and has very few fancy graphical elements. On the other hand, it's clear that MOG took a little extra time to make their app a little more attractive, with an interesting layout and good use of icons, tabs, and other graphical elements (even though they copied Spotify's gray color scheme). But in the end, the difference isn't enough to really sway me one way or the other.

Before getting much further, I should mention that I seem to be a minority on this. A number of reviews I read online vastly preferred MOG's user interface. As with everything, it's a matter of opinion. For me, in the category of functionality, Spotify wins hands down. Their interface is perfectly intuitive to me and easy to use, and there really isn't much more to say. On the other hand, I have a number of complaints about MOG.

On MOG's desktop application, you can't resize window or side bar, so I can't see all of the artist information when viewing an album in the main window, and my playlist titles are truncated at about 15 characters. Even though the application stretches the whole height of my screen, there is always a vertical scroll bar showing (which scrolls down to reveal . . . why, nothing!), and the playlist area is compressed into a tiny box about one inch high, leaving about eight inches of blank, unused space below. I can only see five of my playlists at a time and have to scroll to see the rest. When viewing an artist, the view is limited to their top three albums unless I click on the link to show all albums, and then they are displayed in no apparent order (Spotify automatically displays all albums in chronological order). The back, forward, and home buttons are hidden down in the bottom left corner, instead of being in the top left corner where you would normally expect them to be.

To be fair, Spotify has had several years to refine their desktop application over in Europe, while this is only MOG's first iteration. Maybe it will improve.

Another feature is the music queue. Both applications let you create playlists. I found the process a little easier on Spotify. When you select a song to play on Spotify, it starts playing immediately straight from where you are, allowing you to continue browsing or just leave the page as is. In the background, it adds the song to the play queue, along with all subsequent songs on whatever playlist, album, or list of results you're currently looking at. If you select a new song to play from a different playlist, album, or search list, it starts playing THAT song, and adds the subsequent songs to the queue, while removing whatever was in the queue before.

Essentially, Spotify's music queue is based on playlists. It fills up with music from whatever list you're on, and forgets what was there before. This method worked for me with no trouble. In fact, I never even bothered to look at the actual music queue, since it was obvious that Spotify was simply starting where I told it to, and continuing along with whatever was next on my screen. I've read reviews by [people who were perplexed by this behavior](http://www.computeraudiophile.com/content/MOG-v-Spotify-Part-II), but it seems obvious to me. I wonder if the difference is that those people had tried MOG first and gotten used to it.

On MOG, when you select a song to play, it automatically switches your view to the music queue, so you can see the song being added to the bottom of the list. You can also send entire albums or playlists to your queue, and when you do you're again taken to the queue screen where it shows the selected songs being added to the bottom of the list. In this way, you can create a giant music queue that will play until it gets to the bottom.

When it does get to the bottom, MOG goes automatically into radio mode. The default radio station is set to play only songs by the current artist, but there's a simple slider where you can gradually introduce similar music, until at the far end you're listening only to similar artists with no music by the original artist that you started with. This feature is actually pretty cool for those who like getting random music. Personally, I don't like it. I have never liked the radio, and the only reason I have ever listened to it was when there was no other choice. My preference is for my music player to play exactly what I tell it to and no more. Fortunately, MOG has an option to turn the radio feature off, so that once you reach the end of your music queue, it goes silent. This is how I keep it, but I'm still not totally comfortable with MOG's music queue. To me, Spotify's simple playlist mode is more natural.

## ![Spotify Icon](/assets/images/spotifyIcon.png){: width="25px" height="25px"} Music Selection: Spotify

MOG and Spotify both boast music libraries in the millions. The big problem with assigning a "winner" here is that it all depends on what music you need to find. I saw one review that assigned a winner to one application simply because the other didn't have the Beatles. Well, I couldn't care less whether the Beatles is available. All I care about is whether the music **I** like is available. It turns out that MOG seems to have more music that I like. Everything that I had in my Spotify playlists, I was able to find on MOG, plus a few extra albums from some of the more obscure symphonic metal bands I like. But both music services lacked at least something—for example, neither has any music by Tool (which is the fault of Tool, not MOG or Spotify). The point is that music selection will vary for everybody depending on what they're looking for. There are no easy generalizations here.

However, one advantage of Spotify is that it can play any MP3 files or other music that you already have on your computer, so you can access that music straight from your hard drive instead of streaming it. So it doesn't matter that I can't find any Tool songs in Spotify's database, because I can listen to the ripped MP3s I already have on my computer. Plus those songs don't have to be streamed, which saves my internet connection a little extra bandwidth. MOG reports that they are working on this functionality, but it isn't there yet.

Another thing worth mentioning is the quality of the search function—it's one thing to have music in the library, but it's another thing entirely if you can't find said music when you want it. In my experience, the search on Spotify gives me what I want right at the top of the search results almost every time, and their search is almost instantaneous. With MOG, the search sometimes takes a few seconds (which can seem like an eternity sometimes), and I occasionally have to sort through long lists of other music before finding what I was looking for, or else refine my search a few times.

## ![MOG Icon](/assets/images/mogIcon.png){: width="25px" height="25px"} Music Quality: MOG

MOG's song stream by default at 320 Kbps and may drop to 160 Kbps for slower connections, whereas Spotify streams at 160 Kpbs by default unless you're subscribed to the costlier Premium service. Obviously, MOG has better sound quality. I'm not a huge audiophile, so I really don't notice a big difference between the two unless I listen to them right next to each other. If I play a quick clip from MOG, then the same clip from Spotify immediately after, I can tell the difference. It's not really huge, but it's there.

About 95% of the time, I would be fine with Spotify's quality. But there are a few cases where it does matter. I have a few albums that contain a lot of atmospheric sounds, and when those are lost the album just isn't worth it anymore (example: A Pleasant Shade of Grey by Fates Warning). So the win clearly goes to MOG in this case, but in reality I would be fine with Spotify's quality most of the time.

## ![Spotify Icon](/assets/images/spotifyIcon.png){: width="25px" height="25px"} Stealing My Bandwidth: Spotify

It's always nice when an application [steals a portion of your bandwidth](http://www.geekwire.com/2011/practical-nerd-hidden-price-free) for clandestine activities without notifying you. Luckily, Spotify does exactly that. Spotify includes a lovely, hidden peer-to-peer transfer ability that lets you and others share music without even knowing it. Apparently Spotify does this in order to speed up music transfer times, so your music gets delivered to you without interruption. I'm just glad they didn't let me know that, so I could learn it second hand from an internet article.

OK, I've been a little tongue-in-cheek here. But I have experienced a noticeable slowing in my internet speeds ever since I installed Spotify on my computer. When my wife and I both have Spotify running on each of our computers, web pages load noticeably slower than they would otherwise. When Spotify isn't running, there's no problem. Just something to be aware of.

I haven't noticed any slowing while using MOG so far, but that doesn't necessarily mean it isn't happening.

## Version Comparison: Not Enough Data

MOG and Spotify both have different versions and levels of subscription—a free version, a standard version, and a premium version. Although the features are slightly different between the two services, both services charge the same fees. The free version is free and supported with ads, the standard version costs $5 per month, and the premium version costs $10 per month.

I have used the free versions of both, but not for very long. I was unable to tolerate the ads and was more than happy to pay $5 a month to have unlimited music streaming with no ads. So I can't really give you a decent comparison of the free versions.

I also haven't bothered to try the premium versions of either service. The primary features are the ability to stream songs to your mobile device and to download music for offline playing. I don't have any use for these features, since I don't even own a cell phone, let alone a smart phone, and I don't have any need to play music in an offline setting.

## The Verdict

![Spotify Icon](/assets/images/spotifyIcon.png){: width="60px" height="60px"}

After using both music services for lengthy periods of time, I find that I am much more comfortable using Spotify. Something about it makes me feel at home. It's easy to use, and from the get-go I never felt confused or frustrated. But with MOG, it took me a while to figure out, and I run across some annoyance or another on a daily basis. That might say more about me than it does about either service, but there you go. Despite MOG's higher song quality and Spotify's bandwidth thievery, I'm happier with Spotify overall.

### What about you? Have you ever tried a music streaming service? What's your music application of choice, and why?
