---
from_wordpress: true
comments_id: 162
title: Do You Need a Magic System
date: 2010-03-02T22:34:12-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=162
permalink: /post/do-you-need-a-magic-system/
categories:
  - my fiction
---
This topic was prompted by Ing's reply to my previous post. It's something that I've given a fair bit of thought over the past few weeks. Do I really need a magic system?

First, look at how that question is phrased. I'm not asking if you need magic itself. I'm asking if you need a system. If you're wondering about magic at all, then you're probably writing, or thinking about writing, fantasy. And yes, I do think you need some magic if you're writing fantasy.

So you need magic, but do you need a system?

I don't know about you, but I have heard a lot of talk, from people trying to give advice about writing and getting published, to the effect that you need a magic system. And here's my honest answer to the question. You don't _need_ a system. You may _want_ one, but it all depends on what you're trying to do.

I'm convinced that part of the reason some people, including me, tend to feel the need for a system is because of roleplaying games. I spent an inordinate amount of time playing RPGs as a kid. I probably spent a quarter of my entire teenage years thinking about or playing RPGs. The idea of magic as a system of rules with specific spells and effects is practically ingrained into my DNA. Of course, roleplaying games do need a system, because otherwise they wouldn't be games. Games need rules.

But novels are not games. Novels are stories, and everything in them must serve the needs of the story.

I have heard nothing that really illustrates how magic can serve the needs of the story better than [Sanderson's First Law of Magic](http://www.brandonsanderson.com/article/40/Sandersons-First-Law), which states:

> An author's ability to solve conflict with magic is DIRECTLY PROPORTIONAL to how well the reader understands said magic.

In other words, you should not use magic to solve a problem in your story unless the reader understands how that magic works. If you fail to explain how the magic works, and you use it to solve a problem, your reader will think you're cheating. You've committed the crime of Deus Ex Machina.

Imagine if, in The Hobbit, Tolkien had not introduced us to the Great Eagles until the end of the book. You're reading along, the Battle of Five Armies is raging, you have no idea that Great Eagles exist in Middle Earth, and all of a sudden they swoop in to save the day. You would have thrown the book against the wall in disgust.

If your story needs to solve problems with magic, you need some kind of system. It doesn't have to be complicated or even important to the overall story. But it does need to have rules, so the reader understands how the problem is being solved.

Let's say you don't need to solve many problems with magic. In your story, magic is more like a feature of the setting. It's there solely to add flavor, wonder, or perhaps humor. Then screw the rules.

My favorite example of this kind of magic is from George R. R. Martin's _A Song of Ice and Fire_ series. There is practically no magic in the first book. There are some freaky, undead creatures, and a few dragons that are hatched by throwing their eggs into a raging fire. Nothing really overt. No problems are solved by these things. He throws a little more magic into each successive book, but there do not appear to be many rules involved. Each act of magic seems relatively unrelated to the others. In fact, rather than solve problems, the magic tends to _cause_ problems for the characters. It's there to add a sense of mystery, horror even, and to do bad things to the characters. It's more setting material than anything. In effect, Martin is writing historical fiction with a few fantastic elements thrown in for flavor.

There's also a middle ground, where certain magical things have rules, because you need them to solve problems, but other things don't. Martin is starting to cross into this territory with a few of his characters as they become more familiar with their magical talents and start using them to do stuff. A lot of modern and paranormal fantasy falls into the middle ground. There's a whole spectrum of possibilities. Notice that Sanderson's law refers to proportions, not absolutes.

So let's say you're not into the whole magic system thing. Maybe you need to be writing something that aligns more closely to historical fiction, with just a few magical elements thrown in. A foreign, fantastical setting. Some strange creatures that may or may not be overtly magical. Monsters. Maybe the focus of your book is more on politics or war.

Personally, I tend to fall more on the system end of the spectrum. It feels natural to me, and it fits with my analytical, detail-oriented mind. On the other hand, having read Martin and loved what he did with magic in those books, I can definitely see myself writing something like that. I love the idea of magic as something rare, mysterious, and horrifying.

The middle ground is not for me. That kind of magic usually comes off feeling fake and arbitrary to me, without any of the advantages you get from treating magic as either mostly system, or mostly setting.

Anyway, take it for what it's worth, and see how it works for you.