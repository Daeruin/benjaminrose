---
from_wordpress: true
comments_id: 2888
title: Why English has Both C and K
date: 2020-01-17T20:26:45-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/post/why-english-has-both-c-and-k/
permalink: /post/why-english-has-both-c-and-k/
categories:
  - uncategorized
---
Why does English have both C and K that make the same sound? My 11 and 12 year old daughters asked me this question today. I have researched it before, but couldn't remember. So here's a little explanation. Maybe it will stick in my head this time.

Back in the day, the Greeks adapted the Phoenician alphabet for themselves, and it ended up being used by lots of different people who spoke different languages, including the Etruscans. The Greek alphabet has letters for both the G and the K sound—gamma and kappa. But the Etruscans didn't have a G sound in their language. To them, G sounded like K. For whatever reason, they chose gamma to represent their K sound and ignored kappa. Back then, gamma had a curved shape like a C.

The Romans inherited this tradition, but unlike the Etruscans, they did have a G sound. Since they were already using gamma for the K sound, they invented a new letter by adding a little line to the letter C. This is why G looks like a slightly modified C. Meanwhile, poor old kappa was basically ignored for centuries.

Old English inherited the grand old tradition of using gamma, shaped like a C, for the K sound and ignoring kappa. The letter K was reintroduced much later when we started borrowing more Greek words. In those cases, we used the original kappa to represent the K sound.

So why does C sometimes sound like S? We can blame that on the French. Over time, they started pronouncing the K sound more like a CH and eventually like an S. With the Norman conquest, English absorbed many French words. In those words, C is pronounced like an S.

In part due to the confusion of C having two sounds, many older words that used to be written with C, like "cyng," were converted to the K, so we now write "king." But we still have lots of holdovers that still use C for the K sound.

This is what we get for using another language's alphabet, which had already been mutilated for a different language, and then borrowing thousands of words from several other languages.