---
from_wordpress: true
comments_id: 982
title: War of the Arrows
date: 2012-07-27T09:20:33-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=982
permalink: /post/war-of-the-arrows/
categories:
  - 'movies &amp; TV'
---
Set during the second Manchu invasion of Korea, _War of the Arrows_ follows Nam Yi, a gifted archer whose sister is kidnapped by Manchurian raiders on the day of her wedding. I loved this movie. The cinematography is beautiful and the action is awesome. Despite some of the hyperbole that's typical of Asian action films, it had a gritty, realistic feel that was reminiscent of The Last of the Mohicans (a fact that is mentioned in the movie's trailer and was one of the main drawing points for me, since Last of the Mohicans is one of my favorite movies of all time). The archery chase scenes were intense and brilliantly done. If you can handle the blood and violence, I highly recommend this movie.

<iframe width="560" height="315" src="https://www.youtube.com/embed/XDyIjb272kU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
