---
from_wordpress: true
comments_id: 1310
title: "Primalcraft: Making Knapping Interesting"
date: 2017-08-28T22:16:34-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1310
permalink: /post/primalcraft-making-knapping-interesting/
categories:
  - Minecraft modding
---
As described in my last post, I'm working on a knapping interface for Primalcraft. The basic idea will be similar to Terrafirmacraft, where you are shown a "rock" made up of a bunch of little squares, and you click a square at the edge of the rock to remove it. When you've trimmed it down to the right shape, you get your tool head.

I got my hands on an early alpha of Terrafirmacraft 2, and was not pleased with the new knapping interface. It's larger, at 9x9 instead of the old 6x6, which is nice because it lets you make more realistic shapes. There is also a new recipe overlay that highlights the shape of the tool on the grid so you know exactly what to do to make the tool head you want. It certainly makes it easier, but it also makes it really boring. You end up just clicking a bunch of squares really fast without thinking.

One of my goals is to make knapping more interesting than just clicking some squares. I want it to involve a little bit of strategy.

That's why every time you want to create a stone tool head in Primalcraft, the rock you start with will be a random size and shape. Here are some examples:

**Rock 1**

![](/assets/images/primalcraft_rock1.png){: width="247px" height="243px"}

**Rock 2**

![](/assets/images/primalcraft_rock2.png){: width="300px" height="297px"}

**Rock 3**

![](/assets/images/primalcraft_rock3.png){: width="400px" height="399px"}

So clearly, they are different sizes. The size of the tool should matter. A big axe should cut down a tree faster and do more damage when you're trying to bring down that cow you've been chasing.

They're also different shapes. I have a semi-random algorithm that removes pieces from the corners so each rock is a (mostly) unique shape. I haven't made up the exact recipes that will define what shape you need to end up with to get the various tool heads. But I'm planning to have a few variations in both size and orientation (horizontal, vertical, and diagonal). That way, if you get a big rock you could potentially make any type of tool, depending on how small you're willing to go. But if you strategize just right for the unique rock shape you have, you just might be able to get a bigger tool, even if it's not the one you were originally looking for. The smaller rocks will be harder to adapt to.

With those ideas in mind, what could you make out of the rocks above?

**Rock 1. **The first one looks pretty awkward for any tool, and it's small to boot. It seems like it could make a small curved knife, or maybe a small axe if you do it diagonally. But it'll probably be a loss.

**Rock 2. **The second one is almost a perfect spear head, if it weren't for the missing chip on the bottom right. But it will still work. It just needs a little trimmed off each size to make it nice and symmetrical, with a sharp point and flat or concave butt where it can attach to a haft. It's large enough and symmetrical enough that it could be made into an axe or a knife, as well.

**Rock 3. **The third one is large enough that you could trim it down into just about anything. If you wanted to go for something big, you could get a good axe (I'm picturing the cutting edge on the right, tapering down to the left where it would attach to the handle) or spear head.

So that's where I am with knapping. I have the random rocks showing up, but I don't have the recipes created. The past few days I've been trying to make it so you can remove big chunks of rock by clicking nearer to the center, but so far my attempts haven't been successful. It seems like it ought to be doable, but it's hurting my brain. So I might need to move on to something else and let that idea sit on the back burner for a while.

What ideas do you have to make knapping more interesting?
