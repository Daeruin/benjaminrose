---
from_wordpress: true
comments_id: 1176
title: Misconceptions about Plate Armor
date: 2020-02-21T18:23:01-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1176
permalink: /post/misconceptions-about-plate-armor/
categories:
  - medieval combat
---
If you want to know about medieval plate armor, you need to watch this video from a curator of the Department of Arms and Armor at The Met.

<iframe src="https://www.youtube.com/embed/NqC_squo6X4?rel=0" width="480" height="360" frameborder="0"></iframe>

I started this post several years ago and never finished it. It languished in my drafts folder until today. I figured I'd quickly post my rough notes from the above video, for those who are disinclined to watch the whole thing (it's 45+ minutes long).

Armor was not very shiny until the 1600s when knights were no longer needed. Etched prints onto breastplates in 15th century. In the age of chivalry (1400s) it was mostly mail (not chain mail). The best mail was very flexible but so tight you couldn’t poke a pin through it.

You can't rely on most television shows. The History Channel’s so-called experts consisted of a re-enactor and a historian who has written two papers, one of them on Napoleonic warfare, not medieval warfare. The Deadliest Warrior show is completely useless when it comes to historical accuracy. The knight’s armor in Deadliest Warrior is a mix of 250 years of armor and the helmet is put on incorrectly (face piece goes over cheek pieces, not under).

What about books? _The Medieval World at War_ was written by historians, but it only has two images of armor. One image has reproduction armor while the other has mixed periods. _Knights in History and Legend_ has the same bad images, but better descriptions.

Many modern misconceptions are due to Mark Twain and a few older plays, but especially Lawrence Olivier’s _Henry V_ in 1944.

Medieval plate armor is 50–60 pounds, about as much as a 5 or 6 year old child. The Marshal of France in the 14th century could do all kinds of athletic feats while wearing full plate armor: somersaults, cartwheels, jumping into the saddle, jumping over a horse, and climbing the underside of a ladder. Professional soldiers’ lives depending on how well they could fight with armor.

Knights could easily get up and down from a horse. We have images from the 1200s through the 1500s showing knights with one foot in the stirrup and one hand on the saddle pommel. One piece of folklore claims that they used to construct cranes to lift knights onto horses. Why would they do that when the solution is as simple as a footstool? The video above showed a clip of a historian in historical armor mounting a horse easily.

Another historian had armor tailor made based on real 15th century armor, they showed a clip of him running full speed quite easily. Another video of historians in armor fighting with poleaxes.

Another video of armor owned by Henry VIII, showing the articulated inner elbow and feet, amazing breadth of motion. The breastplate of this armor needed a screw in front.
