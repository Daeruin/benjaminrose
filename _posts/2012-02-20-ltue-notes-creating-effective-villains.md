---
from_wordpress: true
comments_id: 793
title: "LTUE Notes: Creating Effective Villains"
date: 2012-02-20T21:35:27-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=793
permalink: /post/ltue-notes-creating-effective-villains/
categories:
  - writing and publishing
tags:
  - LTUE
  - villains
---
Today's selection of notes from LTUE comes from a presentation on creating effective villains by Leigh Averett. Leigh is an unpublished writer who loves villains and has made a particular study of them. She was very interesting and entertaining to listen to, but the nature of how she presented things made it a bit hard to take good notes.

Four main points for creating effective villains.

**1. Motivation**

Motivation is a staple need for all characters, but it's especially important for villains. Insanity is not motivation—it's a weak excuse for writers. Better examples: vengeance; to rule the world; to be the best assassin. The villain must be justified in their own mind.

**2. Why are they evil?**

Rule: The villain is not evil because they do bad things; they do bad things because they are evil. The evil comes first. Bad things stem from an evil heart. (Important because a hero can do bad things, but it's about intent and where the heart is. It goes back to motivation.)

**3. The villain is independent of the hero**

The villain does not exist just to fight the hero. He isn't evil just because there's a good hero. You can't hate the villain just because he's a villain.

Evil overlords have a lot of wasted potential. How do they feel when they're alone? Probably just very sad because they've put themselves in a place where nobody can love them. Only fear.

**4. Antagonist is not the same as villain**

Example: In Les Miserables, Javier is the antagonist, but he is not a villain. He has a good heard with noble intentions. On the other hand, Thenardier is a villain. He does all sorts of awful things.

Villain and hero are archetypes. Stories don't require those archetypes, let alone as primary characters. It's your choice to use them.

**Q&A Notes**

Villainous flaws:Look up [Peter's Evil Overlord List](http://www.eviloverlord.com/lists/overlord.html)

Examples of great villains: Emperor Palpatine, Clu, any villain from Brandon Mull (The Candy Shop Wars, Fablehaven), The Lord Ruler.

The fallen hero—Is he irredeemable or not? What would motivate him to stay in a fallen state?

Female villains (this was my question)—Very difficult. Women are more emotionally complex. A female villain often has to be emotionally dead, and that makes them hard to do.
