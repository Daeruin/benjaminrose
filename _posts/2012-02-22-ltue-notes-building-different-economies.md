---
from_wordpress: true
comments_id: 802
title: "LTUE Notes: Building Different Economies"
date: 2012-02-22T20:18:09-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=802
permalink: /post/ltue-notes-building-different-economies/
categories:
  - writing and publishing
tags:
  - economy
  - LTUE
  - politics
---
Today's selection of notes from LTUE comes from the panel "Building Different Economies" which in the booklet description was actually called "Building Different Economies/Politics." The latter title turned out to be more accurate, which makes sense—it turns out economy and government are intimately related. The panelists were L. E. Modesitt, Robison Wells, and Dan Willis. David Farland was listed on the schedule, but I don't remember him being there. The subjects covered in this panel were all over the place, so pardon if my notes seem a bit jumbled.

**Pet peeves**

  * There has never been a city in the middle of the desert, with one exception. I didn't catch the name of the city, but it's a city on the Sinai peninsula that is on a crossroads between caravan routes.
  * In dystopian fiction, the isolated group of people in a contained area. Most of the time they wouldn't be able to feed themselves.

**Other notes**

There is no civilization without taxation. There must be a give and take between the taxation level and what the people get out of it.

The complexity of a society depends on its density. A society that is spread out across a large area will have less conflict and require fewer rules. Dense cities always require more rules.

There will be no trade until there is an agricultural surplus. One person must supply enough food for themselves and several others. The others can then focus their time on making stuff instead of getting food.

There must be a certain amount of political cohesion before you have a society that can support technology. Advanced technology requires three things: agricultural surplus, a transportation network, and educated people to maintain the system. The Greeks had many incredible advances in science but couldn't take advantage of them because they were never unified. Example: The [Antikythera mechanism](http://en.wikipedia.org/wiki/Antikythera_mechanism) which was an ancient mechanical computer designed to compute astrological positions. It was created around 1 BC and nothing similar came around until 1300 years later.

Three political systems that can truly support advanced civilization and technology: democratic systems, oligarchy, and constitutional empire. (It seems like there was another one that I didn't catch as I was taking notes.)

On magic: Magic must have value in society. We are a tool using society, and magic is a tool. Questions to ask: How does the magic user make a living? How does he live day to day?