---
from_wordpress: true
comments_id: 62
title: Word Count Report
date: 2008-03-20T23:26:42-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=62
permalink: /post/word-count-report/
categories:
  - word counters union
---
I'm a little disappointed to have escaped a flogging! Although I've written my words every day, I neglected to post my report. I thought I'd be flogged for sure.

It's taken me two days to get my text widget installed in the sidebar. It took me at least an hour last night to update to the newest version of WordPress, so I could even have widgets available. Then, tonight, I had to widgetize the code in my theme's sidebar so I could even attempt to use the widgets, and I had to update the theme's style sheets so my widgets wouldn't look all screwy. Since I don't know PHP or CSS, it was a bit of a challenge, but I did it!

So now you'll see my daily and running word counts over at the top of the sidebar. I'm also planning on starting a journal page like Gatatonic has done over at Whiteacre, if he doesn't mind me copying his idea. But that'll have to wait for tomorrow. Tonight, I'm going to try to get some decent sleep (if possible—our toddler appears to be getting sick, so cross your fingers for me!).

Words today: 125

Running total: 583