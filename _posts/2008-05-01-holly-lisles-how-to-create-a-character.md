---
from_wordpress: true
comments_id: 74
title: "Holly Lisle's _How to Create a Character_"
date: 2008-05-01T22:56:54-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=74
permalink: /post/holly-lisles-how-to-create-a-character/
categories:
  - writing and publishing
---
So I just finished creating my first character using [Holly Lisle's "Character Creation Workshop."](http://www.hollylisle.com/fm/Workshops/character-workshop.html "Holly Lisle's "Character Creation Workshop") It was interesting and enlightening. I was able to invent some details about my character that I may not ever have thought of otherwise. I was also able to flesh out two of the most important aspects of any character: what does he or she want more than anything else, and what does he or she want to avoid more than anything else (and what is he or she willing to do to get/avoid it).

I'm not sure I want to do this with all my characters, though. For one thing, some of Holly Lisle's workshop questions are so specific that it would be quite strange if they really applied to every character. For example, one question is "How did the character's pet once save his life?" Can you imagine if every one of your characters happened to have a pet who once saved their life? Maybe I can think of other specific and unique questions for each character.

Have you tried Holly's workshop? What did you think? What do you do to create your own characters?
