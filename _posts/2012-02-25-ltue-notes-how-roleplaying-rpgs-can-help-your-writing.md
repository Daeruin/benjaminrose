---
from_wordpress: true
comments_id: 809
title: "LTUE Notes: How Roleplaying (RPGs) Can Help Your Writing"
date: 2012-02-25T21:08:34-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=809
permalink: /post/ltue-notes-how-roleplaying-rpgs-can-help-your-writing/
categories:
  - writing and publishing
tags:
  - LTUE
  - roleplaying
---
Today's selection of notes from LTUE comes from the panel "How Roleplaying Can Help Your Writing." The panelists were:

  * Al Carlisle. Al is a professional psychologist who specializes in serial killers. I think someone misunderstood what type of roleplaying the panel was about when he got assigned to this panel!
  * Adam Meyers. Adam is an actor who is starting his own gaming company. Their first product will be a supplement for Pathfinder. He also gave a presentation on historical weaponry on the first day of LTUE (it was a good one).
  * Robert J. Defendi. Bob has written probably hundreds of gaming supplements as well as stories.
  * Dan Willis. Dan is a novelist and I got the impression he wasn't really into gaming.

Summary of how roleplaying can help your writing:

# **IT CAN'T.**

Seriously, that's about what the panel boiled down to. This one was kind of a dud. It's also the last panel I attended and the last in my series of notes. Sorry to disappoint! :)