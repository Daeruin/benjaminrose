---
from_wordpress: true
comments_id: 1325
title: "Primalcraft: Basic Knapping Is Done"
date: 2017-09-13T22:38:15-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1325
permalink: /post/primalcraft-basic-knapping-is-done/
categories:
  - Minecraft modding
---
I'm back with another update on my Minecraft mod, Primalcraft. It's been a little over two weeks since I started working on knapping for Primalcraft, and I've got all the basics working.

To do knapping, you need to find a hammerstone, which is just a round stone that you can easily hold in your hand, and a stone flake. In Primalcraft, you can occasionally find both of these types of stone laying around on the ground, and you can also get them from digging around in the dirt or gravel (although that's slow work until you get proper digging tools). Another type of stone you'll find is a core stone, which is a large piece of flint that you can chip into good tool-sized stone flakes using your hammerstone. Core stones are a prize since you can carry it around and create many tools from it.

Once you have a stone flake and a hammerstone, put one in each hand and right click. This opens the knapping interface. When you open the knapping interface, you'll get a rock in the top left corner, and you have to chip away little pieces from the edges until you get it into the shape you need. Once you have the right shape, the tool head will appear in the output slot, so you can grab it and put it in your inventory. Assuming you've made some cordage and a wooden shaft, you can now create a stone hatchet, knife, or spear.

You'll notice that every time you open the knapping interface, you get a rock of a different size and shape. So you have to strategize a bit to make the tool you want and may even have to settle for a different tool than you were planning on.

In the far future, I hope to add skills to Primalcraft, so you'll have a knapping skill that can make it easier to get the tool head you want.

But next on the docket is simply creating all the differently sized recipes for hatchets, knives, and spear heads, and making unique tools from each one.