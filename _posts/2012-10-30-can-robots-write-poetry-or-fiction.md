---
from_wordpress: true
comments_id: 1006
title: Can Robots Write Poetry? Or Fiction?
date: 2012-10-30T13:08:59-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1006
permalink: /post/can-robots-write-poetry-or-fiction/
categories:
  - fiction
  - software
---
![Bios robotlab writing robot](https://upload.wikimedia.org/wikipedia/commons/thumb/d/d0/Bios_robotlab_writing_robot.jpg/512px-Bios_robotlab_writing_robot.jpg){: width="276px" height="207px"}

I have seen a number of articles in recent weeks on the topic of whether a computer could write a book. One software company has software that will automatically write newspaper articles on certain topics. A college business professor is working on software that  has already written 200,000 nonfiction books, half of which he has for sale on Amazon.

I haven't yet seen an example of what these software programs produce, but I'm extremely skeptical of their quality.

Let's do an experiment. Following are two poems. Each is a xenia epigram, a poetic form originally found in Latin literature. One was written by poet Luke Wright for the BBC. The other was written by a computer after being given instructions about the poetic form.

Can you tell which is which?

Here they are:

**To Truth, by ??????**

> To truth I offer this thanks,\
> when needing something like reality\
> When I'm writing and drawing blanks,\
> I almost settle using actuality.
>
> I am in search of more,\
> trying to sing your praise!\
> It's you I very much adore,\
> lacking in so many ways.

**To Felicity, by ??????**

> Felicity, my dear, my thanks\
> the cheque you sent was great.\
> Tomorrow I'll go to the bank\
> my rent's already late.
>
> And sorry for the shoddy rhyme\
> I'm tired, I'm not on it,\
> perhaps if you send more next time\
> I'll scribble you a sonnet.

Which was written by the computer and which by the human? Leave your guess in the comments. Don't look at the other comments until you're ready to make your guess.
