---
from_wordpress: true
comments_id: 1110
title: Wheel of Time Book Covers in Dutch
date: 2013-02-28T16:54:40-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1110
permalink: /post/wheel-of-time-book-covers-in-dutch/
categories:
  - art
  - fiction
---
Some time ago I posted images of the [covers to all the Wheel of Time ebooks.](/post/the-wheel-of-time-e-book-covers-image-heavy-post/ "The Wheel of Time E-Book Covers (image-heavy post)") Here are the covers of the Dutch print versions.

**New Spring**

[![0 Eeen Niew Begin](/assets/images/00-Eeen-Niew-Begin.jpg){: width="400px" height="620px"}](/assets/images/00-Eeen-Niew-Begin.jpg)

&nbsp;

**The Eye of the World**

[![1 Het Oog van de Wereld](/assets/images/01-Het-Oog-van-de-Wereld.jpg){: width="401px" height="621px"}](/assets/images/01-Het-Oog-van-de-Wereld.jpg)

&nbsp;

**The Great Hunt**

[![2 De Grote Jacht](/assets/images/02-De-Grote-Jacht.jpg){: width="409px" height="616px"}](/assets/images/02-De-Grote-Jacht.jpg)

&nbsp;

**The Dragon Reborn**

[![3 De Herrezen Draak](/assets/images/03-De-Herrezen-Draak.jpg){: width="371px" height="609px"}](/assets/images/03-De-Herrezen-Draak.jpg)

&nbsp;

**The Shadow Rising**

[![4 De Komst van de Schaduw](/assets/images/04-De-Komst-van-de-Schaduw.jpg){: width="376px" height="616px"}](/assets/images/04-De-Komst-van-de-Schaduw.jpg)

&nbsp;

**The Fires of Heaven**

[![5 Vuur uit de Hemel](/assets/images/05-Vuur-uit-de-Hemel.jpg){: width="399px" height="621px"}](/assets/images/05-Vuur-uit-de-Hemel.jpg)

&nbsp;

**Lord of Chaos**

[![6 Heer van Chaos](/assets/images/06-Heer-van-Chaos.jpg){: width="396px" height="621px"}](/assets/images/06-Heer-van-Chaos.jpg)

&nbsp;

**A Crown of Swords**

[![7 Een Kroon van Zwaarden](/assets/images/07-Een-Kroon-van-Zwaarden.jpg){: width="405px" height="633px"}](/assets/images/07-Een-Kroon-van-Zwaarden.jpg)

&nbsp;

**Path of Daggers**

[![8 Het Pad der Dolken](/assets/images/08-Het-Pad-der-Dolken.jpg){: width="403px" height="635px"}](/assets/images/08-Het-Pad-der-Dolken.jpg)

&nbsp;

**Winter's Heart**

[![9 Hart van de Winter](/assets/images/09-Hart-van-de-Winter.jpg){: width="405px" height="644px"}](/assets/images/09-Hart-van-de-Winter.jpg)

&nbsp;

**Crossroads of Twilight**

[![10 Viersprong van de Schemer](/assets/images/10-Viersprong-van-de-Schemer.jpg){: width="413px" height="620px"}](/assets/images/10-Viersprong-van-de-Schemer.jpg)

&nbsp;

**Knife of Dreams**

[![11 Mes van Dromen](/assets/images/11-Mes-van-Dromen.jpg){: width="400px" height="603px"}](/assets/images/11-Mes-van-Dromen.jpg)

&nbsp;

**The Gathering Storm**

[![12 De Naderende Storm](/assets/images/12-De-Naderende-Storm.jpg){: width="400px" height="638px"}](/assets/images/12-De-Naderende-Storm.jpg)

&nbsp;

**The Towers of Midnight**

[![13 De Torens van Middernacht](/assets/images/13-De-Torens-van-Middernacht.jpg){: width="400px" height="624px"}](/assets/images/13-De-Torens-van-Middernacht.jpg)

&nbsp;

I think they are well done, and I know some people like them a lot . . . but personally, I'm not fond of them. They're definitely atmospheric, but they are too generic for my taste. I wouldn't even guess they were fantasy novels if I didn't already know.

What do you think? Better or worse than [the English ebook covers](/post/the-wheel-of-time-e-book-covers-image-heavy-post/ "The Wheel of Time E-Book Covers (image-heavy post)")?
