---
from_wordpress: true
comments_id: 1604
title: "Primalcraft: A Breakup"
date: 2018-09-17T21:02:16-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1604
permalink: /post/primalcraft-a-breakup/
categories:
  - Minecraft modding
---
I haven’t posted for over two months, and my Minecraft modding plans have changed quite a bit. I had gotten very stuck on the world gen features I was planning for Primalcraft, and couldn’t find a way around or through the problems. I realized I’d been working on this monstrous mod for over two years, and it was likely I would never be able to finish it to my satisfaction.

So something had to change. I’ve decided that rather than make one gigantic, all-encompassing mod, I should try to release some of the features separately, in smaller mods. I probably have well over a dozen different features that are fairly substantial and could be released on their own with relative ease.

My first mini-mod is now basically done and ready for some testing before release. It’s called BasketCase, and all it does is add wicker baskets into the game. Wicker baskets have less storage than a chest, but they're handy because they are portable. You can put stuff in them and carry them around in your inventory. I'll be posting a little wiki here on my site soon.