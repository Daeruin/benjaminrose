---
from_wordpress: true
comments_id: 605
title: Losing Excitement
date: 2011-06-15T20:34:19-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=605
permalink: /post/losing-excitement/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
I suppose this will be one of those somewhat whiny, self-indulgent writerly posts. Feel free to disregard.

I've been losing some of my excitement for my current work-in-progress. This seems to be a pretty normal pattern for me. I work on a story project for a few months, and after a while the excitement starts to just go away.

I'm not sure why. Maybe it just doesn't feel new anymore. Maybe it seems like too much work. Maybe the challenges seem too tough. Maybe I like the idea of writing better than the reality. Maybe I just don't _want_ it badly enough. Maybe my attention span isn't as great as I thought it was. Or maybe it's the opposite—maybe my attention span is fine, but it requires more effort to stay engaged. Maybe if I gave the project more attention, I could reignite my excitement.

I'm not sure any of those is the actual reason. More likely it's a mix of some, or all, of them.

Or maybe the real culprit is that I've been feeling so blasted _tired_ lately.

Regardless, work on the project has slowed to a crawl. My last two writing sessions have been aborted prematurely in favor of less demanding activities.

I'm not asking for any kind of encouragement or pep talk. I'm just throwing stuff out there. Hopefully I will look back on this post in a while and laugh and shake my head at myself. Who knows what the future holds?