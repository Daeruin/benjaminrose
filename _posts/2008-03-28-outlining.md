---
from_wordpress: true
comments_id: 66
title: Outlining
date: 2008-03-28T21:05:22-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=66
permalink: /post/outlining/
categories:
  - writing and publishing
---
How much outlining do you do when you write? I'm particularly interested in the answers of those of you who have actually written works of substantial length. For those who haven't written lengthy works, do you feel you need an outline? How far ahead do you look? And, for everyone, do you feel that knowing where your story is headed takes away some of the excitement of writing?