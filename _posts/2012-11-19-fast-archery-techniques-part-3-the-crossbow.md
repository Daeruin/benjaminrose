---
from_wordpress: true
comments_id: 1028
title: "Fast Archery Techniques, Part 3: The Crossbow"
date: 2012-11-19T23:19:04-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1028
permalink: /post/fast-archery-techniques-part-3-the-crossbow/
categories:
  - medieval combat
---
In my past posts on archery speed shooting techniques, I've talked about some methods for shooting bows much faster than many people think possible. If you haven't seen them, there are links at the bottom of this post. But for now, it's time to visit some ways to speed up the rate of fire for the medieval crossbow.

Quick caveat: I'm not an expert. I've just gathered what resources I could find online, hoping that you'll find it interesting and helpful. Please feel free to comment below.

The medieval crossbow has a much shorter string than other types of bows, which means that to generate the same amount of power, the draw weight has to be increased. A crossbow with a 100 pound draw weight can be cocked ("spanned" using medieval parlance) by hand, but won't have much power. To get decent power from a crossbow, the draw weight has to be more like 300 or 400 pounds. I've heard it mentioned that they can even get up to 2000 pounds of draw weight. To span a crossbow with that kind of draw weight, you need mechanical help. Several different methods were developed.

**Method 1: Hand Spanning**

![crossbow hand span](/assets/images/crossbow-hand-span.jpg)

Hand spanning is actually one of the fastest methods of loading a crossbow, but as mentioned above it can only be done with a low draw weight. Most crossbows need to be more powerful to really get the job done.

[Hand Method Video](https://www.youtube.com/watch?feature=player_detailpage&v=HagCuGXJgUs#t=138s). This video compares the relative rate of fire of an English longbow and a crossbow being spanned by hand. You can see the method really well—it's not complicated. Notice the stirrup on the front of the crossbow, which is missing in the picture above but is used in other spanning methods, too. The crossbowman manages to get off 5 shots in 51 seconds, for about 10 seconds per shot.

**Method 2: Belt Hook**

![crossbow belt hook](/assets/images/crossbow-belt-hook.jpg)

With this method, you attach the string to a hook on your belt, stick your foot in the stirrup, and use your legs and back to pull back the string.

[Belt Hook Method Video](https://www.youtube.com/watch?feature=player_detailpage&v=7g-0-RK3cjk). I didn't think this method would be much faster than the regular hand spanning method, but according to the video it only took 7.5 second per shot. (Also notice that the longbowman in this video was shooting once every 3.33 seconds compared to 5.1 seconds in the video above. Lots of things could factor into that.)

**Method 3: Rope and Pulley**

![crossbow rope](/assets/images/crossbow-rope.jpg)

This is the best picture I could find. Basically, you hook a rope to the butt of your cross bow, loop or hook it to the string, and pull up on the rope. I have a couple videos of the method. Unfortunately, neither gives a good idea of how fast this method might be for a practiced crossbowman.

[Rope and Pulley Method, Video 1](https://www.youtube.com/watch?feature=player_detailpage&v=rIAa_quiyTo#t=19s). This video starts as a demonstration of the windlass method (described below), but he makes a detour to explain how the rope and pulley method works. You only need to watch about 20 seconds. After that he mentions another method, the goat's head lever, but you can't see the device very well. I'll talk more about that one in a minute.

[Rope and Pulley Method, Video 2](https://www.youtube.com/watch?feature=player_detailpage&v=wz8kpSuQeFk#t=31s). It's hard to tell if this rope spanner actually has pulleys, or if the rope is simply threaded through a metal loop near the hooks. In any case, it looks really awkward to set up and takes about 50 seconds to get off a single shot, depending on when you start counting.

**Method 4: Goat's Foot Lever / Pulling Lever**

![crossbow goats foot](/assets/images/crossbow-goats-foot.jpg)

The goat's foot lever is an ingenious device. Check out the videos.

[Goat's Foot Lever Method Video](https://www.youtube.com/watch?feature=player_detailpage&v=iIkxyjVu9gc). The shooter's arm obscures the mechanism a little bit, but as you can see from the above picture, the two claws hook onto little metal posts that stick out from the side of the stock. By pulling on the lever, the claws slide along the posts and pull the string along with it. In this video, he shoots a crossbow bolt once every 12 seconds or so. Not as fast as I'd hoped, but obviously much faster than the rope and pulley or the windlass method, which I'll describe below.

**Method 5: Pushing Lever**

![crossbow push lever](/assets/images/crossbow-push-lever.png)

The pushing lever is essentially a goat's foot lever in reverse. Rather than hooking to posts near the back of the stock and pulling the string, it attaches to the front of the crossbow and pushes the string.

[Pushing Lever Video](https://www.youtube.com/watch?feature=player_detailpage&v=ULi5a6pQRXA). This video is in German, but it demonstrates pretty clearly how a pushing lever works. It doesn't give a good idea of how long it would really take to shoot a bolt, but I would imagine it's very similar to the goat's foot lever.

**Method 6: Latchet Crossbow**

I couldn't find a good picture that was available for use, but you can see some really nice [pictures on Tod Todeschini's website](http://www.todsstuff.co.uk/crossbows/latchet-crossbows.htm "Latchet Crossbow").

This is a more advanced mechanism using a lever that's built right into the crossbow itself. It's similar to the goat's foot lever. Here's a video of the latchet crossbow in action:

[Latchet Crossbow Video](https://www.youtube.com/watch?feature=player_embedded&v=tW4lYh8Yg38). He gets off four shots in about 38 seconds, making it about 9.5 seconds per shot—faster than the goat's foot, but still not as fast as hand spanning or the belt and hook method.

**Method 7: Windlass**

![crossbow windlass](/assets/images/crossbow-windlass.jpg)

The windlass is basically a winch that's attached to the butt of the crossbow. It has to be attached and removed every time, so it's extremely slow, but also takes the least amount of strength. According to [Sir Ralph Payne-Gallwey's 1903 treatise on the crossbow](http://books.google.com/books?id=wXVPAAAAYAAJ&source=gbs_navlinks_s) (page 134), it should take about 12 seconds to pull the string all the way back, plus the time it takes to attach and remove the windlass itself, allowing one shot per minute. Videos 1 and 4 below debunk this somewhat.

[Windlass Method, Video 1](https://www.youtube.com/watch?feature=player_detailpage&v=OpuVK1jwMIA#t=513s). With the various explanatory pauses, it's hard to tell how long it would really take, but it seems to be about half a minute all told, which is less than Sir Ralph claimed. This is the best of the videos I found on the windlass—in fact, you probably shouldn't even watch the next two videos. I only included them because I found them first and they were already here.

[Windlass Method, Video 2](https://www.youtube.com/watch?v=RJ70pAyhu6I "Computer animation of a windlass crossbow"). This is a computer animation of a crossbow being loaded with a windlass. Somewhat interesting.

[Windlass Method, Video 3](https://www.youtube.com/embed/oNhiZSECniw "Windlass Method of Spanning a Crossbow"). In this video, he's using a modern winch. It takes a full minute to load the bow and take a shot, but he does waste some time. A trained crossbowman could certainly do it faster.

**Edit:** [Windlass Method, Video 4](http://youtu.be/WEOeZTV9wiA). WATCH THIS ONE! Here's a very excellent video that makes it much easier to time. He takes 6 to 7 seconds to draw the string all the way back, which is twice as fast as Sir Ralph claimed. It's unfortunate that he started the first loading cycle with the windlass already attached, so we can't use that one for timing. But we do see two full loading cycles, one lasting 49 seconds and one lasting 36 seconds. The extra time on the longer one was due to some tangling of the ropes. I imagine a practiced crossbowman could do it even faster.

**Method 8: Cranequin**

![crossbow cranequin2](/assets/images/crossbow-cranequin2.jpg)

The cranequin is basically a ratchet winder. I couldn't find any videos of this mechanism in use, but Sir Ralph (page 134) claims that it's even slower than the windlass, allowing a shot every minute and a half. At that rate, it's unlikely the cranequin was used in actual warfare, especially considering the much faster methods available.

**Method 9: Screw and Handle**

![crossbow screw>](/assets/images/crossbow-screw.jpg)

This is a somewhat more primitive version of the cranequin. The screw goes into a shaft and hooks onto the string. A handle on the end is turned, pulling the screw and string back. I haven't been able to find any other pictures or videos of this method. Sir Ralph (pages 82 to 83) is the only source I've seen, and he claims it's even slower than the cranequin method. Here's a video of a guy [spanning a miniature crossbow called an assassin's crossbow](https://www.youtube.com/watch?v=se_N8CrooPY&feature=youtu.be) that uses a screw mechanism.

**Summary**

<table>
  <tr>
    <td>
      <strong>Method</strong>
    </td>

    <td>
      <strong>Time per Shot</strong>
    </td>
  </tr>

  <tr>
    <td>
      Hand Spanning
    </td>

    <td>
      10 seconds
    </td>
  </tr>

  <tr>
    <td>
      Belt Hook
    </td>

    <td>
      7.5 seconds
    </td>
  </tr>

  <tr>
    <td>
      Rope and Pulley
    </td>

    <td>
      50 seconds?
    </td>
  </tr>

  <tr>
    <td>
      Goat's Foot Lever
    </td>

    <td>
      12 seconds
    </td>
  </tr>

  <tr>
    <td>
      Pushing Lever
    </td>

    <td>
      12 seconds?
    </td>
  </tr>

  <tr>
    <td>
      Latchet Crossbow
    </td>

    <td>
      9.5 seconds
    </td>
  </tr>

  <tr>
    <td>
      Windlass
    </td>

    <td>
      30–60 seconds
    </td>
  </tr>

  <tr>
    <td>
      Cranequin
    </td>

    <td>
      Longer than windlass?
    </td>
  </tr>

  <tr>
    <td>
      Screw and Handle
    </td>

    <td>
      Longer than Cranequin?
    </td>
  </tr>
</table>

It looks like the belt hook method wins the day, followed closely by the hand method, the latchet crossbow, and the goat's foot lever. For weaklings and those who have all day, the rope and pulley, windlass, or cranequin methods would suffice. It's also worth noting that the faster methods tend not to work as well as the draw weight of the crossbow gets really high, which is one reason why the windlass and cranequin were invented in the first place.

**Note:** You might want to check out my other archery posts:

  * [Part One](/post/speed-archery-fast-and-deadly/ "Speed Archery: Fast and Deadly") — Two videos of fast archery techniques from Russia.
  * [Part Two](/post/archery-speed-shooting-techniques-part-2/ "Archery: Speed Shooting Techniques, Part 2") — Two videos of Lars Andersen demonstrating his incredibly fast shooting and one of Lajos Kassai showing ancient Hun archery.
  * Part Three — You're reading it now.
  * [Part Four](/post/speed-archery-techniques-part-4/ "Speed Archery Techniques, Part 4") — More videos showing some speculation on Native American archery, recreations of ancient Turkish techniques, and more fast shooting from Russia.
