---
from_wordpress: true
comments_id: 338
title: New Story Idea
date: 2011-02-01T22:27:17-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=338
permalink: /post/new-story-idea/
categories:
  - writing and publishing
---
About a year ago, I came home from [LTUE](http://ltue.org) overflowing with a new sense of excitement to start writing again. I worked hard on a new story idea I had and felt great about it. It was full of cool magic, interesting characters, and atmospheric settings.

Somewhere along the way, the excitement died down. When I started working from home, I thought "Great, now I can finally get more done on that story."

Did it happen? No. I'm writing less now than I was before. I went through a rough couple of months writing-wise. I felt depressed about my writing and even entertained brief thoughts of giving it up so I wouldn't have to feel that frustration and disappointment anymore.

But the bottom line is that I can't stop thinking about stories. I'm not an idea factory like [Brandon Sanderson](http://brandonsanderson.com/). When I sit down to write, I'm slow as tar. Every sentence is a struggle with my inner demon (some call it an inner editor; mine is a demon).

And everywhere I turn, I hear published authors saying that you have to put in your time before you get good. I hear people saying they wrote thirteen novels before getting published. I hear people saying you have to write at least one piece of crap novel before you figure out how it's really done. I hear people saying you have to write a million bad words before you enter professional territory. So just keep at it, champ! Don't worry if it sucks! It probably will, but you have to get through it!

That just makes me depressed.

But I have come up with a plan. I am giving myself permission to write one story that will probably be complete crap, but it will also be a blast to write. I'm trying to focus on having fun. I'm trying to let go so I don't have to face my inner demon for a while.

This story is going to be epic. It's going to have elves, dwarves, wizards, and yes, even hobbits. It's going to have fireballs and assassins. It's going to have magic rings and swords. Maybe I'll even throw in an orphan who turns out to be a king. I'm going all out. Tolkien is going to be turning over in his grave.

I'm going to let it all loose and just get it out of my system. I'm not even giving myself goals or deadlines. To the devil with that crap. I need to quit stressing out and just give myself permission to write whatever the heck I feel like. It won't be perfect, and I don't care. I just need to remind myself that writing is supposed to be enjoyable.

If you could write one story, never mind the cliches, never mind the market, never mind all the writing advice you've heard, what would it be?