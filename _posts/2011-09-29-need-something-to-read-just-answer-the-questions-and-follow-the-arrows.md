---
from_wordpress: true
comments_id: 711
title: Need Something to Read? Just Answer the Questions and Follow the Arrows
date: 2011-09-29T11:17:50-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=711
permalink: /post/need-something-to-read-just-answer-the-questions-and-follow-the-arrows/
categories:
  - fiction
---
Are you in the mood for some speculative fiction, but not sure what to read? Look no further than SF Signal's guide to NPR's top 100 fantasy and science fiction books. There's something for everyone. Click on the picture to find the original in all its massive glory, and let me know which book the chart led you to. I was led to The Codex Alera by Jim Butcher. I've heard a lot of good things about Mr. Butcher. I guess I'll trust the chart and check it out.

[Guide to NPR's Top 100 SF Books](http://www.sfsignal.com/archives/2011/09/flowchart_for_navigating_nprs_top_100_sff_books/)
