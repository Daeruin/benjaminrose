---
from_wordpress: true
comments_id: 586
title: "World Building, Part II: The Cure"
date: 2011-05-24T21:58:55-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=586
permalink: /post/worldbuilding-part-ii-the-cure/
categories:
  - writing and publishing
---
In my last post I talked about some of the dangers of world-building disease and hinted that world building might actually be helpful in some way. This post is about the helpful part.

I recently discovered a writing blog that I enjoy: The Creative Penn, blog of author Joanna Penn. Joanna is a smart, self-published author with a lot to share about writing and publishing books. The other day she featured a guest post from CJ Lyons, who writes thrillers. The post was titled "Worldbuilding: Don't Do It!"

The first two sentences of this post are: "**People often associate world building with science fiction or fantasy.  But I’d like to talk about a different way to build a world, one that works for any genre**."

CJ Lyons goes on to share a method of world building based on character emotion and telling details. It's actually a very interesting and helpful post, but I can't help but disagree with the fundamental assumption the post is based on—the idea that the proposed method of world building can work for any genre, and that there must be something wrong with the type of world building that science fiction or fantasy authors typically engage in. (You might want to [read that post](http://www.thecreativepenn.com/2011/05/22/world-building/) before going on; it's fairly short.)

{%
  include figure.html
  image-url="https://upload.wikimedia.org/wikipedia/commons/b/b9/Sample_conworld.jpg"
  width="200"
  height="200"
  wrap="left"
  alt="A Rendered Conworld"
  caption="A Rendered Conworld from Wikipedia"
%}

The traditional method of world building, at least for authors of fantasy and science fiction, often proceeds from the **top down**. You first define the physical world itself, starting with the fundamentals: the laws of physics (if they are different) and/or magic, the solar system and astronomy, and the planet itself (size, geology, weather, flora, fauna, etc.). The reasoning is pretty simple—all of these things dramatically effect everything else. The story possibilities are radically different if you change even one of those parameters.

Another method is to proceed from the **bottom up**. You might want to tell a story that involves windmills, or creatures with certain characteristics, so you design the local regions next, then you design the planet, solar system, and physics/magic around those details. This has a lot in common with CJ Lyons' idea. In both cases, you're starting with smaller details.

But CJ Lyons' method starts with much smaller details. And that's the issue. In my opinion, details that are too small don't tell you enough to base big decisions on. I might know that there's a sheriff who shot most of his own deputies, but that doesn't tell me one whit about the physical characteristics of the world he lives in. On the other hand, the world a character lives in does provide information about the character. The world defines the character, limits the character, and provides context for the characters personality and actions.

Of course, it's possible to continue building on such small details—in fact it's necessary if you start that way. And many authors may thrive on such a process. They enjoy creating stuff from whole cloth, out of thin air, or whatever metaphor you prefer. They may be "pantsers," or organic storytellers. They may feel stifled under the burden of world building from the top down.

But sometimes having those large-scale decisions made up front can actually free your creativity. I like to think of top-down world building like formal poetry. **When you set out to write a sonnet, you know you have to abide by certain rules**. The meter, the rhyme, and the length are all predetermined. Some people feel stifled by those rules. And yet Shakespeare wrote 154 sonnets, all using the same rules, and never ran out of creativity.

Writing without limitations can be stifling, too. There can be something overwhelming about trying to tame a gigantic, chaotic mass of ideas. How do pick just one concrete detail out of that huge, seething vat of possibilities? The problem is simply inverted.

Both methods have their advantages ([Wikipedia has a good discussion of the two methods](http://en.wikipedia.org/wiki/Worldbuilding)). I suppose a lot of it comes down to your own process and what works for you.

But some of it depends on what genre you're writing in, too. A story set in the present day, in a location you are familiar with, requires virtually no world building at all. You might need to do a little bit of research here and there, but not much. A story set in the present day in an unfamiliar country, or in the past, requires more research, but still very little of what I would call real world building. This is where CJ Lyons post goes a bit wrong in my opinion. You might call that stuff "building a fictional world," but that's completely different from "world building."

World building means creating new rules, new places, new creatures, _new worlds_. If you're writing fantasy or science fiction, world building is an absolute necessity. You can't get away without it. And the deeper into fantasy you go (epic fantasy as opposed to paranormal romance), the more necessary it is. Too much depends on those top-level decisions.

So I don't think CJ Lyons' ultra-bottom-up approach works for _every_ genre. **There's a reason why world building is so closely associated with fantasy and science fiction**. They _need_ it.

Here's an example of why I feel this way, based on my own experience. When I first started trying to write in earnest, I sat down to start my epic fantasy and spent the next two or three years trying to create stories starting from small details. I was told to "just write." And I felt frustrated and helpless the entire time. I felt like there must be something wrong with me, because I couldn't decide what characters to use or what would even happen to them.

But this time around, with the new story I recently started, the minute I started making large-scale decisions about my world and what type of magic it contained, story possibilities immediately began presenting themselves. The more decisions I make, the more ideas I have.

It might sound counter intuitive, but it works for me. **World building is my story-making cure**.
