---
from_wordpress: true
comments_id: 7
title: Time to leave
date: 2006-12-31T17:04:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=7
permalink: /post/time-to-leave/
categories:
  - my fiction
---
_Time to leave._ This one thought stuck in the back of Weathulf's mind, refusing to be banished. They had camped here in this high valley for weeks—no, it was months now—waiting for the old man to speak. Each day they rose from their blankets, cold and stiff, looking to him, hoping for some word. But each day he merely shook his head, his blind eyes staring south.

A wind that was almost warm rose from that direction. From a high ridge Weathulf looked down at the land of the Hau, seeing little at this distance but a green and yellow blanket creeping up the edges of the foothills. Weathulf could almost smell the life rising up with the wind from those fertile plains and wide valleys. His hunger grew the keener, thinking of that bounty. This high in the mountains, game was scarce and only becoming scarcer each day. The snares were empty again this morning. Soon they would be forced to move on, or starve. The old man would have to face that reality.

Weathulf turned back towards camp, resolved to speak his mind on the issue. His path this morning had led him across several scree slopes, and he had a rough time of it scrambling back up. The stump of his left wrist ached in the cold.
