---
from_wordpress: true
comments_id: 15
title: Dragon Luck
date: 2007-03-08T00:20:34-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=15
permalink: /post/dragon-luck/
categories:
  - my fiction
---
A bead of sweat trickled down Nomet’s brow, making a wet track through the sandy grime caked there. It stopped at his eyebrow, dispersing into the dark hair. He wiped at it with the back of his hand, then held his arm there to shade his eyes temporarily from the harsh sun. Of his shirt and pants, only rags were left. He had lost his veil and head wrap two days ago in a sand storm, so his scalp baked and blistered, stung by the salt of his sweat. Still, he was lucky. It was not so hot that his sweat dried even as it emerged from his burned pores.

Soon luck would have nothing to do with it, however. A few more hours in the sun would see him weak and delirious with heat sickness. Another day without water, and he would surely die. He worked his cracked lips, but his tongue would not move, and no sound emerged.

_I do this for her. For my beloved._ Those words in his mind, if not on his lips, sustained him for another few steps, his feet sinking deep into the sand as he struggled along the bottom of a sand dune. He tried to picture his beloved’s face, but found he could not. How long had it been? Weeks, surely. Perhaps a month on this desperate quest. A hopeless quest to find a dragon in the heart of the fiery sands.

Perhaps it was this last, despairing admission of the foolishness of his quest that finally caused the dragon to appear. The sandy slope of the dune above him began to shift, flowing like water away from a leaping shark. The great horns emerged first, cragged splinters worn smooth by the friction of sand. Those black thorns rose like death from the sand. Nomet stood transfixed, hardly seeing the scaly head with its protruding teeth and waving feelers trailing from the corners of the mouth.

The dragon’s thick, snake-like body heaved suddenly up into the sunlight, cascading sand down to knock Nomet from his feet, burying his legs. Then he cried out in raw fear, his trance broken. Nomet had not found the dragon; the dragon had found him. Of that he was suddenly sure.

A hissing sound emerged from the dragon’s mouth. The dragon’s lips stretched open over its glistening teeth. Nomet realized the dragon was laughing.