---
from_wordpress: true
comments_id: 349
title: Witch Song by Amber Argyle
date: 2011-02-20T14:32:33-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=349
permalink: /post/witch-song-by-amber-argyle/
categories:
  - art
  - fiction
---
I had a great time at LTUE. Although I'm a hopeless introvert, I managed to meet a few people thanks to the generosity of my friend [Michelle](http://theinnocentflower.blogspot.com/). One of the people I met is [Amber Argyle](http://amberargyle.blogspot.com/), a writer whose first book, Witch Song, is coming out this September from Rhemalda Publishing. Witch Song is a young adult fantasy with a beautiful cover:

{%
  include figure.html
  image-url="https://3.bp.blogspot.com/_DPWtMKhs11Y/TUhs8KFJs7I/AAAAAAAAAK8/g46BO_jYWDE/s1600/Witch+Final+front+cover.jpg"
  width="368"
  height="241"
  alt="Witch Song by Amber Argyle"
  caption="Witch Song by Amber Argyle"
  link="http://amberargyle.blogspot.com/p/witch-song.html"
%}

Here's the concept behind the book:

> The world is changing.
>
> <div>
>   For thousands of years, witch song has controlled everything from the winds to the shifting of the seasons. But not anymore. All the Witches are gone, taken captive by the dark Witch, Espen.
> </div>
>
> <div>
>   As the last echoes of witch song fade, Espen grows stronger as winter and summer come within the space of a day. Now she’s coming for the one she missed—a shy, untrained girl of fifteen named Brusenna.
> </div>
>
> Somehow, Brusenna has to succeed where every other Witch has failed. Find Espen. Fight her. Defeat her.
>
> Or there won’t be anything left to save.

It sounds great, doesn't it? It reads great, too. I really enjoyed the first chapter, which is available for free on Amber's website.

Amber is holding a contest on her blog to celebrate the publication of Witch Song. She's giving away several books that I'm really excited about. The first is [Song of the Dragon by Tracy Hickman](http://www.amazon.com/Song-Dragon-One-Annals-Drakis/dp/0756406072). (He was at LTUE this year too, and he's an incredible speaker.)

Check out Amber's website to see [more details about the contest](http://amberargyle.blogspot.com/2011/02/giveaway-of-signedhardback-song-of.html) and read [the first chapter of Witch Song](http://amberargyle.blogspot.com/p/witch-song.html).
