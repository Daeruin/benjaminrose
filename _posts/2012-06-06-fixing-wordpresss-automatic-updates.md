---
from_wordpress: true
comments_id: 923
title: "Fixing WordPress's Automatic Updates"
date: 2012-06-06T09:41:02-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=923
permalink: /post/fixing-wordpresss-automatic-updates/
categories:
  - metablogging
  - software
---
![Wordpress](https://s.w.org/style/images/about/wmark.png){: .image-wrap-left width="300px"} I use WordPress to run my website. It's free, it's (mostly) easy to use, and it's easily modified. It has hundreds of beautiful themes to enhance the look of your site, and it had hundreds of plugins to extend the functionality of your site and your WordPress installation.

One area where WordPress has historically NOT been easy to use is the initial installation and the installation of updates to the software. It involved a painful, manual transfer of dozens of files, folders, and subfolders your website's server using an FTP program. These days, some website hosts offer one-click installation of WordPress, which is a huge boon (although my host doesn't offer this feature, I managed just fine).

However, installing updates has still been a hassle. WordPress tends to release new updates frequently, on the order of every month or two, and then they put a big, bright banner across the top of your administration panel warning you of the update and urging you to get it done. This has been one of my biggest sources of discontent with WordPress from day one. They update frequently, badger you about it, and then make it time-consuming and frustrating to accomplish.

**Enter Automatic Updates**

So it was with great glee that I heard that with version 2.7, WordPress had released a feature allowing you to install updates automatically. This was to work for both the main WordPress software and all your plugins.

And it was with an equal amount of disgust that I discovered the process didn't work. At least, not for me. The service was patchy and reportedly didn't work for a great number of users, due to host and server issues that were apparently beyond WordPress's control.

**What Went Wrong**

When you click a link to initiate an automatic update, you'll get the following screen:

![wordpress automatic upgrade plugin failed 2 WordPress Automatic Upgrade Plugin Failed or Not Working](https://www.nerdgrind.com/files/2009/9/wordpress-automatic-upgrade-plugin-failed-2.png "Wordpress Automatic Upgrade Plugin Failed or Not Working"){: width="500"}

If you're having the same problem as me, after entering your information and clicking on Proceed, you'll get a screen looking something like this:

![wordpress automatic upgrade plugin failed 1 WordPress Automatic Upgrade Plugin Failed or Not Working](https://www.nerdgrind.com/files/2009/9/wordpress-automatic-upgrade-plugin-failed-1.jpg "Wordpress Automatic Upgrade Plugin Failed or Not Working"){: width="500"}

The error is "Failed to connect to FTP Server (yourserver:port)" and it occurs even when you are sure all the requested information is correct. When I was having the problem, I got my hostname, username, and so forth directly from the instructions provided by my hosting service, but it still wouldn't work. I double and triple checked my password, and it continued to fail.

**I'm glad to report that after two years or so, I finally solved the problem.**

From browsing around forums and help sites, it became clear that the same fix wasn't working for everyone. It's a complicated procedure with numerous things that can go wrong. I'll tell you what fixed it for me, then give a few other possible fixes that might work for you. If these fixes don't work, please don't ask me to troubleshoot your problem. I'm not a technical whiz. I'm just reporting on a few things I found that might help. I really hope they do.

So, here are some possible solutions to this problem that I have found.

**What Worked For Me: Changing the Hostname to "localhost"**

This is so simple, it makes me depressed how long it went unsolved. When entering the hostname, simply enter "localhost" without the quotes. Worked like a charm for me. I haven't the faintest clue why this would work. It's like magic.

**Other Possible Solutions**

  * Add http:// or ftp:// in front of the hostname (e.g., http://ftp.domainname.com).
  * Use the IP address of your FTP server instead (e.g., 127.0.0.1).
  * Check which port your FTP server requires. Add it to your hostname using a colon (e.g., ftp.domainname.com:21). The default is port 21, but some use 22 or something else.
  * Add or remove "@yourdomainname" after your username (e.g., username@domainname.com).
  * Eliminate any special characters (e.g., *+!#$, etc.) from your FTP password.
  * Make sure your server is running the latest version of PHP (currently version 5).
  * Change your PHP settings to enable or disable Fast CGI mode (some have been fixed by turning it on, some by turning it off).
  * Try entering a telnet userid/password, if you have one.

**What NOT To Do: Change File Permissions**

There are lots of bad, bad ways to fix this problem, and most of them involve changing file permissions. Doing this the wrong way can open big security holes in your website.

There is a way to do it correctly, without making your site vulnerable to hackers. I'm far from an expert on this topic, but if none of the fixes above works for you, and you suspect file permissions might be part of your problem, please read the following article before doing anything else. (The screen shots above are taken from this article as well.)

[WordPress Automatic Upgrade Plugin Failed or Not Working](http://www.nerdgrind.com/wordpress-automatic-upgrade-plugin-failed-or-not-working/)

This fix apparently involves logging into your server with a secure shell (SSH) or telnet. I have no idea how to do that, but it might be worth looking into if nothing else works for you.

Good luck to you!

**Do you have another solution to this problem? Let me know in the comments.**
