---
from_wordpress: true
comments_id: 289
title: Another Rudession
date: 2010-11-06T09:32:49-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=289
permalink: /post/another-rudession/
categories:
  - music
---
Anyone remember those Rudessions that our former writing buddy Gat/Whiteacre used to post? This is the guy those were inspired by. Jordan Rudess is the piano player for Dream Theater, one of my favorite bands ever.He also has a tendency to insert out-of-place or silly little interludes into otherwise normal songs, hence those Rudessions. But that aside, he's an absolutely incredible pianist. I believe he studied at Julliard.

<iframe width="560" height="315" src="https://www.youtube.com/embed/bDEEBK0KxXQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This next video shows the band all together. This song is "Dark Eternal Night" from their album _Systematic Chaos_. It's become one of my favorites of theirs. Beware—it's long. Not everyone will have the stamina to listen to such incredible, mind-numbing, musical awesomeness for that long. If you wish, skip to what's one of the best guitar solos of all time at about 5:50. This song does feature a Rudess keyboard solo as well, but to be honest he's not my favorite part of the band. I could have picked a number of other songs that do a better job of featuring his talent. Actually, I'm not that into guitar solos, either. But every now and then, John Petrucci puts together a solo that totally fries my brain. This is one of them.

<iframe width="560" height="315" src="https://www.youtube.com/embed/EkF4JD2rO3Q?start=344" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
