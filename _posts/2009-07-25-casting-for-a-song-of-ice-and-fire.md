---
from_wordpress: true
comments_id: 123
title: Casting for A Song of Ice and Fire
date: 2009-07-25T22:07:35-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=123
permalink: /post/casting-for-a-song-of-ice-and-fire/
categories:
  - fiction
---
I've heard some exciting news regarding the production of A Song of Ice and Fire for HBO. They are now producing a pilot episode in Ireland, which will help determine which among about 10 new shows will see the light of day.

They've also cast some of the characters. Eddard Stark will be played by Sean Bean (Boromir in The Lord of the Rings movies, and if that doesn't tell you who he is, you probably shouldn't even be reading this blog). He also played Sharpe in the Sharpe's Rifles series. Very exciting!

Tyrion Lannister will be played by Peter Dinklage, who's been in a lot of shows, but perhaps most notably as Trumpkin the dwarf in The Chronicles of Narnia (he was also in the movie Penelope recently, and he's been in other probably more famous shows as well).

Robert Baratheon will be played by Mark Addy, who played the drunken priest in A Knight's Tale.

The characters of Jon Snow, Joffrey Baratheon, and Viserys Targaryen have also been cast with pretty much unknown actors. A couple of them don't even have entries in IMDB.com.

If the show gets picked by HBO, the first season probably won't come out until fall 2010 or even 2011, and since I don't have HBO, I won't be able to see until it comes out on DVD after that. There's a long wait in store—if it ever happens at all.