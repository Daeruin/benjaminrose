---
from_wordpress: true
comments_id: 98
title: First Chapters
date: 2008-06-26T21:47:18-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=98
permalink: /post/first-chapters/
categories:
  - writing and publishing
---
The first chapter of a book can be really hard to get right. Here's a summary of advice from The Secrets Podcast Special Editions #1 and #2 by Michael A. Stackpole.

The first chapter is a microcosm of what will follow in the rest of the book. It gives an idea of the characters, the world, and the conflict to come. First chapters sell the book to an editor. You need to hook the editor by the end of the first page (not a hard and fast rule, but a safe bet).

Here are the things the first chapter must accomplish:

  1. Introduce characters who will be central to the story. It’s best if it focuses on one or two of them, preferably one. This allows the reader to concentrate on the characters. The characters and how the reader feels about them will determine whether they read further. This means that first chapter must provoke a reaction to the character—for good or ill doesn’t matter, as long as the reaction is emotional and strong.
  2. Give a glimpse of the world—not too much, no info dump. Show the world how it is with no explanation. Curiosity will pull the reader further into the story.
  3. Introduce the conflicts in the world and for the main characters. This is where readers find the on ramp to the plot. A sprawling good versus evil epic won’t have the main protagonist and antagonist square off on page one, but the good and bad forces should be introduced so the reader knows they are poised to hammer each other later on.
  4. Pose and leave unanswered a few questions to lead the reader to keep reading. These questions are most effective when they address a character. If the author has characterized the characters well enough that the reader is able to project a future for them, the reader will continue to read to see if his guess is correct.

If you can get all these elements into the first chapter, while avoiding common mistakes, you stand an excellent chance of getting your reader further into the book.

A book with multiple main characters will essentially have more than one first chapter, except you won’t rehash things already mentioned in earlier “first” chapters.