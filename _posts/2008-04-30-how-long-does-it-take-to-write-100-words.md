---
from_wordpress: true
comments_id: 73
title: How long does it take to write 100 words?
date: 2008-04-30T23:17:35-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=73
permalink: /post/how-long-does-it-take-to-write-100-words/
categories:
  - writing and publishing
---
I have a question for the Word Count Buddies. On average, how long does it take you to write your daily 100 words? What's the longest it's taken you? The shortest?