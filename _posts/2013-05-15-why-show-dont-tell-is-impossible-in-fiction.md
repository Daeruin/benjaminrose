---
from_wordpress: true
comments_id: 1191
title: "Why *Show, Don't Tell* Is Impossible in Fiction"
date: 2013-05-15T06:00:20-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1191
permalink: /post/why-show-dont-tell-is-impossible-in-fiction/
categories:
  - writing and publishing
---
What I'm about to tell you isn't revolutionary. Others have preached the true word before. I've seen hints on [Victoria Mixon's blog](http://victoriamixon.com/2012/04/02/3-things-to-know-about-exposition-telling/ "Victoria Mixon's blog"), and I know [John Brown](http://johndbrown.com/ "John Brown's blog") did a presentation at a writing conference (don't go to those links yet; I'm going to explain it better here!). But I have yet to see the truth about "show, don't tell" collected into a single, clear article. This is my attempt at it.

"Show, don't tell" is one of the most common pieces of writing advice ever given. The thing is, it's impossible to show in writing, because showing is inherently visual. Showing is physical. Writing is verbal. Writing _is_ telling, and it can't be anything else. If writing could show, it would be a movie or a play.

> **Example**: If a man says "I love you" to a woman, he is telling. If a man kisses a woman, cooks her favorite meal, and spends time with her, he is showing her that he loves her. He's not just talking; he's acting.
>
> **Example**: If a grandmother explains to her grandkids what Venice looked like last fall, she is telling. If a grandmother takes her grandkids there on vacation so they can see it for themselves, she is showing them. She's not just talking; she's physically showing the place to her grandkids.

Words are telling. Action is showing.

In fiction, words are all we have. It's _all_ telling.

What we mean by showing in writing is that the writing allows the reader to create a strong mental image, as if they were seeing it or experiencing it first-hand. _As if_ they were being shown. _As if_ they were there.

So if writing is all telling, how do we create that sense of verisimilitude and vicarious experience in the reader? How do we show?

There are different kinds of telling, and some are better than others at creating that mental image for the reader. The different kinds of telling vary depending on who you're talking to, but they tend to boil down to four basic categories:

  * Exposition
  * Argumentation
  * Description
  * Narration

**Exposition** is the conveying of factual information. In the greater world of the written word, exposition is exemplified by things like the business report, scientific paper, news story, and encyclopedia article. In fiction, exposition is when you take the time to explain facts about your fictive universe—who's related to whom, where certain places are within the setting, and how a scientific principle or magical spell works.

**Argumentation** is the attempt to convince the reader of a point of view. Advertisements, resumes, and letters to the editor are good examples of argumentation. You don't see much of this in fiction, but authorial commentary generally falls here. The author is attempting to convince you, the reader, to feel or think a certain way by addressing you directly. Fiction can employ argumentation obliquely through the subtle use of theme.

**Description** is the construction of a visual image of a specific person, place, or thing in the reader's mind. Description tends to be woven into the other types of telling, especially narration. Poetry often relies heavily on description.

**Narration** is the relating of a series of events—things that are happening. Narration is the main component of good fiction. Outside of fiction, narration occurs whenever we tell someone about something that happened to us. Biography is largely composed of narration.

Some people add a few more types of telling to the list: summarization, introspection, recollection, sensation, emotion, action, dialogue and transition. I haven't studied this topic in such depth that I could easily explain to you why some people think these modes should be distinct from the four above. But it seems to me that these secondary categories could pretty easily be defined as a subset or minor variation of one of the primary four categories.

Exposition and argumentation are generally poor at creating visual images and vicarious experience, although they both have their uses. Description and narration are better, but only when used in the right way.

> **Telling Example**: Carl's sister came over the next day and told him that mom had died that night. He felt an overwhelming sadness wash over him. He would never see his mom again.
>
> **Showing Example**: Carl had the newspaper laid out on the kitchen counter. He was scanning the headlines when he heard someone coming down the hallway. He looked up and saw his sister stop in the doorway. She had on a faded dress. Her face was hidden in shadow. "Mom died last night," she said. A hollow feeling spread through Carl's chest. His hands clutched at the edge of the kitchen counter as if to stop himself from falling. His shoulders heaved as a wracking sob escaped his throat.

The telling example uses a mix of narration and exposition to tell what happened and how the character feels, but it's too generic. It's just a summary of what happened to Carl.

The showing example also uses narration, and it's still telling—it's just words on the screen. But the manner of the telling allows the reader to easily construct a strong visual image of what's happening, as if they were sitting in the room watching the scene unfold. The part about the hollow feeling spreading through Carl's chest, even though it's not visual, enables vicarious experience by helping the reader imagine the physical sensation the character is experiencing. Conveying action and description this way takes longer than using exposition or summary, so you have to be careful about what you choose to focus on.

Unfortunately, fiction pundits have created their own vocabulary for these things, and "showing" is firmly ensconced as something that's both real and possible in fiction. So you'll hear writing experts talking about exposition, narration, telling, and showing in different ways than I've explained them here. (Go and read the link to Victoria Mixon's blog now if you want. She's a smart editor, but I feel like she gets lost in the expert jargon—despite her claims to the contrary—and stops being clear almost right away.)

Bottom line:

In fiction, showing means the judicious use of rich, visual detail and concrete, physical action to help the reader construct a visual image of a scene and imagine what it would be like to have that experience. _As if_ they were being shown. _As if_ they were there.
