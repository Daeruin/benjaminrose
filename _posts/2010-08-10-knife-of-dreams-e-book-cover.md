---
from_wordpress: true
comments_id: 213
title: Knife of Dreams E-book Cover
date: 2010-08-10T20:53:55-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=213
permalink: /post/knife-of-dreams-e-book-cover/
categories:
  - art
  - fiction
---
Now [THIS](http://www.dragonmount.com/News/?p=1354) is what I have always dreamed of seeing on the hardback cover for the Wheel of Time books instead of that colossally bad garbage that Darrell Sweet puts out. This makes me want to swear and cry at the same time—for positive reasons.

[![Knife of Dreams E-book Cover by Michael Komarck](/assets/images/11-Knife-of-Dreams-eBook-Michael-Komarck-cover.jpg){: width="539px" height="819px"}](/assets/images/11-Knife-of-Dreams-eBook-Michael-Komarck-cover.jpg)

[Michael Komarck](http://www.komarckart.com/index.html) is the painter, and coincidentally I just discovered him earlier this week from his incredible depictions of scenes from George RR Martin's Song of Ice and Fire series. He has done some illustrations for the 2009 Song of Ice and Fire calendar, but I can't find the link anymore. Do a Google search and you'll find something. It's worth it. Amazing art.
