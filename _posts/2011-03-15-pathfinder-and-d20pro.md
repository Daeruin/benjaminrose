---
from_wordpress: true
comments_id: 363
title: Pathfinder and d20pro
date: 2011-03-15T22:28:46-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=363
permalink: /post/pathfinder-and-d20pro/
categories:
  - gaming
  - software
---
It's been a while since I posted anything about gaming. Lately I've been playing a new game that some friends introduced to me. It's called [Pathfinder](http://paizo.com/pathfinder), and many RPGers have probably heard of it before. Especially since Wizards of the Coast ditched DnD 3.5 and moved to 4e, Pathfinder has been filling in the huge gulf and pleasing a lot of gamers.

![Pathfinder Core Rulebook](/assets/images/pathfinderCoreRulebook.jpg){: width="400" .image-wrap-right}

The first thing that struck me about Pathfinder was the [amazing art](https://www.facebook.com/album.php?id=156235407748746&aid=24661). Their [core rulebook](http://paizo.com/pathfinderRPG/v5748btpy88yj) is chock full of fantastic, full color art. Every page is full color, and the entire book is high quality and well designed. It's also nearly 600 pages long! The only RPG book that rivals it, to my knowledge, is the [Game of Thrones d20 book](http://www.amazon.com/Game-Thrones-D20-Based-Open-Gaming/dp/1588469425), which caused Guardians of Order to go bankrupt.

The second thing that struck me about Pathfinder wasn't so great. It's an RPG that takes advantage of Wizards of the Coast's [d20 Open Gaming License](http://en.wikipedia.org/wiki/D20_System). Essentially, it's Dungeons and Dragons version 3.5 with a fair number of tweaks and improvements. Lots of people call it DnD v3.75. At first this sounded promising. Maybe they fixed all the crap that I hate about the d20 system!

Well . . . not so much. While they did fix some things, none of them were the core issues that bug me so much. Still, the game didn't look any worse than several other d20-based products I've purchased and played before. And Pathfinder has the bonus of a really cool lineup of adventure modules and lots of support products to make playing—and GMing—funner and easier.

So when my friends asked nonchalantly who would be the GM and made innocent puppy dog eyes at me, I caved. Besides, there's this other cool product out there called [d20pro](http://www.d20pro.com/), a software application that lets you play d20-based games in a virtual environment and simplifies a lot of what it takes to be a GM.

I'll have to talk about d20pro in another post. It's pretty darn cool, and it deserves its own post.
