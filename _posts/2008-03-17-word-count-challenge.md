---
from_wordpress: true
comments_id: 59
title: Word Count Challenge
date: 2008-03-17T23:00:03-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=59
permalink: /post/word-count-challenge/
categories:
  - word counters union
---
I've joined a word count challenge. We're going to write at least 100 words a day. I wrote 117 today, and it exhausted me. No, really work exhausted me. My head is killing me, but I stayed up to write my words anyway. I'm happy with them. I don't know that they'll make it into my novel, since I had to invent a new scene without thinking too much about it. We'll see.

My hope is that this challenge will do for me what I intended my blog to do when I started it: give me daily motivation to write.

I've listed my word count buddies in the sidebar to the right, near the bottom (alphabetical order—nothing I can do about it, unfortunately).