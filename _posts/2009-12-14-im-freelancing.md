---
from_wordpress: true
comments_id: 133
title: "I'm Freelancing"
date: 2009-12-14T23:01:06-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=133
permalink: /post/im-freelancing/
categories:
  - writing and publishing
---
I am officially working on my first freelance writing job. If you've been wondering why I haven't been posting on the blog, or even better, posting word counts on my novel, you now know why. But who am I kidding? I'll be lucky if more than two people even read this, let alone wonder why I haven't been giving them much else to read.

In any case, I've been enjoying the job, and I'll enjoy the paycheck at the end even more. I'm working on rewriting a 50-page, 30,000 word book on the HCG dieting plan. This is my kind of work, not because of the subject matter, but because of the _type_ of work. I get to work through a disorganized piece of writing from a non-professional and turn it into something clean, organized, and polished. I love this kind of stuff.

One of the challenges of this job is working around the copyright status of the original work. The work was self-published by a doctor in Italy, and the author is now dead. We haven't been able to track down who might own the copyright now, but we have found dozens of unauthorized copies available on the internet. So, even though copyright problems are unlikely, we still want to avoid any potential issues and maybe even copyright this work for the company. To do that, I have to rewrite the work while attempting not to infringe on the copyright of the original. It's a little bit like trying to modify a puzzle by re-cutting all the pieces and still ending up with something resembling the original puzzle.

To complete the project, I've been writing in the evenings after the kids go to bed. I can't necessarily write every day, since I do have a busy life that includes a family of three children under four years old and more importantly only one car, which severely restricts when and how I can get all of life's errands done. Depending on how late I want to stay up, I can usually get in a couple solid hours of work. I've been working through roughly 1,000 words on each day that I write. In the slightly over two weeks I've been working, I've produced about 12,500 words.

This shames me. Back when I was trying to write my novel, I could barely squeeze out 100 words, yet here I am a year later pounding out 1,000 words a day on something that's not nearly as cool. At the rate I've been going, I could almost have won NaNoWriMo.

To be fair, the writing is pretty different. This time, I already know what I need to say. It's sitting right there in front of me, and I just have to re-say it a different way. With my novel, it was all coming out of my own head right there on the spot. Nobody was telling me what to say.

This reinforces to me something I discovered about myself sometime in the past several years: I'm really not that creative. Don't get me wrong. I have some creativity, which I know I can harness to good effect. But when it comes right down to it, I just don't have a huge reserve of creative juices just waiting to be poured out of me and onto the page. I have to squeeze with all my might to produce a few miserly drops, and work hard to make those drops appear better than they are.

No, I'm not a starry-eyed poet. Just look at me, enjoying the rewriting of a badly written dieting book. I'm a soulless editor at heart. But you know what? I'm fine with that. It's good to know yourself.

And anyway, regardless of it all—when I'm done with this freelance job, I'm getting back in the game. I'm going to write that novel. Even if I have to do it 100 painful words at a time.