---
from_wordpress: true
comments_id: 185
title: Weapons That Made Britain
date: 2010-06-21T19:27:37-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=185
permalink: /post/weapons-that-made-britain/
categories:
  - medieval combat
  - 'movies &amp; TV'
---
[Ing over at Blog Ing just wrote a post about Deadliest Warrior](http://www.dallenrose.com/?p=427), and it reminded me of another similar show I've run into recently. It's called Weapons That Made Britain, and it's cool because it mixes historical fact with modern weapon testing by a medieval weapons expert. The guests on Deadliest Warrior are called experts, but sadly they don't get to really show off their technique very much. They just get to slice through pig carcasses and gel dummies. But the guy on Weapons That Made Britain is a real historian and weapons master, and he really gets to show off sometimes. There's one episode where he's on a horse, weaponless, charging a guy who has a sword, and he manages to disarm the guy and take the sword. Of course, it is staged, but it's still amazing. This is a great resource for those who may have questions about medieval weapon use. A few samples:

  * [Sword tests](https://www.youtube.com/watch?v=eyAkA4Fc6CY)
  * [Shield tests](https://www.youtube.com/watch?v=VsZnTCQptWc)
  * [Lance tests](https://www.youtube.com/watch?v=XHCpoCSUbW0)
