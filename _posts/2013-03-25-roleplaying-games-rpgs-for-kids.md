---
from_wordpress: true
comments_id: 1064
title: Roleplaying Games (RPGs) for Kids
date: 2013-03-25T21:57:58-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1064
permalink: /post/roleplaying-games-rpgs-for-kids/
categories:
  - gaming
---
I was glad when a friend asked me to write about roleplaying games (RPGs) for kids, because it's something I've put a fair amount of thought into, and I relished the excuse to look into it a bit more. I started playing tabletop RPGs when I was about 11 years old and never stopped. I definitely intend to introduce my kids to the hobby one day. Two of my brothers have already started roleplaying with their older kids.

Luckily, the market for roleplaying games is really healthy. RPG design and theory have expanded drastically in the past 20 years or so, and the number of options out there is truly staggering. There are tons of RPGs designed just for kids—way more than I had initially suspected before writing this post. Many of them are very inexpensive or even free. I haven't tried any of them (yet), and there are just too many for me to make specific recommendations. But down below I'll give an overview and list some pages where you can go to find one that's just right for your kids.

Before picking one and diving right in, there are a few things you'll want to consider before introducing your kids to roleplaying games.

**Violence and Monsters**

Consider your kid's maturity level with regard to violence and scary situations. RPGs originated with war gaming, and many RPGs are tied closely to fantasy adventure stories, so combat plays a big role in most games. A lot of RPGs designed for kids do a good job of downplaying such violence and focusing on other types of conflict and gaming challenges, so you have plenty of choices in that regard. For example, in one game each kid plays a fuzzy stuffed animal character, and when opponents are defeated, they simply go to sleep. Even in some more mature games, violence is abstracted to "hit points"—a simple numerical value that indicates character death upon reaching zero. The level of realism in these games is rather like cartoon violence, where every blow just bounces off until the last one.

Other games are targeted towards adults or kids with a higher tolerance for violence. Some kids deal just fine with violence in RPGs. After all, it is in the imagination. I personally started out with a rather violent RPG called Middle Earth Role Playing (MERP). Tolkien's Middle Earth can be a rather violent place, and the game reflected that. There were damage tables that describe severed limbs, blood, crushed skulls, and all manner of physical harm. I started playing that game at 11 years old and still remember the first fight my first character got into. I was playing a dwarf, and we came across a nasty orc. I decided to have my dwarf use his war hammer to hit the orc, and thanks to a lucky roll on MERP's detailed injury tables, my dwarf ended up shattering the orc's kneecap. I thought it was awesome, and since then I've always preferred RPGs with more realistic and gritty combat.

However, even if you choose a game with such violence in the mechanics, you don't have to focus on the gory descriptions or even use them at all. It's all up to you—that's one of the great things about roleplaying games. You don't need to feel tied to the rules; you can change them at any time, for any reason, if it increases your enjoyment of the game. If the level of violence described in the game doesn't make you comfortable, you can change the way you describe it to your kids. Gloss over the violent aspects of the game or make it more cartoony. You may also want to avoid having humans as bad guys and instead focus on monsters. If your game has monsters, you may want to avoid monsters that are too scary. This is one thing that will keep my waiting until my kids are older.

Some games probably aren't appropriate at all. Horror, post-apocalyptic, or cyberpunk games will have themes that may be too mature. Stick to animals and cartoon-like games for younger kids and heroic fantasy, space opera, or superheroes for older kids. When choosing an adventure for your kids to explore, maybe steer away from a storyline where the characters have to perform a series of assassinations to start a guild war, and instead focus on a plot where the characters have to defeat goblins to reach a magic sword or something along those lines.

**Math**

The original roleplaying games, and many modern RPGs, relied a lot on dice, numerical scores, and math formulas to mediate the outcome of actions within the game. Some modern games do away with a lot of that math and instead use other mechanisms, such as a bartering system, to decide how the story goes. But most still rely on math to one degree or another. This can be a challenge for some kids. Their idea of fun isn't crunching a bunch of numbers or looking up a bunch of charts.

Luckily, there are a lot of games designed for younger kids who haven't mastered basic math skills yet. Some games are designed for kids as young as 5 or 6 years old.

**Attention Span**

Kids generally aren't interested in making sure all of the rules are being followed exactly right and all the formulas are being followed perfectly. If you spend too long looking up rules or figuring out results, they won't have fun.

One way to solve this problem is to choose a game that you already know really well—one you've played enough to know the rules without looking them up, or make them up if you need to. If you aren't familiar enough with any RPGs that are appropriate for your kids, there are lots of games that are quite simple to learn and perfect for kids with shorter attention spans (see below).

Expect your kids to want to do outrageous things. If they want to do something that doesn't fit into the rule systems of the game or that you're not sure how to handle, just roll with it. Let the kids have fun! That's the whole point, right? Just make sure they have a blast—otherwise they won't want to keep playing.

I had this experience with one of my nephews. He chafed at the restrictions and ended up bored by his first game. Luckily my brother knew that all the kid needed was a little more freedom to do whatever cool stuff he came up with in his head. Their games end up being much more free form and narrative, without a lot of attention to the rules. He loves it.

**Ease Them Into It**

There's nothing more natural than roleplaying to kids. That's exactly how many of them play all day long without even thinking about it. My two little girls love to pretend they're ponies or cats or princesses. The only unfamiliar part of roleplaying games is the idea that while you can try just about anything, you still have to roll dice and follow certain rules to figure out the actual results of your intended actions. So it can help to ease kids into the hobby. There are a few ways of doing this.

Kids will have more fun with a game that has elements they are familiar with. There are lots of games based on popular books, movies, and TV shows. The setting of Middle Earth had a great draw for me when I was a kid (and still does). Your kids may be into a specific setting, like Star Wars or Redwall. There are tons of games to choose from—just about any genre you can think of. Younger kids tend to enjoy games where they can pretend to be animals, especially mice. Other kids will enjoy anything where they get to play a pirate. Picking a game that focuses on a setting you know your kids already enjoy will make it that much more fun for them.

Another way to ease kids into it is to pick a boardgame with roleplaying elements. Descent is a good example for slightly older kids. I've also heard good things about Mice and Mystics.

**Look for Other Good Advice**

I got a lot of the above advice from other sites. There is a lot of good information out there. Try starting with these articles. I really recommend reading them, because they offer different perspectives and recommend some games that I didn't list below. A quick Google search on the subject will reveal even more articles and good resources.

  * [Teaching Kids to Roleplay is Only Natural](http://www.wired.com/geekdad/2008/09/teaching-kids-t/) by Ken Denmead on Wired.com. Good article broken down into age groups with advice and suggested games.
  * [RPGs for Kids](http://www.tlucretius.net/RPGs/kids.html) by Edmund Metheny and Sophie Legacé. Some brief words of advice followed by a long list of games with fairly detailed descriptions.
  * [Roleplaying Games for Kids](http://www.darkshire.net/jhkim/rpg/whatis/kids.html) by John H. Kim. This is a list of games and other articles with very brief descriptions. There is a section on free RPGs for kids.
  * [Role-Playing Games and Kids](http://www.roleplayingtips.com/articles/roleplaying_games_and_kids.php) by Katrina Middelburg-Creswell. Katrina runs an RPG club for teenagers and has lots of good advice for that age group.
  * [Introductory RPGs for Kids](http://www.feartheboot.com/ftb/index.php/archives/1822) by Dan Repperger of the podcast Fear the Boot. A short list of kid-friendly games.

** The Big List of Games**

I have only played one or two of these, and read through a couple more. Several of them are based on old versions of Dungeons and Dragons. I hesitated to list them at first. I've played a few more modern versions of the game, and don't feel they were especially conducive to gaming with younger kids due to their complexity. However, they can be good for teenagers, and if you are already into D&D, they can be a good choice for just that reason. The games will be easy for you to run, and your kids will sense your enjoyment of the game.

So, what do I recommend? My own kids are still pretty young, with the oldest being 7 and still pretty sensitive to violence. I would probably start out with _rpgKids_ or _Hero Kids RPG_ for my 7-year-old and _Fuzzy Heroes_ for my two girls (ages 4 and 5)—see links below. For slightly older kids, I think _Swashbucklers of the 7 Skies_ looks like a blast, as does _Project Ninja Panda Taco_ and _Do: Pilgrims of the Flying Temple_. As kids get into their teenage years, they can probably play just about any game out there, depending on their interests. I'm especially looking forward to introducing my kids to _Mouse Guard_ due to its setting and interesting narrative elements, as well as the new _The One Ring_ game because it does such a great job of evoking the feel of Middle Earth.

I had wanted to do more with this section, but alas, I have a newborn baby on my hands, and this post has already been sitting here 75% finished for two months. I hope you find this helpful.

_D&D Variants_

  * [Labyrinth Lord](http://www.goblinoidgames.com/labyrinthlord.html) (free, based on old-school D&D circa 1981)
  * [Swords and Wizardry](http://www.swordsandwizardry.com/) (free, based on old-school D&D circa 1979)
  * [OSRIC](http://www.knights-n-knaves.com/osric/index.html) (free, based on 1st ed. D&D using the Open Gaming License)
  * [Tunnels and Trolls](http://www.freedungeons.com/rules/) (free, simplified clone of original D&D)
  * [Pathfinder Beginner Box](http://paizo.com/products/btpy8osv?Pathfinder-Roleplaying-Game-Beginner-Box) ($35 printed box set, based on D&D 3.5, probably better for teenagers)

_PDQ Variants_

  * [PDQ](http://www.atomicsockmonkey.com/freebies/di/pdq-core.pdf) (free, core game for other paid games from Atomic Sock Monkey)
  * [PDQ#](http://www.atomicsockmonkey.com/freebies/PDQ.pdf) (free, modified version of PDQ optimized for swashbuckling campaigns)
  * [Swashbucklers of the 7 Skies](http://www.evilhat.com/home/swashbucklers-of-the-7-skies/) ($15 PDF, based on PDQ#, pirates with super powers)
  * [Zorcerer of Zo](http://www.atomicsockmonkey.com/products/zoz.asp) ($15 PDF, based on PDQ, fast paced fairy tale stories)

_Other Games_

  * [Dragon Age](http://greenronin.com/dragon_age/) ($29.95, printed book based on video game, simple system but art and thematic elements may be for teenage kids)
  * [Mouse Guard](http://rpg.drivethrustuff.com/product/60496/Mouse-Guard-Roleplaying-Game) ($20 PDF, simplified version of Burning Wheel, based on the comic books, kids play mice, looks fun for adults, too)
  * [rpgKids](http://newbiedm.com/rpgkids/) ($2.99 PDF download, ages 4 and up)
  * [Faery's Tale Deluxe](http://www.greenronin.com/store/product/grr3201.html) ($19.95 printed book, fairy tale game)
  * [Do: Pilgrims of the Flying Temple](http://danielsolisblog.blogspot.com/2007/08/do-pilgrims-of-flying-temple.html?m=1) ($10 PDF, $25 print + PDF, lighthearted game about kid monks trying to solve problems)
  * [Happy Birthday Robot](http://danielsolisblog.blogspot.com/2009/08/happy-birthday-robot.html?m=1) ($10 PDF, $25 print + PDF, imaginative storytelling game)
  * [Cat](http://rpg.drivethrustuff.com/product/92647/Cat-%28Revised-%26-Expanded%29) ($5 PDF, simple game where each kid plays a cat protecting humans from things people can't see)
  * [Meddling Kids](http://rpg.drivethrustuff.com/product/83837/Meddling-Kids) ($6 PDF, simple Scooby-Doo themed game for kids 7 and up)
  * [Unbelievably Simple Roleplaying System](http://www.lulu.com/us/en/shop/scott-malthouse/unbelievably-simple-role-playing-system-usr/ebook/product-18853511.html) (free from Lulu.com, very simple RPG suitable for both kids and adults, any genre)
  * [Hero Kids RPG](http://herokidsrpg.blogspot.com/p/hero-kids-overview.html) ($6 PDF, fantasy game for kids aged 4 to 10, comes with premade characters and an adventure)
  * [Project Ninja Panda Taco](http://rpg.drivethrustuff.com/product/109127/Project-Ninja-Panda-Taco) ($12 PDF, a game about masterminds and their minions trying to take over the world)
  * [Fuzzy Heroes](http://www.fuzzyheroes.com/catalog.php?item=2&catid=Inner) ($11, I think in print, kids play stuffed animals)

_Board Games_

  * [Mice and Mystics](http://boardgamegeek.com/boardgame/124708/mice-and-mystics) ($50-60, board game, characters play humans who have been turned into mice and must traverse the castle to save the kingdom)
  * [Descent](http://www.amazon.com/Descent-Journeys-Second-Edition-Board/dp/1616611898) ($30-50, board game, characters delve into a dungeon, fight monsters, and find treasure)

Update: Here's an awesome list of [RPGs for kids](http://rpg.drivethrustuff.com/rpg_teachkids.php?src=DTRPGFB) from Drive Thru RPG.

What about you? Are you planning to introduce tabletop roleplaying to your kids? What games have you tried, and what are you looking forward to trying with your kids?