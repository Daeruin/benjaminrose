---
from_wordpress: true
comments_id: 1243
title: "What I'm Listening To: To-Mera, TesseracT, and Means End"
date: 2013-08-19T21:21:28-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1243
permalink: /post/what-im-listening-to-to-mera-tesseract-and-means-end/
categories:
  - music
---
Ing over at Blog Ing recently posted about a new band he discovered, and he linked over to my music category here on the blog. I haven't been paying much love to that category lately, which isn't to say that I haven't been listening to music and discovering lots of cool bands. Quite the opposite.

So it's time to share some cool and interesting music that I've been enjoying these days. Here goes.

First is **To-Mera**, a progressive metal band from England fronted by the Hungarian singer Julie Kiss. To-Mera features nonstandard song structures, crunchy, technically awesome guitar riffs, interesting atmospheric sounds, and the occasional jazz interlude. Kiss's voice is pretty unique in the genre, as she sings almost entirely without vibrato. At first I thought it was just part of their jazz leanings, but then I listened to some of her songs from a former band, and she sang the same way then. In any case, it's part of To-Mera's musical style. The song I'm going to share is called The Lie. It's one of their heavier songs and features a fun jazz interlude shortly into the song (at about 2:10).

<iframe width="560" height="315" src="https://www.youtube.com/embed/uLiTBArXUW4" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Next is **TesseracT**, a djent band also from England. Highly technical, with lots of djenty guitar riffs, ethereal atmospheric elements, and a mix of melodic and aggressive vocals. On their second album they got a new singer and ditched the aggressive vocals completely, which I felt kind of ambivalent about. They said that's actually what they had wanted all along but they felt the metal community wouldn't be as accepting without the aggressive vocals. The song I'm going to share is Nascent, which I think was their first official single and features all the elements that make the band unique.

<iframe src="https://www.youtube.com/embed/grwmUTrO180" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Finally we have **Means End**, another djent-style progressive metal band with some jazz infusion, choral elements, complex crunchy guitar riffs, and philosophical lyrics. One of the goals of Means End is to incorporate "intricate melodic patterns introduced in baroque music [and] chord structures not belonging to the diatonic scale, etc. ([source](http://thecoreofbrutality.blogspot.com/2013/05/means-end-interview-and-bio.html))" Their songs tend to feel a little overwhelming at times as they have so much going on that doesn't fit what you expect. The song I'm going to share is Nox Aurumque, an interpretation of the modern choral piece by composer Eric Whitacre.

<iframe src="https://www.youtube.com/embed/WHxgXfRCLeQ" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

So there you go. That's what I've been listening to lately. Fairly soon I'll share some more, this time from bands that fall closer to the symphonic/gothic side of the genre—bands like Nightwish, We Are The Fallen, and Kamelot. I'd like to do another one that reviews the latest from Dream Theater and similar bands like Circus Maximus.

What are you listening to?
