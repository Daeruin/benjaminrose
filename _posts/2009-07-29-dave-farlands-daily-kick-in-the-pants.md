---
from_wordpress: true
comments_id: 125
title: "David Farland's Daily Kick in the Pants"
date: 2009-07-29T21:26:55-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=125
permalink: /post/dave-farlands-daily-kick-in-the-pants/
categories:
  - writing and publishing
---
I like to share things that have helped my writing, or at least helped my thinking about writing. Podcasts, Web sites, newsletters, whatever. One thing I've really been enjoying lately is David Farland's Daily Kick in the Pants. David Farland, in case you haven't heard of him, is a very successful author of fantasy who also publishes science fiction under the name Dave Wolverton. I haven't read any of his books, but I know him by reputation. He teaches writing courses at BYU, where Brandon Sanderson attended his classes. I think more of my readers have read Sanderson and most have liked him, so it means something to say that Sanderson has said some really good things about David Farland ([here's a small example from an old blog post](http://www.brandonsanderson.com/blog/552/David-Farland-Workshop)).

I also knew a guy in college who was David Farland's nephew. Yeah, really impressive of me, I know!

The Daily Kick in the Pants is a daily e-mail newsletter that David Farland sends out. He's full of great advice on both the professional aspects of being a writer and the craft of writing. It's good enough advice that I've been filing it all away in a folder that I can browse through if I need to find information on something in the future.

In short, [go sign up for David Farland's Daily Kick in the Pants](http://www.davidfarland.net/) (on his website, the button to subscribe is in the right-hand sidebar).