---
from_wordpress: true
comments_id: 142
title: Respectful Professionalism
date: 2010-01-24T18:13:06-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=142
permalink: /post/respectful-professionalism/
categories:
  - fiction
---
[Yesterday](/post/example-of-wots-iconicity/) I mentioned something I didn't like about Brent Week's Night Angel Trilogy—an interesting similarity to Robert Jordan's world of the Wheel of Time. I actually softened it a bit before posting, because I didn't want to be mean or rude.

Just a few minutes later, I decided to look up the [blog of Patrick Rothfuss](http://www.patrickrothfuss.com/blog/blog.html), author of the awesome The Name of the Wind. Coincidentally, he had recently [interviewed Brent Weeks](http://www.patrickrothfuss.com/blog/labels/Brent%20Weeks.html). I quote the relevant portion:

> What's the most hurtful thing someone has ever said in a review of your book?

> It wasn't a review, but on a forum, someone posted a topic of "Brent Weeks raped Robert Jordan." That was pretty cool, especially because RJ was pretty much a hero of mine.

I wasn't sure whether to feel bad or not. Ostensibly, this was the most hurtful thing anybody ever said in a review. Yet Weeks says it was pretty cool. Is he merely dissembling? Is he trying to soften the hurt? Or was it really not the most hurtful thing?

It made me think of something Brandon Sanderson said on his blog. Unfortunately, I can't find the original reference—can't find the right combination of keywords in Google and can't be bothered to read for hours to find it. But it was something to the effect that now he's a published author, he has to be a little more careful in what he says about other authors. You don't want to say something mean about someone you may meet in a convention the next week. In other words, you have to be a respectful professional.

I don't think this is something you can put off, though. You can't just wait until you cross that golden line of "being published." A prospective agent or a new fan may come to your blog and start reading old posts. If you really hope to be a professional author some day, your professional attitude must start now.

Still, does that mean you have to stop reviewing books altogether? If you read a book you hate, do you just have to never mention it again? What do you think?
