---
from_wordpress: true
comments_id: 18
title: Who should I write about next?
date: 2007-04-02T19:58:06-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=18
permalink: /post/who-should-i-write-about-next/
categories:
  - writing and publishing
---
Edit: There used to be a poll here.

And why are you interested in that character? Poor Weathulf—I didn't get to finish his first story, but he's got some interesting things to undergo, too. Or maybe you want to know more about one of the unnamed characters. Cool! Let me know.
