---
from_wordpress: true
comments_id: 764
title: "LTUE Notes: A Character-Driven Approach to Plot Development by Eric Patten"
date: 2012-02-12T21:38:16-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=764
permalink: /post/ltue-notes-a-character-driven-approach-to-plot-development-by-eric-patten/
categories:
  - writing and publishing
tags:
  - LTUE
  - plotting
---
Yeah, it's been a while since I posted anything. I'm back. Maybe.

I went to [LTUE](http://www.ltue.org/ "Life, the Universe, and Everything: A Symposium of Science Fiction and Fantasy") again this year, and it was awesome as usual. I decided to post some of my notes from the various panels I attended. Today's selection is from Eric Patten's presentation titled "Build a Bare Story: A Character-Driven Approach to Plot Development."

The presentation focused on three things:

  * Figuring out what your audience wants
  * Finding a concept to base your story on
  * Use a character-centered approach to create a story

Eric gave a brief description of the standard three elements of story: plot, character, and milieu (also known as setting). He then described how each of us naturally gravitates toward, or at least is naturally more talented in, one of the three elements. And each genre and sub-genre of science fiction and fantasy tends to focus to different degrees on those three elements. This tells you on a pretty basic level what your audience wants from your story. Eric had a sweet Venn diagram to illustrate this idea, but I didn't get a copy. (Edit: Here's a link to [Eric Patten's website](http://www.ejpatten.com/2012/02/life-universe-and-everything-symposium.html) where you can download a PDF of the presentation.)

Next came a discussion of concept. He gave a pretty simple formula:

  * An X does Y at Z. (Where X is a character, Y is a basic plot or action, and Z is a setting.)

Here's the concept for a story I've been thinking about and working on occasionally: "A world-weary dwarf rebels against his masters by saving a hunted elf in a land on the brink of war." Eric's concept for his first book was something like this: "A group of kids from the suburbs must defeat monsters using garbage for weapons."

Finally Eric discussed how to create the story. First he mentioned that there are two basic approaches to plotting a story. One is a top-down approach, which is exemplified by the hero's journey and the standard three-act structure from screenwriting. The other is a bottom-up approach, which is the character-centered approach.

In the character-centered approach, one starts by asking four basic questions:

  * Who is the character?
  * What does the character want?
  * How are they going to get it?
  * What's stopping them from getting it?

You're supposed to ask these questions for every character in the story, starting with the protagonist and the antagonist. From there, additional characters will pop up, and you continue on from there. For each answer you give, you must ask "Why?" and continue asking it. The end result will be a series of interactions between characters. Then you start looking at a top-down approach in order to give your story a stronger structure. So you'll frame all these questions and answers and character interactions in a hero's journey or a three-act structure.