---
from_wordpress: true
comments_id: 1502
title: "Primalcraft: A Time for Every Season"
date: 2018-02-04T21:17:57-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1502
permalink: /post/primalcraft-a-time-for-every-season/
categories:
  - Minecraft modding
---

{%
  include figure.html
  image-name="primalcraft_seasons1.png"
  alt="Primalcraft Seasons"
  caption="Primalcraft Seasons"
%}


From the very beginning, I always wanted Primalcraft to have seasons. I love Minecraft's open world concept—a world that I can change and reshape as desired. But there's also something special about a world that changes on its own. Seasons provide a perfect sense of added realism and vitality to the world, and give the player a context and rhythm to their gameplay that goes beyond just day and night.

It turns out, trying to control Minecraft's weather system is a . . . _stormy_ proposition. Before writing any code, I spent quite a while studying the mod Tough as Nails (brought to you by the Biomes O' Plenty group). I was disappointed to find out they had to rely on ASM to get full control of Minecraft's weather. ASM is a method of modifying Minecraft's source code, which is frowned upon in modding circles because it changes the assumptions upon which all other Forge-based mods are built and makes it harder for mods to be compatible with each other.

But this feature is important to me, so I decided to go ahead and give it a try. I'm quite pleased that I managed to find a way to do everything I needed _without_ ASM. However, I had to create a whole new WorldProvider, which required me to create a new ChunkProvider, and while I was at it I went for a new WorldType.

  * **WorldProvider**—Controls almost all of Minecraft's weather. Replacing the WorldProvider allowed me to change when snow is generated in the world, when snow collects on top of blocks, and when water can turn into ice. It also allowed me to change the length of the day/night cycle. Right now I'm only using that to make days fast enough to test an entire year's worth of seasons in just a few minutes. Eventually, I'll make days longer in the summer and shorter in the winter. Usually modders only create new WorldProviders for their own custom dimensions (Overworld, Nether, and The End are all dimensions). I simply replaced the Overworld's WorldProvider with my own.
  * **ChunkProvider**—Every WorldProvider needs a ChunkProvider, so I was forced to create a new one. I welcomed this, because it allowed me to easily strip all the structures from world generation. They simply don't fit with the concept of Primalcraft, where the player's job is to advance from the Stone Age into something more civilized with their own wits and bare hands. Ditto for vanilla Minecraft monsters. No more zombies or creepers. Eventually, there will be more animals to populate the world.
  * **WorldType**—Creating a new WorldType allowed me to add a new choice to the world type menu during world creation. If you scroll past the usual vanilla choices (Large Biomes, Amplified, Customized, etc.), eventually you find Primal World. This allowed me to make large biomes the default.

Probably the funnest thing about this feature is that leaves now change color with the seasons! I found a cool way to randomize leaf colors, so every tree has a variety of shades. This screen shot shows off the autumn colors in the current version of Primalcraft. I think they're pretty spectacular! The yellow-orange trees are birches. The brighter reds are oak trees. The green ones are my custom hickory trees; I haven't added their colors yet.

{%
  include figure.html
  image-name="primalcraft_seasons2.png"
  alt="Autumn leaves in Primalcraft"
  caption="Autumn leaves in Primalcraft"
%}

Unfortunately, there's still one problem area. Although I've managed to make it snow during winter, it still looks like rain. That's because all the visual effects of Minecraft are handled by the client, and the code for rain and snow is shared. To decide whether to display snow instead of rain, the client code looks at the biome's temperature. And each biome's temperature is unchangeable (it's a _final_ field in Java). That means even though it snows in the winter, and snow collects on the ground, it still looks and sounds like rain. Really aggravating.

I haven't decided what to do about it yet. I may just nuke all the vanilla biomes and replace them with my own, so I can more easily control the temperature.

I also need to write some code to make snow and ice melt at the appropriate times, and change the frequency of rain and snow based on the season. I'd also like to provide some sort of GUI that displays the day and season, to make it a little easier for the player to keep track of time.

After seasons have been fully implemented, I can finally start adding tons of realistic survival food to Primalcraft—a fundamental aspect of the game.

That's it for now. I've got more work to do!
