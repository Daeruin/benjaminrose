---
from_wordpress: true
comments_id: 783
title: Leslie Phillipsen DOES Want to Date You!
date: 2012-02-13T22:40:26-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=783
permalink: /post/leslie-phillipsen-does-want-to-date-you/
categories:
  - metablogging
  - music
tags:
  - leslie phillipsen
  - roll a d6
---
I guess I have to take it all back. Leslie Phillipsen DOES want to date you.

When Connor Anderson posted his "Like a G6" spoof "[Roll a D6](/post/roll-a-d6-music-video-featuring-hot-rpg-chick-and-her-goofy-pals/ "Roll a d6 – Music Video with Hot RPG Chick and Her Nerdy RPG Pals")" on YouTube, the internet went wild, and lots of geeks started trying to figure out who was the hot chick in the video. Turns out it was one Leslie Phillipsen, and she was generating a lot of traffic on my blog. To try to capitalize on the activity, I [blogged about the Leslie Phillipsen phenomenon](/post/leslie-phillipsen-does-not-want-to-date-you/ "Leslie Phillipsen Does Not Want to Date You"), noting that she was just a friend of Connor's and likely had no interest in dating geeked-out gaming nerds. At the time, a helpful reader posted a link to her Facebook profile, proving that she already had a boyfriend.

Well geeks, it turns out that Leslie Phillipsen is now single, and she's started a contest where one lucky winner gets to take her out on a date.

**Update 1 June 2012**: The contest has ended, and the video has been removed.

Here's [a link to the entry form](http://sixtwentyfour.com/lifestyle/win-date-leslie-phillipsen/), and I've embedded the accompanying video below for your viewing pleasure.

<iframe src="https://www.youtube.com/embed/tDKCbf8xWQw" width="560" height="315" frameborder="0"></iframe>

Personally, the whole thing makes me glad I'm happily married. But I won't turn down the boost to my blogs traffic stats. I'm narcissistic that way.

Good luck to all you single geeks out there. Maybe this is your chance for a happy Valentine's Day.
