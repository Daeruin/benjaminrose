---
from_wordpress: true
comments_id: 69
title: Flogging the Union
date: 2008-04-15T23:27:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=69
permalink: /post/flogging-the-union/
categories:
  - word counters union
---
Three days ago, I missed writing my 100 words. I have as of yet received no floggings from the Word Count Unioneers. To commemorate this flagrant oversight, I have decided to leave my gaping 0 word count listed in the side bar until you have corrected your gross omission.

For those of you who neglected to flog me: You have been flogged!