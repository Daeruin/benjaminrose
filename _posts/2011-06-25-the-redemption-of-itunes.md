---
from_wordpress: true
comments_id: 617
title: The Redemption of iTunes
date: 2011-06-25T07:00:30-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=617
permalink: /post/the-redemption-of-itunes/
categories:
  - music
  - software
---
Several years ago, my wife bought me an album on iTunes, and it was a singularly awful experience that convinced me never to buy music from iTunes again. But iTunes may have just redeemed itself.

Now, most people know me as an unrepentant fan of Apple products. Regardless of how evil their corporate monster becomes (it's hard to say who is worse—Apple, Google, or Facebook), Apple always releases quality products that people love to use. iTunes is no exception. It has had its detractors but that doesn't change the fact that it revolutionized the music marketplace, became the single most popular source for music downloads, and ultimately made electronic music downloads respectable and even desirable in the eyes of music labels.

Nothing was more natural than for me to eventually buy some music from iTunes (OK, it was my wife, but I would have done it eventually). I was delighted with my new album . . . until something obscure went wrong, and iTunes started telling me I didn't own that album anymore. I couldn't listen to it for several weeks until I got things straightened out. That experience completely soured me to iTunes. I had paid money for music, and because of their ridiculous digital rights management (DRM) software, I was prevented from having access to the music that I owned.

I went back to CDs for about five years, waiting for iTunes to rise out of the muck and make it worth my while to bother again.

That day has finally come.

The redemption of iTunes came in two steps. The first step occurred a while ago, when Apple finally managed to convince the music labels that DRM sucks, everyone hates it, and they would make more money without it. So finally, albums began to appear in DRM-free versions, and I began to think iTunes might be worth a try again. Unfortunately, not all music is DRM free yet, and that's what was holding me back. Why take the risk when I can buy a cheap CD online and never have to worry about some obscure software glitch stealing my music?

Then Apple announced [iCloud](http://www.apple.com/icloud/features/ "iCloud"), the next step in the redemption of iTunes. As cool as DRM-free music is, iCloud takes the cake. It actually makes DRM irrelevant. Here's how it works: Buy music on the iTunes store. It's automatically registered with iCloud. Then download that same music to any device you own, for free, anytime, as often as you like. And here's the icing on the cake. For a measly $20, you can do the same thing with ANY music you own, whether you bought it from iTunes or not. Plus, iCloud will automatically match your music to the equivalent songs that _are_ on iTunes and supply you with a higher quality version, if it's available.

All those music pirates are dancing with glee at this, because for twenty bucks, they essentially get to legitimize all those thousands of stolen songs they got from Napster or wherever. And the music labels are pretty happy too, because instead of getting nothing for all that stolen music, at least they're getting $20 (or whatever cut they get from Apple). Plus it makes people even more likely to actually buy stuff from iTunes now instead of stealing it. Everybody wins.

There's still a small, cynical part of me that tells me iCloud will still be vulnerable to glitches and failures. iCloud hasn't been released yet, so we can only wait and see exactly how it works.

If iCloud really works like they say it will, iTunes may have finally been redeemed in my eyes.
