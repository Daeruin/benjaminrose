---
from_wordpress: true
comments_id: 37
title: Distributed role playing
date: 2010-01-17T14:05:26-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=37
permalink: /post/distributed-role-playing/
categories:
  - gaming
---
The other day I had this strange idea for a [roleplaying game (RPG)](http://en.wikipedia.org/wiki/Role-playing_game). I've been calling it "distributed roleplaying." A central idea of most roleplaying games is that one person, the Game Master (GM), provides the setting, story, and even some of the rules for the game, while the players control only a single character. Essentially, the GM controls everything else.

I've spent many years in the role of GM, and for many years I enjoyed it. It was a lot of fun to provide stories and challenges for my friends to interact with via their game characters. Besides, I didn't know anyone else who wanted to be the GM, so it was just about the only way for me to get to roleplay at all.

But I burned out. Being the GM takes a lot of time and effort behind the scenes. You have to prepare the setting, which beyond simply knowing what the world is like may entail creating maps and writing down details for the players. You also have to provide a story and control all of the non-player characters, which takes a lot of mental energy during the game. Finally, you must be a master of the game rules. For many games, the rule books can easily be over a hundred pages long.

It's just too much work for me anymore. I don't have the time at home for all the preparation, and I don't have the energy during a game to keep track of all the details and changing circumstances.

So I started trying to think of ways the GM's duties could be shared by the players, and I came up with distributed roleplaying. Here's the idea.

Basically, each of the GM's duties are distributed among the players. I separated the GM's duties into several activities:

  1. **Inventing the background.** Before starting the game, the players would have to discuss with each other what kind of story they want to tell. They could come up with the basics of the setting, some ideas for a general plot, and some non-player characters to inhabit the game. At the beginning it would be pretty generic. Most of this will actually get fleshed out during the game.
  2. **Controlling the non-player characters.** Once the game starts, the non-player characters you've already come up with get divided among the players. (Each player also controls their own player character.) As new non-player characters come up during the game, those get distributed as well. In the beginning, you get to decide on the possessions of the characters you control. If you have a particularly strong non-player character, such as an evil overlord, the players could take turns controlling him or her.
  3. **Inventing the setting.** This could involve several distinct roles depending on how many players there are. One player would be in charge of describing the landscape, the buildings, and the weather. Another player could control all the items and objects—what they look like, when they break or get lost, whether they have special properties. This would include clothing, gear, weapons, and other possessions.
  4. **Arbitrating the rules.** All the players would have to understand the rules of the game to the best of their ability. If questions come up, the players vote on what to do.

Would something like this work? Not as is. There has to be somebody deciding what happens next. That's where the final role comes in.

**Framing the scene.** The players each take turns framing a scene. This could be set up several different ways.

  1. If there's only one major enemy in the game, such as a Dark Lord, the player currently controlling him decides what his next move is and informs those controlling the setting and other non-player characters of the basic requirements for the scene. Then those players get to come up with all the specifics. The player in charge of the setting describes where the characters are, and everyone decides what their characters do, both player and non-player.
  2. If there are various villains or major non-player characters, you can simply take turns giving them the limelight. Otherwise it would proceed as in #1.
  3. If the antagonist doesn't have any major plans at the moment, perhaps one of the player characters has a goal, and the other players agree to go along. Play then proceeds pretty much as in #1.

How crazy does that sound? I'm not sure it would actually work. It would be an interesting experiment to try it out. One challenge is that the overall story within the game could lack cohesion and continuity. You'd need to have fellow players who were all dedicated to telling the story as specified in the background setup, and who are mature enough not to take it in a direction the others wouldn't enjoy.

You roleplayers out there, let me know what you think (there are only a few of you).