---
from_wordpress: true
comments_id: 47
title: Excuse Report
date: 2007-10-20T19:00:54-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=47
permalink: /post/excuse-report/
categories:
  - gaming
  - 'life &amp; stuff'
  - metablogging
  - writing and publishing
---
Here is a report on all my excuses for not writing recently. First off is the new baby. She's five weeks old but still not sleeping through the night and still needing to be held a lot. She's nursing, which takes Mom out of the picture for a good amount of time, meaning Dad has to help out more than usual.

Second is the fact that we're moving. I got a decent raise at work recently, so we could finally afford a bigger place where we can actually fit. We found a duplex that's twice the size of our current place for an amount we can afford. It has a big fenced yard and it's in a great neighborhood in Farmington. We're really excited! The move happens a week from today, so we're really busy packing up all of the nonessentials and getting all those pesky moving tasks done like notifying the bank and transferring the utilities.

Finally, I've joined an on-line, play-by-post RPG. It should be a lot of fun, but it will take up a good amount of the time I now have left for blogging and working on my other writing projects. If any of you want to check out the game, it's on a Google group at [http://groups.google.com/group/Thayrth](http://groups.google.com/group/Thayrth). I haven't posted much yet, but I plan to post my character's details soon. The game is using the Riddle of Steel rules and a custom setting with a number of house rules.

What are _your_ excuses?
