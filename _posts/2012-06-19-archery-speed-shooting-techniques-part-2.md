---
from_wordpress: true
comments_id: 938
title: "Archery: Speed Shooting Techniques, Part 2"
date: 2012-06-19T21:08:50-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=938
permalink: /post/archery-speed-shooting-techniques-part-2/
categories:
  - gaming
  - medieval combat
  - 'movies &amp; TV'
---
A little over a year ago, I wrote about a couple of [archery speed shooting techniques](/post/speed-archery-fast-and-deadly/ "Speed Archery: Fast and Deadly") and posted some YouTube videos. Since then, I've come across a few more videos of archers shooting incredibly fast. These videos debunk decades of ridiculously bad archery rules in role playing games and show that some of the archery stunts pulled by the likes of Legolas in the Lord of the Rings movies may not be that far off the mark.

In this video, a fellow named Lars Andersen (I can only assume he's from the Scandinavian area) has developed his own technique that allows him to shoot three arrows in less than one and a half seconds.

<iframe src="https://www.youtube.com/embed/ggDfJLB8jTk?rel=0" width="500" height="315" frameborder="0"></iframe>

He found all modern techniques for fast shooting to be inadequate to achieve the speed requirement described in the historical work "Saracen Archery," which is an English translation of a Mameluke work on archery, written in the year 1368. He doesn't actually show his technique, because he's still trying to perfect it. I'm not sure if his technique can be done only with three arrows, or if it would work with more.

**Update**: Lars Andersen (not Anders**o**n) has uploaded a new video that is going viral. In this video he shows that his technique allows him to shoot 11 arrows before the first arrow hits the ground. He also has a few closer shots where you can get a look at the actual technique, although it's still not very clear.

For a [comprehensive look at Lars Andersen](https://targetcrazy.com/blog/lars-andersen-and-medieval-speed-archery/) and the controversy he's generated, check out TargetCrazy.com.

<iframe src="https://www.youtube.com/embed/2zGnxeSbb3g?rel=0" width="560" height="315" frameborder="0"></iframe>

The next video shows a Hungarian named Lajos Kassai who has perfected the art of shooting arrows from horseback. The video takes a minute to get into the meat, so feel free to skip to about 1:20 to get to the good stuff. With two guys throwing small targets into the air, Lajos fires 12 arrows in just over 17 seconds and hits every single target. The targets are disks about a foot in diameter and appear to be maybe 15 feet away. He shows how he holds the arrows using the pinky finger of the same hand that holds the bow.

After that, he performs a similar feat while riding a horse at 20 miles an hour, firing 6 arrows in 10 seconds and hitting every target.

<iframe src="https://www.youtube.com/embed/2yorHswhzrU?rel=0" width="480" height="360" frameborder="0"></iframe>

One thing I wonder about with these videos is how much damage those arrows would do to real people. The arrows don't appear to be flying out of that bow with incredible speed. With the second video, I also wonder how accurate he would be at greater distances, say 100 feet or so.

Regardless, it seems pretty clear that it's possible to shoot much faster than most people believe. And since archery was a key military tactic in ancient times and had many thousands more practitioners than we have today, you can be sure they had developed their speed and accuracy to a much greater degree than a few random devotees who posted their videos online.

**Update**: You might want to check out my other archery posts:

  * [Part One](/post/speed-archery-fast-and-deadly/ "Speed Archery: Fast and Deadly") — Two videos of fast archery techniques from Russia.
  * [Part Two](/post/archery-speed-shooting-techniques-part-2/ "Archery: Speed Shooting Techniques, Part 2") — You're reading it now.
  * [Part Three](/post/fast-archery-techniques-part-3-the-crossbow/ "Fast Archery Techniques, Part 3: The Crossbow") — Methods for quickly spanning or cocking a crossbow.
  * [Part Four](/post/speed-archery-techniques-part-4/ "Speed Archery Techniques, Part 4") — More videos showing some speculation on Native American archery, recreations of ancient Turkish techniques, and more fast shooting from Russia.
