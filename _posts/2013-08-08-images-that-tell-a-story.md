---
from_wordpress: true
comments_id: 1226
title: Images that Tell a Story
date: 2013-08-08T21:13:47-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1226
permalink: /post/images-that-tell-a-story/
categories:
  - art
---
<script async defer src="https://assets.pinterest.com/js/pinit.js"></script>

Lately I've really been enjoying [Pinterest](https://www.pinterest.com/daeruin/). For years now I've wished there was a good repository of good fantasy art to use for inspiration in roleplaying games and for my writing. Deviantart has a lot of good stuff, but it's hard to find just what you want. Elfwood has some good stuff, but it gets lost in piles of mediocre and just plain bad art.

But on Pinterest there is a group of people already sifting through the art they encounter and posting only the best. I've found a lot of amazing fantasy art on Pinterest. There's also a lot of awesome photography of fantastic real-world places and historical weapons and armor.

Some of my favorite images are those that have story just oozing out of them—there's a tale begging to be told about what's happening in the picture. I love these because they really get my story sense tingling and make me itch to write.

Here's one of them:

<a data-pin-do="embedPin" href="https://www.pinterest.com/pin/527554543820568630/"></a>

And another:

<a data-pin-do="embedPin" href="https://www.pinterest.com/pin/527554543820261281/"></a>

One more:

<a data-pin-do="embedPin" href="https://www.pinterest.com/pin/527554543820185142/"></a>

Wait, one more:

<a data-pin-do="embedPin" href="https://www.pinterest.com/pin/527554543820087783/"></a>

There is so much awesome art out there, I had a hard time winnowing it down to just these. I'm thinking that I'll make this a regular feature of my blog, where I share some of my favorite Pinterest pins on a given theme.

Now that my life has started to calm down, maybe I can even finish one of the half dozen blog post drafts I have sitting around here.

Anyway, if you liked any of these, check out [my Pinterest boards](https://www.pinterest.com/daeruin/) for more.

