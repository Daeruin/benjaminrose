---
from_wordpress: true
comments_id: 505
title: "Roll a d6 - Music Video with Hot RPG Chick and Her Nerdy RPG Pals"
date: 2011-05-05T22:02:04-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=505
permalink: /post/roll-a-d6-music-video-featuring-hot-rpg-chick-and-her-goofy-pals/
categories:
  - gaming
  - 'movies &amp; TV'
  - music
tags:
  - leslie phillipsen
  - roll a d6
---
Today for your viewing pleasure I offer a catchy music video of a hot gaming chick and her nerdy pals singing about roleplaying games. The song is a parody of "Like a G6" by Far East Movement. It's been recreated from scratch by the nerds themselves using Garage Band. Pretty impressive, actually.

Before you view, you should know that I don't necessarily condone Dungeons and Dragons, any form of rap or R&B music, dressing up in medieval garb, or any roleplaying game that uses six-sided dice. But the song is pretty funny and well done. Check it out, yo. ([Here's a link](http://youtu.be/54VJWHL2K3I) if the embedded video below doesn't work.)

<iframe src="https://www.youtube.com/embed/54VJWHL2K3I" width="500" height="310" frameborder="0"></iframe>

Song written by Connor Anderson and Zac Smith. Video directed by Connor Anderson. Starring Leslie Phillipsen, Connor Anderson, Zac Smith, and Aaron Mull. And yes, for all you hopeful male nerds out there, Leslie Phillipsen is the hot chick in the video, but I doubt she'll appreciate your advances.

So, what do you think? Pretty well done, eh? But more importantly . . .

Do **_you_** condone any of the following?

  * Dungeons and Dragons (any version)
  * Any form of rap or R&B music
  * Dressing up in medieval garb
  * Any roleplaying game that uses six-sided dice

If so, please justify yourself.

**Note:** I've posted a few follow-ups to this post: [Leslie Phillipsen Does Not Want to Date You](/post/leslie-phillipsen-does-not-want-to-date-you/ "Leslie Phillipsen Does Not Want to Date You") and [Leslie Phillipsen DOES Want to Date You](/post/leslie-phillipsen-does-want-to-date-you/ "Leslie Phillipsen DOES Want to Date You!").
