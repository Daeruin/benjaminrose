---
from_wordpress: true
comments_id: 70
title: Writing Plan
date: 2008-04-21T21:04:54-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=70
permalink: /post/writing-plan/
categories:
  - word counters union
  - writing and publishing
---
I've come up with a writing plan. I've been feeling like a rudderless ship with my writing. I've never been very spontaneous with my creativity. I like to write lists and outlines. Filling in the gaps is where my creativity blossoms. I can't seem to tell a story without knowing what the story is first. What's the point of writing every day if you don't know where you're going? Chances are, huge chunks of my writing will end up on the cutting room floor wasted.

So I've decided to violate Word Count Union rules a little bit. I'm going to dedicate my daily words to writing my novel's outline before I continue. The idea is that making an outline first will make all my writing more efficient. We'll see how it goes.

When I need a break from that, I'll be starting a new short story to submit to Writers of the Future. I don't know what it's about yet, but I'll think of something.