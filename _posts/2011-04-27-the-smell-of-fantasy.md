---
from_wordpress: true
comments_id: 489
title: The Smell of Fantasy
date: 2011-04-27T21:41:40-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=489
permalink: /post/the-smell-of-fantasy/
categories:
  - gaming
  - writing and publishing
---
Here's a very interesting idea: a series of perfume oil blends based on Dungeons and Dragons. The [Black Phoenix Alchemy Lab](https://blackphoenixalchemylab.com/shop/imps-ears/imp-packs/imp-pack-rpg-classes/) has created a unique scent for each of the canonical races, classes, and alignments from D&D. Pick the three that represent your character for a completely unique scent for each character.

This strikes me as potentially useful for fiction writing as well. Smell is a powerful sense and can evoke place and emotion very strongly. Perhaps you could use scents like those from the Black Phoenix Alchemy lab to help you get into a character's head or help sink you into your fictional world so you can get into the writing Zone.

Personally, I have a very poor sense of smell. Often my wife will comment on a scent or ask me to smell something, and I just have to shrug my shoulders. If I do manage to catch the scent somehow, I usually have to ask her to identify it for me. So for me, smell is something I don't think much about, because it so seldom intrudes into my daily experience. And so I have a hard time remembering to use smell as detail when I'm writing.

What about you? How sensitive is your sense of smell? When was the last time you read, or wrote, a great description that included scent?
