---
from_wordpress: true
comments_id: 647
title: Creative Death
date: 2011-07-20T22:43:29-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=647
permalink: /post/creative-death/
categories:
  - art
  - writing and publishing
---
A while back, I found a blog post that completely revolutionized how I view my own creativity. The blog post is called "[How to Manage Your Life as a Creative Person: The Creative Lifecycle](http://astorybookworld.blogspot.com/2011/06/managing-your-life-as-creative-person.html)" by Deirdre Eden Coppel.

I had never heard of the concept of a creative lifecycle before, but it made a lot of sense to me. I've been through this cycle numerous times without realizing it. I used to think that when I went through a period of low creativity, there was something wrong with me. But thanks to this blog post, I finally realized that it's perfectly normal to have periods of low creativity while you recharge and prepare for your next creative rush.

Deirdre talks about two types of low creative periods. The "normal" type occurs at the end of a project, as you are finishing it up, and is called "creative natural death." The other type occurs in the middle of a project, leads to unfinished and abandoned work, and is called "creative murder."

In my own creative endeavors, I haven't yet had the pleasure of experiencing the so-called natural death of a creative cycle. My attempts at writing novels have all ended in creative murder so far. I think that each of those projects might have ended quite differently if I had understood the idea of a creative life cycle. Maybe they wouldn't have ended at all. Maybe I could have peacefully put them aside for a time with the understanding that I would pick them up again as soon as my creative batteries had been recharged.

I'm not sure I like viewing the end of a project as the default or normal method of coming full circle in the creative life cycle. Perhaps a better model would describe smaller continual cycles of creativity throughout a single project, and when you string all those little cycles together, you get the overall lifecycle of the entire project.

I like that idea much better, because otherwise the creative lull I'm currently experiencing feels more like another creative murder. I'd rather it were a natural lull that's part of a greater creative cycle allowing me to keep looking forward to its completion.

Have you been through a creative lifecycle before? Have you experienced creative murder? How did it feel?