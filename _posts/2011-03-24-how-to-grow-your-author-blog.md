---
from_wordpress: true
comments_id: 378
title: How to Grow Your Author Blog
date: 2011-03-24T09:05:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=378
permalink: /post/how-to-grow-your-author-blog/
categories:
  - metablogging
---
One of the misconceptions I had about starting a blog was that people would simply appear out of nowhere to read it. While I have had a few random visitors who chose to leave comments, my readers consist largely of a few fellow aspiring writers who I either know in real life or who know one of those people. There are a few things I've learned by cruising around the web.

  1. One of the most common pieces of advice I've heard about how to grow your author blog is to <font size="3" color="gray">be consistent</font>. Get yourself on a schedule and stick to it. Once a week. Twice a week. Whatever. As long as your readers can anticipate that post. I've never heard anyone specifically mention frequency, but it's almost universally implied that you should write at least once a week. Any less than that, and people will get bored and forget you.
  2. One of the best pieces of advice I've heard came from [Michelle Davidson](http://theinnocentflower.blogspot.com/). She's built quite a large and loyal following on her blog, and according to her the key was to find other bloggers and become a regular, <font size="3" color="gray">thoughtful commenter on their blogs</font>. Then those bloggers and their followers eventually find their way back to your blog, and your audience grows. To implement this strategy, I've started checking out each of Michelle's followers to find bloggers who seem to have some common ground with me (other than being a writer) and begin commenting on their blogs. The problem with this strategy is that it's time consuming. As a father of three with a full time job, I don't exactly have hours every day to spend commenting on blogs. So I'm trying to target blogs that seem particularly promising so I can be as efficient as possible.
  3. Another thing that goes without saying is that your <font size="3" color="gray">content needs to be interesting</font>. Your content needs to be interesting enough that people feel compelled to comment and share with others. I struggle with this one. I often write about stuff that I personally find interesting, but those sometimes turn out to be my least successful posts (using the number of comments as a gauge).

This is where the following blog post comes in:

#### [10 Steps to Making Your Author Blog a Rockin’ Success](http://victoriamixon.com/2011/03/23/10-steps-to-making-your-author-blog-a-rockin-success/ "Permanent Link to 10 Steps to Making Your Author Blog a Rockin’ Success")

It's written by Judy Dunn, owner of [Cats Eye Writer](http://www.catseyewriter.com), where she shares information about content marketing for authors. Check it out, then pop back here and let me know what you think.

Judy's post is full of awesome, but the part that really struck me was the advice on <font size="3" color="gray">identifying and focusing on your audience</font>. It made me wonder if maybe I write about too many different things. Not just writing, but music, games, my life, and so on. Perhaps I could try to tie these things into a central theme that resonates with a particular audience.

**What's your purpose for owning a blog?**

**What advice do you have about growing your blog?**

**Who is your audience? How do you identify them, and how do you keep them coming back?**