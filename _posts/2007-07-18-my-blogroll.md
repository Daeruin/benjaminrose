---
from_wordpress: true
comments_id: 27
title: Check out my blogroll! It rocks!
date: 2007-07-18T22:45:01-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=27
permalink: /post/my-blogroll/
categories:
  - 'life &amp; stuff'
---
It struck me the other day that some of my readers may have looked at my blogroll (the links under Blogs I Read over to the right) and wondered "Why should I care about these other blogs?" Well, here's why.

  * [Aestril](http://aestril.wordpress.com/ "A sister-in-law's blog.") calls her blog "Oh, Who Knows . . ." Well, I know: it's a blog featuring some really good writing that I think you'll enjoy. Drop by and enjoy her fiction, and say hi while you're there.
  * [Blog Ing](http://blogging-ing.blogspot.com/ "My brother's blog.") is my brother's blog. Not much to see there, yet, but anyone who knows him will agree it's bound to be some of the most incisive, witty stuff in the blogosphere.
  * [Married to a Chef](http://marriedtoachef.blogspot.com/index.html "A sister-in-law's blog.") belongs to my sister-in-law, who, as you may have guessed, happens to be married to a chef. They're both great. So are their pastries.
  * [Normal Pains and Pleasures](http://normalpainsandpleasures.blogspot.com/index.html "My wife's blog. She's great.") is my wife's occasionally updated web log. Go see the pictures of my wonderful little boy and get a little insight into my family.
  * [Ride On](http://lackhand.blogspot.com/ "My cousin's blog.") is my awesome cousin's blog. He's one of the busiest guys I know. As if full time school, working, and wife and new baby weren't enough to occupy his time, he tries to post occasionally to his blog.
  * [Write On](http://riotimus.blogspot.com/ "A fellow writer and friend's blog.") comes from the mind of a fellow writer, Riotimus. You're sure to enjoy his dry wit along with the occasional Jack Jackson story. "'I bet you don’t have any smart-allicked thing to say to me now that I’ve crushed your laranix.' _Larynx_, thought Rob Roberts, _Larynx_, as he sank down onto the tree trunk."
