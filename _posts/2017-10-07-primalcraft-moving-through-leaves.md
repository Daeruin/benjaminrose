---
from_wordpress: true
comments_id: 1339
title: "Primalcraft: Moving Through Leaves"
date: 2017-10-07T20:05:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1339
permalink: /post/primalcraft-moving-through-leaves/
categories:
  - Minecraft modding
---
I recently finished Primalcraft's knapping feature, and I felt the need to tackle something a little easier for a while. So last night I started trying to figure out how to make it so the player can move through leaf blocks.

One of the less realistic parts of Minecraft is that leaf blocks are completely solid. If you can manage to get on top of a tree, you can walk around on top of the leaf blocks just like you were on solid ground. In Primalcraft, I wanted to do away with that.

During my research last night, it started to seem like this feature wouldn't be possible. I found a couple of older mods that had done it, but only by using ASM. Frankly, I don't even know exactly what ASM is, but I know it's kinda complicated and is sometimes frowned upon. I wanted to accomplish this using regular features of Forge.

Most things in Forge you do through the use of events. Events are like little hooks placed into Minecraft's code. You can latch onto the hook and interrupt Minecraft's normal flow with your own code. But after looking over the list of available events, I couldn't find one that seemed appropriate. For a while, I tried playing around with the PlayerTickEvent, but couldn't get access to any blocks that way.

Then I posted on Forge and realized I had been looking at an old list of Forge events. Turns out there's one that hooks right into what I needed: the point where collision boxes are being processed. After that, it took me less than an hour to figure things out.

So now, in Primalcraft, players move _through _leaves. This also happens to avoid the annoying problem of being able to get around in forests. In vanilla Minecraft, you have to go around low trees. Now you can just go through them. To compensate a little, I made it so you move slower than usual by 20%. So if you're trying to run away from wolves at night, they will still catch up to you.
