---
from_wordpress: true
comments_id: 865
title: "Michael Komarck: My Favorite Fantasy Artist"
date: 2012-05-16T21:26:54-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=865
permalink: /post/michael-komarck-my-favorite-fantasy-artist/
categories:
  - art
---
Today I just wanted to take a moment to highlight my current favorite fantasy artist: Michael Komarck. He has done lots of art for popular fantasy lines like A Song of Ice and Fire (calendar and card game), Magic: The Gathering, The Wheel of Time e-book covers (see my post on the [Knife of Dreams e-book cover](/post/knife-of-dreams-e-book-cover/ "Knife of Dreams E-book Cover")), and many others. He has been nominated for a Hugo award this year, and I think he should win.

I'm just going to link to several of my favorites over from [Michael Komarck's official website](http://komarckart.com/index.html). I highly recommend that you take a few moments and browse around his website. There's some really awesome stuff over there.

{%
  include figure.html
  image-name="michaelKomarck_dragonsInTheArchives.jpg"
  link="http://komarckart.com/bk_cov02.html"
  alt="Dragons in the Archives"
  caption="Dragons in the Archives<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_gardensOfTheMoon.jpg"
  link="http://komarckart.com/bk_cov33.html"
  alt="Gardens of the Moon"
  caption="Gardens of the Moon<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_insideStraight.jpg"
  link="http://komarckart.com/bk_cov25.html"
  alt="Inside Straight"
  caption="Inside Straight<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_theArtOfGeorgeRRMartinsASongOfIceAndFire.jpg"
  link="http://komarckart.com/bk_cov10.html"
  alt="Jaime Lannister on the Iron Throne"
  caption="Jaime Lannister on the Iron Throne<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_orissSamiteGuardian.jpg"
  link="http://komarckart.com/ccg_mtg01.htm"
  alt="Oriss, Samite Guardian"
  caption="Oriss, Samite Guardian<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_scytheTiger.jpg"
  link="http://komarckart.com/ccg_mtg21.htm"
  alt="Scythe Tiger"
  caption="Scythe Tiger<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_luminarchAscension.jpg"
  link="http://komarckart.com/ccg_mtg22.htm"
  alt="Luminarch Ascension"
  caption="Luminarch Ascension<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_veteranArmorsmith.jpg"
  link="http://komarckart.com/ccg_mtg17.htm"
  alt="Veteran Armorsmith"
  caption="Veteran Armorsmith<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_angelicBenediction.jpg"
  link="http://komarckart.com/ccg_mtg10.htm"
  alt="Angelic Benediction"
  caption="Angelic Benediction<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_dojiSeo.jpg"
  link="http://komarckart.com/ccg_l5r17.html"
  alt="Doji Seo"
  caption="Doji Seo<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_rafiqOfTheMany.jpg"
  link="http://komarckart.com/ccg_mtg08.htm"
  alt="Rafiq of the Many"
  caption="Rafiq of the Many<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_serGregorClegane.jpg"
  link="http://komarckart.com/ccg_agot03.html"
  alt="Ser Gregor Clegane"
  caption="Ser Gregor Clegane<br/>Click image to buy a print!"
%}

{%
  include figure.html
  image-name="michaelKomarck_blancHarnoisAnachronism.jpg"
  link="http://komarckart.com/ccg_ana20.html"
  alt="Blanc Harnois"
  caption="Blanc Harnois<br/>Click image to buy a print!"
%}

If you haven't visited [Michael Komarck's website](http://komarckart.com/index.html) yet, it's really, truly worth while. Awesome art awaits your viewing pleasure.
