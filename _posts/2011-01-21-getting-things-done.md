---
from_wordpress: true
comments_id: 317
title: Getting Things Done
date: 2011-01-21T18:36:57-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=317
permalink: /post/getting-things-done/
categories:
  - 'life &amp; stuff'
  - software
---
Normally, I'm a very organized kind of guy. Since starting to work from home, however, I've found it more of a challenge. Perhaps it's due to the challenge of trying to separate my work life, family life, and personal time when everything is now located on one computer. Or perhaps it's because there's nobody to impress anymore. My lovely wife could care less how organized I am and in fact usually makes light fun of my compulsion.

So I've started looking into some ways of staying organized. One that really appeals to me is David Allen's Getting Things Done® (I can't believe he has that phrase trademarked), or GTD. I won't bore you with the details. The nice thing about this method is that there are numerous software programs for the Mac made specifically to help implement GTD. So I've spent an unfortunate amount of time this week trying to figure out which one can help me get more done stay organized.

Probably the most interesting one is called [Life Balance](http://www.llamagraphics.com/products). I think it could help just about anyone, regardless of whether you want to try the GTD method or whether you just want to get more done, be more organized, or strike a better balance in life. The basic idea is that you define about 2 to 7 life goals or functional areas of your life. Then you take these areas and tell Life Balance how important each one is to your whole life by way of a pie chart. You just grab the borders of each slice and drag it around with the mouse. I defined eight areas: Work, Health, Finances, Family, Fun, My Business, Chores, and Writing.

Within those areas you start to fill in smaller goals, projects to help you achieve your goals, and small tasks—anything you need to get done or remember. Each one can occur just once, regularly, or on a specific date. As you fill these in, you can set an importance and a level of effort. When you're done, you go to your To Do list. The software takes into account everything you've entered—deadlines, levels of importance, and so forth—and tells you what to do.

There's one more major feature called Places. You can use Places to filter your big To Do list down to stuff you only want to work on right now. These are things like At Work, Errands, and At the Computer. For me, I'm always at the computer, or at least nearby. I rarely leave the house these days. For me, it made more sense to use slightly more conceptual spaces to help me focus, such as Work Time, Personal Time, and About the House.

I'm enjoying Life Balanced quite a bit. There are a few others I've tried out, like [Midnight Inbox](http://www.midnightbeep.com/products/midnight-inbox/), [Things](http://culturedcode.com/things/), and [OmniFocus](http://www.omnigroup.com/products/omnifocus/). They have sexier user interfaces (and I'm a sucker for beautiful software), but in the end Life Balance made more sense in my head. It has a 14-day free trial. If you're having a hard time keeping balance in your busy life, maybe you should check it out.

What about you? How do you stay organized?
