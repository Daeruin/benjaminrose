---
from_wordpress: true
comments_id: 871
title: Working on the Novel Outline
date: 2012-05-03T19:27:49-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=871
permalink: /post/working-on-the-novel-outline/
categories:
  - writing and publishing
---
I am working on the outline for my novel, and it's coming along slowly but surely. I have an almost complete beat sheet. It's not a huge deal, but it's been a while since I posted anything about my writing, and I just wanted to let everyone know that I'm still working on something. I've joined an online writer's group and submissions are due on the 5th, so I'm scrambling to get this outline ready to submit. From what I've heard, nobody has ever submitted an outline to the group before. I'll be the first. Hopefully it's not too boring.

Here's a brief description of the book:

> Hakka, a world-weary dwarf, is leaving his ancestral home to get away from their oppression and lies when he encounters a lost elf boy who needs help protecting a powerful artifact from an invading army. Hakka decides to protect the helpless boy, but a cruel and ambitious orc chieftan named Tegua wants the artifact too. Tegua is willing to kill anyone in his way, and if he succeeds, everyone Hakka loves will be destroyed by the coming invasion. To protect the boy and keep the artifact away from the invaders, Hakka must return back to the corrupt city that has already betrayed him or face the oncoming horde alone.

Well, I'd better get back to it.