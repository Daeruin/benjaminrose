---
from_wordpress: true
comments_id: 124
title: Adventures in Sci-Fi Publishing
date: 2009-07-27T19:40:25-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=124
permalink: /post/adventures-in-sci-fi-publishing/
categories:
  - fiction
---
[Adventures in Sci-Fi Publishing](http://www.adventuresinscifipublishing.com/) is back in production! I'm just so happy. It's easily my favorite podcast. Shaun Farrell is an excellent interviewer, and Sam Wynns makes a great co-host. The first episode after the big break is an [interview of Greg Van Eekhout](http://www.adventuresinscifipublishing.com/2009/07/aisfp-78-greg-van-eekhout/), author of _Norse Code_. Since I'm writing a story based in part on Norse mythology, I was really interested in this one. I was a little disappointed to find out that the story was based in Los Angeles (IIRC). I don't get into modern fantasy much. But I liked the interview enough to [check out the first few chapters](http://writingandsnacks.com/?p=104) of the book. Shaun's interviews do that to me. I liked what I read, and now I want to [win a free copy of the book](http://www.adventuresinscifipublishing.com/2009/07/win-a-free-copy-of-norse-code/).

If you own an mp3 player and you like sci-fi or fantasy literature, you owe it to yourself to check out Adventures in Sci-Fi Publishing. It's great to listen to during your commute, your daily jog, or whatever.
