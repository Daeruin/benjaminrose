---
from_wordpress: true
comments_id: 35
title: Sword of the New World
date: 2007-09-08T00:00:25-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=35
permalink: /post/sword-of-the-new-world/
categories:
  - gaming
  - software
---
"Why we play fantasy games, part three: Because special attacks that make the air around you glow and pulse with magical life are much cooler than head-butting an angry tramp."

"Why we play fantasy games, part four: Because we like big, shiny swords. And also, because it's frightening outside, with its unnecessary brightness and horrible blue roof."

So true. If only I'd come up with such cleverness on my own. Alas, I had to copy it from [another Web site](http://www.eurogamer.net/article.php?article_id=77356) that reviews computer games.

The link in the previous sentence goes to a review of a new game I ran into this week. Although I don't think very many of my readers are into MMOGs (massively multiplayer online games), I think some of you may be interested in the game for other reasons. So what's the game? Why, Sword of the New World, dummy. (Yes, I like to talk to myself.)

I was first attracted to Sword of the New World when I heard it was free. That's right, free. You can download it from their Web site and play for free. Until your character reaches level 20, that is. Then you start paying. Still, it's a nice way of letting you decide whether you're going to like the game before having to pay for it.

The real reason I'm talking about Sword of the New World, though, is it's aesthetic coolness. The game looks fantastic. Unlike many other MMOGs, it's based on Europe's colonial times. You create an entire family of adventurers exploring a new world to try and save your mother country from hostile invasion. Swashbucklers, musketeers, and pirates abound. The whole design of the game is based on the 17th century Baroque style, and it looks fantastic. Check out some of the screen shots and movies on [the game's Web site](http://www.swordofthenewworld.com). Very cool. Even if you don't like gaming, you should be able to appreciate the talented artwork and quasi-historical adventuresome feel to the game.

For my gaming readers, Sword of the New World has a few other interesting features. As I already mentioned, you play an entire family of adventurers. They hang out in your "barracks," an opulently furnished family headquarters, when they're not out doing stuff. When you log on, you choose three, yes three, characters to take out adventuring. You control them all at the same time. So no more worrying about trying to find a healer to join your group. You've got room for your own. It seems to me that this gaming mode would open up a lot of interesting possibilities that would keep game play consistently stimulating.

Enough about that. Because in reality, this whole post was just an excuse to post those two hilarious quotes up at the top. And also, I'm trying to avoid that horrible blue roof for as long as possible.
