---
from_wordpress: true
comments_id: 422
title: More Majestic Maps
date: 2011-04-19T19:29:59-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=422
permalink: /post/more-majestic-maps/
categories:
  - art
---
Look at these cool maps. Just look and salivate.

OK, I'd better say a little more than that! These maps were all found on [Cartographer's Guild](https://www.cartographersguild.com/forum.php), a site I found thanks to helpful commenter [drow](https://donjon.bin.sh/). Each map has its own unique style. Click on the map to view the full-size version over on Cartographer's Guild. The detail in these maps is amazing. You won't regret it.

**Edit**: You will have to click on over to the [Cartographer's Guild](https://www.cartographersguild.com/forum.php) and register to view some of the maps. But trust me, it's worth it. These are freaking awesome maps, and I want you to be able to see them.

&nbsp;

**Tarnath** by Troedel

[![Tarnath](https://www.cartographersguild.com/attachment.php?attachmentid=30764&d=1289136445 "Tarnath"){: width="500"}](https://www.cartographersguild.com/attachment.php?attachmentid=30801&d=1289389422)

The map of Tarnath was created by overlaying transparent images from profile maps onto the existing outlines and then blending and colorizing them. If you look at the full size version of any of these maps, please make it this one. Zoom in and look at the amazing detail on the mountains and river valleys. I think that of all the maps I've seen so far, I like this one's style the best.

**The Eastern Continent of Tarnath** by Troedel

[![The Eastern Continent of Tarnath](https://www.cartographersguild.com/attachment.php?attachmentid=33620&d=1297978053 "The Eastern Continent of Tarnath"){: width="500"}](https://www.cartographersguild.com/attachment.php?attachmentid=33620&d=1297978053)

This is a representation of the easternmost continent of Tarnath. The style is somewhat different, and the place names are in German rather than English, but you can tell it's the same continent. This map has an almost creepy feeling to it—the forests resemble growing mildew, the coastline looks stormy as if the waves were breaking against the shore, and the colors make it feel like dusk is settling on the land. It almost makes me shiver!

&nbsp;

**The Khanate of Montray** by Diamond

[![The Khanate of Montray](https://www.cartographersguild.com/attachment.php?attachmentid=34618&d=1301069265 "The Khanate of Montray"){: width="500"}](https://www.cartographersguild.com/attachment.php?attachmentid=34618&d=1301069265)

The Khanate of Montray is a post-apocalyptic version of part of California, which you'll quickly recognize once you start looking at some of the place names. The style of this map is a little comic-booky, if that's the right word. The mountains, hills, and forest icons were taken from a set of Photoshop paintbrushes created by a member of Deviantart. I've lost the link, but I'm sure I could help you find it if you're interested.

&nbsp;

**Mír'aj** by Ascension

[![Mir'aj](https://www.cartographersguild.com/attachment.php?attachmentid=33871&d=1298568412 "Mir'aj"){: width="500"}](https://www.cartographersguild.com/attachment.php?attachmentid=33871&d=1298568412)

This one has a definite Arabic type feel to it, which is partly due to the desert setting and colors, but in many ways is due to the excellent font choices, especially the Arabic-style script around the edges. This is another map that will be better appreciated if you zoom in.

&nbsp;

**Demagoria** by Katto

[![Demagoria](https://www.cartographersguild.com/attachment.php?attachmentid=16783&d=1252758555 "Demagoria"){: width="500"}](https://www.cartographersguild.com/attachment.php?attachmentid=16783&d=1252758555)

I love the old style of this map. Not much detail in the way of terrain, but I like the subtle colors that represent political delineations, the parchment texture, and the antique-looking icons in the corners.

&nbsp;

Which one is your favorite, and why? Which would you rather see inside a fantasy book?
