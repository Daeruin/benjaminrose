---
from_wordpress: true
comments_id: 158
title: "LTUE Report: How to Write a Story that Rocks"
date: 2010-03-01T22:41:10-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=158
permalink: /post/ltue-report-how-to-write-a-story-that-rocks/
categories:
  - writing and publishing
---
And now you get the first of my reports on [LTUE](http://www.ltue.org/LTUE2010.html). I'm planning to write brief summaries of some of the sessions I liked the best, along with some specific things I learned. I have a horrible aural memory, so this is also a way for me to try cementing some of these things in my mind. I hope you also get something out of them.

**How to Write a Story that Rocks**

This session was by far my favorite, and thanks to the magnanimity of the presenters and the techno-wizardry of their friends, [you can get the handouts and watch a video recording of it](http://johndbrown.com/2010/02/how-to-write-a-story-that-rocks/), too. And as an extra bonus, you can also see a shot of the back of my head from some of the camera angles. The camera shot looking straight on at the presenters shows my head near the left hand side. It's the very short, blond, starting to bald head. My sister's dark curly hair is right next to me.

The presenters were engaging and enthusiastic. [John Brown](http://johndbrown.com/) is a local Utah author from the boonies of Rich county. I've heard him talk about writing on the [Writing Excuses](http://www.writingexcuses.com/) podcast, and thought he seemed articulate and interesting. I also attended a reading by him during LTUE, and really enjoyed it. I am planning to read his book, _Servant of a Dark God_. You should check out his [free short story on his website](http://johndbrown.com/fiction/); it's a very good read. The other presenter was [Larry Correia](http://larrycorreia.wordpress.com/), another local Utah author whose books have been selling well enough for him to quit his day job. (Incidentally, one of my coworkers knows him; I only wish I had discovered this fact before the conference so I had something to talk to him about.) Larry is really funny and a nice guy.

OK, enough of that. Down to the presentation itself. Since the entire presentation is available at the above link, I'm going to focus on a few key things that impressed me.

First, they stressed how important it is that you have something to say—something that's alive in your mind, something that excites you. Big revelation, huh? Well, it was to me at the time. The story I started working on last year (wait, two years ago—crazy) had undergone some changes that I made because of imagined worries about saleability and what my potential readers might want. Completely ridiculous, I know. But it had reached a point where my love for the story had dwindled enough that I didn't have the right passion for it any longer. Yet there were some things I really liked about it. I had come up with a few really cool ideas. And as I thought about it after this presentation, I realized that I couldn't reconcile the cool stuff with the not-so-cool stuff anymore, or at least to do so would change the story so utterly as to be unrecognizable.

So I decided to start something completely new. I took the presenter's advice and made a list of my favorite fantasy stories, because that's my favorite genre and it's what I want to write. Then I made a list of all the coolest things from those stories, another list of the coolest character concepts from those stories, and another list of the coolest magic system components. I'm using those lists as a touchstone as I brainstorm for my new story. I'm not stealing any ideas (yet). I'm just using those ideas as a gauge for the coolness of my own ideas.

On my way home from the conference that night, I was humming with creative energy. I thought, first and foremost, my story needs _magic_. Knock-your-socks-off, awesome magic. I want to write fantasy, for crying out loud. Why else would I do that if not for the magic? Well, I can think of some legitimate reasons, but none that really make me excited to write. This was one of the main failures of my first story idea, which I am now abandoning. The magic system in that story did not excite me.

Driving on the freeway after the conference, I had a vision of a mage working magic that was unique and intriguing. Over the past few weeks, I've had several more ideas to flesh the system out, and it has turned into something that, I must say, is pretty awesome. I can't wait to write a story about it and show people how cool it is.

Well, this has gone on longer than I intended. I'll report on a few more gems that I got from this seminar later.