---
from_wordpress: true
comments_id: 644
title: "Kevin J. Anderson's Terra Incognita, an Experiment in Cross-Promotion"
date: 2012-03-08T21:19:31-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=644
permalink: /post/kevin-j-andersons-terra-incognita-an-experiment-in-cross-promotion/
categories:
  - fiction
  - music
tags:
  - kevin j anderson
  - progressive rock
---
![The Edge of the World by Kevin J. Anderson](https://i.gr-assets.com/images/S/compressed.photo.goodreads.com/books/1344271649l/6014190.jpg){: .image-wrap-left width="280px"} The author Kevin J. Anderson has done an interesting experiment to help promote his latest series of fantasy novels: he's collaborated with some musicians to produce an album full of music based on his novel. He and his writer wife Rebecca Moesta wrote the lyrics and the music was written by [Erik Norlander](http://en.wikipedia.org/wiki/Erik_Norlander), a keyboardist and composer who has worked with many progressive rock outfits over the years.

The books part of a trilogy called [Terra Incognita](http://www.wordfire.com/index.php?option=com_content&view=category&layout=blog&id=60&Itemid=209), set in a sort of Age of Discovery type setting. The three books are The Edge of the World, The Map of All Things, and The Key to Creation.

The album is called Terra Incognita: Beyond the Horizon. It features an impressive array of musicians, including vocalists James LaBrie (Dream Theater), Michael Sadler (ex-Saga), John Payne (Asia Featuring John Payne), and Lana Lane (the Queen of Symphonic Rock). Performers include David Ragsdale (Kansas), Gary Wehrkamp (Shadow Gallery), Kurt Barabas (Amaran's Plight), Chris Brown (Ghost Circus), Chris Quirarte (Prymary), and Mike Alvarez. There are a lot of big names in that list. You can find info about the album on the same page as the books via the link in the previous paragraph.

I first heard about this in a writing class where we were talking about marketing and promotion. This experiment is a good example of cross-promotion, where you try to catch people who might easily cross over into new markets. Kevin J. Anderson reasoned that many fantasy fans are also fans of progressive rock, which is true in my experience. It's certainly true of me, and I was really intrigued by both the books and the album.

I haven't read the books yet or listened to the whole album, but I'm not sure I ever will. I looked into them and didn't see anything that excited me right off the bat. I listened to [clips from the music tracks](http://www.amazon.com/Terra-Incognita-Beyond-the-Horizon/dp/B0052FDKKU "Clips of Terra Incognita: Beyond the Horizon"), and unfortunately it's just not my thing. One of the difficulties of progressive rock is that it actually encompasses a very wide range of styles—that's part of what makes it progressive. It's been several months—heck, maybe a year—since I listened, but I seem to remember a lot of synthesizers, which almost always turns me off. But maybe that's your thing; I'll leave it up to you to investigate and decide. [**Edit**: I listened to the whole album on MOG the other day, and it confirmed my initial opinion. It's definitely not my thing. In fact, I found it quite awful, but that's my own bias speaking.]

I also looked at the book reviews on Amazon and checked out the reviews. What I saw was very surprising—the first book in the series has one of the worst ratings I've ever seen on Amazon.

Bear with me for a moment while I talk about Amazon ratings. In general, Amazon ratings follow a very standard curve. I'm just guessing here, but in my experience about 75% of the ratings are 5 stars, 10% are 4 stars, 5% are 3 stars, 1% or less are 2 stars, and about 10% are 1 stars. The average rating is generally about four stars. In other words, the rating system is basically worthless, since almost all products end up with the same distribution of stars and the same average score, within a very small range. (OK, I actually just looked this up to see if there was any legitimate information on this, and apparently I'm right, according to [this scientific study](http://dl.acm.org/citation.cfm?id=1562800&dl=ACM&coll=DL&CFID=69683902&CFTOKEN=18826127).) There are only two times you can learn something useful from Amazon ratings. One is by reading the content of the reviews for trends—common complaints, information about features you wouldn't otherwise know, and things like that. The other is when you find a statistical anomaly that actually has less than 75% of the ratings in the 5-star category. That's when you know something probably sucks.

Pardon my little rant on Amazon, but it helps put the rating of these books into perspective. The Edge of the World, first book in the Terra Incognito series, has a mere [28% in the 5-star category](http://www.amazon.com/Edge-World-Terra-Incognita/product-reviews/0316004197/ref=cm_cr_dp_all_summary?ie=UTF8&showViewpoints=1&sortBy=bySubmissionDateDescending). That's practically unheard-of for a product on Amazon. To be fair, the average is still 3 1/2 stars, and there may be several good reasons for the odd distribution. First, there are only 25 total reviews, so the sample size is small. Second, Anderson is mainly known for his science fiction, and some of his SF fans may simply be biased against his fantasy work. There's also the fact that Anderson has written over 100 novels, 47 of which have been on some kind of bestseller list, and has won numerous awards. So there is a chance these books actually are good, but I haven't yet seen any direct evidence that is convincing enough to make me spend my money and my time on them.

So these last three long-winded paragraphs have just been my way of saying I'm not planning to read them despite the interesting cross-promotion.

It's a great idea, though.

**[Edit**: I just found out that Kevin J. Anderson has been picked to write a novelization of Rush's new album Clockwork Angels. Read my blog post about it [here](/post/what-do-rush-and-kevin-j-anderson-have-in-common/ "What Do Rush and Kevin J. Anderson Have in Common?").]
