---
from_wordpress: true
comments_id: 120
title: The First Law Trilogy
date: 2009-07-01T21:24:26-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=120
permalink: /post/the-first-law-trilogy/
categories:
  - fiction
---
I recently completed _The FIrst Law_ trilogy and wanted to share some of my thoughts on it. _The First Law_ consists of _The Blade Itself_, _Before They Are Hanged_, and _Last Argument of Kings_. I find the titles all fresh and intriguing, if a little confusing. They're all quotes from history. "The blade itself incites men to violence" is from Homer. "We should forgive our enemies, but not before they are hanged" is Heinrich Heine. (Yeah, who? He was a German poet in the 19th century.) "Last Argument of Kings" was the inscription Louis XIV put on his cannons. (The book has nothing to do with cannons—although it does feature blasting powder—but the reference is metaphorical to a certain awesome supernatural force involved in the story.)

I really enjoyed reading this series. Joe Abercrombie brings a fresh voice to some tired fantasy tropes. His series has both grit and humor, an interesting combination, and turns some of the conventions of the genre on their head.

There's a part in the first book where this barbarian character is looking for a wizard. He finds the tower, passing through a little village. There's a blacksmith at the forge and a bald butcher cutting up a cow. There's a guy in white robes with a flowing white beard sitting on the steps of the tower—who turns out to be the butler. The wizard turns out to be the guy butchering the cow. In the second book there's a typical fantasy quest for a magical object, but it turns out to be a complete failure (and yet manages to be fun to read). And so on.

My favorite character was Sand dan Glokta, formerly a dashing and successful young officer who was captured and spent two years being tortured in a dungeon. He's now a cripple, a complete physical wreck who can barely get out of bed in the morning, and he's become a torturer himself, having learned from the best. He's cynical, merciless, and clever, and it's an absolute blast to listen to his sarcastic inner monologue as he bribes, threatens, and tortures his way through the dangerous political landscape—and, despite his pragmatic ruthlessness, proves that he's still basically a good person (when circumstances allow). All the characters have this excellent mix of moral ambiguity about them, which makes the story feel that much more authentic.

But the main thing I really loved about these books was the gritty violence. I've always been a bit of a violence monger in stories. Not necessarily gratuitous violence, or even graphic violence. In fact, some graphic violence really turns me off (particularly in modern war movies). But I love me a bit of good action, especially if it involves some cool swordplay. The First Law books have that in abundance. Abercrombie writes great action scenes. It's no-holds-barred combat, messy and brutal and intense.

Still, I have to admit that after three books worth, I became somewhat inured to it. The later fight scenes didn't thrill me as much and they started to lose their impact. In the third book, there's a mammoth conflict, with three different armies including various supernatural forces converging to wreak havoc on the same city. It takes around a hundred pages to get through, jumping around between all the various viewpoint characters (remember this is epic fantasy). And this is where Abercrombie really impressed me. After this huge climax was over, there was still a hundred pages left—and _I wanted to keep reading_! I was more excited about the psychological fallout and how characters were going to resolve their personal problems than I was about the massive battle. For me, that's an impressive bit of storytelling.
