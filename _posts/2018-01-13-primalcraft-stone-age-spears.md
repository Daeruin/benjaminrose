---
from_wordpress: true
comments_id: 1490
title: "Primalcraft: Stone Age Spears"
date: 2018-01-13T18:28:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1490
permalink: /post/primalcraft-stone-age-spears/
categories:
  - Minecraft modding
---
I took a few weeks off from modding to build some shelves in our basement storage room, and then the holidays got all crazy, with lots of sickness in the house. After more than a month away, I'm back at it again.

This time I bring you a feature I'm really excited about: **Stone Age spears**.

![Stone Age spears in Primalcraft](/assets/images/primalcraft_stoneSpear.png){: width="750px" height="422px"}

I had been pretty intimidated at the prospect of tackling this feature, because I knew it involved Minecraft _entities_. But I realized just last week that I could probably take on the challenge now. I've already built the ability to create thrown weapons, and they're also entities. The main thing that's different about spears is the fact that their texture is applied differently.

It turns out I got the whole feature done in just a few hours stretched across two or three evenings.

The funnest part about this project was researching stone spearheads from the Stone Age

![Stone Age spear head illustrations](/assets/images/knapping_projectilePointTypes.png){: width="500px" height="702px"}

I created multiple different knapping recipes to try to represent the different styles of projectile points from this illustration. I think it's fun to have a variety of possibilities for the player. My hope is that it will be relatively easy to craft spear head without needing to look at a recipe. As long as you're making something that seems realistic, it should work. This should be helped by the fact that each recipe can also vary a small amount. If you take a tiny bit of extra stone off one side while knapping, the recipe will still work.

Although the different styles don't matter mechanically, size does. Larger spear heads do more damage, but give the spear a slower attack speed. Bigger spear heads still do more damage overall.

To make this feature work, I created a new item based on a regular Minecraft sword. That makes it so spears can be used in melee combat, if you want. Then I overrode the right clicking function to let you throw the spear. The thrown spear itself is based on vanilla Minecraft arrows, with some tweaks. Spears don't disappear when you hit someone. You'll be able to find them laying on the ground nearby and pick them back up again. Nor do people end up with spears poking out all over their body after being hit. Also, spears don't have nearly the range that arrows do.

The two hardest parts were the texture, and persisting the different damage and speed values. The texture required me to create two new classes, just to assign my spear texture to the thrown spear, something that could be done in a single line of code in older versions of Minecraft. I'm not sure why that changed, but it was pretty frustrating. I ended up figuring out how to do it by looking at another mod that looks great (Geomastery by JayAvery). To keep the varying damage and speed values from crafting, to the spear item, to the thrown spear, and back to the picked up spear item, required me to use Minecraft Forge's capability system. Thankfully I'm pretty familiar with that system. (I had to use it to create my thirst feature.) It was a bit tedious, but I finally got it all working.
