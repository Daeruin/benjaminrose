---
from_wordpress: true
comments_id: 1266
title: "My Minecraft Mod: Primalcraft"
date: 2017-08-09T19:50:56-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1266
permalink: /post/my-minecraft-mod-primalcraft/
categories:
  - Minecraft modding
---
I started working on my Minecraft mod in March of 2016, so it's been well over a year, almost a year and a half. That feels insane to me. I've probably spent hundreds of hours on it. I could have written a novel by now.

After I started playing Minecraft, it didn't take long for me to feel like everything was too easy. And there were some parts of the game that really bothered me, like punching logs and floating trees. Soon I found Terrafirmacraft, which was amazing and kept my attention for many more hours. There were still certain things I didn't love. Again, the challenge seemed to die down very quickly. I found another mod, Better Than Wolves, that was even more challenging, but had even more things that bothered me. When I found out that Terrafirmacraft 2 was ditching its focus on detailed survival gameplay over to magic and adventure gameplay, I decided to embark on making my own mod.

The goal of Primalcraft is to simulate what our primal ancestors had to do to survive. Primalcraft will bring a lot more detail and challenge into the stone age part of Minecraft's gameplay, while providing a greater sense of realism. The goals I wrote down a year and a half ago were:

  * Experience survival like our ancestors did in the Stone Age.
  * Encourage exploration.
  * Encourage multiple bases with trails and roads.
  * Encourage caving.
  * Encourage building beautiful and functional things.
  * Provide a system of progression that’s challenging and rewarding.

I now realize that I was being very ambitious, especially considering I had almost zero programming experience. I have had to learn Java on top of Forge and Minecraft code. It's been challenging and really fun. And it takes forever.

For now, I'm focusing on the first bullet point—Stone Age survival. I have several major features and a bunch of minor features done. That will be the subject of the next post.