---
from_wordpress: true
comments_id: 64
title: "Word Count Challenge: Phase Two"
date: 2008-03-24T23:15:02-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=64
permalink: /post/word-count-challenge-phase-two/
categories:
  - word counters union
---
I had an idea today. Or, to phrase it like the [Hot News on Apple's site](http://www.apple.com/hotnews/) (see the announcement about Safari from March 18th), I today had an idea. It's about where the word count challenge should eventually be headed. Part of what the word count challenge accomplishes is the incremental creation of a novel. After a year, you should have a substantial chunk of novel finished, if not an entire novel (first draft though it may be). What then?

I think the word count challenge should at some point evolve into 100 words of revision every day. Maybe the commitment to revise could start when you feel confident that your 100 new words are coming habitually, so you don't have to worry about it as much, or maybe when you feel your novel is mostly there. This would be _in addition to_ the regular, new 100 words. It would take the intent of the word counting to the next level. Just something to think about for the future.

Just in case you've missed it, I've moved my word counting to the top of my sidebar along with a journal of my writing efforts that you can reach via the link in the top right corner. Don't forget to check and keep me honest!