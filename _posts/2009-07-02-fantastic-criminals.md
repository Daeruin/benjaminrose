---
from_wordpress: true
comments_id: 121
title: Fantastic Criminals
date: 2009-07-02T18:50:59-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=121
permalink: /post/fantastic-criminals/
categories:
  - fiction
---
I have noticed that a number of recently popular fantasy books feature criminal protaganists. It's kind of like a mini-trend. Here are the ones that I know about or have recently read:

  * [Mistborn](http://www.brandonsanderson.com/book/Mistborn) by [Brandon Sanderson](http://www.brandonsanderson.com). Vin is an orphan who starts out as a small time thief in a small time crew and gets recruited into a new crew with supernatural powers and ambitions of a much more political and lofty nature.
  * [The Lies of Locke Lamora](http://www.scottlynch.us/books.html) by [Scott Lynch](http://www.scottlynch.us). Locke is an orphan who becomes the leader of a small but highly skilled team of con artists.
  * [The Way of Shadows](http://www.brentweeks.com/books/) by [Brent Weeks](http://www.brentweeks.com). Azoth is an orphan in a small gang of kid thieves, and he becomes the apprentice to one of the best assassins in the business.
  * [The Name of the Wind](http://www.patrickrothfuss.com/content/books.asp) by [Patrick Rothfuss](http://www.patrickrothfuss.com/). Kvothe isn't a career criminal like the ones above, but he's done his share of criminal deeds.
  * [The Blade Itself](http://www.joeabercrombie.com/the_blade_itself.htm) by [Joe Abercrombie](http://www.joeabercrombie.com). Logen is a warrior who's killed too many men and made too many enemies. He's been outlawed by his king and he has nowhere to go.

Are there others that I've missed?