---
from_wordpress: true
comments_id: 65
title: True Names
date: 2008-03-25T23:10:37-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=65
permalink: /post/true-names/
categories:
  - fiction
  - 'life &amp; stuff'
  - metablogging
  - word counters union
---
Last night I began reading _A Wizard of Earthsea_ by Ursula K. LeGuin, and I'm really enjoying it. The reason I picked it up makes me feel kind of . . . traitorous. A week or two ago, my wife and I watched _The Jane Austen Book Club_, a feel-good, predictable drama about five women and one man who read one Jane Austen book a month, then get together and talk about it. Of course, each book has parallels in the lives of each person, and Austen teaches them all how to overcome their problems.

The one guy in the group is a fantasy geek, and nobody expected him to actually show up, so he's kind of a misfit. He's constantly trying to get one of the women to read a book by Ursula K. LeGuin, because he says it's good writing no matter the genre, and she keeps putting him off because she doesn't believe him. Eventually she reads it, loves it, and of course falls in love with him, too.

So I decided that to call myself a true fan of the fantasy genre, I probably ought to read at least one book by LeGuin, one of the original fantasists. I've picked up her books numerous times at the bookstore, but never actually started reading. You see, I have this thing with fantasy names. I just couldn't bring myself to read a book where the main character was a wizard named Ged. I mean, what was the guy going to do, discover oil in his village and move to Beverly? I had a similar problem with Robin Hobb's Assassin books, and was similarly very wrong to have so judged them. I must say, LeGuin's writing is absolutely superb.

One of the aspects of magic that has emerged from the first pages is the idea of true names as a way to exert power over things. This is one reason I've always avoided using my real name on the Internet. I fear some unknown person will gain the power to summon me and command me to do dastardly and shameful deeds. No, really, it just makes me uncomfortable for some reason. On the other hand, I always feel like a bit of a faker when using a handle online. So for whatever reason, I've decided to go ahead and start using my real name. At the very least, people will know who exactly to thank for the beautiful fiction I've posted here!

So you'll see that on the Word Count Buddies website, I've used my true name: Ben. Go ahead and summon me. I dare you.