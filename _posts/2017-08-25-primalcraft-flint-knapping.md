---
from_wordpress: true
comments_id: 1295
title: "Primalcraft: Flint Knapping"
date: 2017-08-25T15:44:38-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1295
permalink: /post/primalcraft-flint-knapping/
categories:
  - Minecraft modding
---
![Filt knapping in Primalcraft](/assets/images/knapping_howToMakeAxeHead.jpg){: width="400px" height="269px"}

Lately I've been working on flint knapping for Primalcraft. Our primal ancestors used flint and similar stones to create tools through a process called knapping. Flint was especially prized because of the special way in which it fractures to create sharp, hard edges. Obsidian was often used, too. It makes especially sharp edges but is more brittle than flint and has a tendency to shatter if you're not careful. Chert, jasper, and quartzite were also used extensively.

To knap a piece of flint or other stone, you carefully strike the edge with a round hammer stone or piece of bone to chip pieces off the edge. Flint breaks off in very thin pieces called flakes. Often the flakes were used for smaller tools like arrowheads. After you get the rough shape, you can further refine and sharpen the edge with progressively finer tools, until at the end you're using a small, sharp piece of bone or stick.

Most knapping videos are deathly boring. This one is a little less boring than some because it gets straight into the knapping without a lot of yapping (although he's using an inauthentic piece of flint that was cut with a saw).

<iframe src="https://www.youtube.com/embed/PQCpBwfO2CI" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Terrafirmacraft is the only Minecraft mod I know of that has done flint knapping. I'm planning to use the same basic idea as Terrafirmacraft, with a few twists. For example, you'll have to be holding a hammerstone in one hand and a core stone (a large piece of flint from which the initial tool-sized stone comes) in the other. When you right click, the knapping interface will open. Every piece of stone will be a slightly different shape, forcing you to strategize how to best get the right tool from that piece of stone. Each tool head will have multiple possible shapes. Some will have more durability or do more damage than others. I'm also hoping to allow the use of different tools to knap different sizes of flakes from the edges.

So far I have the knapping interface itself mostly done, and I'm working on creating the unique stone shapes. I need to create the recipes and hook them up to produce the actual tool heads. I'm learning a lot, and I expect this will take me some time. Maybe a few weeks.
