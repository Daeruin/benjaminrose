---
from_wordpress: true
comments_id: 83
title: Neil Peart, 2nd Worst Lyricist of All Time
date: 2008-05-22T21:49:59-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=83
permalink: /post/neil-peart-2nd-worst-lyricist-of-all-time/
categories:
  - music
---
**Edit**: See my post about [Neil Peart's death here](/post/remembering-neil-peart/).

According to Blender magazine's top 40 list published in the November 2007 issue, that is. For those of you who don't know, Neil Peart is the drummer and lyricist for Rush, one of my favorite bands ever. Apparently he got beaten out of the number one slot by Sting.

I was really surprised when I saw Peart's name on the list, because he's probably my favorite lyricist, hands down. Many of his songs have really inspired me. I've gotten a lot of positive, creative energy from his lyrics.

So what netted him the number two slot, according to Blender? They call his lyrics "richly awful tapestries of fantasy and science." True, he wrote a lot of fantasy in his early days. And to be honest, his fantasy lyrics are not my favorites (with the notable exception of 2112, one of the greatest rock albums of all time). But I wouldn't call them awful, not by any means, and certainly the fantastic subject matter isn't reason enough by itself to label something terrible.

Still, Blender magazine is entitled to their opinion. And just to put some perspective on what that opinion is, here is an excerpt from the lyrics of the song that topped their 100 best:

> (yoouuuuuu!!!)
>
> soulja boy tell em
>
> ayy i got this new dance fo yall called tha Soulja Boy
>
> (yooouuuu!!!)
>
> u gotta punch den crank back three times from left 2 right
>
> (aaaaaaaahhhhhhhh yooooouuuuuuu!!!)
>
> Soulja Boy Up In This Hoe
> Watch Me Crank It
> Watch Me Roll
> Watch Me Crank Dat Soulja Boy
> den Super Man Dat Hoe
> Now Watch Me YOOOOUUUU
> (Crank Dat Soulja Boy)
> Now watch me YOOOOUUUUUU
> (Crank Dat Soulja Boy)
> Now watch me YOOOOUUUUUU
> (Crank Dat Soulja Boy)
> Now watch me YOOOOUUUUUU
> (Crank Dat Soulja Boy)

Yeah. I guess they have a really good point. If only Peart could write lyrics that brilliant.
