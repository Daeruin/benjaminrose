---
from_wordpress: true
comments_id: 156
title: Working on the New Novel
date: 2010-02-28T21:58:44-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=156
permalink: /post/working-on-the-new-novel/
categories:
  - writing and publishing
---
After [attending LTUE](/post/ltue-report/) a few weeks ago, I decided that I need to start a new story. Something that I'm really passionate about. Something cool and awesome. Something epic and fantastical. On my way home from the first night of the conference, I had several cool ideas that excited me, and they've been brewing in my mind ever since. I finally had enough to write down some specifics today. So I wrote about 350 words describing a new magic system I've developed. It's kind of strange and a little creepy.
