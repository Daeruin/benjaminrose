---
from_wordpress: true
comments_id: 52
title: Nightwish
date: 2008-01-17T22:20:40-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=52
permalink: /post/nightwish/
categories:
  - music
---
I've undergone almost a complete reversal on my opinion of this band. I borrowed one of their CDs from a friend, and after a couple of listens—I'm totally hooked. A superficial listen to this band is not enough. They have such a range of musical expression in their sound that your really need to give each track a couple of listens to take it all in. And considering the album I have, Once, has thirteen tracks (one of them clocking in at over nine minutes), it takes a while.

Having said that, I'm sure some people can easily dismiss them right away. Overall, their sound can be described as symphonic metal (I've heard some people call them operatic metal due to the lead singer's vocal style), and they tend towards the dramatic, even bombastic side of things. Fans of true symphonic or operatic music will probably hate the thick, crunchy guitars, and fans of metal in general will probably fall into two categories: those who dismiss them as over-the-top nonsense, and those who think their blend of classical and metal is pure genius.

I've heard that Nightwish's music changes a lot from album to album, but I haven't heard any of Nightwish's music other than the one album I have, so I'll focus on that one. The tracks on Once are so varied that I don't think I can do the album justice without saying something about almost every song. But I'll try, because I've read many of those song-by-song reviews and they frankly bore me. How could they not be boring, unless you're already familiar with the songs?

Nightwish's best songs are the ones where the orchestral accompaniment really shines. The majority of the songs include a full orchestra, and many include a large choir. The genius behind all this is the keyboard player (and the keyboards get surprisingly little time), a fellow with a long Finnish name that I can't be bothered to look up. At some point or another, you can hear almost every instrument in the very real orchestra (no synth!), from the piccolo right down to the bassoons, and it sounds just like you're listening to a movie soundtrack sometimes. Still, it's not quite like anything you've heard before, because in front and behind it all beats the massive, brutal crunch of the electric guitars and on top soars the operatic vocals.

Unfortunately, it doesn't always work. There are about five tracks on the album that I invariably skip, because I . . . well, I almost hate them. Nightwish is sadly not free of the Drinking Song Syndrome (which I've mentioned in other posts, for those of you who may be joining my blog only recently), and some of their more experimental songs just don't work for me. An example is track four (I don't know many of the track names because I don't have the CD liner). The lyrics are about a Native American tribe, and the song incorporates a Native American flute, chanting, and someone speaking in an actual Native American language. To me, it comes across as fake and unbearably pretentious, not least because it comes from a band in Finland. How much could they seriously care about Native Americans? Maybe I'm judging them unjustly, but there it is. Oh, and the song is also rather boring.

I want to mention one other thing: the vocalist. Nightwish is a female-fronted band, like all the other bands I've been talking about so far, but on the album I have they have begun to try incorporating some male vocals as well. The mix works well, and sounds a lot like Lacuna Coil sometimes. The lead vocalist, Tarja, has a very unique style that I have mixed feelings about. Her voice is very powerful and rich with great range to it, but her vibrato is rather huge and she has a habit of sliding and scooping into her notes, a style that I find irritating. She also has a very noticeable accent, which tends to jar you out of the listening experience sometimes. Gladly, I've found that I can ignore those tendencies and focus on the good parts.

My overall rating of Nightwish is a tough call, because I have such polarized reactions to their individual songs, and even to various parts within each song. If I'm able to ignore the songs I dislike, however, I would easily give Nightwish a very high score—in fact, I'd put them right at the top of my list. When they're good, they're unbelievably good. I've been listening to them almost straight for about two months now. I think that speaks for itself.

Here are a few links in case you want to check them out. Give them a second chance if you've already looked into them. But remember, you can't really grasp the full depth of this band without giving the whole album a listen.

  * Their [home page](http://www.nightwish.com/).
  * Their [MySpace page](http://www.myspace.com/nightwish).
  * Their [Wikipedia article](http://en.wikipedia.org/wiki/Nightwish).
