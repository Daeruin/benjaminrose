---
from_wordpress: true
comments_id: 1268
title: "Primalcraft: Current Features"
date: 2017-08-11T06:51:08-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1268
permalink: /post/primalcraft-current-features/
categories:
  - Minecraft modding
---
Yesterday I talked about some of my goals in creating my Primalcraft mod for Minecraft. As I mentioned then, I'm focusing mainly on creating a detailed and challenging Stone Age survival experience. To that end, I've currently finished several major features of the mod:

  * **Stone age tools**. Your first tools are rocks and sticks. You have to find hammer stones and core stones to make stone tool heads, then bind them to a haft with cordage made from grass. (I have yet to create a nice interface for knapping the tool heads.)
  * **Basket weaving**. You can use grass and bark to weave wicker baskets for your first form of storage.
  * **Hunger and thirst**. You get hungry faster. Your first meals will be grubs and locusts. You will also get thirsty. You can quench thirst by drinking from rivers and ponds with an empty hand.
  * **Hunting**. Animals run away from you and cannot be tamed. (I intend to make it so you can throw rocks. Bows and javelins will also be available. I also intend to allow taming eventually.)
  * **Hostile wolves**. Wolves are your primary enemy. At night, they will hunt you down and eat you for dinner unless you have fire to keep them at bay. All the fantasy monsters are gone. (I want to create more stone age predators like sabretooth tigers.)
  * **Campfires**. To make a campfire, you have to rub sticks together to get a burning ember, then put it on tinder and blow to create flames. (There are no torches yet.)
  * **Copper ore**. Copper currently spawns but cannot be obtained.
  * **Gravity**. Trees require tools to chop down, and they fall instead of hovering in the air. Every block in the game will fall if not properly supported. Many blocks require certain tools and materials (like spades and mortar) to be suspended above ground, and they will fall if placed too far out vertically without support.

I have also created lots of smaller features. You start with a full 3x3 crafting grid in your inventory. There are no crafting tables. There are dirt slabs. Punching logs and stone hurts you. Baskets can be picked up by shift-right-clicking. Grass drops straw, leaves drop sticks, dirt drops grubs and stones, logs give bark, and broken tools sometimes yield parts that can be reused. There's hide armor (currently uncraftable, but it exists), and you can cut your feet on sharp rocks if you don't have shoes. There is a new kind of tree: hickory (but you can't get planks from it yet).

I'm currently in between major features, having just finished coding my hostile wolves. I haven't decided what to work on next. Whatever it is, I'll give some updates as I go.
