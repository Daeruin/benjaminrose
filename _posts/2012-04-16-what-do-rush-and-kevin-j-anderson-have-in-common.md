---
from_wordpress: true
comments_id: 852
title: What Do Rush and Kevin J. Anderson Have in Common?
date: 2012-04-16T22:43:48-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=852
permalink: /post/what-do-rush-and-kevin-j-anderson-have-in-common/
categories:
  - fiction
  - music
---
It's happening again. [Last week I posted](/post/kevin-j-andersons-terra-incognita-an-experiment-in-cross-promotion/ "Kevin J. Anderson’s Terra Incognita, an Experiment in Cross-Promotion") about Kevin J. Anderson's experiment in cross-promotion, where he hired a musician to write an album full of progressive rock music based on Anderson's fantasy novel _Terra Incognita:_ _The Edge of the World_.

Yesterday I found out that [Rush](http://www.rush.com/rush/index.php) is finally releasing a new album called _Clockwork Angels_. And they've hired Kevin J. Anderson to help lyricist Neal Peart write a novel based on the album.

_Clockwork Angels_ will be Rush's first album in I think over five years . . . which is kind of an odd way to say it, I guess, since their last three albums have each been about five years apart. Rush is flush with cash, I imagine, so they're in no hurry to get new albums out. But they're also musicians and performers to the core of their being, and just can't help writing new music. So now we have _Clockwork Angels_ coming out.

![Rush](https://consequenceofsound.net/wp-content/uploads/2019/03/Rush.png)

What makes me excited about this album is that Rush is returning to their roots. _Clockwork Angels_ is a concept album, something Rush hasn't done for what, 35 years or so? Maybe more. [**Edit:** I looked it up. Rush has never produced a pure concept album. They have done many epic songs, but there were always shorter singles included with them on the albums. The last truly epic song they wrote was Cygus X-1 Book Two: Hemispheres, released in 1978. Their next two albums each had 10-minute songs, but I don't consider them epics.] The [press release](http://www.roadrunnerrecords.com/news/rush-clockwork-angels-20120411) says: "_Clockwork Angels_ chronicles a young man’s quest across a lavish and colorful world of steampunk and alchemy as he attempts to follow his dreams. The story features lost cities, pirates, anarchists, an exotic carnival, and a rigid Watchmaker." Rush has explored epic fantasy ("The Necromancer") and science fiction (_2112_) when both were in their infancy (or maybe teenage years—it's just a metaphor, alright!), not to mention myth (_Hemispheres_). Steampunk is a relatively new development, and now Neal Peart is tackling that branch of speculative fiction.

The singles that have been released also show Rush finally innovating again musically. Their past two albums in particular have given me the impression that they were just phoning it in. The music possessed very little of the strange and interesting that were Rush's hallmark and part of what put them in the genre of progressive rock. Over the years they slowly turned back to plain hard rock, until their last two albums, _Snakes and Arrows_ and _Vapor Trails_, were very vanilla. Nothing to really interest those of us who swooned over masterful pieces like _2112_, "La Villa Strangiato," "Jacob's Ladder," and "The Camera Eye" back in the day.

I think the new era of internet distribution and the rise of the digital single as the driving force in musical sales has finally spurred Rush to rethink themselves and what they can do. They recorded and released two singles simultaneously ([Caravan](https://www.youtube.com/watch?feature=player_embedded&v=WadX98ciDtU "Caravan by Rush") and [BU2B](https://www.youtube.com/watch?v=EYTOZU9rRj8 "BU2B by Rush")) about a year ago, just because the _could_. There were no particular plans to make a whole album, let alone a concept album. They just had some creative ideas and put them out there. Then they went on tour, because that's what Rush does, with just two new songs. And people flocked to see them live again, because that's what Rush fans do.

And here we are a year or so later, and they've created an entire concept album that looks like it might reach the heights of _2112_, at least musically and lyrically, if not in sheer popularity. I'm not sure who contacted whom first—Rush or Kevin J. Anderson—but it's very cool to see this first-ever reversal: an album being turned into a work of literature instead of the other way around.

Dang. I might have to read that [_Terra Incognita_](http://www.wordfire.com/index.php?option=com_content&view=category&layout=blog&id=60&Itemid=209 "Terra Incognita") series after all, just to see whether I should be dreading or longing for the _Clockwork Angels_ novelization to come out.

_Clockwork Angels_ (the album) comes out on June 12th. Here's a teaser to whet your appetite:

<iframe width="560" height="315" src="https://www.youtube.com/embed/6SQsv0JI1P8" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
