---
from_wordpress: true
comments_id: 67
title: Just write
date: 2008-03-31T20:35:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=67
permalink: /post/just-write/
categories:
  - writing and publishing
---
Some of my readers are probably already reading the blog [Writer Unboxed](http://writerunboxed.com). Today there was an [interview of Raymond Obstfield](http://writerunboxed.com/2008/03/31/author-interview-raymond-obstfeld/), a writing teacher who's written 27 novels. His interview answers were refreshingly brief and to the point. I simply had to quote this one here on my own blog:

> "Q: Writers have lots of excuses not to write. But with 27 titles to your credit, obviously that’s not a problem for you. What advice can you offer aspiring writers to help them overcome their fears and just get on with the work?
>
> RO: My secret is that I know when I sit down to write that whatever comes out will be crap. It will probably be crap the second and third drafts. It only starts to resemble something less craplike after A LOT of rewriting. But, to me, that’s the fun part. So, I don’t beat myself up for writing badly; I just work on it until it’s better.
>
> I will admit that every book I’ve ever started I’ve wanted to quit. I always come to a point where I say I can’t write it anymore, give back the money, let me go to bed and pull the covers over my head. But I just sit down, keep writing, and eventually it’s done."

He also says the most common mistake new writers make is that they don't rewrite enough. His advice is like balm to my soul after the writing depression I was in last night!
