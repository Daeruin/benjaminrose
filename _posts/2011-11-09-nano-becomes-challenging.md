---
from_wordpress: true
comments_id: 744
title: NaNo Becomes Challenging
date: 2011-11-09T23:08:24-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=744
permalink: /post/nano-becomes-challenging/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
In this post I engage in a lot of complaining and whining. It's probably not that interesting, but I feel a pathetic need to justify my failure to achieve my daily word count goals for NaNoWriMo. Read at your own risk.

So I cruised through my first few days of NaNo without too much trouble other than staying up late to get my words in. . . then the weekend hit, and I had to work some overtime. On Sunday I had a major crash from sleep deprivation and ended up with a killer headache that kept me away from the computer screen all day. The same work project that kept me working overtime on Saturday bled into Monday and Tuesday night, and I didn't meet my word count goals either night. So as of yesterday I was a total of about four days behind, which meant that my daily word count needed to be bumped up from 1,667 to 1,883 per day if I was to finish on time.

Then today happened. It was a crazy work day, trying to put the final touches on this big project I'm trying to get done before headed out of town while dealing with a leaky furnace (yeah, furnace—apparently ours has a condensation pump that failed). We had a repair company come give an estimate on the damage, and unfortunately there's mold. So we have a big nasty repair bill headed our way unless we can get the furnace company to pay up. Their furnace repair guys showed up at 9pm and didn't leave until 10:30pm. In between all this we tried to have a birthday party for my little girl Jane. It was a pretty simply affair this time, with Missy being sick and all the crazy work and house stuff going on.

So here it is midnight, and I'm blogging about how hard it is to get lots of words written with so much crap going on, when I should be writing. This post is about 350 words, so I could have increased my NaNo word count by that much. So before I look like too much of a hypocrite, I'd better sign off and get to bed. I have a five hour drive ahead of me tomorrow and a wedding to attend the next day.