---
from_wordpress: true
comments_id: 405
title: How to Deconstruct Fantasy Tropes for Your Own Benefit
date: 2011-04-05T22:40:57-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=405
permalink: /post/how-to-deconstruct-fantasy-tropes-for-your-own-benefit/
categories:
  - writing and publishing
---
Today I thought I'd share a little bit of the work I've been doing for my current work in progress. It's going to be a fantasy story in the sword-and-sorcery style. If you want to know a little bit more about it and how it came to be, you can read this post: [New Story Idea](/post/new-story-idea/).

So here's how I'm going about it. I started with many of the standard fantasy tropes and cliches, and have been busily deconstructing and twisting them about until some of them are practically unrecognizable. First on the list was the standard fantasy races: elves, dwarves, and orcs. The technique I've been using is to take some of the cliches about these races that we take for granted and ask myself how I can justify those cliches in a "realistic" way. I put "realistic" in quotes because this is a fantasy story, and it does involve magic. What I mean by "realistic" is that there is an explanation that relates to the real world when possible, and when that isn't possible, the magic involved has a consistent internal logic to it.

**Ye Olde Dwarven Cliches**

{%
  include figure.html
  image-url="https://dungeonsmaster.com/wp-content/uploads/2010/02/dwarf1.jpg"
  width="264"
  height="317"
  alt="The Dwarf Archetype from Pathfinder"
  caption="The Dwarf Archetype from Pathfinder"
%}

So take the following cliches about dwarves as an example:

  * They like to dig tunnels underground.
  * They like to build things from metal and rock.
  * They are greedy.
  * They are short and stocky.
  * They always grow long beards.
  * Even the women.

Fueled in part by role-playing games, virtually every treatment of dwarves since The Lord of the Rings has taken these cliches for granted. As artists looking to entertain the jaded masses, we don't want to tread this same well-worn and boring path. We want to strike out boldly on our own and create something fresh and interesting while keeping a certain amount of familiar ground for the reader to cling to. It's the idea of blending the familiar and the strange.

So I've taken the standard cliches and done one of three things to each one:

  1. Kept it intact
  2. Ditched it entirely
  3. Deconstructed it

**1. Keepers**

One of the tropes I've kept is that dwarves are short and stocky. I think it goes without saying that if you make dwarves tall, they cease to be dwarves. The fact that dwarves are short is a reasonable expectation on the part of the reader.

In fact, this reveals a terminology error I haven't been careful about in this post so far. There is a difference between a trope and a cliche. Rather than exert myself, I'll simply quote another site, in this case [TVTropes.org](http://tvtropes.org/pmwiki/pmwiki.php/Main/HomePage):

> "Tropes are devices and conventions that a writer can reasonably rely on as being present in the audience members' minds and expectations. On the whole, tropes are not clichés. The word clichéd means "stereotyped and trite." In other words, dull and uninteresting."

So certain things are keepers because they're expected but haven't descended into triteness yet. We need a certain number of tropes or things become too strange and unrecognizable.

**2. Ditchers**

While a few things are keepers, we want to take a few of the old cliches and toss them right out the window. We want to keep this fresh and interesting, right? So I've jettisoned a few cliches about dwarves. First on my list was that dwarven women have beards. That's just silly and gross. I don't think I need to say anything more on that subject.

**3. Deconstructors**

Deconstructors? That doesn't sound right. Anyway, you know what I mean. There are some cliches that we want to keep, but not as they are. We want to twist them a little. We want to give them a reason to be there, to breathe new life into them.

For example, take the idea that dwarves are greedy. Greed is essentially a personality trait. Does it make sense that an entire race of people would all be greedy?

Certainly not! We don't want to treat dwarves like an exaggeration of a subset of humanity. We want to treat them as a realistic race in their own right.

So let's put our thinking caps on and try to come up with a "realistic" (notice the quotes again) reason why dwarves might universally crave the possession of _stuff_. Being the fantasist that I am, I want to relate this reason to magic somehow.

Do you want to know what I came up with?

I'm not sure I'm ready to share just yet. Maybe I want to surprise you. But I'll give you a hint.

It relates to the stereotypical dwarven beard. Because isn't the idea of magical beards something that's just begging to be used?

{%
  include figure.html
  image-url="https://www.blogcdn.com/wow.joystiq.com/media/2008/03/danielw_irondorf021s.jpg"
  width="225"
  height="235"
  alt="Dwarf"
  caption="By the magical beards of my ancestors!"
%}
