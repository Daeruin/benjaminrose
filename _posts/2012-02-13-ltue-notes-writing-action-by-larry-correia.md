---
from_wordpress: true
comments_id: 768
title: "LTUE Notes: Writing Action by Larry Correia"
date: 2012-02-13T21:49:53-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=768
permalink: /post/ltue-notes-writing-action-by-larry-correia/
categories:
  - writing and publishing
tags:
  - action
  - LTUE
---
More notes from LTUE. Yay! Today's selection comes from Larry's Correia's workshop on writing action.

I had attended the same workshop at LTUE last year and don't remember learning a whole lot. I went this year because it was the most interesting panel and I was somewhat curious to see whether it would be any better. It was. At least, I feel like I came away with a lot more specific pieces of advice. So, here we go.

It's easy for action scenes to take up a lot of space in your book compared to the rest of the story. Example: Larry's book _Monster Hunter International_. Action scenes can also be very brief. The fight scenes in _The Lord of the Rings_ are actually pretty short. Example: Boromir's final fight against the orcs. You need to know your audience and tailor your action sequences to them.

Avoid the action checklist: So-and-so did this. Then he did this. Then he did this. Then he did this. You don't have to detail every single movement—your readers have imaginations for a reason. Example: A character was facing off against policemen with only a baton. The writer spent an entire paragraph describing how the character held the baton in order to beat the policeman senseless. Eventually was revised to say "He hit the policeman with the baton." It didn't matter how he was holding it.

You need to learn as much as you can, but you don't need to be an expert. Don't let lack of knowledge intimidate you into finding out more. Talk to experts and you'll learn things you didn't even know you didn't know, such as the emotions of being in a gun fight, the effects of adrenaline, etc. Seek out people who are bona fide experts. Look up "wound ballistics" in Google and be prepared for some gore.

Mix up the type of action sequences. Don't be repetitive with the type of scenes. Instead of three shootouts, turn one into a car chase, or whatever.

Action is not a separate piece of the book. You don't stop everything else just to describe the action sequence. You can develop characters and convey plot info during action scenes—and you _should_.

Your point-of-view character determines the types and amounts of emotion in each action scene. Your writing style can even be changed depending on the POV character. Cold and calculating, full of rage, scared, etc.