---
from_wordpress: true
comments_id: 316
title: The Ultimate DnD Gaming Room
date: 2011-01-30T22:27:02-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=316
permalink: /post/the-ultimate-dnd-gaming-room/
categories:
  - gaming
---
Tonight while vising a gaming forum that I hang out at, someone posted this link. This is a guy's DnD gaming room.

[The Ultimate DnD Gaming Room](http://www.acaeum.com/forum/viewtopic.php?t=8714)

This place has a built-in strobe light and fog machine. There are surround-sound speakers on the rafters. There's a neon sign saying "Dungeons and Dragons" on the door. The walls are covered in swords, fantasy artwork, and shelves filled with games. The door requires a hand-made skeleton key to open. There are four closets with portcullises for doors.

The amount of time and money this guy must have spent on the room and all the gaming products on the shelves is astounding. Now this is what I call dedicated gaming.
