---
from_wordpress: true
comments_id: 53
title: Social Combat
date: 2008-02-10T21:39:20-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=53
permalink: /post/social-combat/
categories:
  - gaming
---
The title of this post probably sounds a bit odd out of context. I'm talking about social combat in roleplaying games. As a roleplayer, I often want to play characters who are socially adept. There's just one problem. I'm not socially adept. Regardless of how badly I want to play a glib bard or a honey-tongued nobleman, I just can't. I am forced to play dumb brutes and backwards woodsmen, if only for the sake of believability.

The reverse problem also exists. Some people have a really hard time turning off their rapier wit. They can create brutish orcs if they want, but somehow they always revert to talk. Their orc can convince Feanor to sell one of the Silmarils without even trying.

To help with mismatches in the social skills of players and their characters, I've decided to create a social combat system for roleplaying games. Characters will have a set of scores that define their social abilities, and players will choose actions like Rationalize, Placate, Escalate, Cite, and the like. Various modifiers will apply, such as cultural differences and the character's relationship with the other participants, and the player rolls dice to see how well the character does. The other participants roll their own dice to see how well they react. Of course, players are still encouraged to roleplay these actions to the best of their ability.

There are lots of obstacles to creating this system, and I'd like to start posting about some of the difficulties I'm having. But before I go ahead and assume you're interested in all those problems, let me ask a question for all my roleplaying readers: Would you really want a social combat system in the first place? Are the problems I mentioned above an issue for you at all?