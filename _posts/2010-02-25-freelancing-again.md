---
from_wordpress: true
comments_id: 154
title: Freelancing Again
date: 2010-02-25T22:39:47-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=154
permalink: /post/freelancing-again/
categories:
  - 'life &amp; stuff'
---
Just wanted to check in and account for my lack of updates on LTUE as I had promised. I had another freelance job drop in my lap, and I couldn't refuse. This one has kept me busy pretty much every free moment I would otherwise have. Not sure how long this will last. It seems like it will be a fairly long-term gig, but not always as time consuming as it is now. Once this first flurry of work gets done, I'll have a little more free time. I hope. Because it will hurt to continue putting off writing much longer.
