---
from_wordpress: true
comments_id: 1556
title: Pure Copper d20 from Norse Foundry
date: 2018-07-07T18:03:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1556
permalink: /post/pure-copper-d20-from-norse-foundry/
categories:
  - art
  - gaming
---
Today's post is about dice. I have friends who've gotten aluminum [Gravity Dice](https://store.gravitydice.com), and they are very cool. I've also been following [Artisan Dice](https://www.artisandice.com) for several years, slavering over all the interesting and unique dice they make—[ancient bog oak burl](https://www.artisandice.com/order/ancient-bog-oak-burl/), [mammoth tusk](https://www.artisandice.com/order/mammoth-ivory/), [red jasper](https://www.artisandice.com/order/red-jasper-with-copper/), and the like. And I've finally taken the plunge.

For my birthday, I got a pure copper d20 from [Norse Foundry](https://www.norsefoundry.com/products/single-d20-in-copper-by-norse-foundry).

{%
  include figure.html
  image-name="pureCopperD20_shiny.jpg"
  width="750"
  height="563"
  alt="Super shiny pure copper d20"
  caption="Shiny"
%}


I'm really excited about this die. So excited that I even made a video.

I wanted to do a video mainly to show off the size and let you hear the sound of it rolling on my wooden desk. It's incredibly satisfying to hold this die in my hand. Rolling it is a bit mixed for me. I love the massive thunk as it settles, but part of me also misses the bounciness of my regular dice.

<iframe src="https://www.youtube.com/embed/rNlAS8eJSLU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

If you're not into videos or want more details, read more below.

## Tarnish

The die arrived pretty clean, but had some grayish black tarnish in some areas. It was worst on the 19.

{%
  include figure.html
  image-name="pureCopperD20_tarnish.jpeg"
  width="500"
  height="590"
  alt="Pure copper d20 tarnish"
  caption="Tarnish"
%}


I cleaned it with a solution of vinegar, salt, and flour. I put in about 2/3 flour for a thickener, 1/3 salt for a little abrasiveness, and just enough vinegar in the make a thick paste. I used a soft cloth to rub the paste all over, then a toothbrush to clean out the numbers. The die was super clean and shiny afterwards. Unfortunately, I had already taken most of the pictures before cleaning it, and I'm too lazy to take more pictures.

{%
  include figure.html
  image-name="pureCopperD20_cleaned.jpg"
  width="750"
  height="563"
  alt="Pure copper d20 cleaned"
  caption="Cleaned with vinegar"
%}


It's kind of hard to see in this picture, but notice that the points aren't very exact. The shiny point shown here is one of the worst. The point is bit flattened on the 7 and 19 sides, so the point of the 1 doesn't quite meet the others exactly. It's only noticeable up close.

## Number engraving

The depth of the engraved numbers is pretty variable. For example, the 1, 16, and 20 are quite deep, but the 3 is barely even past surface depth. Check out the depth of the 16 versus the 3 in this image. The numbers themselves are quite serviceable. The font is clean and easy to read. I like that there's no paint or lacquer to obscure the pure copper of the die.

{%
  include figure.html
  image-name="pureCopperD20_numberDepth.jpeg"
  width="500"
  height="489"
  alt="Pure copper d20 number depth"
  caption="Engraving depth"
%}


In this picture you can also see some lines on the surface. I'm not sure if they are tiny grooves leftover from when the die was cut, or if they're a result of Norse Foundry's polishing process. I kinda like them—they give the die a bit of a raw feel. And for me, that's a big part of what this die is about. Other metal dice are probably prettier, but for me, knowing I'm holding pure, solid copper is amazing.

## Size

The die is almost exactly the same size as my other d20s. If anything, it's slightly bigger. I'm happy about this, because reviews for other metal dice that I looked at online mentioned that they were smaller than their regular dice. Not so with Norse Foundry's metal dice.

{%
  include figure.html
  image-name="pureCopperD20_sizeComparison.jpg"
  width="750"
  height="563"
  alt="Pure copper d20 size is the same as my other d20 dice"
  caption="Size comparison"
%}


This picture also shows off the variation in the crispness of the cuts. The edge at the base of the 17 is very crisp and sharp, while the edge to the left of the 17 is pretty rounded.

## Hardness

Make no mistake, copper is metal and therefore this die is very heavy. I love that. But don't roll this guy on your wooden table without expecting the wood to get dented and gouged. And for the love of all that's holy, don't roll it on a glass table top.

However, copper isn't the hardest of metals. My five-year-old son dropped my brand new copper die on our tile floor. After a minor heart attack, thinking of his poor crushed toes (he was fine—it missed him), then worrying about cracked tile (which was also fine), I eventually noticed that one of the points was dented flat. #$*%&$!#&!!! So yeah. While the copper totally could have cracked the tile, the tile won, and my precious is now damaged. And after rolling it for a while longer I'm already noticing other scratches and dings. If you don't like that idea, don't buy it or keep it somewhere very safe.

{%
  include figure.html
  image-name="pureCopperD20_dingsAndDents.jpg"
  width="500"
  height="405"
  alt="Dings and dents on pure copper d20"
  caption="Dings and dents"
%}


In this picture you can see the smooshed point on the left, as well as other dings and scratches.

## Wrap-up

This die isn't going to win any awards for exactness. The edges aren't all crisp, the points don't always meet exactly, and the depth of the numbers is quite variable. If that's important to you, get yourself some [Gravity Dice](https://store.gravitydice.com).

It's also fairly fragile for a metal die. Expect it to get a bit dinged up if you actually use it for anything. If that concerns you, then keep it safely stored in a soft place and only get it out to hold and admire, or get something more durable like a zinc-based die or something crazy like [tungsten](https://www.norsefoundry.com/products/16mm-norse-foundry-tungsten-polyhedral-dice-set).

But if, like me, you love the idea of holding the weight of pure, solid copper in your fist, this is the die for you. Thanks, Norse Foundry!

![Pure copper d20 up close](/assets/images/pureCopperD20_shinyZoomed.jpg){: width="750px" height="585px"}
