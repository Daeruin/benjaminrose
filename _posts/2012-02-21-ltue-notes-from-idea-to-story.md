---
from_wordpress: true
comments_id: 796
title: "LTUE Notes: From Idea to Story"
date: 2012-02-21T21:38:17-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=796
permalink: /post/ltue-notes-from-idea-to-story/
categories:
  - writing and publishing
tags:
  - ideas
  - LTUE
  - plotting
---
Today's selection of notes from LTUE comes from the presentation "From Idea to Story" by Jess Smiley, who is indeed a very smiley man. His presentation wasn't very dense, but he's an enjoyable guy and it was fun to listen to him. He's a comic book artist, and his slides were full of great visuals. But this was also a very hard presentation to take notes on. Jess has posted a few handouts from his presentation [online](http://cargocollective.com/jesssmartsmiley).

1. **Use mind maps.** They let you make associations between ideas. They form a record of your thought process. And they let you make interesting idea combinations—great tool for creativity and idea generation.
2. **The note card method.** Write down a beginning, a middle, and an end to a story, each on one note card. Three to five short, simple sentences. Then take each one of those, and write a beginning, middle, and end just for it. Continue expanding until you have a whole big story. You can shuffle the cards around.
3. **Practice safe design—use concepts.** Concept for Jess's comic series: Vampire + Loses Teeth + Last Witch on Earth.
  ![Upside Down: A Vampire Tale](/assets/images/upsideDownJessSmartSmiley.jpg){: width="235"}
4. **Make the process as fun** as the finished product is supposed to be.
5. **The rough draft.** This is what the whole story looks like. You work out the pacing, get to know the story better, and see what you're up against.
