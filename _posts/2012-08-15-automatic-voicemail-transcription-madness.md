---
from_wordpress: true
comments_id: 985
title: Automatic Voicemail Transcription Madness
date: 2012-08-15T15:50:10-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=985
permalink: /post/automatic-voicemail-transcription-madness/
categories:
  - 'life &amp; stuff'
  - software
---
I've subscribed to several different [VoIP](http://en.wikipedia.org/wiki/Voip) services over the years, from Vonage to Google Talk. Besides being cheaper than regular phone service, VoIP has lots of cool, modern features. Among other things, I like the convenience of accessing my voicemail online. One of the services VoIP companies are trying out these days is automatically transcribing your voicemail and sending it to your email.

Alas, computers haven't gotten as far as we'd all like on that frontier, so this almost always results in unintended hilarity. I thought I would share a few examples I've received lately.

  * _"Sauce. I thought my woman was out shopping."_ It's a new expletive. Sauce!
  * _"Hey Ben, John Johnson room with private label music for cynical slash."_ The perfect music for depressed teenagers.
  * _"Ben happen. Adam. Kinda late. Must get to work. We need a re-ordered madness."_ Are you sure about that? You don't sound like you've run out of madness, but I'll order some more if you insist!
  * _"Hey, I don't know if you have a problem or not, but we wanted to try grabbing hold there."_ Grabbing hold where?!? Yes, I do have a problem with that.
  * _"Hey, Ben, it's me again. I was just happening."_ So glad you're happening!
  * _"I wanted to warn you that 204 of us are 6. Sorry."_ I really didn't need a couple hundred six-year-olds on my hands today. Sauce!

The moral of the story? If you're looking into a VoIP provider and they list automatic voicemail transcription as one of their services, don't take it too seriously. At best it provides a good laugh every now and then.