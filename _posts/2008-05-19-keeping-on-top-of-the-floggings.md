---
from_wordpress: true
comments_id: 78
title: Keeping on Top of the Floggings
date: 2008-05-19T23:04:52-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=78
permalink: /post/keeping-on-top-of-the-floggings/
categories:
  - word counters union
---
I have to admit that I have not been on top of administering floggings lately. I have been checking some people's counts daily, but have neglected to flog them for missed days. Others I have not been checking on at all. I feel bad about it, because I felt really disappointed when I missed a day and nobody seemed to notice. The Union isn't really fulfilling its purpose if nobody's keeping track of you.

I don't think I will ever have time to watch everybody in the Union. I only have a limited amount of time in the evenings to write my words, let alone anything else.



I'd like to suggest that we take the idea of Word Count Buddies a little further. Maybe we should form little subgroups and commit to conscientiously checking and flogging everyone in our subgroup. Of course, this wouldn't mean you should stop checking up on the others. This would just be to help make things a bit more manageable if you need it, and to help make sure everybody has a guaranteed Word Count Buddy. What do you Word Count Buddies think?