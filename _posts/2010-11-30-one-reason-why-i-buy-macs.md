---
from_wordpress: true
comments_id: 296
title: One Reason Why I Buy Macs
date: 2010-11-30T10:16:16-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=296
permalink: /post/one-reason-why-i-buy-macs/
categories:
  - 'life &amp; stuff'
---
I recently overheard an argument between two friends about the merits of Macs versus PCs. The discussion came about because one of them had just bought a Mac laptop for school. The other has never liked Macs and thinks the only reason to buy them is either to do graphics or to express some kind of perceived superiority based on the higher cost of Macs.

So why do I buy Macs instead of PCs? Here's one reason: reliability. In a [recent survey of 79,000 technology users](http://www.macrumors.com/2010/11/30/apple-smokes-the-competition-in-reliability-survey/), Apple was rated the most reliable of a variety of tech companies, including Toshiba, Sony, HP, Compaq, and Dell. Dell was second to last.

I currently own a Mac that's seven years old. It finally died last month. During that time, I personally know a number of people who have gone through one, two, or even three PCs—not because they became outdated, but because they crashed and died.

That's just my personal experience. I have heard of Macs crashing and burning, though not anyone I know personally. No piece of equipment that complex is immune. I also know plenty of people who have owned their PCs for several years with no problems and only replaced them because they were outdated. I did the same thing with my seven-year-old Mac. Although it still functioned a month ago, we hadn't used it for quite some time due to outdated software. I've since bought two more Macs—one for the family and one for my job—in order to keep up with the technology curve.

So I'm not saying that Macs never die and PCs can't be reliable. And I'm not saying Macs won't need to be replaced eventually. But for me, if I'm going to spend hundreds of dollars on a purchase that I rely on for my job, that I use more than any other tool in my home, and that contains a lot of sensitive and personal data, I'm going to get the most reliable product I can. The extra cost is more than worth it to me.