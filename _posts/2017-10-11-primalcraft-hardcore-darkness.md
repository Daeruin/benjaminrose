---
from_wordpress: true
comments_id: 1348
title: "Primalcraft: Hardcore Darkness"
date: 2017-10-11T06:15:01-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1348
permalink: /post/primalcraft-hardcore-darkness/
categories:
  - Minecraft modding
---
In vanilla Minecraft, it's never completely dark. Although the default brightness is set to "Moody," the lowest available, in practice virtually everybody manually sets their brightness to 100%. I do it, myself. Not because I hate the darkness, but because if I can make it easier on myself, I will.

Setting the brightness to 100% makes it so even deep underground with no torches, you can still see. Even at night, in the rain, with a new moon, you can still see (kinda).

In Primalcraft, I'm taking direct control over Minecraft's brightness setting, also known as the gamma setting. The brightness of the world is based on three factors:

  1. How far underground you are. Light from the sun naturally reaches up to 15 blocks underground. The deeper you go, the darker it gets. The brightness meter starts out at 100% and decreases until at 15 blocks from a cave entrance, the setting is as low as it can go. It isn't exactly pitch black, but it's effectively so.
  2. The phase of the moon. Of course it's naturally darker at night, even with the brightness setting at 100%. When it's a full moon, brightness is at 100%. You can still see pretty darn well. Brightness decreases as the moon wanes and goes to 0% on the new moon. You can still make out the shape of blocks, especially brighter ones like sand and water. But it's _really_ dark.
  3. The weather. When it's raining, brightness decreases to 40%. In a thunderstorm, it decreases to 20%.

This is just one of those little features that I hope makes Primalcraft a little more realistic than vanilla Minecraft.