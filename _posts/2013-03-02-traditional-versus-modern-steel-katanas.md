---
from_wordpress: true
comments_id: 1159
title: Traditional versus Modern Steel Katanas
date: 2013-03-02T09:22:28-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1159
permalink: /post/traditional-versus-modern-steel-katanas/
categories:
  - medieval combat
---
Is the traditional method of making katanas better than using modern steel? Here's the answer from modern expert sword maker Walter Sorrels.

<iframe src="https://www.youtube.com/embed/uL2bpuyTHto?rel=0" width="500" height="300" frameborder="0"></iframe>

To summarize, katana blades made from modern steel have superior performance. They are harder and keep a sharper edge. They are also cheaper. The primary advantage of the traditional technique is that the folding process allows you to use lower-quality steel. Historical Japanese bladesmiths used the folding method because it was the only way they could make a good blade with the low-quality steel that was available to them. The other advantage is that the folding process makes an interesting and beautiful blade.
