---
from_wordpress: true
comments_id: 637
title: The One Writing Tool I Never Use
date: 2011-06-30T08:23:45-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=637
permalink: /post/the-one-writing-tool-i-never-use/
categories:
  - writing and publishing
---
I just saw a brilliant blog post over at Writer Unboxed, one of my favorite writing-related websites. The blog post was about low-tech tools for writers, and we're not talking about the ubiquitous ballpoint pen and notepad. These tools are a little cooler than that.

You should [jump over there and check out the blog post](http://writerunboxed.com/2011/06/30/the-three-writing-tools-i-can%E2%80%99t-live-without/), because it's really quite ingenious. I'll give a brief recap, though, in case you don't feel like reading the original.

The first writing tool is the dive slate, which you can write on underwater. Perfect for hanging in the shower and jotting down those flashes of inspiration that seem to strike in that anti-writing place. The second is a lighted pen, which has a simple light that shines on your paper as you write. Perfect for keeping on your nightstand so you don't have to fumble around to turn on a lamp and wake up your partner. The third is an accordion key chain with an attached pen and pad of sticky notes. Perfect for taking with you out on those writerly walks where you're looking for inspiration. Naturally, the original blog post was much more entertaining to read. You might want to go read it yourself.

![Notebook](/assets/images/notebook.jpg){: .image-wrap-left width="226" height="288"} But it got me to thinking. As cool as those three ideas are, I'm not sure I would ever use them. I don't even have the standard notepad at my bedside as it is, let alone a lighted pen. I have never been that kind of writer. For some reason, I feel no compulsion to write down every idea that seems brilliant, especially when I'm trying to sleep. It seems to me that if the idea is really that brilliant, I will remember it. And if I forget it, well, there are a million other ideas where that one came from. Something else is bound to come up at some point.

This kind of makes me feel like a fake. OK, not really. But it does make me feel a little left out, in a way. I mean, what is a writer that doesn't keep a notebook by his bed?

Am I alone in this?
