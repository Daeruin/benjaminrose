---
from_wordpress: true
comments_id: 1255
title: "What I'm Listening To: Nightwish, Kamelot, We Are the Fallen"
date: 2014-01-11T11:15:20-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1255
permalink: /post/what-im-listening-to-nightwish-kamelot-we-are-the-fallen/
categories:
  - music
---
It's been a while since I posted. Suffice it to say that life has me awfully busy and stressed out. It's hard to find the time and motivation to write. So here's an easy blog post for you: another in the series of what music I'm listening to. In my last post, I focused on djent and avante garde, jazz-infused progressive metal with a heavy metal component. This time I'm going to focus on symphonic metal.

First comes **Nightwish**, a band that hails from Finland. I first discovered them when their album Once came out. Their singer Tarja Turunen had a very operatic style that manages to mesh strangely well with the band's power guitars and heavy drums. The band mixes in lots of string sounds, choral elements, synthesizers, and ethnic instruments to create an interesting tapestry of sound. It's all very bombastic, and I love all the drama. Most of the music is composed by Tuomas Holopainen, the keyboard player, and he cites movie soundtracks as one of his biggest influences.

Nightwish's next album (Dark Passion Play) featured a new singer, Anette Olzon. Anette's style was more in a pop-rock vein, and they managed to make it work really well. But her voice lent a different sound to the band—definitely more radio-friendly and easier to sing along to. On top of that, the bass player Marco contributed more of his strident vocals to the band. Overall, I liked the album just as much, but in a different way. Anette stuck around for the next album, Imaginaerum, but left the band partway through the tour to be replaced by Floor Jansen. From the YouTube video's I've seen, Floor should be able to replicate both the operatic vocals of Tarja and the more poppy vocals of Anette. All of this change makes it very hard to pick a single song that's representative of Nightwish's sound. In the end, I simply have to pick one that I like. Here's Romanticide, the original version performed by the operatic Tarja. It's one of the first Nightwish songs I heard and fell in love with.

<iframe src="https://www.youtube.com/embed/kZS8MJywG-U" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Next up is **Kamelot**. Unlike the other two bands I'm featuring today, Kamelot has a male singer. Although they hail from Florida, their sound has a really strong European power metal influence. Their earlier albums fall into a class of music that's too cheesy for my taste. But their 2005 album The Black Halo introduced a darker, harder element that really caught me. The next album, Ghost Opera, introduced even more symphonic elements. The song I've chosen to represent Kamelot is called Rule the World. On the album, it is fronted by a one-minute symphonic introductory track, and I'm really glad I was able to find a YouTube video that combines the two, because then the string elements at the beginning of Rule the World make a lot more sense. The guitar riff that comes in at just after a minute totally rock.

<iframe src="https://www.youtube.com/embed/nV7_paPAsNU" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

Finally we have **We Are the Fallen**, a band composed of former Evanescence guitarist Ben Moody and American Idol contestant Carly Smithson. Back when I was watching American Idol, the Irish Carly was one of my favorites. In a way I was glad she didn't win, because that allowed her to go on and do her own thing instead of being owned by American Idol. And I'm definitely glad she teamed up with Ben Moody and the former Evanescence band. We Are the Fallen has been criticized for sound too much like Evanescence, but I think that criticism is stupid. Of course they sound like Evanescence. They basically ARE Evanescence with a new singer. And as much as I love Amy Lee's voice, I think Carly holds her own really well. The bad news is that We Are the Fallen was dropped by their label after their first album came out, and I haven't seen any sign of a new album for a few years. Here's hoping they come back.

<iframe src="https://www.youtube.com/embed/zzTZeeMCUBk" width="560" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
