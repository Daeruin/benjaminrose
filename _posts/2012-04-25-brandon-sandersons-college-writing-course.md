---
from_wordpress: true
comments_id: 867
title: "Brandon Sanderson's College Writing Course"
date: 2012-04-25T20:57:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=867
permalink: /post/brandon-sandersons-college-writing-course/
categories:
  - writing and publishing
---
This will be a relatively short one today, but it should be immensely interesting to aspiring writers, especially of the fantasy genre. Brandon Sanderson teaches a creative writing class at Brigham Young University. This isn't the creative writing class you probably took in college where you spent a short while on various different formats like poetry, flash fiction, and short stories, all in the stuffy "literary fiction" genre so beloved of most college English professors. No, no. This is a class that actually teaches you to write a novel—but not just any novel: a genre novel that you might make money from. This is a class that teaches you a set of practical writing tools that you can use, adapt, or discard as needed to get real writing done. There is more real knowledge in the first 10 minutes of this class than I got from an entire semester's worth of class time in my old creative writing class in college.

OK, enough gushing. The cool thing is that some students filmed this class and are now posting the entire thing online. You should check it out. I'll embed the first one here, but you should probably go visit their website, [Write About Dragons](http://www.writeaboutdragons.com/home/), if you want to get the rest of them in order.

<iframe width="560" height="315" src="https://www.youtube.com/embed/CbL-84SkT4Q" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
