---
from_wordpress: true
comments_id: 1264
title: Personal update
date: 2017-08-03T19:58:41-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1264
permalink: /post/personal-update/
categories:
  - 'life &amp; stuff'
  - metablogging
---
Three and a half years. Yeah, it's been that long since I posted anything on my blog. Shortly before my fifth child was born, it all became too much. I now have six kids, and my youngest is 2 years old. I've moved twice and changed jobs twice since then. So all that happened.

For a long time, I simply felt overwhelmed, and didn't have time for anything outside of working and caring for my family. I gave up all my hobbies. Creative writing, blogging, tabletop RPGs, fencing—it all went out the window.

But as the kids get older, and we slowly move out of baby phase, I've found myself with a little more spare time. And I've found new hobbies.

A couple years ago, we bought Minecraft for my oldest son. One fateful day, I decided to try it out, and I got hooked. Soon I was trying out mods, and soon after that, I was getting sick of mods because they didn't do quite what I wanted.

And now I'm writing my own Minecraft mod.

If I post on my blog in the near future, it will likely be about that. I've been doing lots of interesting research that's tangentially related to some of the topics I used to write about. You may find some of it interesting. Or not.

If I even have any readers left, after all this time.