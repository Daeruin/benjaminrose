---
from_wordpress: true
comments_id: 1517
title: Realistic World Gen Mods for Minecraft
date: 2018-04-02T22:28:12-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1517
permalink: /post/realistic-world-gen-mods-for-minecraft/
categories:
  - gaming
  - Minecraft modding
  - software
---
I've been researching Minecraft mods that add realistic, or at least improved, world generation. There aren't that many. Because Minecraft world gen is difficult to mod, to say the least. I'm particularly interested in mods that offer more than just new biomes. I'm looking for mods that change the way terrain is generated, add significant blocks based on real geology, or improve where biome types occur.

Here's what I've found, in alphabetical order:

  * [BetterGeo](#bettergeo1710)
  * [Climatic Biomes](#climatic-biomes1102)
  * [Dooglamoo Worlds](#dooglamoo-worlds-112)
  * [Geographicraft](#geographicraft-112)
  * [New Dawn](#new-dawn-1710)
  * [Realistic Terrain Gen](#realistic-terrain-generation-1102)
  * [Terrafirmacraft](#terrafirmacraft-1710)
  * [VintageCraft TerrainGen](#vintagecraft-terraingen-18)

Click on the mod title below to go the mod's download page.

### [BetterGeo](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2421442-bettergeo-more-geology-in-minecraft-updated-2016) (1.7.10)

{%
  include figure.html
  image-name="minecraftWoldGenMods_betterGeo.png"
  alt="Example terrain from BetterGeo"
  caption="Example terrain from BetterGeo"
%}

**The pitch**: "BetterGeo is a mod with more geology for Minecraft! We have increased the geological content and also made it appear more like reality by adding rock types, minerals, metals, gemstones and exciting areas of application for the new materials! Geological features like stratigraphy, veins and pipes are also introduced in BetterGeo. As of 2.0 new realistic soils are added which replace dirt. The block hardness varies and the blocks are therefore mined with different difficulty. We have used Moh’s scale of mineral hardness as a reference for the relative hardness of our new blocks. The new smeltery also requires different amount of heat relative to the metal's melting point."

**My take**: I haven't played this mod for more than a few minutes, but it seems really cool. The mod adds many different kinds of rock from all the major categories (igneous, metamorphic, and sedimentary), and they all spawn in realistic situations based on Earth geography. Diamonds spawn in kimberlite "pipes," or vertical stone formations. Pumice, an airy volcanic rock, floats on water. As far as I can tell, BetterGeo doesn't change the terrain generation at all—just the types and locations of stone. My only complaint in my brief time on this mod is that some of the textures are pretty ugly and don't really match each other. This mod warrants more play time.

### [Climatic Biomes](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/wip-mods/2823012-climatic-biome-placement) (1.10.2)

{%
  include figure.html
  image-name="minecraftWorldGenMods_climaticBiomes.png"
  alt="Chapparral biome from Climatic Biomes"
  caption="Chapparral biome from Climatic Biomes"
%}

**The pitch**: "Rather than a noise field, the temperature and humidity radiate from "basins" of influence, including dominating hot and cold poles and smaller basins to keep things interesting (and then there is a weak noise field to make the edges less straight). Then, the climate is used to place not biomes but its own, more detailed biome zones, with smaller, more specific groups. This does often result in some very large biome but at the same time the players is likely to have travel much sorter difference to find a wide range of biome types, and there is some logic to the placement and the map moves gradually from hot to cold and (indepently) from wet to dry."

**My take**: I loved the concept when I first read about it. However, in my brief playtest, I felt like I was simply playing vanilla Minecraft. The terrain itself is, by design, vanilla. The only real difference is the logic of how biomes are laid out. Perhaps if I had spent a little longer, I would have noticed the differences in biome placement. The mod was created for 1.10.2 but never made it to full release status, so there's not much hope of getting updates.

### [Dooglamoo Worlds](https://minecraft.curseforge.com/projects/dooglamoo-worlds) (1.12)

{%
  include figure.html
  image-name="minecraftWorldGenMod_dooglamooWorlds.jpeg"
  alt="Glaciers in Dooglamoo Worlds"
  caption="Glaciers in Dooglamoo Worlds"
%}

**The pitch**: "This mod expands the scale and realism of world generation (hopefully not too much) and makes it so that you can explore without seeing the exact same biomes over and over. There are 8 geofactors that contribute to the terrain generation and features. Each has a value from very low to very high. The biomes, features, and conditions around you can give you clues to the values of the geofactors." The geofactors are: elevation, stone density, uplift, volcanism, erosion, era, temperature, and precipitation.

**My take**: This mod is a recent discovery of mine and is a strong contender as far as realism, variety, and beauty of the terrain. As with other mods in this post, biomes are defined by combining different parameters in different ways. By combining the 8 different geofactors, the mod author says you get 65,000 different "biomes." Elevation provides probably the most noticeable effect, as the base terrain can soar up to level 150 or more, with mountains on top of that. It's very picturesque and way more epic than vanilla Minecraft. The next most noticeable to me was temperature and precipitation, as the drier areas tend to generate more plain dirt, coarse dirt, and podzol on the surface. Many areas of the world have stratified stone. Uplift creates more jagged terrain, with occasional outcroppings of bare stone. Archipelago islands, volcanoes, and glaciers can be found. In addition, ores are generated based on the 8 factors so they are found in more realistic locations. There are a few things that would take this mod to the next level: more realistic rock types and trees, and realistic rivers. But next to Terrafirmacraft, this mod was by far my favorite. Bonus points for being up-to-date with the latest version of Minecraft, too.

### [Geographicraft](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2516874-geographicraft-stop-chunk-walls-control-size-and) (1.12)

{%
  include figure.html
  image-name="minecraftWorldGenMod_geographicraft.png"
  alt="Banded climates from Geographicraft"
  caption="Banded climates from Geographicraft"
%}

**The pitch**: "Geographicraft allows you to control climate zones, ocean sizes, biome frequencies, and continent sizes. Make Minecraft worlds into Yourcraft worlds! Geographicraft is a 1.8 version of Climate Control, renamed to make the function clearer. Set climate zones to smaller, more manageable sizes so you can find a variety of biomes more easily. Change the frequencies of common biomes and climate zones to occur as often or as rarely as you want. Prevent hot biome-cold biome transitions, or mix all the biomes together. Restore true oceans to vanilla, or keep a universal continent with large lakes. Create a variety of land sizes, from giant continents tens of thousands of blocks across to islands that fit on a single map. Move biomes from one climate zone to another. Turn the oceans into giant deserts, endless jungle, frozen ice plains, or vast mountain ranges, if you want. Fill the oceans with islands of your choosing. Rearrange temperate zones to make warm and cool zones distinctive. Put mountains or forests into hot zones as oases."

**My take**: Like Climatic Biomes above, this mod does nothing to the terrain and offers no new blocks or biomes. Rather, it allows you to tweak and control the placement of biomes and the type of terrain that generates. My favorite aspect of this mod is the ability to set climate bands so hot climates only appear near the equator and cold climates in the far north or south. Although the mod can do much more than that, I feel like that feature merits its inclusion in this list. The mod can work with biomes from other mods, making it super flexible.

### [New Dawn](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/1283481-1-7-10-forge-new-dawn-completely-new-terrain) (1.7.10)

{%
  include figure.html
  image-name="minecraftWorldGenMods_newDawn.jpeg"
  alt="Example of New Dawn terrain"
  caption="Example of New Dawn terrain"
%}

**The pitch**: "New Dawn pre-generates a humidity and temperature noise map (as well as some others) then decides for biomes based on the combined noise fields. This means you will for example never find deserts next to snow, because the temperature drops gradually. You will find Savannah or Jungle near a desert and following the dropping temperature through temperate plains you will eventually find snow covered areas or frozen oceans. While trying to keep this realistic, I still try to keep the game playable however. Therefore those temperature/humidity maps are—compared to the real world—unrealistically small. So a desert might be only 2000 blocks away from an arctic sea."

**My take**: I created a New Dawn world and flew around for quite some time, just to see the terrain. I felt like I only saw a few really different biomes. The landscape was, for the most part, quite smooth, without much jaggedness and no floating islands (that I could find). Overall, it seemed pretty bland. I like the concept of biome being determined by a mix of characteristics like temperature and humidity, but the terrain shapes in New Dawn did not excite me or make me want to start a new survival world. You can find a more comprehensive review along with lots of pictures [here](http://mcmodder.net/new-dawn/).

### [Realistic Terrain Generation](https://www.minecraftforum.net/forums/mapping-and-modding-java-edition/minecraft-mods/2524489-realistic-terrain-generation-rtg-realistic-biomes) (1.10.2, 1.12)

{%
  include figure.html
  image-name="minecraftWodlGenMods_realisticTerrainGeneration.jpeg"
  alt="A mountain in Realistic Terrain Generation"
  caption="A mountain in Realistic Terrain Generation"
%}

**The pitch**: "Realistic Terrain Generation (RTG) is a mod that adds a new world type which generates realistic terrain. It doesn't add new blocks. It doesn't add new mobs. It doesn't even add new biomes. It simply generates more realistic-looking terrain for existing Overworld biomes (including those added by other mods)."

**My take**: The mod authors aren't much for documentation. The best advertisement for the mod is pictures—see [here](https://imgur.com/a/DZz69) for some good ones. This mod is quite impressive on its own, but it really shines best when combined with a great biome mod like Biomes O' Plenty. The terrain itself has a ton of variety and is quite beautiful. Hills and mountains tend to be smoother than vanilla Minecraft. It has the best rivers anywhere, probably even better than Terrafirmacraft. It has stunning desert landscapes with stratified stone. It's _amazing_ at what it does. Its main deficiency is that it really is JUST terrain generation. To get any other realistic features, such as additional stone or tree types, or geological features beyond the basics, you need additional mods. RTG has explicit support for a number of other biome mods that create really impressive worlds that you'll enjoy playing on for a long time.

### [Terrafirmacraft](https://wiki.terrafirmacraft.com/Biome) (1.7.10, 1.12)

{%
  include figure.html
  image-name="minecraftWorldGenMods_terrafirmacraft.png"
  alt="Terrafirmacraft river"
  caption="Terrafirmacraft river"
%}

**The pitch**: "_Survival Mode as it should have been._ The goal of TFC is to make things more believable, and to give the player a sense of accomplishment. Mastering skills, and searching far and wide to gather materials before you can build amazing castles, homes, fortresses, towns, cities, etc. The overhauled terrain generation also creates a beautiful, varied background as a canvas to start your builds. Starting with the world, TFC has thrown out Vanilla generation and started on a fresh slate. Sea-level has been raised to twice the height to accommodate 3 separate, varying layers of stone underneath, each spawning their own ores and minerals depending on which of the many new stone types it is comprised of . . . The Biome determines the shape of the terrain and basic temperature and rainfall guidelines . . . along with the altitude."

**My take**: This list wouldn't be complete without Terrafirmacraft. I've played many hours on the mod, and as far as I'm concerned it's still the gold standard of realism, playability, and beauty. In general, the terrain is very nice looking, with very little that would seem out of place in the real world. It includes realistic rivers, many realistic types of stone in layers, and lots of realistic tree types. An interesting quirk is that there's a different type of dirt for every type of stone, giving you a clue about the type of stone beneath the surface. This results in most dirt being grayish in color, lending Terrafirmacraft a distinctive look. This mod sets a high bar.

### [VintageCraft TerrainGen](https://minecraft.curseforge.com/projects/vintage-terrain-generation?gameCategorySlug=mc-mods&projectID=228381) (1.8)

{%
  include figure.html
  image-name="minecraftWorldGenMods_vintageCraftTerrainGen.png"
  alt="Vintage TerrainGen scene"
  caption="Vintage TerrainGen scene"
%}

**The pitch**: "Alternative Terrain Generation more similar to older Versions of Minecraft. This mod is a variant of the mod [VintageCraft](https://www.curseforge.com/minecraft/mc-mods/vintagecraft) Terrain Generator that uses only Vanilla Blocks. Biomes are merely an emergent feature of a combination of forest, rainfall, temperature and fertiliy values. This also produces very fluid transitions from biome to biome. Trees are generated in a very dynamic way, allowing near infinite variations and sizes of the same tree. The Terrain is dominated by Landmass, there are no Oceans currently. The Terrain generates Strata - various rock layers with at different depths. Overhangs look more natural and usually larger."

**My take**: Like other mods in this post, this one uses a method of combining characteristics to create biomes. I like the concept, and it works quite well here. Most of the time, the terrain seems more realistic and pretty than vanilla terrain. The lakes are particularly lovely. The dynamic way of creating trees creates a lot of variation and interest, but I find many of the trees to be quite ugly. The algorithm for adding leaves needs some work. I'm not very fond of the dramatic overhangs this mod produces, as they all tend to appear at the same vertical height and produce a lot of floating islands (always a downer in something that's touted as being realistic). The mother mod, VintageCraft, uses the same terrain generation system but with many more types of stone, soil, and trees to add more interest and realism, so it gets points for that. It uses a 32x32 texture pack by default, but I find many of the textures to be very ugly, in my opinion.

What do you think of these mods? Have I missed any great mods that add realistic terrain generation?
