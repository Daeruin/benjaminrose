---
from_wordpress: true
comments_id: 30
title: The Seven Potter Sins
date: 2007-07-24T12:17:55-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=30
permalink: /post/the-seven-potter-sins/
categories:
  - 'life &amp; stuff'
---
I was going to post this on Friday night, before the Deathly Hallows was released, but um, I was at a store waiting in line for this great, um, thing I wanted to buy, and I've spent all my time since then focusing on nothing else, and wondering how anyone could not be doing it too, and wishing it weren't over. Anyway . . .

Sometimes I think Potter-mania has driven even ordinary fans too far. I've seen people do things for the love of Potter that I don't think they would do for anything else. So here's my list of the Seven Potter Sins, the commission of which is a sure sign that you are a hopeless Potter-phile:

  * **Pride.** Excessive belief that the Harry Potter books are the best books in the world, which interferes with your recognition of the goodness of other things. This is the Potter Sin from with all others arise.
  * **Envy.** The unbalanced desire for the Harry Potter books to have the same word count as Robert Jordan's Wheel of Time series or Terry Goodkind's Sword of Truth series.
  * **Gluttony.** An inordinate desire to consume more Harry Potter books than you need. If you've read the series more than twice, you are in danger of gluttony.
  * **Lust.** An inordinate craving for Harry Potter books. You may find yourself staying up all night to read, or falling asleep with your books in your arms, or daydreaming about Harry Potter books at bad times.
  * **Anger.** Rejecting the friendship of anyone who isn't a fan, and the inability to understand how anyone could not like Harry Potter.
  * **Greed.** The desire to own more Harry Potter books while ignoring other good books, like the Lord of the Rings or Eragon.
  * **Sloth.** The avoidance of anything other than reading and thinking about Harry Potter.

So, avoid these Potter Sins, and you'll be a better person. Now if you'll excuse me, I must return to reading this great book I just got, er, I mean, I'm now going to engage in some very worthwhile activity that has nothing to do with Harry Potter. I've already wasted too much time on other things.