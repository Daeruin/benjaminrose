---
from_wordpress: true
comments_id: 40
title: Dramatizing the Fellowship on the Big Screen
date: 2008-01-12T00:12:30-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=40
permalink: /post/dramatizing-the-fellowship-on-the-big-screen/
categories:
  - fiction
---
A few months ago, when I posted my thoughts on rereading The Fellowship of the Ring, I had a few additional ideas I wanted to talk about. I already talked a little bit about the differences between the movie and the book, but I'd like to expand on that a little.

I realize that many scenes in the movie were made different because of the necessities of the film medium. With less time and the need to present everything visually, there are restraints on what you can do in a movie. For example, the Council of Elrond would have been unutterably boring if presented exactly as it is in the book. Although Jackson could have let Gandalf sit on a chair and tell the story of his confrontation with Saruman, he made a good choice in showing the encounter. I can understand the motivation for presenting the Council of Elrond not as a bunch of people sitting around telling each other about things that happened years ago, but as a group of people arguing about what to do with the Ring. But I was still disappointed. I can't articulate specifically why, but I don't think that scene quite worked as it should have. I think my main disappointment was with the depiction of Elrond: a grumpy Agent Smith with a receding hairline. Hardly the ageless and wise Elrond Tolkien wrote about.

Another scene that didn't work for me was the encounter with the Nazgul on Weathertop. At first I thought it was pretty cool, but after thinking about it I realized that they got it totally wrong. In the book, the Nazgul did not flee because Aragorn can running at them with a torch and set them all on fire. The just left, because their task was done: they had stabbed Frodo, and he would soon become a wraith and bring the Ring straight to them. I have little doubt that if the Witch-King of Angmar, the Lord of the Nazgul, had wanted to kill Frodo and take the Ring, he could easily have done it. But that wasn't his goal. The plan would have worked if Frodo had not been carried by Glorfindel's horse across the Ford.

One difference I do approve of is the omission of Tom Bombadil. Sorry Lackhand. I know you like that part of the book, but it has never been one of my favorite parts. Tom is something of an anomaly in the books, anyway. He doesn't quite fit into Tolkien's mythology as nicely as one would like, and in a way I'm surprised Tolkien allowed that of himself. If they were going to trim anything to fit the movie into three hours, Tom Bombadillo was the right choice.

I've rambled on longer than I intended, but I have two more scenes I want to at least mention: the pass of Caradhras, and Boromir's last stand. In the books, it is the mountain itself that sends the snows and throws the fellowship back; in the movie, it's Saruman. I was very surprised by that change, but I can now see how it fits snugly into the plot. As for Boromir's last stand, I'm curious what you thought of it.