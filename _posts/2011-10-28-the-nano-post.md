---
from_wordpress: true
comments_id: 737
title: The NaNo Post
date: 2011-10-28T09:02:35-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=737
permalink: /post/the-nano-post/
categories:
  - gaming
  - 'life &amp; stuff'
  - writing and publishing
---
NaNoWriMo, for those who may not know, stands for [National Novel Writing Month](http://www.nanowrimo.org/en/about/whatisnano). Beginning on November 1, you try to write an entire novel, or at least 50,000 words, before the month ends.

I've been meaning to try it out for several years now, but it seemed like there was always some major obstacle—a new baby, a new house, whatever. Finally, this year, I thought I could finally do it. We do have a fairly new baby, but he'll be almost three months old when NaNo starts, and we've got a decent routine down. We do have three kids in diapers (OK, one of them only at night), and all of life's various distractions. But I really thought I could do it this year. Maybe I'm finally used to the fatherhood thing (took me long enough).

I even have a great idea for my novel that's making me very excited. If you're not interested in the genesis of the idea, you can skip to the next paragraph. It started, like most of the story ideas I had as a kid, with a roleplaying game. After years of suggestions and pleas, someone other than me finally agreed to be run a new game. It had been a long time since I was a regular player, so I was stoked to play, even if it did happen to be [Pathfinder](http://paizo.com/pathfinderRPG), part of the d20 system that I loathe. The campaign he chose to run has an interesting fantasy-horror theme, and the character I settled on turned out very compelling. Almost immediately, I began to envision my own stories using the character. I haven't been this excited about writing something for years.

NaNo is supposed to be a fun, seat-of-your pants experience where you let go of all your writing inhibitions, cram your internal editor into solitary confinement, and just write like mad. However, being the detail-oriented, organization-obsessed individual that I am, I couldn't help but be sucked into the [series of NaNo preparation posts over on Storyfix.com](http://storyfix.com/nail-your-nanowrmo) by the brilliant Larry Brooks. Brooks is a huge proponent of story planning, and his series covers a wide array of topics taken partly from his book Story Engineering and partly from Brooks' own vast intellectual storehouse of advice on how to get a novel done. I have been excitedly building up my story plan brick by brick.

Then reality came crashing down. First a huge project deadline at work. We're planning to launch a whole new program at the beginning of November. I thought I could get it all done early, but I should have known better. I'm writing and editing and researching like mad to get it all done, and it looks like things will bleed significantly into November. Overtime may not be optional. My story plan has ground to a dead halt, and I know from experience that I suck at seat-of-your-pants writing.

Then, as if that weren't enough, my brother suddenly decided to get married to his longtime girlfriend—on November 11th. I'm really happy for them, and it's an awesome event, but it does throw a damper on NaNo. When you have to write an average of nearly 1,700 words a day, taking a whole weekend off to travel down to southern Utah for a big family event kind of throws a wrench into the works.

I'm still going to give it a shot, but I'm not going to stress myself out over it. In the end, if I get even halfway done, it'll be further than I've ever gotten before. Wish me luck.