---
from_wordpress: true
comments_id: 776
title: "LTUE Notes: Dialog Tags and Speech Patterns"
date: 2012-02-16T20:43:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=776
permalink: /post/ltue-notes-dialog-tags-and-speech-patterns/
categories:
  - writing and publishing
tags:
  - dialog
  - LTUE
---
Today's LTUE notes come from the panel "Dialog Tags and Speech Patterns" with panelists Angie Lofthouse, David Butler, Kasie West, Lani Woodland, and Tyler Whitesides.

Mistakes and pitfalls:

  * Using too many adverbs on your dialog tags. Example: JK Rowling! She does this way too often. Supposed example from the books: "No!" Ron said objectingly.
  * Masquerading narration as dialogue. Example: Maid and butler dialog, "As you know, Bob . . ."
  * When characters say something solely for plot purposes.
  * Any kind of repeating pattern is bad

How to convey information without using dialog:

  * Just use narration. No problem as long as you don't do it too much.
  * Arguing characters would naturally tend to repeat information that was known to both of them, so it will sound more natural that way.
  * Don't do it at all. Worldbuilding information, especially, should be assumed. Let the reader live in the world and figure it out by context and experience.

How to create authentic character voices:

  * Do a first pass without worrying about it. Then fix all the voices during edits.
  * If all your characters sound the same, then maybe you don't know them well enough. Spend the time fleshing them out, do character sheets, exploratory dialogs, etc.

How much dialog is too much? Just make sure the dialog is actually moving the plot forward and you won't go wrong.

About accents. Some panelists came out against, some defended doing accents as long as you keep it light. You don't need to go crazy. Just a few spelling changes here and there, maybe a bit of syntax change, just enough to get the idea across.

On "said" and when to use it. You can avoid using dialog tags completely by using "beats," which are short actions that identify the speaker

  * Bad example: "No way," said John. He backed up against the wall.
  * Good example: "No way." John backed up against the wall.

You can also just leave off tags for short exchanges, but that can get confusing if the reader has to keep track of who's speaking. You can use "said" occasionally if you need it to help identify a character in fast-moving short dialog sequences.

Gender differences in dialog:

  * What do they notice? Women might notice the flower arrangements, while men won't.
  * Give your manuscript to readers of both genders and have them watch out for bad gender dialog.
  * In general, men talk less, so women writers may need to go in and hack out a lot of dialog.
  * But you also need to know your audience. In romance they may want more dialog or more talk about feelings from their men.
  * Treat your characters as individuals. How they talk comes from their own individual personality as much as their gender. Some men really are very talkative.

Advice on doing it right:

  * Everything should have an element of tension, including dialog. Maneuver characters so they are talking at cross purposes.
  * Enter the conversation late. Let the reader figure out what they're talking about through context.
  * Reread the conversation several times so you can remember it, then paraphrase it out loud to see if you say anything differently. You may drop some words or use different phrases that sound more natural.
  * Remind the reader of the setting. Drop in little descriptions or occurrences to keep the setting present.