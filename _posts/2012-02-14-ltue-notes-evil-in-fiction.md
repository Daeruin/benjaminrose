---
from_wordpress: true
comments_id: 770
title: "LTUE Notes: Evil in Fiction"
date: 2012-02-14T22:02:48-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=770
permalink: /post/ltue-notes-evil-in-fiction/
categories:
  - writing and publishing
tags:
  - evil
  - LTUE
---
Today's selection of notes from LTUE comes from the panel "Evil in Fiction: Creating Conflict." Unlike the past two that I've blogged about, this one was a panel instead of a single person. The panelists were: James Dashner, Clint Johnson, J. Scott Savage, Jennifer A. Nielsen, and Al Carlisle (he is a psychologist who specializes in serial killers, if I've got the right name).

Why do we put darkness and evil into our stories?

  * Evil contrasts with the eventual light and triumph at the end.
  * When you finally figure out who you are, that occurs during the hardest part of your life.
  * The dark is exciting, unpredictable, full of twists and turns.
  * Dark personalities take time to develop.

On the use of "evil" versus other terms:

  * From Al Carlisle: I've never met an evil person who felt the were evil. To them, their evil actions are all very logical and normal.
  * Not all stories require pure evil, but they do require an antagonist (later clarified to mean "an opposing force," not just a villain). The hero's antagonist should be strong where the protagonist is weak and be able to exploit those weaknesses.
  * Sometimes the antagonist is inside you. Sometimes it's just a choice between two good things and is meant to force the character to define who they really are. Example: the movie Sweet Home Alabama. So no, stories don't require evil.

Interesting comments/ideas to make evil less cliche:

  * You can show the protagonist awaking to or recognizing the evil inside themselves, or the part of them that responds to evil. Example: the movie Red Dragon (I think) where the detective tries to catch the killer by recreating his actions and starts to understand part of the thrill.
  * Victims perpetuating the wrongs done to them.
  * Good people doing genuinely bad things.
  * Readers don't want unexplained things—they want clarity.