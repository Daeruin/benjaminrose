---
from_wordpress: true
comments_id: 57
title: An Austistic Fiction Voice
date: 2008-03-12T22:32:46-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=57
permalink: /post/an-interesting-story/
categories:
  - fiction
---
Yesterday I posted a link to an article in an e-zine called [Reflection's Edge](http://www.reflectionsedge.com/about.html). They say they are a publication for housing good genre fiction and for helping writers improve their writing. I poked around the site a little, and discovered on their front page this short story called ["Viscosity Breakdown"](http://www.reflectionsedge.com/archives/mar2008/vb_sf.html) by S. Foster.

It's an odd little story. I almost stopped reading it after the first page, because I didn't really sense any story going on and the writing seemed horribly stilted. If you continue reading for a bit, though, it gets interesting, and you realize why the first bit sounded so strange. It's written in the character's voice, a fellow who seems to suffer from both autism and Tourette's syndrome at the same time. (Be warned, there is a little bit of explicit language.)

What do you think of this author's use of voice? Did it work for you? Could it have been effective using a close third person point of view with a neutral voice instead of first person?
