---
from_wordpress: true
comments_id: 28
title: Latro in the Mist
date: 2007-08-18T10:09:49-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=28
permalink: /post/latro-in-the-mist/
categories:
  - fiction
---
I just finished reading Latro in the Mist, by Gene Wolfe. This is my first Wolfe novel. I've seen his books on the shelf countless times, but hadn't ever gotten around to trying him out. Luckily for me, my wife got me this book for my birthday.

Latro in the Mist is a compilation of two novels, Soldier in the Mist and Soldier of Arete. It's the story of a Roman mercenary who is injured fighting for the king of Persia against the Greeks. A blow to the head causes him to forget each day the previous day's events but in return he gains the ability to see and speak to the invisible gods, goddesses, and monsters among them and occasionally grant others the ability to see them, too. Caught up in the squabbles of gods and men, Latro keeps a record of his experiences in a scroll to try to keep a tenuous grip on his identity.

This is a very strange book. Each chapter is a new entry from Latro's scroll. Latro describes events as he experiences them, whenever he has time, sometimes leaving large gaps in the narrative. The reader is often left to piece together what must have happened, and because Latro frequently forgets the names and faces of his companions, the reader must sometimes try to identify them based solely on Latro's brief descriptions. The narrative feels very surreal at times. Reading Latro's record is like looking at a landscape while traveling through the mist (hence the book's name).

I was especially fascinated with Wolfe's depictions of the gods. They are invisible to the masses but nevertheless very real; Latro's encounters with them leave you marveling and shivering at the same time. The gods are simultaneously divine and monstrous, altogether different beings from humans.

Although I know only a little about Greek history, Wolfe seems to have done his research while writing this novel. It is very situated within the history of Greece, and Wolfe gives enough details (some of them in his foreword) to let you know he's not just making everything up. The book takes place after the amazing Spartan victory over the masses of king Xerxes of Persia (only a few years after the famous stand of Leonidas and his 300 soldiers against the same army).

All in all, I was very pleased to discover this book. Wolfe's prose is among the best I've read, and I look forward to reading another of his books. Despite how enjoyable it was to read, I was severely disappointed with the ending—I don't want to give anything away, but I think you'll see what I mean if you read it for yourself. Just enjoy each delicious page as you go.