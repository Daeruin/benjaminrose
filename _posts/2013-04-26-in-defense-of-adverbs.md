---
from_wordpress: true
comments_id: 1180
title: In Defense of Adverbs
date: 2013-04-26T12:39:43-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1180
permalink: /post/in-defense-of-adverbs/
categories:
  - writing and publishing
---
A common piece of writing advice is to not use adverbs. We're told that they're weak, that we should find strong verbs instead. As with all writing advice, this one is situational. Some would-be writing gurus with good intentions take a situational piece of advice and try to make it seem universal. A writer for the Macmillan Dictionary Blog recently [advised writers](http://www.macmillandictionaryblog.com/new-years-resolution-no-adverbs) that deleting all adverbs and adverbial phrases would result in a piece of writing that means exactly the same thing while being easier to read.

But the "no adverbs" advice has been laid to rest by Geoffrey Pullum, a professor who writes for the Chronicle of Higher Education, in an article titled [Being an Adverb](http://chronicle.com/blogs/linguafranca/2013/02/20/being-an-adverb/). Says Pullum:

> "Applying this adverb-erasing recommendation across the board would be disastrous, in random ways. In some cases it would cause a spectacular change of sense: the slogan of the British department-store chain John Lewis, _Never Knowingly Undersold_, would become _Undersold_. Quite often it would yield vapid slop with the wrong meaning: _Defusing a bomb must be done carefully_ would become _Defusing a bomb must be done_; _The dog had been brutally treated_ would become _The dog had been treated_. Sometimes it would create outright ungrammaticality: _a carefully worded letter_ would become *_a worded letter_."
>
> "The truth is that nothing as mechanical as abandoning adverbs (or certain subclasses of adverbs) is going to uniformly improve your prose. . . . Like the familiar advice to avoid passive clauses, it is never followed by the people who recommend following it."
>
> "The writers they admire never follow it either. And I don’t mean just that fine writing with adverbs is possible; I mean that **all** fine writing in English has adverbs (just open any work of literature you respect and start reading)."

As usual, there's a grain of truth in this piece of advice. There are undoubtedly extraneous adverbs that can be cut to make your writing tighter and better. Just don't go overboard. Sometimes a good adverb is _exactly_ what you need.
