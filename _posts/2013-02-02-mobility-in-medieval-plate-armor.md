---
from_wordpress: true
comments_id: 1083
title: Mobility in Medieval Plate Armor
date: 2013-02-02T14:49:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1083
permalink: /post/mobility-in-medieval-plate-armor/
categories:
  - gaming
  - medieval combat
---
Does medieval plate armor slow you down and make you easier to hit in melee combat? On the face of it, you'd definitely think so. But the truth of it is that plate armor was designed to allow freedom of movement and weighed less than the gear of a modern soldier or firefighter. That's about all I'm going to say for now. There are a ton of great resources on this topic online, so I'm just going to point out a few of them.

[**Arms and Armor—Common Misconceptions and Frequently Asked Questions**](http://www.metmuseum.org/toah/hd/aams/hd_aams.htm)

This article from Dirk H. Breiding of the Metropolitan Museum of Art debunks a number of myths regarding ancient armor, including the idea that plate armor made the wearer immobile. He also points out that not all plate armor was too expensive for common folk. That's something I hadn't realized before. But as with everything, there were different levels of quality and cost.

**[Weapons That Made Britain: Armor with Historian Mike Loades](http://youtu.be/WMuNXWFPewg)**

This video shows that full plate allows one to stand up from a fall with relative ease. Mr. Loades does a controlled fall off a running horse and gets back up while wearing plate armor. Although to be honest, he does look a little awkward as he stands up.

<iframe src="https://www.youtube.com/embed/WMuNXWFPewg?rel=0" width="480" height="360" frameborder="0"></iframe>

**Other Videos**

This next video is fairly low quality, but does an excellent job showing the range of movement and speed that are possible while wearing a suit of plate armor. I'd hate to face this guy's whirling sword.

<iframe src="https://www.youtube.com/embed/H6IL2giKNN8?rel=0" width="480" height="360" frameborder="0"></iframe>

Here's a French video showing some guys in full plate armor doing various things like climbing up and down ladders, getting up from prone positions, doing jumping jacks, and demonstrating some cool half-swording techniques.

<iframe src="https://www.youtube.com/embed/5hlIUrd7d1Q?rel=0" width="560" height="315" frameborder="0"></iframe>

Of course, that doesn't mean plate was like wearing your birthday suit. All that armor still weighs 50 to 70 pounds, some of which is weighing down your arms and legs. Some scientists actually did a study to see how much effort it takes to [wear plate armor while running on a treadmill](https://www.bbc.com/news/science-environment-14204717). Unsurprisingly, it's harder than wearing no armor at all—about twice as difficult in terms of total energy expenditure. It was also harder than wearing the same amount of weight in a backpack. The plates on the arms and legs requires your muscles to work harder to move around.

**How would we model this in a roleplaying game?**

It seems to me that plate armor wouldn't make one any easier to hit, as long as you have the minimum strength required to move your arms and legs. You would get tired faster, for sure. I remember seeing a video from the historian Mike Loades that showed some guys fighting in full plate armor. They got tired really fast. I can't find that video now. But in my experience, most fights don't last long enough for that to be a factor. You might want to start factoring it in after a full minute of intense fighting, if the combat lasted that long.

Plate armor does have other vulnerabilities. It takes a long time to put on, and you can't do it on your own. If you're wakened in the middle of the night, you're stuck without your armor. Plate armor is also vulnerable to piercing weapons like poll axes. Half-swording techniques let you use a sword like a giant spike to pierce armor. Arrows? Not so much. Plate armor that's properly formed, hardened, and tempered is [practically invulnerable to arrows](/post/can-arrows-penetrate-medieval-armor/ "Can Arrows Penetrate Medieval Armor?").
