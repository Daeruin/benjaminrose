---
from_wordpress: true
comments_id: 115
title: "Mistborn 3: The Hero of Ages"
date: 2008-10-24T15:52:42-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=115
permalink: /post/mistborn-3-the-hero-of-ages/
categories:
  - fiction
---
I just finished reading Brandon Sanderson's latest book, The Hero of Ages, last book in the Mistborn trilogy. I loved Mistborn 1: The Final Empire, but was a little disappointed with Mistborn 2: The Well of Ascension. So I was really hoping that the third book would be good and make good on the promise of the first one.

I was not disappointed. I loved it.

It's hard to say much without giving away spoilers, and there are plenty of them. All of the big mysteries that Sanderson introduced in books one and two are explained in a satisfying way. It was fun to try and guess which character was the author of the pre-chapter quotes. Unlike the previous two books, where the pre-chapter quotes were explained relatively early, these aren't explained until the end. I was pretty certain which character it was simply due to the tone and style of the writing, but I couldn't be sure. I ended up being right, and then I immediately felt stupid that I hadn't made other guesses based on my initial one. I've never been very good at figuring things out, both in books and movies, so I was pretty pleased with some of the guesses I made with this book. They weren't really that difficult, though, since the pre-chapter quotes are very explanatory and reveal many secrets as you go along.

I was a little disappointed in the action with this book. I expected the Allomantic battles to get even better and more spectacular, but they seemed to stay kind of the same. The conflict was much different in this book, though, so I suppose it was to be expected. Still, the action in these books is one of my favorite things about them, so I felt a little cheated. There's one fight scene, though, at the climax of the book, that was so awesome it nearly had me in tears—yes, I fully admit it. It was a big payoff.

When I went to Sanderson's book signing, he mentioned that he'd already had a few hate mails regarding the ending of the book. And I can see why. He took a huge risk with the ending, and did something quite out of the ordinary. I was disappointed on one level, which I think you'll understand when you read it, but I think it worked. It shouldn't be too surprising, if you've been paying attention.

I was also happy that Sanderson reduced his reliance on the phrase "So-and-so paused," which I mentioned in my original post about Mistborn 1.

What else to say? I think those are the only semi coherent thoughts I have to share on the book. Again, I recommend it. It was definitely a good read.