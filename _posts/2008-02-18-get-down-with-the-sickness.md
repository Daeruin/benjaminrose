---
from_wordpress: true
comments_id: 55
title: Get Down with the Sickness
date: 2008-02-18T09:40:53-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=55
permalink: /post/get-down-with-the-sickness/
categories:
  - 'life &amp; stuff'
---
I really feel like complaining right now, so here goes. I hate being sick. It sucked before I had kids, but now it really, really, really sucks. Especially when my wife is also sick. And my two children. All at the same time. We've all got RSV. Yep, every single one of us.

Whew. Man, that felt good. For more fun info on the state of my household, visit [my wife's blog](http://normalpainsandpleasures.blogspot.com/2008/02/illness-vortex.html). There are also some very cute pictures and stories about the kids.