---
from_wordpress: true
comments_id: 43
title: Robert Jordan is Dead
date: 2007-09-17T20:53:18-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=43
permalink: /post/robert-jordan-is-dead/
categories:
  - 'life &amp; stuff'
---
It's true. He died yesterday afternoon at 2:45 pm. I won't repeat all the details here, just give you a few links:

  * [Dragonmount](http://www.dragonmount.com/RobertJordan/?p=90), his official blog, where the news first appeared
  * [The Associated Press](http://ap.google.com/article/ALeqM5gBy7pK1U-kIvTHx4PYeiI8rqBkmg) article reporting the news
  * [Wotmania.com](http://www.wotmania.com/wotmessageboard2showmessage.asp?MessageID=68400) with some info on the future of the books
  * [Wikipedia.org](http://en.wikipedia.org/wiki/Robert_Jordan) article on Robert Jordan
