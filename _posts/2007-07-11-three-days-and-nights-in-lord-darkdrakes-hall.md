---
from_wordpress: true
comments_id: 24
title: "Three Days and Nights in Lord Darkdrake's Hall"
date: 2007-07-11T23:02:49-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=24
permalink: /post/three-days-and-nights-in-lord-darkdrakes-hall/
categories:
  - fiction
---
I found the short story [_Three Days and Nights in Lord Darkdrake's Hall_](http://www.strangehorizons.com/2007/20070129/darkdrake-f.shtml), by Leah Bobet, in the fiction archives for the magazine _Strange Horizons_. I thought it interesting that of the 28 short stories the magazine has posted on-line this year, only two were fantasy. The rest were science fiction.

This was one of the fantasy stories, and I enjoyed it. Curiously, the story is entirely devoid of magic, except for one very brief reference to its existence. The story is also light on setting. It takes place entirely in the stronghold of Lord Darkdrake, and mentions no other place names at all. You almost get the feeling it could have taken place in historical medieval Europe, and so it reads more like a work of historical fiction than fantasy.

I found all that encouraging. It shows that fantasy short stories don't have to employ magic or fantastical settings in order to be successful stories.

My only problem with the story—and it's more likely a problem with me than with the author's writing—is that I couldn't figure out the meaning of one of the story's key moments of dialogue. The outcome of the story is supposed to hang from the protagonist's reply to a certain question. Yet at the end I found myself unable to figure out what that reply even means in the context of the story.

You should take a few minutes to read _Three Days and Nights in Lord Darkdrake's Hall_. And let me know what that mysterious statement means when you're done.
