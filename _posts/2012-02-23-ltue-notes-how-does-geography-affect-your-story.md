---
from_wordpress: true
comments_id: 804
title: "LTUE Notes: How Does Geography Affect Your Story?"
date: 2012-02-23T20:35:23-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=804
permalink: /post/ltue-notes-how-does-geography-affect-your-story/
categories:
  - writing and publishing
tags:
  - geography
  - LTUE
---
Today's selection of notes from LTUE comes from the panel "How Does Geography Affect Your Story?" The panelists were Robert J. Defendi, Joshua Perkey, Isaac Stewart, and Howard Tayler. Defendi is a big (literally and figuratively) RPG writer, Isaac is an artist who does Brandon Sanderson's maps, and Howard does a sci-fi webcomic. I don't know Joshua Perkey and don't recall his background.

As with some of the other panels, this one was wide ranging and my notes may reflect that.

Perhaps the biggest effect of geography is travel time. Another effect is on clothing and shelter. What do people make their houses from? Natural resources are a huge issue.

**Weather and travel**—Ships in the Caribbean needed 2 pounds of food and 8 pounds of water per person per day. You would burn 4500 calories per day just moving around and heating your own body in Antarctica.

All parts of the Roman Empire were within 2 months of travel time. Leaders of great empires took the long view. When the Romans started building their network of roads, it was for their children or grandchildren.

Natural barriers like rivers and mountains make natural national borders because they are defensible and also form communication barriers. Communication barriers make it hard to maintain as part of the country and also make it easier to animalize (and hence fight with and kill) the people on the other side.

**Horses and travel time**—Do your research before putting in bogus stuff about how fast people can travel on horses. The Mongols could travel super fast because they had 12 horses per person. Oxen could only go so far in a day and could only turn so sharply when hooked up to a plow. This was actually the origin of the measurements for furlough and acre—the amount oxen could cover in a single day. The shape of the fields and towns was determined by what the animals could do. The original layout of trails and roads followed pig and animal tracks. Wagons were very impractical in many cases. Donkeys were better for rough terrain.

Look up Isaac Stewart's slide show for his presentation on maps at <http://isaacstewart.com/maps-ltue.pdf>.