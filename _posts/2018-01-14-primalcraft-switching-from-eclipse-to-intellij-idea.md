---
from_wordpress: true
comments_id: 1497
title: "Primalcraft: Switching from Eclipse to IntelliJ IDEA"
date: 2018-01-14T19:02:06-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1497
permalink: /post/primalcraft-switching-from-eclipse-to-intellij-idea/
categories:
  - Minecraft modding
---
I haven't been _entirely_ idle during the past month or so. From day one, I've been coding Primalcraft using [Eclipse](https://www.eclipse.org/home/index.php) as my IDE (integrated development environment). The main reason is that every single tutorial I found used Eclipse. As a novice, I decided to use what all the tutorials were using. It was much easier to get off the ground that way.

But I've heard some pretty good things about [IntelliJ](https://www.jetbrains.com/idea/) on the Minecraft Forge forums. While Eclipse is a very mature and ubiquitous product, IntelliJ is newer and sports some pretty fancy features. After toying with the idea for months, I finally decided to take the plunge and switch from Eclipse to IntelliJ.

I used [this tutorial by diesieben](http://www.minecraftforge.net/forum/topic/21150-using-intellij-idea-for-mod-development-with-forgegradle/). The switch was pretty easy.

Once I got started, I found that IntelliJ was automatically checking for a whole host of potential problems that Eclipse wasn't. Virtually every file in my project had some kind of warning. So I started working methodically through them, chipping away bit by bit. It took me several weeks to fix all the issues IntelliJ found.

Thanks to IntelliJ, I now feel like my mod is much more solid than it was before. Although I miss a few things about Eclipse, overall I'm pretty happy with IntelliJ. It seems much smarter, the interface is more modern, and it's just more pleasant to work with.
