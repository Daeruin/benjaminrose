---
from_wordpress: true
comments_id: 45
title: Plumb and Flyleaf
date: 2007-09-30T12:51:16-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=45
permalink: /post/plumb-and-flyleaf/
categories:
  - music
---
So I finally had a chance to check out the bands Plumb and Flyleaf this weekend. I don't have a whole lot to say about either of them, mainly because their musical style, while high quality, doesn't really fall into my normal realm of favorites. [Flyleaf](http://site.flyleafmusic.com/" title="Official site for Flyleaf) is, like Riotimus already mentioned, more along the lines of post-grunge than metal, and I certainly wouldn't classify them as progressive, goth, or symphonic metal. They write good music; it's just not my style.

[Plumb](http://www.plumbinfo.com/" title="Official site for Plumb) is interesting. Early on they were marketed as a band, but these days Plumb is just the solo artist Tiffany Arbuckle Lee. I wasn't able to find much of Plumb's early music except for the short clips on iTunes, and I've already stated how useless I find those clips. The official Plumb website opens up with music playing, and it turns out that Tiffany has moved to much softer and more obviously Christian stuff recently. And I actually like it, even more than the earlier, relatively heavier music of early Plumb. It's still not my favorite kind of music, but it's something I can appreciate for its own sake and in a completely different way than my favorite bands.

So that's my take on those two bands. They're not really the same as the other female-fronted bands I've talked about before. If I had to rank them along with the others, they'd fall pretty low on my personal list, though as I've said, not from quality issues. Thanks to those who mentioned them to me, though! I like exploring new music, regardless of whether I end up falling in love with it.
