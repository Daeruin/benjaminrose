---
from_wordpress: true
comments_id: 100
title: "I'm Going to Write a Novel"
date: 2008-07-03T22:24:20-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=100
permalink: /post/im-going-to-write-a-novel/
categories:
  - word counters union
  - writing and publishing
---
The Word Count Challenge has been great for me. For years now, I've daydreamed about writing, about being a writer. But I never actually wrote anything, except for here and there a couple of pages that didn't really lead anywhere. Thanks to Liz and the Word Count Challenge, I've written more publishing-oriented fiction in the past three months than I have in the past thirty years.

These past three months have been great. But I've still been—well, just tinkering. Too many of my daily 100 words have been tossed off negligently right before crawling into bed, or they've been wasted on "research."

That's about to change. Because I'm going to write a novel.

I'm not just going to write whatever's easy or whatever I'm in the mood for that day. I'm going to write _a novel_.

I've done the math. Michael A. Stackpole says that beginning novelists should shoot for about 100,000 words for their first novel. That's what publishers are looking for these days. If he's pretty close to being correct there, then at 100 words a day, it'll take me three years to write my novel. That doesn't count the outlining or the research. And that's just the first draft.

I don't have time for that. I know for a fact that I will never be able to sustain the energy and dedication required for that. Even if I wrote closer to 200 words a day (which was about my average number of words last month), it would still take me about three years to finish if you include research and revision.

So I figure that if I write closer to 500 words a day, I might be able to churn out a completed novel in just one and a half years or so. How in the world am I going to find time for that?

I've got to stop spending so much dad-gummed time on the internet. I know myself. I've got to cut it off, cause once I start in on it for an evening, it's too hard to stop. So I'm going to start limiting myself to just a couple of days a week checking e-mails and blogs. And, with my wonderful wife's blessing, I'm going to set aside a couple of hours during the weekends to shut myself in the office and work.

I'm going to do this. I'm going to write a novel. Watch out.