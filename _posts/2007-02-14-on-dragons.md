---
from_wordpress: true
comments_id: 11
title: On dragons
date: 2007-02-14T01:14:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=11
permalink: /post/on-dragons/
categories:
  - my fiction
---
Muomno cleaned his blade on the dead man's robe, then stepped over the body to the writing desk. A dim lantern hung over it, illuminating little else. On the desk lay a scroll half filled with the admirable calligraphy of the Empire's historians. In his fright, the dead man had dropped his pen when Muomno entered. It had left a blob of ink to mar the scroll's otherwise empty lower half. Muomno frowned at the blob. He would have wished to see the scroll in its finished beauty, but the historian had made Muomno kill him. It was unacceptable to allow the things he had heard to be passed on. Muomno began reading the scroll, slowly unraveling the formal historian's language.

> From Asayu, historian;
> 
> To Kumet whose fathers were Hata, Unmet and Tabunisis and whose mothers were Atanit, Wanaru and Kik; Kumet whose children are Tayi and Burutri; Kumet lord of Gisaha; Kumet whose soul shall live forever;
> 
> Concerning Asayu's research which is on Kumet's behalf and which regards the lore of dragons among those who call themselves the Burned People;
> 
> I have been with the Burned People for three months. Though I studied their language extensively, some concepts are still difficult to impart in their tongue. It took a great deal of convincing before they began to share their dragon lore with me. They seemed to believe that sharing such knowledge would endanger me, but some of them have come to understand the importance of committing their knowledge to the custodianship of the historians. Their souls may yet live to the extent they continue to impart their histories to me. A fuller account is contained in the formal record of my days.
> 
> Now I give an abbreviated account of my research regarding the dragon lore of the Burned People. Again, a fuller account may be found in my formal record. I set down here only that which I believe will be of interest to you, honorable Kumet.
> 
> According to the Burned People, dragons once lived everywhere, even in the water and und

There the historian's writing ended. Muomno left the tent, then set it on fire, leaving all the historian's scrolls inside to burn. It truly was a pity that such beauty must be destroyed. They had tried to warn the man. They had tried.