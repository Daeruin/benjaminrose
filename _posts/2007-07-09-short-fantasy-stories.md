---
from_wordpress: true
comments_id: 23
title: Short fantasy stories
date: 2007-07-09T19:09:46-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=23
permalink: /post/short-fantasy-stories/
categories:
  - writing and publishing
---
The other night I was thinking about short stories. In the speculative fiction genre, the market seems to favor science fiction short stories over fantasy short stories. Why do you think that is? Or am I wrong about that?

I think science fiction short stories are easier to market for several reasons. It's easier to write science fiction stories in the trendy, conversational styles that many readers find interesting and engaging. Science fiction plots are easier to fit in a short story—discovering an alien species, an encounter with strange technology, dealing with a futuristic disease, flying a spaceship, a snapshot of an alien culture, and so forth. On the other hand, fantasy stories are traditionally epic. It's hard to treat magic, the identifying element of fantasy, like you can treat technology in a short story, and keep it from sounding trite.

What are your thoughts?