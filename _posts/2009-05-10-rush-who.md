---
from_wordpress: true
comments_id: 118
title: Rush who?
date: 2009-05-10T09:15:22-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=118
permalink: /post/rush-who/
categories:
  - 'life &amp; stuff'
  - music
---
A few weeks after I started my job as a proofreader, I was sitting there reading some document with my headphones on, jamming to some of my favorite music to screen out all the office noise. The guy in the cubicle next to me, ignoring my efforts, asked what I was listening to.

"Rush," I said.

"Oh."

And he ducked back into his own cubicle without another word. Huh? Not a single comment about the band? "Who are they," perhaps? Or "What kind of music do they play?" Or, more appropriate, "I love Rush! They're the best band in the multiverse!" Nope. Not one word or comment.

I shrugged and went back to my work.

It was several years later that this episode returned to me and, with horror, I realized that maybe he thought I was listening not to Rush the god-like band, but Rush Limbaugh.

Noooooooooooooooooooooo!