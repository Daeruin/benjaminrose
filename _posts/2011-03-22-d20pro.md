---
from_wordpress: true
comments_id: 361
title: d20pro
date: 2011-03-22T21:13:03-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=361
permalink: /post/d20pro/
categories:
  - gaming
  - software
---
As mentioned [in my last post](/post/d20pro/
categories:), I've been using [d20pro](http://www.d20pro.com/) to run a traditional table-top roleplaying game. d20pro serves as a virtual tabletop with [lots of cool features](http://www.d20pro.com/virtual-tabletop-features.php), displaying maps and character markers, keeping track of character sheets, and even rolling the dice for you. All of your NPCs and monsters are built in. You can share arbitrary files with your players. You can draw custom maps, if you don't have an existing one to load into the software. Maps can have fog of war, so the players only see what the GM wants them to see.

In addition, d20pro takes care of combat for you, in large part. You choose the type of attack, the target, and any modifiers. d20pro rolls the dice, compares it to the target's defense, and lets the GM decide whether it was a hit or miss and modify the results if necessary. Then d20pro rolls damage, and again the GM gets a chance to modify and approve it. d20pro also keeps track of initiative, which as always the GM can modify at will. It has built-in functions for ready actions, charging, and a few other common occurrences. You can also do skill rolls, saving throws, and cast spells. Check out this promo video.

<iframe width="560" height="315" src="https://www.youtube.com/embed/ahGRNINGf64" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

d20pro does have some limitations and flaws. Naturally, it only works for d20-based games. The spell system works OK, but needs some major improvement before it becomes really useful. Currently you have to create all of the spells and their effects by hand, and it's a rather cumbersome process. And since players cannot edit their own character sheets, the GM has to manually select which spells the players choose at the beginning of each day (a rather idiotic rule in both Pathfinder and DnD, I must say). Finally, the overall user interface is a bit awkward at times. Many windows are too small or too large, so either the contents flow out of the window, or it takes a lot of scrolling to see the contents, or the window simply goes off the screen. Some buttons are super tiny, while others are oddly large.

But d20pro is written in Java, which means it works on any platform, and that's a huge bonus for a group with two Mac users. d20pro is also very affordable compared to other virtual table top programs. It's just $30 for a judge license (if you want to run games) and $10 for a player license (if all you want to do is play). Judge licenses let you have 2 players connect for free. Licenses are permanent, one time purchases, not subscriptions. Thank all of creation the sensible d20pro team didn't decide to go with the suck-em-dry software-as-a-service model I hate so much.

Overall, d20pro hasn't necessarily sped up our games at all. Combat has been somewhat simplified, but you occasionally have to spend time tinkering with the software to get it to do what you want (especially with spells), and communicating with 6 guys on a conference call can get chaotic and confusing at times with no visual cues to help things along. Not to mention the occasional technology glitches that are bound to occur, like people's internet connections going out, Skype crashes or inexplicable disconnections, mic problems, and so forth. (I should mention that you don't have to use it for long-distance playing; it can also make a great game aid for in-person games, as well.)

On the other hand, the d20pro team is very responsive to the community, often replying personally to posts in [their user forums](http://forums.d20pro.com/), unlike most other software companies I know. They are also active about developing the software—they are all gamers and use it for their own games. And it has allowed us to play much more often, since it eliminates travel time, babysitting challenges, and the need for a large, quiet, and dedicated space in which to play. Previously we would play every other month or so at most. Now we can play every other week.
