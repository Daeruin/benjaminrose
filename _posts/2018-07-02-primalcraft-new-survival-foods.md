---
from_wordpress: true
comments_id: 1551
title: "Primalcraft: New Survival Foods"
date: 2018-07-02T21:51:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1551
permalink: /post/primalcraft-new-survival-foods/
categories:
  - Minecraft modding
---
One of my goals with Primalcraft is to add true survival foods to the game—food you can find out in the wild that aboriginal cultures or our ancestors might have eaten to avoid starvation, and that you can still eat today if you're in a survival situation. The big barrier to this has been that almost all survival foods are seasonal in nature. Now that I'm done adding seasons to Primalcraft, I can finally get started on adding all this delicious survival food to the mod.

## Spruce Tips

In the spring, spruce trees produce new growth on the tips of their branches. Usually called [spruce tips](http://www.laurieconstantino.com/how-to-harvest-spruce-tips-with-recipes-for-using-spruce-tips-or-pine-tips-or-fir-tips/), these nascent pine needles are very soft and can be plucked and eaten directly off the tree. Survivalists often combine them with other greens or veggies into a yummy salad.

{%
  include figure.html
  image-name="primalcraft_spruceTipsReal.jpg"
  width="640"
  height="414"
  alt="Spruce tips"
  caption="Spruce tips"
%}

In Primalcraft, spruce tips appear randomly on spruce leaves in the springtime. Breaking the leaf block will drop a few spruce tips, which restore half a hunger bar. When summer comes, the spruce tips will slowly start to disappear, so gorge yourself while they last.

![Spruce tip in Primalcraft](/assets/images/primalcraft_spruceTips.png){: width="300px" height="219px"}

## Acorns

In real life, acorns contain a bitter substance called tannin that will make you sick if you eat too much. It's the oak tree's way of trying to prevent us from eating its babies. But resourceful hunter-gatherers discovered that shelling and then soaking acorns in running water would [leach out the tannins](https://www.backyardforager.com/cold-leaching-acorns-three-ways/) and make them very much edible. Once the tannin is gone, you can munch directly on the raw acorn nut or grind them up into a versatile [flour](https://www.thespruceeats.com/cold-leaching-and-preserving-acorn-flour-4007438).

{%
  include figure.html
  image-url="https://www.backyardforager.com/wp-content/uploads/2016/11/MG_1155-1024x683.jpg"
  width="750"
  height="500"
  alt="Leached acorns"
  caption="Leached acorns"
%}


In Primalcraft, you'll notice acorns appearing on the ground underneath oaks and dark oaks in the fall. Right click on an acorn with a hammer stone to produce shelled acorns. Shelled acorns can be eaten, but they'll give you nausea. Place shelled acorns in a wicker basket next to some flowing water for about a day first to turn them into leached acorns. Your leached acorns will restore half a hunger shank. Acorns will start disappearing in the winter.

![Acorn in Primalcraft](/assets/images/primalcraft_acorn.png){: width="300px" height="274px"}

That's it for now. Soon I'll be creating a way to get birch sap, acacia seeds, and cattails.
