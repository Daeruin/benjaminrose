---
from_wordpress: true
comments_id: 137
title: "Example of WoT's Iconicity"
date: 2010-01-23T17:20:50-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=137
permalink: /post/example-of-wots-iconicity/
categories:
  - fiction
---
Is "iconicity" a word? It is now.

I'm reading a series called the [Night Angel Trilogy](http://en.wikipedia.org/wiki/The_Night_Angel_Trilogy) by [Brent Weeks](http://www.brentweeks.com/). It's been really enjoyable for the most part. But something has cropped up in the third book that serves as a prefect example of The Wheel of Time's status as an icon in the fantasy genre.

It turns out that in this story there is a group of magic users who are all female. They live in a large tower called the White Seraph (well, the tower is in the shape of a huge statue; I guess that may not count as a tower). And their spells are described as weaves. Wheel of Time, anyone?

Despite this, I really have been enjoying the books. I would recommend them, with some qualifications. I'll try to post a review once I've finished.