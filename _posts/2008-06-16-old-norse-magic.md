---
from_wordpress: true
comments_id: 93
title: Old Norse Magic
date: 2008-06-16T21:24:51-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=93
permalink: /post/old-norse-magic/
categories:
  - uncategorized
---
I've decided to share some of my research on Vikings and Old Norse society. Today you get a brief summary I've written on Old Norse magic.

Magic was generally the province of women in Old Norse society. Although some men practiced magic, they were looked down upon for engaging in such a feminine activity.

Women practitioners were called _völva_. (Note that the Viking Answer Lady says the term “völva” refers only to spá magic (#2 below), rather than all types of magic.)

There were several types of magic:

  1. **Seiðr** included such activities as spirit journeys, soul healing, acting as a medium for the gods, affecting weather or animals, illusion, and mind manipulation. These were accomplished through the help of the gods, the spirits of the dead, or through potions. Practitioners of seiðr are called _seið-kona_ (seið-wife). Male practitioners were called _seiðmenn_ "Seið" has no direct translation.
  2. **Spá** was the art of foretelling the future or foretelling one's fate. This was accomplished using an inner intuition by directly interacting with the strands of the wyrd (the patterns of fate woven by the three Norns). Practitioners of spá are called _spá-kona_ (spá-wife or spae-wife). "Spá" has no direct translation.
  3. **Galdr** was a magical song, incantation, or chant. Galdr is related to the words nightengale and yell.
  4. **Runes** were used for foretelling and for inscribing spells. This is the one type of magic that was usually the province of men.

Sources: Articles on [Wikipedia](http://en.wikipedia.org/wiki/V%C3%B6lva) and [The Viking Answer Lady](http://www.vikinganswerlady.com/seidhr.shtml).