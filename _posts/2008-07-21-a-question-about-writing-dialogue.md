---
from_wordpress: true
comments_id: 103
title: A Question about Writing Dialogue
date: 2008-07-21T21:10:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=103
permalink: /post/a-question-about-writing-dialogue/
categories:
  - writing and publishing
---
I'm having a bit of trouble with my writing, and I hope you can help me out a little bit. My novel is set in a fantasy world based on Viking mythology. I'm struggling with how to make the dialogue sound authentic. Obviously modern slang is out. But what about contractions? Can my characters say "It's a fine day" or "That's not what I meant" or "I'm going riding today"? Or do those contractions seem too modern?

That's a specific question I have, but it's also a more general problem. Does anybody have any tips or advice on how to make dialogue sound medeival and historical without making it sound archaic and stilted? Have you read anything fantasy or historical fiction recently? How did the author deal with this problem?