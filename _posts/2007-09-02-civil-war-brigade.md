---
from_wordpress: true
comments_id: 34
title: Civil War, Brigade
date: 2007-09-02T16:15:33-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=34
permalink: /post/civil-war-brigade/
categories:
  - gaming
  - writing and publishing
---
If you're wondering why I haven't posted much fiction lately, I'll tell you: I've been working on a game manual. I have a friend who I play games with—board games, mostly, with the occasional role playing game thrown in—who was telling me about this game he found at Deseret Industries for a dollar. It's called _Civil War, Brigade_, one of those wargames with a gigantic and detailed map, but older so it uses little cardboard punchouts instead of figurines. The game manual is also gigantic and detailed, so much so that my friend said he could never get all the way through it. So I offered to read it and figure out how the game works, because I like doing that sort of thing. (I did the same thing for the _A Game of Thrones_ board game for some other friends.)

The _Civil War, Brigade_ game is actually a series of games covering various battles in the Civil War. The one I have is about the Battle of Gettysburg. One of the interesting things that sets this game apart from others is how it handles controlling your troops. You have to send your troops orders from your army's general. The orders take a certain amount of time to reach the intended troops, depending on terrain and distance, and there's always a chance that the receiving leader will reject or misunderstand the orders. And because of the time delay, circumstances may have changed, making the order pointless or even disastrous to carry out—yet you must, as far as you are able. In addition, many of the orders and arrangements of your enemy's troops are hidden from you. All of this is a pretty accurate portrayal of circumstances in the Civil War.

It sounded interesting, so I took it home and started reading. I couldn't get through the game manual, either. It was 40 pages of the most poorly written instructions I have ever read. Mostly it was a problem with the organization. Things just were not given in an order conducive to understanding. There were other problems, too, like the fact that the game author assumed his readers and players already knew something about the organization of armies during the Civil War and the terminology they used to refer to things.

So I decided to rewrite the manual. It sounded like a fun project. I'm kind of sick that way. Besides, I really wanted to try out the game.

Because of the haphazard organization and the complexity of the game, I have to go through it statement by statement and put the information in the proper place, then rewrite it so it fits in context. I highlight each statement in the original manual so I can keep track of what I've done. I'm constantly rewriting earlier portions of the manual because my understanding of things changes as I go or because critical information is explained much later in the book than it should be. It's very laborious. Yet fun. Did I mention I'm kind of sick that way?

I think I have all the main portions of the rules finished. I think. I'm now going through some of the less central-seeming rules, and after that I'll need to go through everything again for some serious editing. I need to make sure I have used game terms consistently and only with their intended meaning (another big problem in the original), and I have to ensure I haven't contradicted myself anywhere or accidentally changed something from the original book. (I have changed a number of things on purpose—mostly just terminology.)

The company that produced the game is now out of business. That seems to happen to a lot of game companies. A bunch of guys who love games get together and try to make a business, and they just can't seem to succeed. Look at what happened to Guardians of Order, the company who produced the immense and lavish role playing game for George R. R. Martin's _A Song of Ice and Fire_ series. They're gone now, bankrupt. I wasn't all that surprised to hear it, actually. They must have spent a fortune producing that huge tome, but they weren't selling it for much more than your average RPG book of half the size.

_Civil War, Brigade_ is now being sold by another company. As far as I know, they aren't producing new games in the series, just selling all the leftover stock from the original company. They have all of the game's original manuals up for download on their Web site. I wonder if they'd be interested in my rewrite—probably not. But maybe they'd put it up for other people to download. Maybe it would find its way into the hands of the original designers. Maybe somebody will recognize me as a gaming genius and pay me big bucks to write their game manuals. Maybe.

I'll let you know when I'm done.