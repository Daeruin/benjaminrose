---
from_wordpress: true
comments_id: 151
title: LTUE Report
date: 2010-02-16T22:22:10-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=151
permalink: /post/ltue-report/
categories:
  - writing and publishing
---
Remember how I [posted about LTUE](/post/ltue-writing-conference/) a couple of weeks ago? It was freaking awesome! Of course, I've never been to a convention before, so maybe I'm just naive and inexperienced.

But I figured you just might be interested in some of the things I learned at the three-day FREE convention—excuse me, conference—er, symposium? Their official material calls it a symposium. Whatever that's supposed to be. Please enlighten me, because I'm too desperate to get this post finished to spend time looking it up. Did I mention it was FREE?

The caliber of the panelists was pretty impressive for a free event. Brandon Sanderson was the guest of honor—not really surprising considering he teaches at BYU where LTUE is hosted. I knew some of the other panelists, including L.E. Modesitt, Brandon Mull, Howard Tayler, Dan Wells, John D. Brown, and Robert Defendi. There were a lot of other panelists including several romance and horror writers. Past guests of honor have included Kevin J. Anderson, Orson Scott Card, Barbara Hambly, Katherine Kurtz, Roger Zelazny, Lois McMaster Bujold, Patricia A. McKillip, Dave Wolverton (David Farland), and Elizabeth Moon. Pretty impressive, eh?

Most of the panels I attended had between one and two hundred people in attendance, although some had significantly less. One reading I attended only had about a dozen people in attendance, but it was awesome and gave me a new book I need to buy (_Servant of a Dark God_ by John D. Brown). Pretty much every event Brandon Sanderson attended was packed solid. Several events had standing room only if you didn't get there early.

The authors and panelists were very accessible, and I saw many of them chatting with attendees out in the hallways. Brandon Sanderson hung out in the registration room for several hours each day just to chat and answer questions. He was accompanied by throngs of ravenous fans everywhere he went.

The events themselves were pretty diverse. Panels ranged from subjects like "A Guy's Take on Romance" and "Worldbuilding Religion" to "What Does an Editor Do, Anyway?" A number of academic papers were presented, things like "The Position of Fair Tale Adaptations" and "A Thousand Words for Sand." There were some presentations on art and film, too, including a short 3D sci-fi film made by some BYU students.

Over the next few days/weeks, however long it takes, I'm going to report in more detail on some of the things I learned and reporting in general on some of the panels I thought were particularly cool. So stick around and check back for more.
