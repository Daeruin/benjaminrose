---
from_wordpress: true
comments_id: 183
title: Novel Aid for Word Users
date: 2010-04-05T20:21:14-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=183
permalink: /post/novel-aid-for-word-users/
categories:
  - software
  - writing and publishing
---
The other day I ran across an interesting piece of software that helps novel-writers who use Microsoft Word. The software helps you keep track of chapters and assemble them into a book. It presents you with an outline view of your chapters where you can drag and drop them to rearrange, and it automatically renumbers the chapters. (I imagine you could use it to organize individual scenes, too, if you simply call them chapters instead; the downside is you'd have many tiny chapters.) It keeps track of page numbers from chapter-to-chapter, and you can Find/Replace in all chapters simultaneously.

It's called [Chapter by Chapter](http://sites.google.com/site/sebberthet/chapter-by-chapter). Maybe you Word writers will like it.