---
from_wordpress: true
comments_id: 300
title: Pandora Experiment
date: 2010-12-14T14:58:20-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=300
permalink: /post/pandora-experiment/
categories:
  - 'life &amp; stuff'
  - music
  - software
---
I tried an experiment today. I turned on Pandora for the first time in several years. I fell in love with Pandora some time ago when I found out that I could listen to an entire station full of progressive metal. An entire station! Full of progressive metal! With nothing else! And no commercials!

Unfortunately, I never had time to listen to it. It was forbidden at work, and I couldn't really play it around the house much due to the kids and all. But now that I'm working from home, it just hit me today: I can thrown on the earphones and play Pandora all I like. That was part one of the experiment.

Part two was to create a new station to really take advantage of Pandora's music genome project. I wanted to choose two different artists and just see what came out. So here's what I based the station on:

  * Enya
  * Metallica

Can you get much different than that? I know. Pretty awesome. Or plain idiotic, depending on your point of view.

First came an Enya song followed by three—count them, THREE—more Irish/new-age influenced songs in a row, one of which I skipped and one that I actually liked quite a bit. Then finally came Metallica's Cyanide, followed by Disturbed and Rob Zombie. I got a few film sound tracks in there, a little Alice in Chains, a couple of Black Sabbath songs. Quite interesting.

So how do I feel about how my experiment turned out? Not so great. It was definitely interesting, but that was the problem. It was TOO interesting. I couldn't stop switching to my Pandora window to check out the artist and song name, ask Pandora what musical genes this song had that made it fit with the others, and in general waste a lot of time. I got a decent bit of work done, but not nearly as much as I could have by playing one of my tried-and-true iTunes playlists that just fade into the background, screen out any unwanted outside noise, and allow me to concentrate fully on my job.

Plus, Pandora has commercials now. That's enough to interrupt my concentration and throw me out of my work utterly and completely.

So, sorry Pandora, but I still can't listen to you at work. You're just too interesting.