---
from_wordpress: true
comments_id: 1076
title: RPG Combat Ideas
date: 2013-01-31T23:16:58-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1076
permalink: /post/rpg-combat-ideas/
categories:
  - gaming
---
I've been thinking a lot about combat systems for [roleplaying games](http://en.wikipedia.org/wiki/Role-playing_game), enough that I've decided to start a series of blog posts about it. I think everybody who has run a [table-top RPG](http://en.wikipedia.org/wiki/Tabletop_role-playing_game) as game master is secretly working on designing their own game, and I'm no exception. I've been working on mine off and on for years. Some of my very early blog readers might remember some of the ideas I posted way back when.

My thoughts on RPG design have changed quite a bit since then. My primary influence in the beginning was _MERP_ (Middle-Earth Roleplaying), also called Rolemaster Lite by some, which is now out of print. MERP and especially Rolemaster have a lot of things I like, namely the detailed critical wound charts and the unique damage possibilities for each weapon and armor combination, but I've since run across a number of other games that have drastically changed my opinion of how my ideal game would work.

_The Riddle of Steel_ (TROS) revolutionized my view of how RPG combat can work, with the idea of defenders being able to choose their own defensive maneuvers and actually roll for them at the same time as their attacker. There's a lot more, though:

  * the idea of having a whole pool of dice that you have to ration between attack and defense
  * historically-based combat maneuvers each with unique effects
  * the idea that initiative simply means you're currently attacking, and two people could easily attack and kill each other simultaneously if they're not careful
  * no abstract hit points; instead, concrete wounds and pain levels are used
  * getting bonuses to your attacks when you're fighting for something your character believes in
  * the idea that if you want your character to be good at combat, you actually can be—you aren't forced to start out as a level 1 weakling

Unfortunately, The Riddle of Steel is out of print. Fortunately, there are another couple of games based on TROS that use many of the same mechanics. One is [Blade of the Iron Throne](http://www.ironthronepublishing.com/), which seeks to provide games in the sword and sorcery genre. It's available as a free PDF download. Another is [Song of Steel](http://www.songofsteel.net/), a game of historical military drama, which is still in development.

[_Codex Martialis_](http://rpg.drivethrustuff.com/product/75133/Codex-Martialis-CORE-RULES-V-23) approaches combat in a very similar manner to The Riddle of Steel, but with significant differences due in part to its reliance on the d20 system framework. Codex Martialis has an increased emphasis on differentiating weapons and their fighting styles, so weapons get bonuses when used at the range they were designed for. Other things I like:

  * historically-based "feats" allow you to customize your fighting style based on your chosen weapon
  * certain defense rolls grant you automatic counterattacks, making combat much more dynamic
  * some rolls results in a "bind," the meeting of weapon-on-weapon that can trigger special offensive and defensive maneuvers
  * armor acts as damage reduction
  * hit points are capped
  * critical hits don't require an additional roll to see if they actually happen
  * crits can deal even more extra damage when using an attack that your weapon was designed for.

I've also run across a few games that I definitely don't want to emulate, and I'm grateful for those, too. [_Burning Wheel_](http://www.burningwheel.org/?page_id=2) is an awesome game in many respects. I love its brilliant lifepath character generation, and it relies on dice pools and character motivations much like TROS. But I absolutely loathe the core mechanic of its combat system—you have to choose your actions in groups of three, and once chosen they're locked in, making it impossible to react to changing circumstances. It adds an interesting level of uncertainty and danger to combat, but it breaks my suspension of disbelief beyond recovery.

I really enjoy the feat system of [_Pathfinder_](http://paizo.com/pathfinderRPG) and the d20 system, because it's fun to watch your character improve and gain new abilities throughout the game. But some feats are pretty silly (Deafening Criticals, anyone?). I also dislike its turn-based combat resolution. Pathfinder and D&D 3.5 aren't the only games to feature turn-based combat, but they are the primary ones to have invented a bunch of nonsensical mechanics around it (attacks of opportunity, frozen statue syndrome when it's not your turn, etc.).  There's more that frustrates me about the d20 system: ridiculously high hit point levels and the strange rationalization that being getting hit by an attack doesn't necessarily mean you've actually been hurt or even struck until the one that reduces you to 0 hit points, the silly idea of having to rememorize spells every day, the outrageous inflation of gold currency . . . the list goes on.

And I have yet to come across a game that makes archery actually interesting, not to mention even remotely realistic.

Of course, my gaming experience isn't really that extensive. There are games I feel I should probably play. For example, I've never played [_GURPS_](http://www.sjgames.com/gurps/), but the little I've heard about it makes me think I would like some of its mechanics. But I've read dozens of game manuals, scouring their combat rules for something that excites or intrigues me, only to be disappointed time and time again with a mundane reliance on hit points, initiative rolls, turn taking, frozen statue syndrome, generic weapons, and so forth.

So I've been working on my own combat system that combines all of my favorite mechanics without making it too complicated. I'll be sharing some of my ideas on the blog in the future.

What are your pet peeves about RPG combat, and what are your must-have mechanics?
