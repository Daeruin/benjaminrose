---
from_wordpress: true
comments_id: 1276
title: "Primalcraft: Updating the mod to a new version of Minecraft"
date: 2017-08-14T22:28:43-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1276
permalink: /post/primalcraft-updating-the-mod-to-a-new-version-of-minecraft/
categories:
  - Minecraft modding
---
I just spent a couple of days updating my mod to a newer version of Minecraft. It's a pain in the tookus.

When I started modding, back in the mists of time, Minecraft was still on 1.8.9. There were a lot of tutorials available, thank the heavens. I started out copying quite a bit of code that I didn't understand very well, but I was able to get things working because I had lots of resources available. The update from 1.7.10 to 1.8 had thrown a lot of modders for a loop, in large part because of the move to json files to define block states and models. I missed that, and came into the modding scene while 1.8 had been around for over a year. Things were pretty stable.

Then Minecraft got sold to Microsoft. They started releasing major version updates every six months. And so in the time I've been modding we went from 1.8, to 1.9, all the way to 1.12. That's four big updates and quite a few minor updates.

I was happy with 1.8.9. But at a certain point I really had to update or risk having my version of Forge being deprecated. At the time, 1.10.2 was the newest version. Learning how to update, and getting everything done, took me months. MONTHS.

So I was not happy at all to consider updating again.

It went surprisingly well. One of the advantages, I think, of having new versions on a more frequent basis is that not quite as much changes between versions.

The biggest change for me as a modder was the fact that ItemStack objects can no longer be null. On older versions, the fact that they could be null meant that my code was littered with null checks like this:

> if (itemStack != null)

There's no way to find all those automatically. With other changes, like changed method names or parameters, Eclipse will highlight them as errors. But with those null checks, there's nothing actually wrong. Eclipse has no idea that an ItemStack can't be null. So I had to comb through my code with a—well, with a fine-toothed comb, to find all of those null checks. I hope I caught them all.

At least I've eliminated all the obvious errors after the update. I'll be testing pretty much my whole mod over the next few days to try to catch any unseen problems or missed null checks.

And soon I need to update to 1.12. I'm holding off for now, since it seems like Forge still has some work to do to be fully compatible with 1.12.2 (they say they're completely rewriting the registry system—yikes!).