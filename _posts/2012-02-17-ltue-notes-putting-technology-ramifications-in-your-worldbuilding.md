---
from_wordpress: true
comments_id: 779
title: "LTUE Notes: Putting Technology Ramifications in Your Worldbuilding"
date: 2012-02-17T21:24:28-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=779
permalink: /post/ltue-notes-putting-technology-ramifications-in-your-worldbuilding/
categories:
  - writing and publishing
tags:
  - LTUE
  - technology
---
Today's selection of notes from LTUE comes from Roger White's presentation, "Putting Technology Ramifications in Your Worldbuilding." His presentation was structured around four main points.

**1. How to think about new inventions**

  * First use of technology is the commodity use. This is the original purpose and it usually replaces an old technology by accomplishing the purpose faster, better, and/or cheaper.
  * Technology then has a surprise use. This is the purposes that society adapts it to but were unforeseen.

Examples:

  * Cars. They were originally invented for doctors so they could reach their patients faster without having to rely on horses. The surprise use was for pleasure and convenience.
  * Computers. Their first use was for calculating missile trajectories. Their surprise use was by college professors sharing research, and then of course things took off from there. (Thinking back on the panel, I might have accidentally mixed this up. I'm pretty sure the part about college professors was referring to the internet, not computers specifically.)
  * Language. Animals and early man both had primitive ways of conveying danger warnings. The first use of language was to make these warnings more useful (convey time, severity, location, etc.). The surprise use of language was teaching (there was no use for a big brain until language made it possible to store more knowledge).

**2. Inspiration versus final reality, or Birds and Boeings**

The inspiration for airplanes was that men wanted to fly like a bird. The final reality was of course very little like birds.

The inspiration for genetic engineering was to make clones of yourself. The final reality was for making new proteins and plastics.

Inspiration comes from basic desires or imagining what to do with a new technology. The final reality is driven by the constraints and problems actually encountered along the way.

**3. When does a technology die?**

Tattoos were used as a method of personal decoration. T-shirts could be seen as a replacement technology for tattoos. Yet tattoos have not gone away. The reason: personal expression causes technologies to stick around. This is why we still play instruments manually instead of using computers all the time.

**4. Structuring society around technology**

Ask the following questions:

  * Is it technology or magic? Magic takes training to produce and is typically done on a case-by-case basis—the apprenticeship/craftsman model. Technology is reproducible by mass production and will quickly change society.
  * Who gets it, who doesn't? How widely is it available?
  * How will government change?
  * How will social relations change?
  * How do the entrepreneurs get rewarded? What's in it for them?

Good examples of well-thought-out technology in movies: Limitless and Moon. They thought out the different ramifications, there were surprise applications.

Bad example: In Time. There was nothing new to this technology. It was just a parable of modern day situations.
