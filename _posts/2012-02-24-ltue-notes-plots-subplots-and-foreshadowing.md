---
from_wordpress: true
comments_id: 807
title: "LTUE Notes: Plots, Subplots, and Foreshadowing"
date: 2012-02-24T20:59:24-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=807
permalink: /post/ltue-notes-plots-subplots-and-foreshadowing/
categories:
  - writing and publishing
tags:
  - LTUE
  - plotting
---
Today's selection of notes from LTUE comes from the panel "Plots, Subplots, and Foreshadowing." The panelists were Bree Despain, James A. Owen, J. Scott Savage, Brandon Sanderson, and Stacy Whitman (Stacy is an editor for Tu Books).

**Subplots**

  * Part of the purpose of subplots is to make your story feel alive. They make your characters feel alive and dynamic.
  * YA is more linear, and subplots are used mainly to give the main character multiple problems to juggle at the same time.
  * Subplots are a reward for careful readers and can contribute to the longevity of your writing career.
  * Multiple plots allow you to make every chapter critical without overloading the reader on one story.
  * Subplots can reinforce and echo the main theme.
  * Integrate the subplots throughout to make your story feel cohesive. Don't lose sight of them. If you add something to the story, you need to see it through.
  * Don't add anything new in the last quarter of a book. That undermines the story.
  * Rather than add more subplots, make them more layered. Each subplot can have increasing levels of awareness for those "in the know" or who are more well read.
  * Subplots need to have a beginning, middle, and end, just like the main plot.

**Foreshadowing**

  * If your book might have series potential, outline further books in the series before finishing the first book. This allows you to put proper foreshadowing in the first book.
  * There needs to be a spectrum of different types of foreshadowing. Subtle foreshadowing can make later events feel more realistic, while obvious foreshadowing can make the reader excited for something that's about to happen.
  * Foreshadowing can sometimes be as much about the distractions as the real clues.