---
from_wordpress: true
comments_id: 117
title: Babies and Houses
date: 2008-11-25T22:37:49-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=117
permalink: /post/babies-and-houses/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
It's been over three weeks since I wrote anything for the blog, and a lot has happened since then, to wit: we had a baby and bought a house. The house is our first; the baby isn't. For the grisly details on the baby, check out [my wife's blog](http://normalpainsandpleasures.blogspot.com "My wife's blog").

Although I've been able to get some moderately good sleep and had snippets of free time most days, I have not been writing. Instead, I restarted my World of Warcraft account—the very day before the baby came. World of Warcraft takes just the right amount of brain power for me to handle right now (i.e., none). Yet it's not at all fulfilling. I crave the satisfaction that writing brings me, but it's just too taxing right now. It takes too much out of me. I worry that it will become more difficult to restart the longer I wait, but I'll just have to tackle that obstacle when the time comes.

In the meantime, we now have the remodeling of our new home to worry about. Some work needs to be done before we can move in, and we're on a pretty strict budget—which means it will take extra time and effort to get the right deals. Sigh. Someday soon, the writing will be part of my life again. I promise.