---
from_wordpress: true
comments_id: 166
title: How to Write a Story that Rocks, part 2
date: 2010-03-03T22:23:45-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=166
permalink: /post/how-to-write-a-story-that-rocks-part-2/
categories:
  - writing and publishing
---
Aside from the stuff I mentioned in [my previous post](/post/ltue-report-how-to-write-a-story-that-rocks/), there are several more things that had an impact on me from the workshop on [How to Write a Story that Rocks](http://johndbrown.com/2010/02/how-to-write-a-story-that-rocks/) at [LTUE](http://www.ltue.org/LTUE2010.html).

But first I want to continue for a bit on the idea of writing what you love from my previous post. The presenters recommended an activity that I mentioned briefly, and I want to talk a bit more about it.

They recommend that you write down 10 stories that you love. Compare them, think about them, and figure out what it is that you love the most about those stories. That's what you should be writing.

As an example, here is my list. These are stories that I'm constantly returning to and thinking about lately. They're roughly in order of how cool I feel they are to me at this particular moment in time. I had a hard time narrowing it down to 10, and some of them are actually series. But of course the exact number doesn't matter. Here we go.

**The Top 10 (Well, 12) Stories I Love at This Particular Moment**

  1. The Blade Itself
  2. The Name of the Wind
  3. The Prince of Nothing
  4. Latro in the Mist
  5. A Game of Thrones
  6. Assassin’s Apprentice
  7. Doomsday Book
  8. Mistborn
  9. The Lord of the Rings
 10. Memory, Sorrow, and Thorn
 11. The Golden Compass
 12. A Wizard of Earthsea

Now for the things I loved about these stories. I broke it down into general characteristics, character concepts, and magical elements.

**General Characteristics I Love About Fantasy**

  1. Complex characters that surprise me and make me feel strong emotions
  2. True danger to the characters and true cost to their actions
  3. Dangerous and adrenalin pumping fights
  4. Snappy and believable dialogue that reveals character motivations
  5. A sense of mystery, history, and vastness
  6. Honor, friendship, duty, and betrayal
  7. Hope and courage in the face of extreme despair
  8. Realistic details tastefully interwoven with narrative
  9. Narrative style that reflects the time and culture
 10. Amazing description
 11. Mysterious and dangerous magic
 12. Awesome magic systems
 13. Incredible settings

One of the interesting things to me was that most of these things are not exclusive to fantasy. In fact, only two of them are. The rest of them you can get in any good book in just about any genre. So I focused my next lists on two things—the part that's intrinsic to fantasy (magic), and the part that's most universal to all stories (character).

**Character Concepts I Love**

  1. A royal bastard training to be a secret assassin, torn between worlds
  2. A hunted, psychotic barbarian trying to be a good man
  3. A wild and violent young girl cast into the world to care for herself
  4. A dangerous warrior who loses his memory and sees the gods
  5. A slight young thief girl who discovers she has magical abilities
  6. A spunky, time-traveling historian stuck in a deadly time
  7. A sarcastic and ruthless torturer with a ruined body and a death wish
  8. A traveling performer who’s orphaned and wants to learn the name of the wind
  9. A silver-tongued young girl whose best talent is lying

How many of those characters do you know? They all come from the books I listed earlier. I noticed that with many of these characters, I focused more on their personalities than their situations, and secondarily on what they can do.

Finally, my list about magic. A few of these won't mean much to you unless you've read the book they're from, but I tried to add a bit of an explanation to some. This list was a little strange because some items were specific magical occurrences, while some were about the overall system. I sorted them out into two sub-lists.

**Magic I Love**
1. Magic that’s as much curse as blessing
  * Giving birth to a shadow assassin
  * Giving life with a kiss, but the body remains ruined
  * The ability to see the creepy, invisible gods, who then manipulate and torture you, and occasionally reward you
2. Magic that’s deeply logical and symmetrical
  * Pushing and pulling metals—where speed, distance, and trajectory depend on your mass and position relative to the object you're pushing/pulling
  * Sympathetic magic—you draw some connection between two objects, will yourself to believe the impossible, and then transfer energy from one to the other (or something like that)
  * Gnostic magic—spells that result in mathematically symmetrical destruction, and rely on your command of language and ability to hold multiple meanings in your mind simultaneously
  * Runes—exotic symbols that bestow power

So there you go. That might give you a little bit of insight into the type of story I'm going to tell.
