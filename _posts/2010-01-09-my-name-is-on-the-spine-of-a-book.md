---
from_wordpress: true
comments_id: 107
title: My name is on the spine of a book
date: 2010-01-09T20:27:59-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=107
permalink: /post/my-name-is-on-the-spine-of-a-book/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
I'm a published author, but it didn't happen in the way you usually think of. I didn't complete a manuscript. I didn't get an agent or a contract from a big New York publisher, let alone an independent small press publisher.

Actually, in the interest of full disclosure, I should say I'm a published _co_-author. And the book was already in its third edition. I joined the two original authors for the fourth edition. I should also say that I probably only wrote about fifteen pages of original material, although it was more than the other two authors for this edition. In fact, my role was really more of an editor than anything.

![Principles of EMD](/assets/images/principlesOfEMD_fourthEdition.gif){: .image-wrap-right}

In case you haven't guessed by now, this happened in the workplace. I work for a company that trains 9-1-1 dispatchers how to handle emergencies over the phone. I was originally hired as a simple proofreader to make sure their lifesaving medical, police, and fire scripts were error-free, in addition to all the accompanying training materials. Together with a colleague, the owner of the company had written a comprehensive [textbook](http://www.prioritydispatch.net/prd_principle.php) on the science and art of being an emergency medical dispatcher. The company usually only prints enough copies for about 18 months at a time, which allows periodic updates to the book to reflect the evolving science of medical dispatch.

In my second or third year at the company, the book was in its third edition, and the owner/author, Dr. Clawson, asked me to manage all the updates to the book. He would feed the changes to me, and I would make sure they got done correctly. It's a large, complicated technical book and making the updates is very time-intensive. Luckily it's the sort of task I enjoy. Gradually I began doing more and more work on the book, until for the fourth edition I was writing nearly all new material with minimal oversight from Dr. Clawson. After spending five years constantly proofreading the book and the medical scripts and training materials it's based on, I had become intimately familiar with the material. It was quite a surprise when Dr. Clawson decided I had done enough work to merit being listed as an author for the book.

So, I've been published—not in the manner of my dreams, but it's something.
