---
from_wordpress: true
comments_id: 42
title: Shoot the Mime
date: 2007-09-20T22:17:29-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=42
permalink: /post/shoot-the-mime/
categories:
  - music
---
Back in the day, my brother, author of Blog Ing, was in a cool rock band called Shoot the Mime. I think most of my blog readers already know that, either from reading Blog Ing or from knowing Ing back in the day. So you probably know that we're trying to help prove Shoot the Mime existed by giving the band a little bit of a Web presence. The goal is to keep Ing's Shoot the Mime on the first page of the Google search results.

So, this blog entry is my modest contribution. Here's a picture of a Shoot the Mime t-shirt that I still wear. (I posted this last night but forgot to include the picture.)

![Shoot the Mime t-shirt](/assets/images/shootTheMimeShirt.jpg){: width="510px" height="450px"}

To read a little more about Shoot the Mime, head over to [Blog Ing](http://blogging-ing.blogspot.com/search/label/About%20music).
