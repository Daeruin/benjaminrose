---
from_wordpress: true
comments_id: 84
title: True Names Again
date: 2008-05-24T00:49:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=84
permalink: /post/true-names-again/
categories:
  - 'life &amp; stuff'
---
I'm finally implementing my plan to use my real first name here on my blog and other places around the 'Net. Just wanted to let you know so as not to confuse anybody.

So from now on, Daeruin = Ben.