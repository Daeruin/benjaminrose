---
from_wordpress: true
comments_id: 1341
title: "Primalcraft: Movement Speed"
date: 2017-10-09T07:25:36-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1341
permalink: /post/primalcraft-movement-speed/
categories:
  - Minecraft modding
---
Continuing on my trend of adding fun but easy features, I've made it so in Primalcraft the player's speed is based on the kind of block they're traveling on or through. This is very similar to the hardcore movement feature of Better Than Wolves.

I like this feature because it will cause the player to strategize about what path they take—especially in the early game when getting back to your campfire before dark can be a matter of life or death. It also feels more realistic, which is a big goal of Primalcraft. I'm hoping that in the middle to late game, it will also be a strong motivation for players to keep common pathways clear of grass, not to mention actually creating real paved pathways.

So what kind of speed differences are we talking about? Leaves cause the biggest slowdown, followed by sand and snow. You'll notice a big difference, and it will feel painful to travel on those materials. Gravel, farmland, and loose dirt all cause a moderate slowdown. Tall grass, reeds, crops, and the like cause a small slowdown that's not too noticeable.

On the other hand, stone, wood, and grass paths actually speed the player up. Everything else, including the ubiquitous dirt and grass blocks, cause no change.

Not much else to say about this one. I'm happy it was so easy to accomplish, and I hope it provides a little more interest and challenge to the game.