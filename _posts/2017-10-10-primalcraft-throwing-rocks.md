---
from_wordpress: true
comments_id: 1343
title: "Primalcraft: Throwing Rocks"
date: 2017-10-10T07:36:24-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1343
permalink: /post/primalcraft-throwing-rocks/
categories:
  - Minecraft modding
---
Today I bring you yet another small but fun feature in Primalcraft: throwing rocks.

The overriding theme of Primalcraft is extreme survival in the stone age. So your earliest tools will always be the simplest of objects: sticks and rocks.

![Throwing a rock in Primalcraft](/assets/images/primalcraft_throwingRock.png){: width="292px" height="294px"}

Given the difficulty of getting stone tools in Primalcraft, and how fragile they are once acquired, I wanted to make it possible to still feed yourself even without crafted tools. And since animals in Primalcraft run away from you, it's important to have a ranged weapon to try to get yourself some lunch.

Enter the throwing rock. You'll find them scattered about the landscape. They are fairly rare, but more plentiful than precious core stones and hammer stones. As with all of these useful rocks, they are more plentiful around gravel, stone, and open grassland. You can also find them in the water (although the visual texture leaves something to be desired; I'll try to work on making it look nicer).

Shift-right-click to pick them up, then use the usual right-click to throw them. In vanilla Minecraft, ranged combat is sadly easy. You can reliably hit just about any enemy from a long distance away, as long as they aren't moving (and vanilla mobs spend a lot of time just standing still). To make things a little more challenging, I've made it so throwing stones won't travel nearly as far as arrows or snowballs. You may have to run to get close enough to beasties to hit them with a rock. Catching animals in the water or on hillsides is a good way to get close enough. So stoning them will still be a bit of an ordeal and may even take all day.

![Throwing a rock in Primalcraft](/assets/images/primalcraft_throwingRockOnGround.png){: width="500px" height="407px"}

On the bright side, throwing-rocks often fall back to the ground so you can recover them for reuse.

And I tried making another video. Success is borderline on this one. At least it uploaded this time!

<iframe src="https://www.youtube.com/embed/YGtVH4oMLHs" width="853" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
