---
from_wordpress: true
comments_id: 134
title: Avatar
date: 2010-01-05T19:47:28-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=134
permalink: /post/avatar/
categories:
  - 'movies &amp; TV'
---
It seems everyone is talking about Avatar. I'm glad it was so successful. I hope it will continue to convince more studio executives in Hollywood of the box office potential of big budget sci-fi movies.

![avatarMovie](/assets/images/avatarMovie.jpg){: width="450px" height="280px"}

Avatar is being released in three different formats. I guess there are probably more accurate technical terms to describe the various formats, but these work for me: IMAX 3D, regular 3D, and plain ol' movie (not 3D). I saw the movie in regular 3D, and it was my first-ever 3D movie experience. From everything I had read, I knew I needed to see the movie in the theater just for the experience, the sheer spectacle of the thing. I did not expect it to be a really great movie. But . . .

I loved it! My wife and I walked out of the movie theater thoroughly satisfied, even amazed.

Which isn't to say it was perfect. It was an awesome movie-going experience, but there were certainly some flaws.

There were certain shots where the 3D effects were completely mind-blowing. The landscape and forest shots were particularly impressive. There were several times where I had to stop myself from trying to brush the ferns and falling leaves in front of my face. Many mid-range shots were utterly realistic and felt completely immersive, as though I were right there in the room. Even better, there were huge portions of the movie where I forgot it was even 3D because the story had drawn me so completely in.

There were certain other shots that were not so impressive. In fact, some shots looked even more fake in 3D than they would have in 2D. There were several shots with just two people in the foreground, where the background was far away and blurry, and it seemed the two people were crisp cardboard cutouts being moved by sticks across the screen. There was another shot of a cramped room where a cabinet on one side jumped out at you, distracting from the scene in the room. These shots were not very frequent, but they did bump me out of the movie immersion several times.

The characters and story were both competent and even compelling, although perhaps not on the level of genius. I've heard some people gripe about cliche characters, predictable plot, and nonsensical plot holes. I can understand where these people are coming from, but none of it bothered me. While there was nothing earth-shatteringly original, I did not feel that anything descended to the level of cliche. In fact, there were several extremely moving moments in the movie. I actually felt my eyes start to water twice near the end (no fluid escaped my eyelids, therefore I did not cry; this is an important distinction to make!). Try not to be too critical, take a few things here and there with a grain of salt, and you will have a great experience.

I was very pleased to hear that the director hired a real linguist—not just someone who can speak or write multiple languages; I'm talking about someone with a PhD in Linguistics—to create a language for his alien race. The language is called [Na'vi](http://en.wikipedia.org/wiki/Na%27vi_language), and it seems poised to become [the next great geek language](http://www.learnnavi.org/), like Klingon. Paul Frommer, the linguist who created it, has done [some interviews](http://www.vanityfair.com/online/oscars/2009/12/brushing-up-on-navi-the-language-of-avatar.html) that I found very interesting. Since the movie demanded that humans be able to speak the language, Frommer was somewhat limited in how exotic he could make the language, but he did incorporate some constructions that are pretty rare among the earth's 3000 languages and certainly foreign to English speakers, such as [infixes](http://en.wikipedia.org/wiki/Infix), [ejective consonants](http://en.wikipedia.org/wiki/Ejective), and [lenition](http://en.wikipedia.org/wiki/Lenition).

So there's my take on Avatar. I definitely liked it, and recommend it with only a few very minor caveats. In fact, I liked it enough that it may very well become one of the very few movies that I actually care to own (along with The Last of the Mohicans, The Matrix, The Lord of the Rings, The Scarlet Pimpernel, and The Princess Bride). That would put it in my top ten movies of all time. Huh. I didn't see that coming.
