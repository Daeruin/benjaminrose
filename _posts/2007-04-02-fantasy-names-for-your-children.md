---
from_wordpress: true
comments_id: 17
title: Fantasy names for children
date: 2007-04-02T19:32:58-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=17
permalink: /post/fantasy-names-for-your-children/
categories:
  - 'life &amp; stuff'
---
In case you didn't know, my wife is currently pregnant with our second child. If you haven't heard yet, this is the announcement! The little one is due in September. We're going to find out in three weeks what the gender is. What do you think it will be?

**Note:** _A poll used to appear here, but I have since discontinued using the plugin required to display it. Apologies to those who have come late to this post._

We can think of several girl names that we both like and would love to use. But for some reason, we have a harder time with boy names. We've considered a lot of names, but none of them works for us. Here's your chance to help us out. I'm thinking maybe we need to consider names that we wouldn't normally think of. What are your favorite fantasy names that you think might actually work for a real name?
