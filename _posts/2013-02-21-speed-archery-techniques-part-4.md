---
from_wordpress: true
comments_id: 1067
title: Speed Archery Techniques, Part 4
date: 2013-02-21T10:25:51-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1067
permalink: /post/speed-archery-techniques-part-4/
categories:
  - medieval combat
---
My posts on speed archery have been very popular and continue to get a lot of traffic. So I've been digging into it more and trying to find more resources for those of you who are interested in the topic. If you haven't seen them already, check out my first three posts in this series. I'll link to them at the bottom of this post.

Today I bring you several interesting videos demonstrating techniques from Native Americans, ancient Turkey, and more from Russia.

This fellow has figured out how the Native Americans likely held and nocked their arrows quickly and quietly. He doesn't do a full-speed demonstration, but he shows it slowly and clearly. It looks intriguing. Check it out.

<iframe src="https://www.youtube.com/embed/H0ahOkSQEBE?rel=0" width="560" height="315" frameborder="0"></iframe>

This fellow describes his recreation of ancient Turkish archery. His method is very similar to the Hungarian archer I linked to in [part two of this series](/post/archery-speed-shooting-techniques-part-2/ "Archery: Speed Shooting Techniques, Part 2"). He gets off a shot every 3 seconds.

<iframe src="https://www.youtube.com/embed/GqqStGeYsj8?rel=0" width="480" height="360" frameborder="0"></iframe>

Here is another video about Turkish archery. It's got a lot of fluff, but around the 3 minute mark he does demonstrate several interesting techniques, including a method for holding two extra arrows in the hand while shooting. Another interesting technique allows him to hold his sword in the hand while shooting, so he can quickly attack with his sword afterward. The video also brings up the use of thumb rings, which is another topic I'd like to delve into.

<iframe src="https://player.vimeo.com/video/26835828" width="400" height="300" frameborder="0"></iframe>

More speed shooting from Russia:

<iframe src="https://www.youtube.com/embed/I4OGiRP_dFY?rel=0" width="480" height="360" frameborder="0"></iframe>

If you haven't seen my other posts, check them out:

  * [Part One](/post/speed-archery-fast-and-deadly/ "Speed Archery: Fast and Deadly") — Two videos of fast archery techniques from Russia
  * [Part Two](/post/archery-speed-shooting-techniques-part-2/ "Archery: Speed Shooting Techniques, Part 2") — Two videos of Lars Andersen demonstrating his incredibly fast shooting and one of Lajos Kassai showing ancient Hun archery
  * [Part Three](/post/fast-archery-techniques-part-3-the-crossbow/ "Fast Archery Techniques, Part 3: The Crossbow") — Methods for quickly spanning or cocking a crossbow
