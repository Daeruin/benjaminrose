---
from_wordpress: true
comments_id: 109
title: The Well of Ascension
date: 2008-09-02T20:17:40-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=109
permalink: /post/the-well-of-ascension/
categories:
  - fiction
---
_The Well of Ascension_ is the second book in Brandon Sanderson's Mistborn trilogy. Although the first book is usually called just _Mistborn_, its full title is _Mistborn: The Final Empire_.

I don't really know how to do this review without giving at least a few minor spoilers (such as which characters survive the first book), so don't read this if you plan on reading The Well of Ascension. And you should read it. It's a good book and well worth your time. However, I didn't think it was as strong as the first book, and I'll tell you why.

First, this whole book was too reactionary. The characters we grew to love from the first book were competent, fun, and they knew what they wanted. The same characters in the second book are constantly fumbling around on the defensive, never sure what to do or what they even want. It was very frustrating for me to read. I couldn't enjoy the characters who used to be my favorites from the last book. The one exception is Vin, who was as compelling as always. Breeze came really close, but he needed more screen time than he got. Oh, I forgot Sazed; I really enjoyed his part of the story as well.

Second, Elend Venture becomes a major character in this book, but he can't carry the load. Sanderson didn't give Elend enough depth; he was just flat through most of the book. He really needed to go though a lot more angst, like Vin, but he didn't change or grow at all. He was just kind of boring, unlike the strong, glib, and convention-bucking guy we knew from the first book.

Now that that's out of the way, I'll move on to some of the reasons I liked the book. As I mentioned in my review of _Mistborn: The Final Empire_, Sanderson is really good at writing fight scenes, and his magic system is very interesting and well thought out. I _love_ reading his fight scenes. He also puts a great deal of thought into presenting an interesting and realistic conflict on a macro-level—I'm talking political intrigue and cultural world building. It's just unfortunate he didn't do his characters the same service this time around. I felt, based on the previous book, that they should have been up to the challenges he invented, but they didn't live up to that precedent.

It's really so much easier to focus on the negative than the positive, because it's the sucky things that stick out. The good things tend to be kind of invisible. So while I have a hard time articulating just what it was I liked about it, I hope you'll take my word for it when I say it was good despite its few flaws. I recommend this book.