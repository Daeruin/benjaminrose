---
from_wordpress: true
comments_id: 105
title: Working on the 21 Days
date: 2008-07-29T22:02:17-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=105
permalink: /post/working-on-the-21-days/
categories:
  - writing and publishing
---
So I've been working on this 21 Days To A Novel exercise, and I wanted to let you know how it's going.

The past couple of days I worked on included days 13 and 14. These steps had me identifying the forces in the world that were there to either help or hinder the characters in various ways and then describing how the world (defined loosely depending on the scale of your story) changes as a result of what the characters do. I found these helpful because I was forced to think on a pretty large scale, which I think will help me flesh out my outline and my actual prose once I get to writing.

I recently skipped two of the 21 days. On day 12, you're supposed to root your characters in the world—find those aspects of your characters that are part and parcel of the world or create them if necessary, then revise their character descriptions to reflect these world tie-ins. I felt that I had already achieved all this naturally as I created the characters. Setting and world-building come pretty naturally to me, so many of my characters' characteristics were based in the world from the very beginning, with little or no thought on my part.

The other day I skipped was day 15, in which you're supposed to choose one setting and write a scene there from each character's point of view. The character should be alone, so the scene gets described solely from his or her viewpoint. You end up with three unique descriptions of the same location. I think it's supposed to help you establish how each character is going to perceive his or her surroundings and so ever more firmly root the character's voice in your head. Maybe I'm wrong, but I didn't feel I need this. It would probably be good practice for me if nothing else, but I opted not to do it.

So today's words were day 16, writing the back cover blurb. I don't feel I have a strong enough sense of what my novel is going to end up like to write a true, marketing-oriented blurb, so I just wrote a one-paragraph plot synopsis a la the Snowflake Method. As part of this, I had to decide how the novel was going to end, which had been a huge sticking point before and was the primary reason I started doing 21 Days To A Novel in the first place. I've been putting it off, trying not to think about it until I had my characters firmly in place, so that I could devise an ending that grew out of my characters. I came up with a good idea for an ending and just stuck it in. There are still a few ending details I'm not sure about, but they'll come to me as I work through my outline, I think.

That's the next day—define the various problems and their solutions, working backwards from the ending. I guess it's not really an outline in the sense of being a list of scenes, but it's a broad overview of the conflicts that are going on. Writing the detailed outline is a few days further into the exercise.

So far, this 21 Days To A Novel exercise has been very interesting and helpful. Obviously, I haven't followed all of the steps slavishly, which makes sense. No one solution should work for everybody. But many of the steps have solved some of the most serious problems I was facing as a beginning writer working on my first real story. If you're running into challenges with characterization or plotting, I highly recommend that you check out the 21 Days To A Novel podcasts and at least listen to them to see if you feel some of the steps would help you. I'll be happy to help you find the podcasts if you're interested.