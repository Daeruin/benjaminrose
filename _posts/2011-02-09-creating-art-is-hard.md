---
from_wordpress: true
comments_id: 343
title: Creating Art is Hard
date: 2011-02-09T12:40:48-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=343
permalink: /post/creating-art-is-hard/
categories:
  - art
  - writing and publishing
---
[Creating art is hard](http://ben.casnocha.com/2010/11/how-to-draw-an-owl.html). (Click on the link.)

Does this apply to you? What's your interpretation?