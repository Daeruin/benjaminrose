---
from_wordpress: true
comments_id: 21
title: Universalis
date: 2007-04-19T22:42:09-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=21
permalink: /post/universalis/
categories:
  - gaming
---
[Universalis](http://www.ramshead.indie-rpgs.com/) is a [role playing game](http://en.wikipedia.org/wiki/Role-playing_game) I recently ran across. Unlike most role playing games out there, Universalis is not based in a specific genre and uses no dice. You use your first game session to define the game world you'll be playing in. Each player has a pool of Coins (beads, chips, whatever) they use to add new facts and events to the game world. You might spend a Coin to declare that this game will be in the fantasy genre. Another Coin would allow you to declare that the game will be set in a world with no oceans. You could even spend Coins to forbid Monty Python jokes, disallow anything remotely Kender-like, or indicate that the tone of the story must be gritty or comical.

Likewise, you spend Coins to create items, specific locations, characters, character traits, and most especially events that happen. Players take turns narrating pretty much anything, as long as they have Coins to pay. There are rules that govern when your turn starts and ends and when you can interrupt another player, and you get a fresh supply of Coins each turn. If you don't like the turn things are taking, you can challenge other players by bidding coins on things that matter to you. Don't want to tell a gritty horror story today? Challenge and pay up. Don't want your honorable knight to die? Challenge and pay up.

Isn't that cool? You could tell any kind of story you want, in any amount of detail. You won't get bogged down by rules and endless dice rolls. It's all narration. The game creates suspense because you never know what your fellow players will cause to happen next. I find this game particularly intriguing because it seems like it could be a good way to help writer's block or even just get the creative juices flowing. Introduce your situation into the game world and see what everyone else comes up with.
