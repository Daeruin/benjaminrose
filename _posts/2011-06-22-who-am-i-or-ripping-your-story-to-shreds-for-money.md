---
from_wordpress: true
comments_id: 607
title: Who Am I? Or, Ripping Your Story to Shreds for Money
date: 2011-06-22T22:25:02-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=607
permalink: /post/who-am-i-or-ripping-your-story-to-shreds-for-money/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
If you've been following some of the comments around here closely, you may have glimpsed a few hints about something that I've been keeping pretty close to my chest. I've only told a couple of people about it—not because it's really that big of a secret, but because it's the kind of thing you don't enter into lightly.

I have been thinking about seriously marketing my editing talents on a freelance basis. Because when it comes right down to it, I'm much better at ripping other people's stories to shreds than creating my own.

Seriously, though. All though college I dabbled and dreamed about being a writer, and in the meantime what did I actually spend my time on? Volunteering for numerous undergraduate journals as an editor. I could have been writing stories or articles of my own, but instead I was hunkered down over other people's writing with a red pen or crouching over a keyboard and mouse with QuarkXPress or Photoshop glaring into my eyes. Heck, I even started my own journal from scratch. I applied for funding, drummed up papers to publish, and produced pretty much the entire thing all by myself. And it turned out quite nice, even if I do say so myself.

After college I spent the next seven years as a proofreader and technical editor of diverse projects in the corporate world, where I swiftly conquered all that was set before me. Everyone cowered in awe of my insanely eagle-like eyes. Catching me making a mistake became a source of major celebration amidst the downtrodden cubicle dwellers of my office. I ruled the written word with an iron fist. My superiors could not help but double my salary in the first five years I was there. What other choice had they in the face of my awesomeness?

And now I must consider offering that awesomeness to the rest of the world. For a meager fee, I will gleefully rip your story to shreds!

Okay, so editing isn't really like that. I got a little carried away there (it's easy to do when you're as awesome as I am). No, editing is a give-and-take process—a collaboration, where the editor and the author both serve the needs of the written word, to better glorify the story. It's all about the story, and making it as beautiful and seamless as possible.

Plus there's a large market emerging for this kind of service, what with the rise of e-books and the many authors who are jumping on the self-publishing bandwagon. A traditional publishing deal comes with editors built in, but many self-published authors suffer without that helping hand, that collaborator whose only goal is to make the end result as polished as possible and help sell the story.

Considering a move like this brings up a lot of questions. What's the going rate for freelance editing? What types of editing would I offer? Would I focus on certain genres? Could I offer additional services that I happen to be able to provide, such as e-book formatting or indexing for nonfiction works? How do I market myself and get more clients?

But the biggest question of all is this: Am I willing to give up my writing for this? Because I probably don't have time for both in my life right now. It's almost like it comes down to the classic question, "Who am I?" Am I the editor, or the writer?

I'm not sure I have the answers yet. But it's churning around in my mind. Maybe the old writing dream was just a daydream—a fleeting and insubstantial diversion from my true calling. I suppose only time will tell.
