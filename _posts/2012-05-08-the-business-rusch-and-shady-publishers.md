---
from_wordpress: true
comments_id: 880
title: The Business Rusch and Shady Publishers
date: 2012-05-08T09:50:07-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=880
permalink: /post/the-business-rusch-and-shady-publishers/
categories:
  - writing and publishing
---
Although I follow a number of authors and writing advice blogs online, I don't really post much about it. Partly because I feel like I don't have much to contribute beyond what others are already saying. There's a lot of great material out there, stuff that new writers wouldn't have had any access to just a few years ago.

But today I read something that really torched me, and I feel the need to share it.

Writing is a lonely business, and too much about the actual business of it is hidden and never talked about. When you become a writer, you blaze your own trail.  That's one reason why all of this material online is so valuable. If you're a writer and aren't already reading Kristine Kathryn Rusch's blog, you should be, particularly her weekly series called The Business Rusch.

<http://kriswrites.livejournal.com>

Rusch cuts deeply into topics of the business of publishing and writing, which is both rare and valuable. In particular, you should check out this piece she wrote on the state of royalty accounting in the publishing world. After posting it, her blog got hacked and so she distributed it to various other blogs to post instead. Here's one of them:

<http://www.thepassivevoice.com/05/2012/kriss-post-spread-the-word/>

When I attended one of David Farland's workshops in Salt Lake City last year, he had mentioned that there was some shady business going on and that many authors were suing publishing companies, particularly over some problems with ebook royalties. At the time, he said that he was not advising new authors to sign traditional publishing deals. I didn't know exactly what the trouble was back then, and I wasn't even close to worrying about actually getting published so I didn't pursue it at all. But Rusch's post illuminates what has been happening. Apparently, many publishers don't track actual sales of ebooks and instead use some generic formula to arrive at a number that's supposed to represent ebook sales. Wha . . . ? Combine that with other shady stuff, and you have a very disturbed publishing industry.

Tread carefully, and educate yourself.
