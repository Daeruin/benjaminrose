---
from_wordpress: true
comments_id: 223
title: The Wheel of Time E-Book Covers (image-heavy post)
date: 2010-08-15T12:15:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=223
permalink: /post/the-wheel-of-time-e-book-covers-image-heavy-post/
categories:
  - art
  - fiction
---
A few days ago I posted a copy of [Michael Komarck's e-book cover for Robert Jordan's _Knife of Dreams_](/post/knife-of-dreams-e-book-cover/). The other covers are worth taking a look at, too. Even the worst ones are better than the [covers done for the print books](/post/wheel-of-time-cover-art-for-the-us-hardback-editions/ "Wheel of Time Cover Art for the US Hardback Editions"), in my opinion. Which one is your favorite?

**Edit**: Since my original post, I've added the e-book covers for _The Gathering Storm_, _The Towers of Midnight_, and _New Spring_ down at the bottom. Thanks to commenter Calamari and Reddit reader Morth for helping with the descriptions the covers.

**SPOILER ALERT: If you haven't read the books yet, be aware that the descriptions of each picture contain some pretty big spoilers.**

Please comment if you have more details or corrections. I will gladly fix them.

&nbsp;

**_The Eye of the World_ e-book cover**

by [David Grove](http://www.theispot.com/grove "David Grove")

![](/assets/images/01-The-Eye-of-the-World-eBook-David-Grove.jpg){: width="429px" height="406px"}

Rand sailing on the river. The tower in the background is probably the Tower of Ghenjei.

&nbsp;

**_The Great Hunt_ e-book cover**

by [Kekai Kotaki](http://www.kekaiart.com/ "Kekai Kotaki")

![](/assets/images/02-The-Great-Hunt-eBook-Kekai-Kotaki.png){: width="429px" height="404px"}

Rand with the Horn of Valere. Selene/Lanfear is on left, and Loial is on the right. In the foreground are trollocs. This scene takes place after they came out of the portals, backtracking to catch the trollocs and steal the horn. However, it's inaccurate because they didn't open the case containing the horn until safe away from the trollocs. It's the same scene as the original printed cover by Darrel K. Sweet, and it still doesn't make much sense.

&nbsp;

**_The Dragon Reborn_ e-book cover**

by [Donato Giancola](http://www.donatoart.com/ "Donato Giancola")

![](/assets/images/03-The-Dragon-Reborn-eBook-Donato-Giancola.jpg){: width="429px" height="415px"}

Rand sitting on the Dragon banner, trying to decide between his humanity (the flute) and the way he thinks he must be (the sword). The artist intentionally chose a scene not explicitly in the book.

&nbsp;

**_The Shadow Rising_ e-book cover**

by [Sam Weber](http://sampaints.com/ "Sam Weber")

![](/assets/images/04-The-Shadow-Rising-eBook-Sam-Weber.png){: width="429px" height="415px"}

Mat coming out of Rhuidean. This is an abstract scene that's not explicitly in the book. Mat is carrying the ashanderei given to him by the Aelfinn and Eelfinn. The two ravens are probably representative of the ravens engraved on the ashanderei. In the background is the Avendesora from which Mat hung.

&nbsp;

**_The Fires of Heaven_ e-book cover**

by [Dan Dos Santos](http://www.dandossantos.com/gallery.htm "Dan Dos Santos")

![The Fires of Heaven e-book cover](/assets/images/05-The-Fires-of-Heaven-eBook-Dan-Dos-Santos.png){: width="429px" height="416px"}

Moiraine, based on the color of the cloak, the bracelet, and the diadem. The papers are flying around, burning, because Lanfear blew up a wagon holding papers in a fight scene near the end of the book. The bracelet is an angreal, almost strong enough to be sa'angreal. However, Moiraine never used it in this scene—Lanfear did.

&nbsp;

**_Lord of Chaos_ e-book cover**

by [Greg Manchess](http://www.manchess.com/ "Greg Manchess")

![Lord of Chaos e-book cover](/assets/images/06-Lord-of-Chaos-eBook-Greg-Manchess.jpg){: width="429px" height="419px"}

![Lord of Chaos e-book cover](/assets/images/06-Lord-of-Chaos-eBook-Greg-Manchess-banner.jpg){: width="429px" height="148px"}

The battle at Dumai's wells, with the Asha'man (right) destroying Shaido with the One Power. If you look closely at the left-hand side of the wraparound image, you can see Loial (green coat) and Perrin (brown shirt and axe) hacking away. It's more visible on [the larger version posted on Tor.com](http://www.tor.com/blogs/2010/03/lord-of-chaos-ebook-cover-by-greg-manchess).

&nbsp;

**_A Crown of Swords_ e-book cover**

by [Mélanie Delon](http://www.melaniedelon.com/ "Mélanie Delon")

![A Crown of Swords e-book cover](/assets/images/07-A-Crown-of-Swords-eBook-Melaine-Delon.png){: width="429px" height="462px"}

Lan rescuing Nynaeve from the water. This is where Nynaeve finally overcomes her block to the One Power.

&nbsp;

**_Path of Daggers_ e-book cover**

by [Julie Bell](http://www.imaginistix.com/ "Julie Bell")

![Path of Daggers e-book cover](/assets/images/08-Path-of-Daggers-eBook-Julie-Bell.jpg){: width="429px" height="437px"}

Aviendha, Elayne, and Nynaeve using the Bowl of Winds to fix the weather. There has been some discussion of who is who in the cover, due to some inaccuracies in the painting. Red-haired Aviendha is on the left but should have shorter hair, and some commenters feel she shouldn't be wearing such a fancy dress. Golden-haired Elayne is in the center, but her hair ought to be more curly and perhaps with a slightly more reddish tinge. She obtained a ter’angreal dagger in this book, shown hanging at her belt. Her dress looks a bit plain for Elayne. On the right is Nynaeve with the long, dark braid.

&nbsp;

** _Winter's Heart_ e-book cover**

by [Scott M. Fischer](http://www.fischart.com/ "Scott M. Fischer")

![Winter's Heart e-book cover](/assets/images/09-Winters-Heart-eBook-Scott-Fischer.jpg){: width="429px" height="408px"}

Rand using the male access key (sa’angreal or ter'angreal?) to cleanse the male half of the True Source.

&nbsp;

**_Crossroads of Twilight_ e-book cover**

by [Greg Ruth](http://www.gregthings.com/ "Greg Ruth")

![Crossroads of Twilight e-book cover](/assets/images/10-Crossroads-of-Twilight-eBook-Greg-Ruth.jpg){: width="429px" height="418px"}

Perrin leaving behind the axe before the battle at Malen. Not sure it was actually snowing in the scene.

&nbsp;

**_Knife of Dreams_ e-book cover**

by [Michael Komarck](http://www.komarckart.com/ "Michael Komarck")

![Knife of Dreams e-book cover](/assets/images/11-Knife-of-Dreams-eBook-Michael-Komarck.jpg){: width="429px" height="429px"}

Rand (holding the Dragon Scepter), protecting Min behind him. His hand is being destroyed by fire from Semirhage. (This is probably my favorite. If you like it, [check out my post on Michael Komarck's art](/post/michael-komarck-my-favorite-fantasy-artist/ "Michael Komarck: My Favorite Fantasy Artist").)

&nbsp;

**_The Gathering Storm_ e-book cover**

by [Todd Lockwood](http://www.toddlockwood.com/ "Todd Lockwood")

[![The Gathering Storm E-book Cover by Todd Lockwood](/assets/images/12-The-Gathering-Storm-eBook-Todd-Lockwood.jpg){: width="429px" height="408px"}](/assets/images/12-The-Gathering-Storm-eBook-Todd-Lockwood.jpg)

Egwene standing in a broken gap high in the White Tower, defending against the attacking Seanchan. Egwene's wand is a sa'angreal she fetched from the storerooms (the same wand used to heal Mat from the dagger). On the right is a Seanchan warrior riding a to'raken.

&nbsp;

**_The Towers of Midnight_ e-book cover**

by [Raymond Swanland](http://www.raymondswanland.com/ "Raymond Swanland")

[![Towers of Midnight E-book Cover by Raymond Swanland](/assets/images/13-The-Towers-of-Midnight-eBook-Raymond-Swanland.jpg){: width="429px" height="429px"}](/assets/images/13-The-Towers-of-Midnight-eBook-Raymond-Swanland.jpg)

Perrin forging his new hammer, with Neald channeling the One Power to make it a power-forged weapon. I think the wolf in the top right corner is supposed to represent either Perrin becoming one with his wolf-self (as this scene is a big turning point in his understanding of himself and what he wants), or an image of Hopper that Perrin attaches to the hammer. In the book it's described as simply ornamentation.

&nbsp;

**_New Spring_ e-book cover**

by [Jason Chan](http://www.jasonchanart.com/ "Jason Chan")

![New Spring E-book Cover by Jason Chan](/assets/images/00-New-Spring-eBook-Jason-Chan.jpg){: width="430px" height="370px"}

Lan and Moiraine.

&nbsp;

My favorites are _Knife of Dreams_, _The Shadow Rising_, and _The Fires of Heaven_—in that order. Path of Daggers, Winter's Heart, and Crossroads of Twilight are all a bit too cartoony or comic-booky for my taste. What do you think? Are they better than the [original covers](/post/wheel-of-time-cover-art-for-the-us-hardback-editions/ "Wheel of Time Cover Art for the US Hardback Editions")?

**Note 1**: If you liked this post, you might like some of my other posts, like [this one about people copying Jordan](/post/example-of-wots-iconicity/) or [these](/post/mistborn/) [three](/post/the-well-of-ascension/) [posts](/post/mistborn-3-the-hero-of-ages/) reviewing Brandon Sanderson's Mistborn series.

**Note 2**: You might want to check out [this post on Tor.com discussing Darrel K. Sweet's death and showing all of his original Wheel of Time covers for the printed books](http://www.tor.com/blogs/2012/04/a-darrell-k-sweet-wheel-of-time-tribute-including-a-memory-of-light-sketch).

Please take a moment, look around, and leave a comment!
