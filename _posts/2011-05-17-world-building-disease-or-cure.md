---
from_wordpress: true
comments_id: 559
title: "World Building: Disease or Cure?"
date: 2011-05-17T21:30:26-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=559
permalink: /post/world-building-disease-or-cure/
categories:
  - writing and publishing
---
Right now I'm in the planning stages for my next book. You may or may not recall [my original post on this subject back on the first of February](/post/new-story-idea/ "New Story Idea"). Yes, it's been two-and-a-half months, and I'm still planning. Does that sound like a long time to you?

It sounds like a long time to me. I have a specific fear of taking too long at this stage. Among fantasy writers, we have a special term for people who take too long creating their fantasy worlds before they start writing—it's called **world building disease**. Poor souls who contract this disease find themselves obsessed with their secondary world. They write many pages on every culture in their world, regardless of their relevancy to the story. They create maps for every major city. They write detailed histories starting with "In the beginning. . . . " They create a fully developed [conlang](http://conlang.org/) with thousands of vocabulary words. Then they do it again for each society on their world, even if the only word of that language that will appear in the story is "hello." The whole process feeds on itself, and the writer becomes more and more obsessed with every detail of their world, until one day they wake up with a long white beard and wondering how the world passed them by. They never got around to actually writing anything.

#### I Love World Building

I built my first fantasy world when I was in high school. I have a huge poster-sized map (hand drawn in full color) and a 20-page text document to prove it. I even ran a roleplaying campaign in that world. Back then, I used to spend almost as much time making characters for the game as I did playing the game itself. I had an idea for a novel based in that world, too, but it never got written. I think I started one time, but didn't get more than a few pages into it. I did write a couple of very short stories set in that world. I think my English teacher gave me an A on them.

So here I am building another world that I plan to write a story in, and I have a healthy fear of world building disease. I have to keep a close watch on myself to keep from getting distracted by interesting tangents. For example, last night I spent several hours researching. I had this idea that would add some really cool flavor to my world, but I wanted to double check a few facts before committing to the idea. I like to try justifying things using plausible science whenever I can.

So last night I was looking up stuff on asteroid belts. (Why does a fantasy writer need to know about this? Maybe I'll explain at some point. Or you could read the book when it's published. Haha.) That led to Saturn's rings and nebulae. I read about the axial tilt of planets, the phases of the planets, orbital inclination, and the creation of moons. This is all good stuff. Maybe more along the lines of what a science fiction writer might want to know, but I think it's useful for fantasy writers as well.

#### The Distraction

{%
  include figure.html
  image-url="https://demonstrations.wolfram.com/Seasons/HTMLImages/index.en/popup_1.jpg"
  width="220"
  height="219"
  wrap="left"
  alt="Axial Tilt and the Seasons"
  caption="Axial Tilt and the Seasons"
%}

Then I found the [Wolfram Demonstrations Project](http://demonstrations.wolfram.com/index.html "Wolfram Demonstrations Project"). It's an open-source software project that allows people to create dynamic, 3D demonstrations of things, particularly scientific stuff, but also a huge range of other topics—even literature. I found this really cool demonstration that lets you build your own solar system. I found demonstrations of interesting astronomical concepts, like [how axial tilt affects the seasons](http://demonstrations.wolfram.com/Seasons/) and [how the orbital paths of binary stars work](http://demonstrations.wolfram.com/TheCelestialTwoBodyProblem/). I also found some cool stuff to help teach my kids about science. Oh, and an [automatic maze generator](http://demonstrations.wolfram.com/InteractiveMaze/). That was awesome.

Unfortunately, I ended up exploring this vast resource for several hours when I should have been focused on information that was relevant to my novel-in-the-making. When I finally came to the surface and realized what I had done, it was close to midnight and I still hadn't made any decisions regarding the original idea I needed to research. It took me a little over an hour, but I did finally make my decisions and get them written down. So now I know a few things about my world's solar system, which provides, at the very least, some interesting flavor for the setting, but may also influence my character's backgrounds and potentially serve as a plot device as well. The night could have been more efficient, but was not completely wasted.

I'm treading a fine line here. I know I have a tendency to get distracted and go perhaps a bit overboard with my world building. But at the same time, I believe this is a necessary and critical step in my novel writing process. In fact, I believe world building can actually be a **cure** for certain writerly ailments. I'll explain more in my next post, [World Building Part II: The Cure](/post/worldbuilding-part-ii-the-cure/ "Worldbuilding, Part II: The Cure").

#### Questions

  * How do you keep yourself on task when researching?
  * How long do you spend working on the background for your novel?
  * Have you ever created a secondary world, for gaming or for writing?
