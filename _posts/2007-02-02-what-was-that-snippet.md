---
from_wordpress: true
comments_id: 8
title: What was that snippet?
date: 2007-02-02T23:18:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=8
permalink: /post/what-was-that-snippet/
categories:
  - metablogging
---
In reading past comments, I realized I left someone's question unanswered. I'm going to answer it now. The snippet of prose in my last post isn't tied to any particular goal just yet. It's not part of a novel (yet) or a short story (yet). It's just a scene I've had in my head for a while. It is, however, tied to a setting in which I have thought of basing a novel, the same setting as my little vignette called Dragon Luck, for those of you who have read that.

I intend for most of my posts here to be mere snippets of prose, since that seems to be all I have time for. The blog format matches my schedule nicely. The snippets may not even be related to each other directly. But I hope they are interesting enough to keep you coming back. Let me know what works for you and what doesn't.