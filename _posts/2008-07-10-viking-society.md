---
from_wordpress: true
comments_id: 102
title: Viking Society
date: 2008-07-10T20:12:14-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=102
permalink: /post/viking-society/
categories:
  - uncategorized
---
The second in my series of reports on my research into the Vikings. Viking society was divided three basic class levels: thrall, bondi, and jarl. There are a couple of other intermediate levels, which I'll describe below. Also, keep in mind that these did vary somewhat by name and function throughout the 300 years or so of the main Viking era and also varied between groups and tribes (Danes vs. Swedes vs. Norse, etc.). Also, keep in mind that this comes from various internet sources and was filtered through me, so it may be more or less accurate.

  1. **Thrall.** A thrall is a slave and cannot own land. A thrall can own goods and money, however, and can craft and sell products at a market in his or her free time. A thrall can purchase his or her freedom to become a freed. A child born to a female thrall goes into the father's class of society.
  2. **Freedman.** A freedman is a thrall who has purchased his or her freedom and is considered a citizen who can own land and participate in the law. A freedman is still dependent on his former owner for many things (permission to undertake business, get married, or start a lawsuit; half of any money won in a lawsuit is owed to the former owner) and cannot institute legal proceedings against the former owner. His or her wergild is half that of a bondi (see below), and in the absence of children the former owner inherits all property. His or her children remain in the status of freedman up to three generations, as well. A freedman may purchase bondi status at a greater price.
  3. **Bondi.** A bondi is a man or woman born free to own land and property. In principle, all bondi own land, but this is not true in reality. A bondi must purchase land or be granted land by a parent or spouse. Failing that, a bondi must work for someone else. Bondi may participate fully in the law, including voting at the althing.
  4. **Jarl.** A jarl is a bondi warrior appointed by the king to lead at least 40 men. Traditionally, all bondi owned and commanded a ship, but some tribes are grant this title to men without a ship.
  5. **King.** A king is elected by the althing to lead the tribe’s warriors into battle and perform all the other duties of the king.

Other groups and positions:

* **Althing.** The althing is the governing assembly of each group or tribe. It is made up collectively of all members of the tribe excepting thralls. The althing meets at appointed times to discuss tribal matters, pass laws, and arbitrate disagreements.
* **Lawspeaker.** The lawspeaker is an elected position whose function is to oversee the meetings of the althing and to recite the law to those assembled (the Vikings were not a literate society). In some tribes the lawspeaker is a permanent position; in others it is limited to a certain number of years.
