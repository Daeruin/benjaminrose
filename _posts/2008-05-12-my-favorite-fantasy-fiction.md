---
from_wordpress: true
comments_id: 77
title: My Favorite Fantasy Fiction
date: 2008-05-12T23:51:12-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=77
permalink: /post/my-favorite-fantasy-fiction/
categories:
  - fiction
---
I was asked recently what my favorite fantasy fiction is. I hedged a little bit. I'm always a bit hesitant to commit to favorites. Things aren't always that clear cut. I like some books for certain elements and not for others.

Still, the book that has to come at the top of my list has to be _The Lord of the Rings_. I recognize that Tolkien has some problems that make it hard for some people to like his work, and there are things about his writing that I'm not especially fond of myself. He was still a genius. And no matter what else I could say about Tolkien and his work, he will always remain my favorite author for one simple reason: he made me love fantasy. Nothing can ever compare to the childhood wonder I felt when I read his books. I've gained a much more mature appreciation for Tolkien now, and that's what keeps him at the top of the list.

So Tolkien is just a given. That aside, I do have other books at the top of my list.

I'm a compulsive organizer in many aspects of my life. But I do not organize my books alphabetically. I organize them by how much I like them. This isn't a hard and fast ranking. Like I said, it isn't clear cut. My books change positions on my bookshelf over time. Something I loved about a book several years ago will lose its place, and the book will slowly make its way closer to the bottom. Or I may reread a book and remember how awesome it was, and it gets placed back near the top again.

So here's a brief overview of some of my favorites, starting at the top shelf.

SHELF 1 is filled with Tolkien. I have _The Hobbit_, _The Silmarillion_, three different editions of The Lord of the Rings, some other stories by Tolkien, and a host of other books about Tolkien and his writing. Also on this shelf are a few other gems that don't fit on the second shelf: the Gormenghast trilogy by Mervyn Peake, _Fahrenheit 451_ by Ray Bradbury, _Jonathan Strange and Mr. Norrell_ by Susanna Clarke, and _Latro in the Mist_ by Gene Wolfe—all works of genius in their own way.

SHELF 2 contains only three authors: George R. R. Martin, Robin Hobb, and Tad Williams. After Tolkien, I believe these three authors are the very best writers of epic fantasy out there. 'Nuff said.

SHELF 3 only has two authors that I'll go into detail about: Phillip Pullman and Parke Godwin. Pullman speaks for himself. If you haven't heard of _The Golden Compass_, you must have been living under a rock the past several years. Godwin is not nearly so well known, but he's worth looking for. If you've never read his stuff, you have to give it a try. I've never been much interested in reading the endless retellings of King Arthur or Robin Hood, but Godwin does wonders with those tales. He roots them firmly in history, but he also works magic into the tales, and he makes the characters feel totally real. Shelf 4 also houses Asimov and Cherryh, among others.

What, no mention of Robert Jordan? He lives on shelf 4. He lived on shelf 2 several years ago. I never even read the last book he put out.

SHELF 5 holds the books that I loved in high school but that haven't survived the ravages of my changing taste as an adult. Among other authors, this includes Terry Brooks, Anne McCaffrey, and all those Dragonlance books. I'm not saying these aren't enjoyable books. I just don't like them as much as those authors who now take up the top shelves these days.

Who is on your top shelf?