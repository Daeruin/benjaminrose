---
from_wordpress: true
comments_id: 387
title: "What You Can Learn From Your Site's Statistics"
date: 2011-03-26T23:01:10-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=387
permalink: /post/what-you-can-learn-from-your-sites-statistics/
categories:
  - metablogging
  - software
---
In keeping with my last post on [how to grow your author blog](/post/how-to-grow-your-author-blog/), I thought I'd share a few things about my site's statistics and how I've been trying to use them to grow my own blog.

![statistics](/assets/images/blogStatistics2011.jpg)

Not too long ago, I installed a plugin on my blog to keep track of statistics. I wanted to try figuring out what kind of content was working the best and what kind of traffic was coming to my blog. It's been very interesting, and I highly recommend that you do it as well.

Here are some interesting tidbits that I learned.

  1. I can see a **clear correlation between number of visits and frequency of new content**. I started tracking stats in early January. Previous to that, I had been posting an average of once or twice a month for several months in a row. My numbers were at an all-time low at the beginning of January. At the end of January, I posted three times. In February, I posed five times. At the end of February, my site reached its peak in visitor numbers. Then I didn't post for nearly a month. My stats have been slowly dropping since my last post in February. They peaked slightly with each post I've done in March.
  2. **My most popular post** was [this one on the Wheel of Time e-book cover art](/post/the-wheel-of-time-e-book-covers-image-heavy-post/). This post has been so popular it blows everything else on my blog completely out of the water. As of this moment, it's had 1,386 views in total. The next most popular page on my site is my welcome screen at 137 views in total. So this one post has ten times as many views as the next highest, and more views than every other post on my blog _combined_. And it has continued to be my most visited page every single day since then. Holy crap. I had no idea I was stumbling onto a traffic gold mine with that post. And check this out. Type "wheel of time ebook covers" into Google. What's the number one result? Voila.
  3. Now type "the first law trilogy" into Google. You get the typical heavy hitters up at the top—Amazon and two from Wikipedia. Joe Abercrombie's own website comes in at number 5. [My post](/post/the-first-law-trilogy/) comes in at number 8—**right above freaking [Felicia Day's website](http://feliciaday.com/blog/joe-abercrombies-the-first-law-trilogy)**. And I say "freaking" in a very admiring way. If you don't know who Felicia Day is, you have some [learning to do](http://feliciaday.com/about). Her online series [The Guild](http://www.watchtheguild.com/) has gotten over 80 million views.

So, what's the moral of the story? I think there are a few things we can get out of this. One way to come at it is to say that you never know what type of content will generate traffic. I had no intent to generate traffic with my Wheel of Time e-book post. I was just sharing some cool art for a series of books that I like. Since I discovered the popularity of this post, I have edited it slightly to include links to other posts on my blog that I think will be interesting to people who like that post.

Despite the seemingly random nature of which posts become popular, it seems pretty clear that many of my popular posts were about popular and timely topics—go figure. I'm talking about books that are selling well, interesting or controversial facts about musicians or writers, and so forth. So tapping into the popular consciousness is definitely a way to get noticed. Perhaps I could do with less droning on about myself. Who the heck would be searching for little old me, anyhow?

Finally, these numbers are definitely telling me something about the frequency of my posts. **Namely, the more I write, the more visitors I get. 'Nuff said.**

  * **Have you noticed anything interesting about the flow of traffic on your own website?**

  * **What have you done to harness the power of the web (other than having a blog in the first place)?**
