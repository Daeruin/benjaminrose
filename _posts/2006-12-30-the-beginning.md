---
from_wordpress: true
comments_id: 6
title: The beginning
date: 2006-12-30T17:48:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=6
permalink: /post/the-beginning/
categories:
  - metablogging
---
I guess I've created a blog.

I suppose I'll start with a brief explanation of what the name of my blog means. Noesis means intuition, thinking, or cognition. I've found other definitions, but I like those synonyms rather well. You might imagine that when I chose the name "Noesis," I was thinking about writing a blog about thinking, or at least about intellectual topics, but you'd be wrong. I'm not sure what I was thinking. I hope this blog will help me figure it out.
