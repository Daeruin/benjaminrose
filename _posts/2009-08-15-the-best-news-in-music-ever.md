---
from_wordpress: true
comments_id: 126
title: The Best News In Music . . . Ever
date: 2009-08-15T20:20:11-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=126
permalink: /post/the-best-news-in-music-ever/
categories:
  - music
---
Nelson is back! Those uncanny twin offspring of Ricky Nelson are getting back together and recording a new album. My favorite cheesy band of the nineties, with their long, flowing golden locks, and going back on tour. True confession: for years I wanted—nay, _ached_ for long blond hair like Matthew and Gunnar. My ill-fated copycat attempt led to something more like a dishwater-colored afro. But enough about me. Let's take a little trip down memory lane:

![Matthew and Gunnar Nelson](/assets/images/nelsonAfterTheRainCover.jpg)

Unfortunately, I couldn't find an online copy of the Nelson poster Ing used to have in his room. Think sky blue pleather and neon guitars, and you'll about have it.

Edit: Found it!

![Matthew and Gunnar Nelson](/assets/images/nelsonPoster.jpg)

If you're genuinely interested in their triumphant modern reincarnation, check out [their website](http://www.matthewandgunnarnelson.com/ "The Nelson Brothers").
