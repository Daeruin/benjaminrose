---
from_wordpress: true
comments_id: 127
title: A Game of Thrones Fever
date: 2009-09-08T20:45:56-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=127
permalink: /post/a-game-of-thrones-fever/
categories:
  - my fiction
---
Some fans of George RR Martin's _A Song of Ice and Fire_ series have started creating [their own promotional posters](http://winter-is-coming.blogspot.com/2009/09/awesome-fan-made-promotional-posters.html) for the _Game of Thrones_ show currently in production. [These posters in particular](http://7kingdoms.ru/2009/postery-fejki-igry-prestolov/comment-page-1/) are awesome, and I thought my fellow ASoIaF fans would enjoy them.

Against my better judgment, I continue to get more excited about the possibility of _A Game of Thrones_ being picked up by HBO. Most of the [roles for the pilot have been cast](http://winter-is-coming.blogspot.com/2001/01/cast-so-far.html), and filming is set to begin soon. HBO executives will use the pilot to decide whether or not to order the show for a whole season. The grassroots, fan-generated buzz about the show is already feverishly high, and I'm becoming infected.

My better judgment tells me to calm down. First and foremost, there is no guarantee the show will even get picked up (although there are lots of indications that it has a good chance). The pilot hasn't even been shot yet, and it's just too premature to have any real assurance of what will happen. Second, I don't even have HBO, nor do I plan to subscribe just because of this show. I don't watch TV much at all, and as much as I love Martin's books, the show alone wouldn't be worth the monthly fee for HBO.

Finally, I have some admittedly hypocritical moral objections to some of the content I know will be in the show—hypocritical because, after all, I was willing to read the material; is it really that different to watch it? Well, to me it does feel different. It's not the violence—I'm perfectly desensitized to that—but the graphical sexual content will make me uncomfortable. So, if the show ever does make it on the air, I'll either be renting the episodes from iTunes or waiting until the DVDs come out, so I can easily skip any scenes that take me out of my little self-delusional bubble of comfort.

So, it'll likely be at least a year, possibly two, before I ever see any actual footage of the show. Way too early to get excited about a show that may not even get greenlighted. I just keep telling myself that.

Way too early.

Way too early.

Way too early . . .

But then there's a little whisper in the back of my mind.

_Winter is coming._