---
from_wordpress: true
comments_id: 640
title: Cool Geek Stuff that Makes Me Want to Stab My Eyes Out
date: 2011-09-01T19:22:57-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=640
permalink: /post/cool-geek-stuff-that-makes-me-want-to-stab-my-eyes-out/
categories:
  - art
  - fiction
  - gaming
  - 'movies &amp; TV'
  - software
---
Here's a short list of geek stuff that's admired and loved by loads of geeks worldwide—but I'd rather spend eternity in the belly of the [Sarlacc](http://www.starwars.com/databank/creature/sarlacc/ "Sarlacc") than ever have to deal with in any form.

  * **Dr. Who**—It looks like it was filmed in the 80s. See also Star Trek below.
  * **The Chronicles of Narnia**—The movies are mediocre, and the books are cloyingly didactic.
  * **Star Trek** (the entire franchise, with the exception of the latest movie)—A few interesting conflicts and logical puzzles, but overall boring and full of the cheesiest tripe ever.
  * **Dungeons and Dragons**, and the entire d20 system—Don't even get me started. I could complain about the idiocy of this game all day.
  * **Magic: The Gathering**—So many cards, it's easy to break, impossible to strategize effectively, and pure luck if you ever win.
  * **World of Warcraft**, and every other MMORPG—Do the same thing over, and over, and over again ad nauseam, and keep paying every month for the privilege.
  * **Zombies, vampires, and werewolves**, in any form. Simply overdone and predictable.
  * **Anime, manga**, and all of the other related art forms that fans insist are different, but are really just the same thing with little variations. Despite the occasional diamond, most of it is pure garbage.

Which one of my hates did you find yourself feeling defensive about? What are **your** least favorite popular geek properties? One of the great things about geekdom is that there is room for us all, despite our differences.