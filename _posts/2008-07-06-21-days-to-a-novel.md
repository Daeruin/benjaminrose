---
from_wordpress: true
comments_id: 101
title: 21 Days To A Novel
date: 2008-07-06T14:04:35-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=101
permalink: /post/21-days-to-a-novel/
categories:
  - writing and publishing
---
If you've been reading my Word Count Journal lately, you'll know that I've been trying this thing called 21 Days To A Novel. Ing asked about it, so I'll expound a little.

21 Days To A Novel is a writing exercise by [Michael A. Stackpole](http://www.michaelastackpole.com/). He initially developed it in his podcast series, The Secrets. It started off in a kind of spontaneous way as he was answering the question of how to come up with ideas. Originally it was four or five days, then as he started getting into it, the exercise turned into 21 days. He's been doing writing seminars on this in various conventions around the country for several years now. I'll go over how the exercise progresses before offering my opinion of it.

On the first five days, you develop your three main characters and establish the initial conflicts you're thinking about. It doesn't have to be three, but that's what he recommends and builds the exercise upon. On day one, you write five statements about your main character. He gives five topics for you to address, but I found they didn't work very well for my Dark Age-like setting. I modified them a little so the topics were more applicable to my setting. On day two, you expand each statement into a short paragraph. On day three, you write one sentence saying what the character is going to do with regard to each paragraph. On day four, you write a sentence starting "But that won't happen because . . . " for each paragraph. The point is to develop five conflicts of different types and scopes for each character. I've had a tough time coming up with five for one of my characters. It feels like almost too many. But it does help you make your characters more three-dimensional, because you have to come up with conflicts for different parts of their lives. On day five, you repeat days one through four for a new character.

I haven't gotten past day five yet, so I can't go into much detail beyond that, but I do remember a few things from listening to the podcasts the first time through. There's one day where you go through all your character sketches and identify and/or modify the conflicts they have in common so you can mesh them together. There's a day where you identify and/or develop ways in which your characters will impact the world they live in, and vice versa. There's a day where you work on establishing your characters' voices. There's a day near the end where you actually write a detailed outline of the book. And so forth.

The thing I like about this method is that the conflicts are developed organically from the characters. It's a very natural process, and you don't need to have a plot already in mind before you begin. On the other hand, I discovered that if you do already have a plot in mind, the process can be a little more challenging. Having to develop five conflicts for each character forced me to modify some plot and character points that I had already established. However, this turned out to be a good thing, because it has made my characters more realistic. It has also begun to give me some ideas of how to flesh out some sections of my plot that were a little vague.

This is really different from the Snowflake method. The conflicts come out of your characters, so you don't have to work to fit your characters into a pre-generated plot. 21 Days To A Novel is also geared more towards people who don't know what to write about, while the Snowflake Method assumes you already have a story in mind. 21 Days To A Novel is good for me, because I have always had an easier time thinking of settings and characters, but a hard time thinking of stuff to happen.

There are a few things I don't love about 21 Days To A Novel. As I mentioned, he does gear it more towards modern or futuristic settings, and it can be challenging if you already have a story in mind. The latter problem isn't so bad, though, as long as you're willing to change your ideas. (You should always be willing to do that if it makes your novel better.) Also, the workload is not very well spread out between days. Day one only requires five sentences. Day five requires all the workload of the previous four days, all in one day.

Of course, the only way to know for sure if it will help _you_ is to try it.

The only way I know of to get the 21 Days To A Novel exercise is by downloading The Secrets podcast. The easiest way to do this is through iTunes. Just search for The Secrets and/or Michael A. Stackpole. [This post](http://www.michaelastackpole.com/?p=48) on Stackpole's Web site also has a good summary of how to get a podcast (specifically his other podcast, Fortress Draconis, but the same principle applies to any podcast).
