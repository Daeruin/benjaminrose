---
from_wordpress: true
comments_id: 32
title: Edenbridge
date: 2007-08-25T11:00:34-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=32
permalink: /post/edenbridge/
categories:
  - music
---
Here's an update on my search for new music. The band under discussion is Edenbridge, another female-fronted metal band. I've had a couple of their songs for some time now, but I had forgotten about them when I wrote my Evanescence-esque post. That probably goes to show how much I like them. It's not that they're bad. They're a fine band. Just not amazing. It wasn't until one of their songs popped up on my iPod while playing in shuffle mode that I realized I'd left them out.

If I had to rank Edenbridge among the other bands I've mentioned, my list would go like this:

  1. Within Temptation
  2. Lacuna Coil
  3. Evanescence
  4. Edenbridge
  5. Nightwish
  6. Allyptic

Within Temptation has totally captured me with their dramatic, atmospheric metal. Sharon del Adel, although not perhaps as skilled as Amy Lee or Cristina Scabbia, has her own unique sound. My wife pointed out that she has the ability to make her voice sound very sweet. I thought that was a good description. My only problem with Within Temptation is that a few of their songs seem to lack the drama and emotion that, in my opinion, defines their sound. Those few songs fail to catch me up. Luckily, the rest of the album more than makes up for those minor deficiencies.

From what I've heard of Lacuna Coil, I'm certain I'd like their albums—I just haven't gotten to them yet. This is the only reason they're ranked #2. I just can't be sure until I've heard an entire album. And while I have loved Evanescence for some time now, the discovery of these other more or less similar band has broadened my horizons and refined my expectations, bumping them down to #3. Despite that, I'll repeat once again that I think Amy Lee's voice just can't be topped.

As for the remaining three bands on the list, I'm fairly confident that hearing more of their music won't shift their positions on the list. We'll see.

Back to Edenbridge. If you're curious about them, you can check out [their Web site](http://www.edenbridge.org "Edenbridge's Web site"). There you can discover, among other things, the all-important favorite color and animal of each band member. If you want to hear some of their music, visit the Laser's Edge Group's Web site. There's a link to it under the Stuff I Like section in the sidebar to the right.
