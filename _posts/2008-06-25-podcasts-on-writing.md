---
from_wordpress: true
comments_id: 94
title: Podcasts on Writing
date: 2008-06-25T22:24:27-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=94
permalink: /post/podcasts-on-writing/
categories:
  - writing and publishing
---
Recently I've been listening to a couple of podcasts about writing. It has been educational to hear about some of the basics of writing from experienced, published professionals. When I already know something they're talking about, it reassures me that I'm at least semi-intelligent and not missing something obvious. When I haven't heard something, it will hopefully make my writing that much better.

The first podcast I started listening to is [Writing Excuses](http://www.writingexcuses.com/) by Brandon Sanderson and two other guys named Dan and Howard. Brandon Sanderson is the author of Mistborn, Elantris, and a few other books that I've heard good things about. He's also finishing the final volume of the Wheel of Time series by Robert Jordan. Unfortunately for Dan and Howard, I can't remember their last names, but they are a horror writer and a comic strip writer, respectively or irrespectively (I'm not sure which). I started listening to Writing Excuses due to a recommendation from our friend [Aestril](http://aestril.blogspot.com/). It's quite enjoyable. Brandon, Dan, and Howard are lively and interesting people to listen to, and they cover a lot of topics from a lot of different angles. OK, I just found out Dan's and Howard's last names as I was looking up the URL for the podcast. Dan Wells and Howard Tayler.

The second podcast I've been listening to is [The Secrets](http://www.michaelastackpole.com/?p=44) by Michael A. Stackpole. As he is fond of mentioning in almost every single podcast, Michael A. Stackpole is a published author with 38 books to his name, including 8 that made it to the New York Times bestseller list (these were his 8 Star Wars novels). I can understand some self-promotion, and it is nice to know he's got that kind of experience under his belt. This is a guy who knows what he's doing. A lot of his advice on the podcast is extremely basic. I'll post some of my notes below in a minute. But like I said, it's nice to either be validated that you know the stuff, or be relieved that you now know it and are getting better. Some of the stuff is less basic and has been really educational for me as a beginning writer. Stackpole has a deep voice that's enjoyable to listen to.  Unfortunately, the podcasts feature some annoying intro recordings and a few of them even have ads (which are pretty incomprehensible, something about a zombie making moaning noises)—but only a few. At least they're no worse than listening to the radio during the commute to work, and in between you get interesting stuff about writing.

Each podcast of The Secrets is a recording of an essay he's written for the podcast or adapted from his writing newsletter, also called The Secrets. As he is also fond of mentioning, you can subscribe to The Secrets newsletter on his website at www.stormwolf.com. In reality, it's pretty tough to find the link that allows you to subscribe, so [here you go](http://www.michaelastackpole.com/store/index.php?main_page=product_info&cPath=2&products_id=2) in case you're interested. It's a dollar an episode. There are a couple of sample issues you're supposed to be able to download, but they're hard to find. I haven't checked them out yet, so I'm not sure what the newsletter is like. If it's like the podcast at all, it's probably worth a buck an issue.

Here are some of my notes from epidsode 9 of The Secrets podcast.

> **Define the Conflict**  
> From The Secrets Podcast #9 by Michael A. Stackpole
> 
> Conflict is the heart of the story. It’s what drives the story. Five basic conflicts:
> 
> 1. Man vs. man (doesn’t have to be a physical clash).  
> 2. Man vs. society.  
> 3. Man vs. nature.  
> 4. Man vs. self.  
> 5. Man vs. the supernatural (or technology).
> 
> Any of these can be the core of the whole story or of an individual character. Nest them for subplots or combine them for step-by-step problem solving.
> 
> Characters need to change the state they’re currently in. Figure out what steps they need to take to solve the conflict. As you figure out each character story arc, you see where they meet and conflict. If you don’t know what your characters need, desire, etc., then define the conflict and figure out how to get them from point A to point B.

It's really elementary stuff, but for a guy like me who's always had trouble plotting, reviewing the basics can sometimes give a solution that should have been obvious, or breathe life into something that was starting to feel stagnant.

I'll try to post more of my notes if I feel they might be useful. I recommend that you check out these two podcasts and see if you like them. By the way, you can also subscribe to them using iTunes if you have it; it's much easier that way, as each episode is downloaded automatically as it is released.