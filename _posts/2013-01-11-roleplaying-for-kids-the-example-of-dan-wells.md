---
from_wordpress: true
comments_id: 1059
title: "Roleplaying for Kids: The Example of Dan Wells"
date: 2013-01-11T15:52:25-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1059
permalink: /post/roleplaying-for-kids-the-example-of-dan-wells/
categories:
  - gaming
---
A while back I promised blog reader Riotimus that I would give some recommendations about roleplaying games for kids. This is a subject I've thought a fair bit about, as RPGs are one of my most favorite hobbies. I started on them when I was 10 years old (that totally sounds like I'm talking about drugs—I suppose I may as well be considering how addicting they are), and I am looking forward to introducing them to my kids.

![Marvel Heroic Roleplaying](/assets/images/marvelHeroicRoleplayingCover.jpg){: .image-wrap-right}

Unfortunately, this is not that post. But I did read a cool blog post from speculative fiction/horror author Dan Wells today that I thought might fill the gap until I get around to writing up a full blog post about it. Dan Wells, if you've never heard of him, wrote a book called I Am Not a Serial Killer, a young adult thriller with fantasy elements. It's a good read, and I recommend it. Anyway, Dan Wells is also an avid roleplayer, and his blog post talks about how he introduced his kids to roleplaying games.

[Go check it out](http://www.fearfulsymmetry.net/?p=1926), if you're interested.

He makes a good point that many kids aren't that interested in rules or limitations when they start out with RPGs. A more narrative system like Marvel Heroic Roleplaying can really fit the bill, especially since a lot of kids are really into superheroes. They are familiar enough with the genre that they can easily get into the whole superhero universe, and they know how to act.
