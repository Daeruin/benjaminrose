---
from_wordpress: true
comments_id: 1319
title: "What I'm Listening To: Nightwish (Again)"
date: 2017-08-29T21:03:03-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1319
permalink: /post/what-im-listening-to-nightwish-again/
categories:
  - music
---
Ah, Nightwish. Love 'em or hate 'em, they're still around, and I'm still listening to them. When I first heard the album Once, I was blown away. I fell deeply in love with their combination of soaring operatic vocals, pounding guitars, and symphonic elements, even while I recognized and occasionally cringed at some of their cheesy bombast.

Before I start reminiscing too much, let me just post this live concert that I was listening to last night, so you can start listening to it while you read. It's really amazing (for my two friends who like this style of music, anyway). They are a great live band, especially with the incredible Floor Jansen now fronting the band.

<iframe width="560" height="315" src="https://www.youtube.com/embed/JYjIlHWBAVo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Since the release of Once, the band has been on a roller-coaster ride with their vocalists. They fired longtime singer Tarja Turunen, whose voice I loved so much, and hired Annette Olzon. I was less excited about Annette's more straightforward rock style, but still enjoyed her signing enough to buy and listen to the next album, Dark Passion Play. I was super excited for Imaginaerum to come out but ended up feeling a little disappointed. Then Annette was fired in the middle of a tour, and Floor Jansen stepped in. At that point I kind of rolled my eyes and figured I was done with Nightwish since my tastes had continued to drift away from symphonic metal somewhat.

But then I decided I needed to give Floor a chance, and I listened to this live concert. Turns out they played my favorite Nightwish song ever, Ghost Love Score. I figured if Floor could do that song justice, she would be OK in my book.

She totally blew it away. She can do [operatic](https://youtu.be/ulN7_7v5Jfk?t=29m27s) vocals, although perhaps not quite as polished as Tarja, but she can also do [rock](https://youtu.be/ulN7_7v5Jfk?t=59m30s) vocals even better than Annette in my opinion. She has great range in a variety of styles and has great stage presence. She's really nails every single song. I'm a believer now. Floor brought Nightwish back for me. I really look forward to what they do next.
