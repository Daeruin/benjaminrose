---
from_wordpress: true
comments_id: 39
title: The Fellowship of the Ring
date: 2007-09-16T21:18:48-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=39
permalink: /post/the-fellowship-of-the-ring/
categories:
  - fiction
---
{: .image-wrap-right}
![The Fellowship of the Ring](/assets/images/fellowshipOfTheRingCover.jpg){: width="350px"}

This morning I finished rereading _The Fellowship of the Ring_ for the third time. I first read it in fifth grade, and I really don't remember anything about that experience, except that it hooked me on epic fantasy. I read _The Two Towers_ and _The Return of the King_ the same year. In sixth grade, I read the _Silmarillion_. My teacher didn't believe I had read it and had to get third-party witnesses from my classmates who had seen me reading it every day for months.

After that, the vast majority of my experience with Middle Earth came through Iron Crown Enterprise's MERP (Middle Earth Role Playing) game. MERP and its big brother Rolemaster were the only role playing games I played for years. Those MERP books were remarkably true to Tolkien's published works. As far as I know, they have never outright contradicted anything Tolkien wrote, which is quite a feat. Of course, they also added a whole lot of their own stuff.

But I still learned a lot of Tolkien lore from those books. I still have them, a whole box full. I used to buy pretty much anything that Iron Crown Enterprises published. I devoured their stuff. With the volumes of information they put on the shelves, I didn't feel a need to reread _The Lord of the Rings_ again until Peter Jackson's movie adaptations came to theaters. After watching _The Fellowship of the Ring_ eleven times on the big screen, I decided it was time to refresh my memory of the original story before the next movie was released. The next year, my wife and I were reading the last chapter of _The Two Towers_ together while waiting in line for the first showing at midnight. The major plot changes Jackson introduced were severely disappointing.

Nevertheless, Jackson's adaptations are still excellent. Once I got over my initial disappointment, I was able to admire the second movie's coolness, and the third was simply amazing.

As I was reading _The Fellowship of the Ring_ over the past month or so (I even read all the poetry!), I spent some time thinking about the differences between book and movie. I even watched a couple of clips from the movie right after reading the corresponding chapter in the book.

One of the scenes I chose to watch was the fight in the Chamber of Mazarbul. When I saw that scene in the theater the first time, I was blown away. I could hardly stay in my seat. I thought it was the coolest fight scene since The Matrix. Upon reading that scene in the book, I was astounded to remember that the cave troll never even entered the room, but _the Balrog did!_ In the book, the cave troll was used only to try breaking open the door. Later, a huge orc chieftain enters the room, and it's him who stabs Frodo with a spear. Aragorn then cleaves the orc's head in twain with Anduril. As the company flees out the room's other entrance, Gandalf stays behind to delay pursuit with a door-sealing spell. Then the Balrog comes in, breaks the spell, and the room collapses. Gandalf is forced to use a word of Command and is nearly broken himself.

It's interesting how the strong visuals and even the dialogue in a movie can completely replace your memory of the book. You tend to forget that twelve years passed between Bilbo's party and Frodo's departure from the Shire, that Frodo was actually 45 years old when he left, and that Bilbo never talked with eggs hanging out of his mouth. (Of course, when I say "you forget," I really mean "I forget"; you may very well have remembered some of these things better than I.)

You forget that the hobbits acquired some really cool weapons that Tom Bombadil takes from the barrow-wights. Tolkien describes the barrow-knives as "long, leaf-shaped, and keen, of marvellous workmanship, damasked with serpent-forms in red and gold. They gleamed as he drew them from their black sheaths, wrought of some strange metal, light and strong, and set with many fiery stones." These are not the shabby knives Aragorn is depicted as giving them in the movie. In case you don't know, "damasked" comes from the term "Damascus steel," which has a distinctive wavy pattern. Try picturing the leaf-shaped Viking dagger with the knife's serpent-like Damascus pattern in red and gold.

![leaf-shaped Viking dagger](/assets/images/cashenVikingBlade.jpg){: width="400"}
![serpent-like Damascus pattern](/assets/images/ebonyDamascusSlimlock.jpg){: width="400"}

You forget that Bree was not a run-down mud-hole full of belching drunks. You forget how cool and well-orchestrated the confrontation at the Ford of Bruinen was before Jackson messed with it (although I actually do approve of the substitution of Glorfindel with Arwen—just not with the execution of it). You forget that three fourths of the book go by before the Fellowship even leaves Rivendell. You forget that Aragorn and Boromir both disobeyed Gandalf's instruction to flee when he faced down the Balrog on the bridge; they drew their swords and stood courageously behind him.

Despite all the changes made, Jackson and his fellow screen writers (Fran Walsh and Phillipa Boyens) incorporated a lot of material directly from the book. I was astonished at the number of the lines in the movie that come straight from the book, including Aragorn's amazed exclamation, "That spear-thrust would have skewered a wild boar!" It works in the book, but it's one of the lamer moments in the movie. Another one that made it into the movie, albeit in paraphrased form, was Pippin's declaration that he was coming because "there must be _someone_ with intelligence in the party" (emphasis mine).

If it's been a while since you read The Fellowship, I encourage you to go back and read some of those scenes again—especially those at the Ford of Bruinen and the Chamber of Mazarbul.
