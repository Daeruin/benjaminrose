---
from_wordpress: true
comments_id: 181
title: How to Write a Story that Rocks, part 3
date: 2010-03-10T22:04:57-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=181
permalink: /post/how-to-write-a-story-that-rocks-part-3/
categories:
  - writing and publishing
---
I still have a few things to mention about this seminar, which I attended at [Life, the Universe, and Everything](http://www.ltue.org/2010_Schedule.html) at BYU last month. The fact that I'm writing three posts just on this one seminar should tip you off to how much I liked it. Yet I fear that if you watch [the video recording of the seminar](http://johndbrown.com/2010/02/how-to-write-a-story-that-rocks/), you won't get the same reaction I got. I've watched a few minutes of the videos, and they're just not the same as being in person. Besides, and I say this quite frankly, there was a fair bit of fluff and stuff that did nothing for me in that seminar. They could probably have trimmed at least a half our of unnecessary stuff and gotten more interesting and helpful information in. But as they themselves admit, this was their first time with this particular format, and it will improve with repetition.

Anyway, onwards we go.

There are two specific brainstorming and plotting strategies I got from the seminar that I want to share with you.

The first is the List & Twist. This is a brainstorming activity. You begin with the most basic of story premises: a character, a setting, and a problem. They only need to be a single line. The premise we worked with in the seminar involved a rancher in rural Utah who has a missing worker. What you do now is start listing all the associations and cliches you can thing of relating to this basic premise. You might list things like rope, cattle, desert, kidnapping, money problems, Mormons, horses, and so forth. You're doing this to get all the typical associations and cliches out of your system. When you feel like you're starting to get a decent sized list, and most of the cliches are out, then you start twisting.

John Brown, the main presenter, gave his an example of his own using his novel. It turned out to have a similar basic story premise to the example we were working with, and I think he chose the example for that very reason. The basic novel for his idea came when he was talking a walk in rural Utah and came across a herd of cows with a big bull. He thought, what if humans were being ranched like cattle? That's a decent "what if" kind of idea, but then he wanted to twist it. So he thought, OK, they're being ranched, but not for their flesh—for their very souls. And, since humans would never submit to being ranched without a fight, the evil ranching overlord god has come up with a system that hides from them the fact that they're even being ranched. That's the premise for his novel. I have to admit that when I first heard this on the Writing Excuses podcast, I wasn't terribly drawn to the idea. But after hearing him read from the book, it started to sound pretty cool, and I can't wait to get my hands on a copy.

The other part of the seminar I found helpful was a plotting strategy. It's not anything I haven't heard before, so probably you've heard similar things before, too. But I'd never heard it put in such a helpful, memorable way. To explain this, I'm going to have to describe what they call a story cycle.

The story cycle begins with the inciting incident. The character(s) is presented with a problem that involves either a threat of unhappiness, a cause of actual unhappiness, or a mystery of some kind. It should involve a conflict and a surprise—in fact, every step of the cycle must have both conflict and surprise. The next step is the character's reaction and decision. This should reveal his motives, desires, and goals. Then the character acts, and it results in some sort of disaster. Now this is the part that really made me go "Hmmmm." An easy way to determine the outcome of the character's action is to say either "Yes, he succeeded, but . . ." or "No, he failed, and furthermore . . ." The point being that something bad must continue to happen so that the character is continually posed with problems. The cycle then begins again, until you reach some kind of final resolution where you can finally say simply "Yes, she succeeded."

Most of what I described wasn't anything particularly new to me, but for some reason the "Yes, but" and "No, furthermore" part of that really stuck with me. It's such a simple and pithy way to remember to continue challenging your character with problems.

More on LTUE sessions to come!