---
from_wordpress: true
comments_id: 596
title: Adventures in Sci-Fi Publishing, Moses Siregar, Free Giveaway
date: 2011-06-08T20:58:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=596
permalink: /post/adventures-in-scifi-publishing-moses-siregar-free-giveaway/
categories:
  - uncategorized
---
**The Intro - Adventures in Sci-Fi Publishing**

I've blogged before about the podcast Adventures in Sci-Fi Publishing and how great it is. Shaun Farrell, the creator and host, interviews fantasy and science fiction authors about their books and the publishing process, and he's a really great interviewer. I've listened to a number of other podcasts of a similar nature, and none of them come close to the quality of content delivered by Shaun. Suffice it to say, you should definitely [check out this podcast](http://www.adventuresinscifipublishing.com/).

The podcast has changed a bit in the past year or so, since Shaun Farrell, the original genius behind it, felt he didn't have time to do it anymore. It takes loads of time to produce a podcast of this quality, and it's all done for free. That's when Moses Siregar and a few other champions stepped in to save the podcast. Several co-hosts now conduct most of the interviews to ensure the podcast remains alive and well. A number of helpful folks also generate lots of interesting content for the website.

**The Real Point - Moses Siregar**

I was a little unsure about the idea of new people doing these interviews, since Shaun's excellent interviews were really what made the show stand so high above its peers. But Moses Siregar has turned out to be quite good as well, and I really enjoy listening to his interviews. He's likeable, well spoken, and clearly an astute and intelligent fellow.

It turns out Moses is also a fledgling author, and his first novel is coming out soon. The first 25,000 words are available as a novella on Amazon.com, and it's currently free. I just downloaded it and plan on reading it on my Kindle soon. And I have to say that the cover art is pretty darn cool—some of the best I've seen for a self-published book.

But here's the best part of it: he's also running a contest for a free Kindle or Nook e-reader (first place) and $100 to a charity of your choice (second place). Sounds like a killer deal, and it goes to support not only Moses and his new book, but your reading habits or your choice of another good cause.

You should go check out [Moses Siregar's blog here](http://sciencefictionfantasybooks.net/free-epic-fantasy-ebook-charitable-giveaway-win-a-free-kindle3-nook-or-100-gift-card/).
