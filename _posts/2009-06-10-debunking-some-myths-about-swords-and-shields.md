---
from_wordpress: true
comments_id: 119
title: Debunking some myths about swords and shields
date: 2009-06-10T20:33:58-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=119
permalink: /post/debunking-some-myths-about-swords-and-shields/
categories:
  - medieval combat
---
I ran across some great videos today that I just had to share. Have you ever really wondered about the SHHHHHING! sound of a sword being drawn in the movies? I never had until I saw this video:

<iframe width="560" height="315" src="https://www.youtube.com/embed/yzbfuI0PMdA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


If you watched the video, you probably got a clue about what he thinks of the mystical katana. If not, this video will make it very clear indeed:

<iframe width="560" height="315" src="https://www.youtube.com/embed/XLWzH_1eZsc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Although I've never held the katana in the mystical esteem held by some, I had bought into the idea that it was at least superior to other swords in some ways. And finally, a similar debunking of shields:

<iframe width="560" height="315" src="https://www.youtube.com/embed/fdHo-1jbX1A" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


The fine fellow who did all these videos has quite a lot of them, all very interesting. I recommend [his YouTube page](https://www.youtube.com/user/lindybeige), or just click on some of his related videos on one of the previous links.
