---
from_wordpress: true
comments_id: 147
title: LTUE Writing Conference
date: 2010-01-30T21:37:27-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=147
permalink: /post/ltue-writing-conference/
categories:
  - 'life &amp; stuff'
  - writing and publishing
---
So I just heard of a cool writing conference called [Life, the Universe and Everything](http://www.ltue.org/LTUE2010.html). It's scheduled for February 11, 12, and 13 at Brigham Young University in Provo, Utah, and this year the illustrious Brandon Sanderson will be attending. [The schedule](http://www.ltue.org/2010_Schedule.html) looks jam-packed with awesome panels and presentations. There are lots of other published authors attending; a few whose names I recognize are [Dan Wells](http://www.fearfulsymmetry.net/?p=368) and [Howard Tayler](http://www.schlockmercenary.com/) (from Sanderson's [Writing Excuses](http://www.writingexcuses.com/) podcast), [Brandon Mull](http://www.brandonmull.com/), and [L.E. Modesitt](http://www.lemodesittjr.com/) (other than Sanderson, Modesitt is the only of these authors I've read, and I really like his stuff).

OK, now here's the best thing about it: it's FREE. Heck, yeah. (Does that phrase give me away as a Utahn?)

I'm definitely planning to attend, at the very least on Saturday for Sanderson's main address and live recording of an episode of Writing Excuses. With my wife's blessing, I'll take a couple days off work to attend the Thursday and Friday sessions, too. Maybe I'll see you there.