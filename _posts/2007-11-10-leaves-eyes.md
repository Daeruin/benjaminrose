---
from_wordpress: true
comments_id: 48
title: "Leaves' Eyes"
date: 2007-11-10T00:32:52-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=48
permalink: /post/leaves-eyes/
categories:
  - music
---
Someone just posted some information on another female-fronted metal band, one that I haven't talked about yet. So I looked them up and listened to a few songs.

The band is called Leaves' Eyes. The name sounded familiar, and sure enough I recognized them. I think I must have followed a link to their Web site at some point back before I started writing about music here, and then forgotten about them.

Here's my take on Leaves' Eyes. They are good, but don't quite rank up there with some of the others I've written about. I think their main flaw is that "drinking song" syndrome (like Ing has mentioned in the past) that many other European bands also have. These bands tend to use a very obvious sort of marching beat, very repetitive melodies, and also tend to use major keys a lot. It gives their music a sort of drinking song feel. Leaves' Eyes doesn't do that to the same degree as other European bands, thankfully—but it's still there.

I do like their use of classical instruments, however, and Liv Krull's voice is definitely good. The variation between folk elements and heavy guitars is cool. It's also cool that they chose some interesting subject matter to write about, namely Leif Ericson's voyage to the Americas. It definitely has that epic feel to it that I like.

All in all, I give them a middle rating—good, but not something I'd listen to a whole lot. If you want to check them out, here is [their official home page](http://www.leaveseyes.de/) and [their MySpace page](http://www.myspace.com/leaveseyespage).
