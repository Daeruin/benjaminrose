---
layout: article
title: Migration from Wordpress to Jekyll, Part X
author: Ben
date: 2020-06-06T06:27:09.653Z
permalink: /post/migration-from-wordpress-to-jekyll-part-x
categories:
  - metablogging
---
Unfortunately, I'm not going to finish my planned series on migrating from Wordpress to Jekyll. I apologize and hope the first article is helpful to someone.

About a month ago, the logic board on my personal computer bit the dust while I was in the middle of drafting the next article in the series. I didn't realize how serious my computer's problem was at the time; I just knew it was having trouble starting up. 

I spent a week trying dozens of things to get the computer to start up again. We were in the middle of the coronavirus lockdown, and most stores were closed, including the Apple Store. After another week or two, I finally found a store that was open to repair my computer—Expercom. Because the Apple Stores are still closed, Expercom was flooded with work, and it took them over a week to get my computer up and running.

The unfinished draft of the next post was lost. It's been a month since I worked on it, and over three months since I started migrating my website. I just don't remember enough at this point to make the articles helpful, and my motivation is gone.

Best of luck to you in your conversion efforts. It's worthwhile, and I believe in you. :)