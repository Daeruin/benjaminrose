---
from_wordpress: true
comments_id: 752
title: "Dragon Age: Redemption by Felicia Day"
date: 2011-11-28T22:22:38-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=752
permalink: /post/dragon-age-redemption-by-felicia-day/
categories:
  - gaming
  - 'movies &amp; TV'
---
I've never played many computer games. Not that I don't enjoy them—quite the opposite. Once I start one, I often become completely absorbed to the detriment of everything else. But I've never had enough discretionary income to really justify spending much on computer games—especially when there are so many great table-top games to occupy my time. Besides, I have a Mac, and historically there have been very few games available.

That's been changing recently, and so has my budget. So I've been looking at what games are available and thinking about maybe getting one, if I can muster the self control to not let it take over my life. And I've been finding lots of really cool-looking games that will work on the Mac.

![Felicia Day in Dragon Age: Redemption](https://vignette.wikia.nocookie.net/dragonage/images/4/46/Dragonageredemption.jpg/revision/latest/scale-to-width-down/140?cb=20110215052749){: .image-wrap-left}

One of those is [Dragon Age II](http://dragonage.bioware.com/da2/home/), a mature, dark fantasy cRPG from Bioware. As I was browsing their website, I discovered that they have commissioned a six-part web series from Felicia Day creator and star of The Guild and also the main love interest in Dr. Horrible's Sing-Along Blog. The series is a live-action story set in the world of Dragon Age and is called Dragon Age: Redemption.

The series turned out better than I expected. Sure, it was done on a fairly low budget, and it shows. However, what sucked me in was the script and the characters. Felicia Day did a great job making the characters likeable, and the dialog was quite good—definitely ahead of most other fantasy shows.

So the series is worth watching if you're a fan of fantasy. Check it out, then come back and let me know what you think. (I would have embedded the YouTube videos here, but you'll enjoy them much more in HD directly on YouTube.)

[Dragon Age: Redemption, Episode 1](http://youtu.be/-093SQo9NWM)
