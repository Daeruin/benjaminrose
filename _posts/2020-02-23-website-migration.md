---
from_wordpress: true
comments_id: 3375
guid: http://www.benjaminrose.com/?p=3375
layout: article
title: Migrating My Website from Wordpress to Jekyll
author: Ben
date: 2020-02-23T19:54:52.000Z
permalink: /post/website-migration/
categories:
  - metablogging
  - software
---
Goodbye, WordPress. I've been using WordPress for many years, but I'm ditching it in favor of a static site generator. This is a huge change. You can expect my entire site to look different. Things may go missing. Links may break. If you've subscribed to my site at all, you'll no longer get updates (please send me a message if you want to keep getting notifications!). Want more details on the why and what? Read on.

For its entire existence, my website has been hosted on my awesome brother's server. He's an internet marketer who has many of his own websites, and it was free and easy for him to host mine. However, my website has grown to take up a lot of space, and it's time for me to be a big boy and get my own hosting service.

At the same time, I've been learning a lot about static site generators and their advantages. I could pretty easily pay for a hosting provider like Bluehost and just copy my WordPress site directly over to them. So why am I moving away from WordPress, and why am I going to use a static site generator?

**1. Security**

I've heard it said that WordPress powers nearly a third of the entire internet, and that makes it a huge target for hackers. WordPress runs on PHP and databases, which are vulnerable to hacking if you aren't careful. To stay safe, you have to constantly update to the latest version and be careful about which plugins you install.

I had a scare of my own just a couple of days ago. My brother contacted me and said his hosting provider noticed I was using an unsafe plugin. This was a plugin that came with my theme. It's not something I wanted or even used. I looked up the plugin and saw multiple reviews from the day before from people saying they this plugin had caused them to *lose their entire site*—years of work wiped clean, destroyed. Needless to say, I immediately uninstalled that plugin.

**2. Performance**

Because WordPress has a database as a back end, it has to dynamically construct your pages every time someone loads your site. This processing takes time. Not necessarily a huge amount of time, maybe depending on how much content you have, what plugins you use, and so forth—but it can definitely be noticeable. Static sites are pure HTML, maybe some Javascript. There's nothing to process. Static sites load incredibly fast.

**3. My Career**

I'm a technical writer by trade. In the world of technical documentation, static site generators have become very popular, especially for developer documentation like APIs. Learning how to use and deploy a static site will give me some valuable experience that I can use to further my career.

**4. Curiosity**

I've been using WordPress for sooooo long. I'm kind of bored of it. I'm looking forward to a change. I like learning new things. I'm going to learn more about CSS, Liquid, and more. This is going to be both challenging and fun.

Having said all that, there are some disadvantages.

1. **The conversion process isn't easy**. There's a fair amount of manual work involved. I don't mind this kind of work, but it will take some time.
2. **Static sites don't handle comments easily**. Because static site generators are, well, static, there's no way to let readers leave comments that are automatically saved as part of a blog post. There are a few workarounds, some of which are technically quite challenging unless you are a programmer. I'm taking the route of using a third-party commenting system. Disqus is the most popular, but it has ads and some privacy concerns. I'm going to try to use Hyvor, which is free up to a certain number of pageviews.
3. **No email service by default**. I'm planning to use Netlify as my hosting provider. They are free for small blog sites like mine. However, they don't provide a lot of the amenities that paid hosting providers have, like email. But I LOVE having my personal email address using my own domain. That means I'm going to have to set up email forwarding and start using Gmail, or pay for a third-party email provider. And what happens to all my old email? I might need to find a way to save it or transfer it over to my new service.
4. **Uncertainty**. I'm moving hosting providers, nuking my website's backend, and moving my site to an entirely new platform. Things are likely to break. As I was typing, I realized that I may lose all my subscribers. I think that is all handled through WordPress.com—I need to look into it.

I think that's all I have to say about it for now. Expect this change to happen sometime in the next few days or weeks. I'm not sure how long it's going to take me. Benjaminrose.com is evolving!

Read more about the actual process I followed here:

1. [Migrating from Wordpress to Jekyll, Part 1](https://www.benjaminrose.com/post/migration-from-wordpress-to-jekyll-part-1)
2. [Migrating from Wordpress to Jekyll, Part 2](https://www.benjaminrose.com/post/migration-from-wordpress-to-jekyll-part-2-the-lost-draft)