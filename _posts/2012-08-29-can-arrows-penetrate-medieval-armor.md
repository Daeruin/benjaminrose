---
from_wordpress: true
comments_id: 944
title: Can Arrows Penetrate Medieval Armor?
date: 2012-08-29T17:23:39-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=944
permalink: /post/can-arrows-penetrate-medieval-armor/
categories:
  - medieval combat
---
Can Arrows Penetrate Medieval Armor?

![Arrow penetrating chain armor](/assets/images/arrowPenetratingMaille.jpg)

I find this question fascinating. As both a gamer and a fantasy writer, I like to get as close to reality as possible, if only to make the fantastic elements of my games and stories that much more fantastic and cool. So a while back, I spent several days researching this question, and I collected a fair number of interesting resources.

To my dismay, I discovered that there are lots of factors to consider when examining this question.

  1. **The specs of the bow and arrow.** Draw weight, arrow type, arrowhead type (material, shape). Modern compound bows can have much higher draw weights than historical bows. Some arrowheads were designed to puncture armor, while others were designed to maximize damage to the flesh but had poor armor penetration.
  2. **The specs of the armor.** Assuming plate armor: thickness, carbon levels (iron, steel, and quality of the steel), tempering and hardening, layers (mail, textile). Historical armor had a huge level of variation in type and quality, making it difficult to use actual historical pieces to make generalizations. From what I've read, this isn't something that we'll get a consensus on anytime soon—yet it's obviously a huge factor.
  3. **The circumstances of the strike.** Angle, distance. Most tests assume direct, perpendicular strikes. Obviously, hitting at an angle will drastically decrease the penetration. Some strikes could glance off and hit another combatant. Distance is a huge factor. Tests seem to indicate that the penetration range is around 20 to 30 yards. Beyond that, effectiveness decreases drastically.
  4. **How deep is the penetration?** Most tests seem to indicate that the deepest penetration was about an inch, maybe two. It usually takes at least two to three inches of penetration to damage any vital organs.
  5. **Economic and social factors.** Cost and availability of armor, skill of the archer, horses, mass archery. Some armors that defend very well against arrows were prohibitively expensive. Skilled archers might be able to make up for many of the problems mentioned so far. In certain historical battles, the tactics of mass archery made a huge difference.

This isn't a simple question. It's riddled with complicated factors that are all interrelated. I never came to a definite conclusion myself, except that in general, it seems like arrows seldom (if ever) would have penetrated plate armor. The circumstances would have needed to be just right for that to happen.

I've collected most of the best resources I found below, if you're interested in digging into this question on your own. This is the only collection like this that I've seen. If you found it useful, or if you find anything that's missing, please leave a comment.

# Articles and Discussions

[English Longbow Testing](http://www.currentmiddleages.org/artsci/docs/Champ_Bane_Archery-Testing.pdf) (PDF), an article by Matheus Bane documenting some testing he did with various types of arrowheads and armors. This test uses a clay slab behind the armor to show how deeply the arrows would have penetrated human flesh. Only the needle point bodkin penetrated plate armor deeply enough to cause a potentially fatal wound.

[Testing reconstructed medieval crossbows](http://www.historiavivens1300.at/biblio/beschuss/beschuss1-e.htm), by Andreas Bichler. The existing medieval crossbows we have are all in unshootable condition and are sequestered in museums. This is an attempt to recreate medieval crossbows and test their performance on different types of armor.

A [fascinating discussion on MyArmoury.com](http://www.myarmoury.com/talk/viewtopic.php?t=15454) on the ability of the English longbow to penetrate plate armor. The folks on MyArmoury are generally smart and very well read on these kinds of subjects. It's a long, involved discussion. Casual browsers need not apply.

This [blog post from medieval recreator Will McLean](http://willscommonplacebook.blogspot.com/2011/12/english-longbow-testing-against-various.html) was the source of one of the other links I've given here, and provides an interesting critique of some experimental bow shooting.

Another blog post from [Will McLean discussing metallurgical findings from historical armor](http://willscommonplacebook.blogspot.com/2006/12/arrows-vs-armor-ca-1400.html).

# Videos

**Longbow versus breastplate.**

[Weapons That Made Britain](http://www.martialtalk.com/forum/showthread.php?87180-Weapons-that-Made-Britain-The-Longbow). In this episode, historian Mike Loades discusses the English longbow. This link leads to a forum where the entire show can be seen via a series of linked YouTube videos.

The following two videos document a test conducted by Mike Loades using a modern arrow-shooting machine to replicate ancient weapons. Although the arrow penetrates the breastplate, it doesn't go deeply enough to cause injury.

<iframe width="560" height="315" src="https://www.youtube.com/embed/D3997HZuWjk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/eRXwk4Kdbic" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**English warbow versus tempered sheet metal.** These three videos are attempts by various groups of people to test the penetration of the English warbow of various kinds of sheet metal and breastplates. Although these tests are interesting to watch, they aren't rigorous enough to really draw conclusions from. Many of the arrows break through the plate, but there's no way to tell how deeply and whether the strikes would have caused significant injury.

<iframe width="560" height="315" src="https://www.youtube.com/embed/q-Xp56uVyxs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/q2lbB3OMNns" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

<iframe width="560" height="315" src="https://www.youtube.com/embed/KCE40J93m5c" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

&nbsp;
**Crossbow against steel breastplate.** This video shows some guys shooting a 1000 pound crossbow using various heads on the quarrels. None of them pierce so far that the quarrel sticks into the breastplate. It looks like a few of them might have pierced up to a quarter inch or so. With some linen padding inside, I doubt the wearer would be significantly injured—although the so-called armor breaker and the heavy quarrel look to pack quite the punch. Probably the biggest flaw in this test is that the breastplate is just propped up on a board. It gets knocked around a lot—not exactly the same as someone wearing it.

<iframe width="560" height="315" src="https://www.youtube.com/embed/76mbOMFjlu0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

# What do you think?
