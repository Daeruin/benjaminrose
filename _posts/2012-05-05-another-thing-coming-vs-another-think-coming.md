---
from_wordpress: true
comments_id: 874
title: "_Another Thing Coming_ vs. _Another Think Coming_"
date: 2012-05-05T22:27:12-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=874
permalink: /post/another-thing-coming-vs-another-think-coming/
categories:
  - writing and publishing
---
My wife, Missy, and I are fond of arguing about language. She's an English major, and I'm a linguistics major, and we sometimes have vastly different ways of thinking about language. It's a lot of fun. Our latest discussion was whether it's more correct to say "another thing coming" or "another think coming." Missy saw an ad using "another thing coming" and scoffed, which surprised me since I have never heard it said nor seen it spelled any other way. I immediately thought of the famous Judas Priest song, "You've Got Another Thing Coming," and had to shake myself out of a nostalgic rock-and-roll daze to finish being outraged that I could possibly be in the wrong.

Unfortunately, rock stars don't make for convincing grammatical authorities, and Missy quickly convinced me that "another think coming" makes a lot more sense. "If you think I like you, you've got another thing coming" really doesn't make much sense. What thing? How can there be another thing if there wasn't a first thing to begin with? Exactly what thing is coming, and what is it going to do? On the other hand, "If you think I like you, you've got another think coming" makes much more sense. It's a bit odd-sounding grammatically, but there's nothing wrong with it, and it actually means something. You're thinking one thing now, but you're wrong and you'll soon be thinking something else.

Still, I wanted to find something authoritative, if possible. A quick internet search turned up a lot of pointless arguments from people who apparently don't know what a dialect is, or if they do, they fail to realize that they speak one, too. Just because you and all your friends say it one way doesn't make it universally accepted. (For my first piece of evidence I submit the word "irregardless.") But I did find a few chunks of real information.

**Prior Use in Literature**

One of the best pieces of evidence we have is prior use in literature. I didn't find anything crushingly authoritative, but there was a handy [post on the English Stackexchange](http://english.stackexchange.com/questions/24167/what-is-the-origin-of-the-phrase-youve-got-another-thing-coming) site that provided a search of Google's NGrams database (which, if you aren't aware, is a massive index of every word and phrase used in over 5 million books that Google has scanned). The results reveal the earliest usage of "another thing coming" to be from [16 December 1906](http://books.google.com/books?id=LAYNAAAAYAAJ&pg=PA214&img=1&zoom=3&hl=en&sig=ACfU3U2ppQbnx9Uxe5mmOFTEkwo_fd6gbg&ci=155%2C368%2C759%2C88&edge=0):

> "But if we did, then we have another thing coming, for this is the cry-baby talk I find in this morning's (Dec. 16) editorial . . ."

I had to do a bit of my own searching to find the earliest usage of "another think coming," but I found one from [1903](http://books.google.com/books?id=Mm8tAQAAMAAJ&pg=RA11-PA29&dq=%22got+another+think+coming%22&hl=en&sa=X&ei=j3KjT4HDG4uGiQKD77jPAw&ved=0CEYQ6AEwAjgy#v=onepage&q=%22got%20another%20think%20coming%22&f=false):

> ". . . and say, Mr. Editor, think we did not do justice to the occasion, and you got another think coming."

It's not very a well formed sentence, but the intent is pretty clear anyway. And here's another example, from [March 1906](http://books.google.com/books?id=NQVZAAAAYAAJ&pg=RA2-PA73&dq=%22got+another+think+coming%22&hl=en&sa=X&ei=NHKjT52LGMSFiALe1eHfAw&ved=0CDkQ6AEwADgK#v=onepage&q=%22got%20another%20think%20coming%22&f=false):

> "May be you think your factory is not a school. If you do, you've got another think coming."

Of course, just because these are the earliest examples I could find on Google NGrams doesn't mean they're actually the earliest examples of these phrases. There could easily be some publication that Google hasn't indexed. There are two commonly cited newspapers used as evidence online (such as by Gary Martin of [The Phrase Finder](http://www.phrases.org.uk/meanings/another-think-coming.html)), but none of the citations I saw provided links to the originals. Apparently these two newspapers haven't been scanned and indexed by Google yet. But I'll thrown them in for the sake of completeness, since one of them dates from 1898.

"Another thing coming" was used in the New York newspaper _The Syracuse Herald_ in August 1919:

> "If you think the life of a movie star is all sunshine and flowers you've got another thing coming."

"Another think coming" was apparently used in the local rival paper _The Syracuse Standard_ years earlier in May 1898:

> "Conroy lives in Troy and thinks he is a coming fighter. This gentleman has another think coming."

**Phonetic Likelihood**

Another argument that makes "another think coming" seem even more plausible is the likelihood of misunderstanding it when spoken. When vocalizing "another think coming," the two k sounds become merged, so it sounds more like "thingkumming." Someone listening might easily think you're saying "another thing coming" instead. It's plausible that people hearing the original phrase misunderstood and then perpetuated their misunderstanding both vocally and in print.

It's like the person I know who innocently used the phrase "6 months encounting" instead of "6 months and counting." That person had obviously never seen the actual phrase in print and was simply attempting to type out phonetically what she'd heard her whole life. It also highlights the fact that idioms don't have to be inherently meaningful. To this person, it didn't matter that "encounting" isn't a real word. It was just part of a phrase she knew. The phrase as a whole had meaning despite her lack of understanding about the individual words in it. This is actually a decent defense against the argument that "another think coming" must be correct because it makes more sense than "another thing coming." Idioms don't have to make inherent sense; they can still be meaningful when used as a whole unit. History and tradition _make_ them meaningful.

But when you have a phrase that really does make sense pitted against one that doesn't, I'll vote for the one that does. Especially when . . .

**The Dictionary Says So**

Finally, it appears that both the Oxford English Dictionary**** and Merriam Webster believe the correct usage is "another think coming." Although I don't have an OED of my own, a poster on Wordreference.com offered this quote from the dictionary:

> **<span style="text-decoration: underline;">think, n.</span>** 2b **_to have another think coming_**: to be greatly mistaken.
> **1937**_Amer. Speech_ XII. 317/1 Several different statements used for the same idea—that of _some one's making a mistake_ . . . [e.g.] you have another think coming.

Considering the OED's reputation, I find it odd that their earliest example seems to come from 1937. It could be that the 1937 example is simply the clearest and most prominent one, not the earliest. I don't know what criteria the OED uses in choosing examples. In any case, we also have "another think coming" used by [Merriam Webster](http://www.merriam-webster.com/dictionary/think?show=1&t=1336115846) as an example of "think" used as a noun, which validates it in my mind.

> **think, noun**
> an act of thinking <has another _think_ coming>

So there you have it. I don't have anything bulletproof, but it appears that "another think coming" is the best bet. I shouldn't have been surprised. My wife is almost always right about matters of usage and pronunciation. Apparently, I had another think coming.
