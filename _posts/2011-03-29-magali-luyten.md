---
from_wordpress: true
comments_id: 398
title: Magali Luyten
date: 2011-03-29T13:17:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=398
permalink: /post/magali-luyten/
categories:
  - music
---
![Magali Luyten](/assets/images/magaliLuyten.jpg){: .image-wrap-right}

The other day I was cruising around the web and came across this amazing singer named [Magali Luyten](http://www.magali-luyten.com/). She's from Belgium, which gave me an instant predisposition to like her, since I lived there for about 8 months a little over a decade ago. I can even pronounce her oddly-spelled last name correctly. I'd try to describe the peculiar diphthong, but it would require technical linguistic terms that would make your eyes glaze over.

Not only that, but she's a female rock singer. I've been on a quest the past several years to find great female-fronted metal bands, as you know if you've been following my blog for any amount of time. I really dig that whole scene—well, not the whole scene. There are some crappy female-fronted bands, just as in any other area of music. But when it's done right, it really works for me. The big four examples are Nightwish, Within Temptation, Evanescence, and Lacuna Coil (all based in Europe except one).

Back to Magali Luyten. Her voice is unlike anything I've heard. It's very low, way down in the contralto range. I'd say Christinna Scabbia from Lacuna Coil has a fairly low voice, but Magali's is lower. Most famous singers, whether male or female, are generally in the higher ranges, with a few exceptions—think Niel Diamond and Cher. So it's unique and different to hear someone fronting a band and singing down in that range. On top of this low range, Magali has a lot of power and sort of a raw edge to her voice, perfect for rock.

But you know what's most refreshing? She's not into the typical frontwoman ostentation. No gaudy gothic dresses, pseudo-Renfaire garb, or skanky sexed-up outfits for her. A plain black tanktop and black jeans are good enough for her. This lady is serious about singing, and it shows. Not that you can't be gothic or skanky and still be serious about music—but I think Magali's appearance and attitude lend her some credence.

Unfortunately, she's been in a few bands that haven't lasted too long. [Beautiful Sin](http://www.myspace.com/thebeautifulsinmetalband) is perhaps more in the gothic or prog style, but they're gone now. She's been featured in Ayreon. Currently she's the lead singer for [Virus IV](http://www.virus4.net/), a straight-up metal band with one album out and another on the way. **Edit**: Virus IV has broken up. Magali appeared on the album [Obsessions by Epysode](http://www.myspace.com/epysode), an ensemble album orchestrated by Samuel Arkan of Virus IV, but she has now taken the position of lead singer for the Belgian metal outfit [Master of Waha](http://www.myspace.com/masterofwaha).

I think I've talked enough about her in my floundering, amateur kind of way. Listen to this video and hear for yourself. Warning: don't click if you dislike metal. But if you're into that scene, I have a feeling you're going to like this.

[VIRUS IV - Frightening Lanes](http://www.myspace.com/video/vid/38923622)

So what did you think? Would it surprise you to hear she's also a vocal coach? Not me. She's got incredible power _and_ control. I think I've found myself a new favorite. Magali Luyten is the real thing.
