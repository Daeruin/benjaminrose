---
from_wordpress: true
comments_id: 440
title: Wheel of Time E-book Covers Update
date: 2011-04-20T00:35:45-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=440
permalink: /post/wheel-of-time-e-book-covers-update/
categories:
  - art
---
Back in August of 2010, I created a long post showcasing all of the cover art for the Wheel of Time e-book covers that had been released until that point. Since then, two more of the e-books have been released—_The Gathering Storm_ and _The Towers of  Midnight_.

The cover for The Towers of Midnight is absolutely amazing and may even trump my original favorite, which was Knife of Dreams.

That original post has been my most popular post on the entire blog so far and even ranks higher than Tor's own page. So I figured I'd round it out and add the full size cover art to that post rather than dilute my incoming traffic too heavily. That way you can also see the new covers in context with all the others.

[Here's the post](/post/the-wheel-of-time-e-book-covers-image-heavy-post/ "The Wheel of Time E-Book Covers (image-heavy post)"). Go check out the new covers.
