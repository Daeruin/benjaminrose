---
from_wordpress: true
comments_id: 906
title: The Inevitable Metablogging Post
date: 2012-05-23T20:05:46-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=906
permalink: /post/the-inevitable-metablogging-post/
categories:
  - metablogging
---
All bloggers do it. That blog post where we talk about our own blogging. As if anyone else but us cared.

But bear with me for a moment. There may actually be something interesting buried in here, if you give it a chance.

The best blogging advice I've seen boils down to the following five rules (though [some expand it to ten](http://janefriedman.com/2012/05/30/top-10-blog-traffic-killers/), these five seem to be the best). I've been thinking about how well I do at these bits of advice. How well are you doing?

  1. **Blog often**. The longer you go without blogging, the less likely visitors are to come back. Me? I have set a schedule for myself to blog twice a week (well, once every three days, because my task management software doesn't have a setting for twice weekly). The blogging gurus say that's not enough if you want a consistent audience. Every day or every other day seems to be best.
  2. **Pick a niche**. Readers are more likely to come back if you're blogging about a subject they care about. If your posts range across the board, they are less motivated to return. Me? I'm terrible at this. Most of my posts revolve around geekery of one kind or another, but I probably don't have a close enough focus. My subjects include writing, reading, gaming, movies, and software. If I blogged every day this wide focus might be overcome somewhat, but as it is, if you want to read about gaming you sometimes have to wait a month or more before I get around to that subject again, whereas the people who aren't interested in gaming might see it  as just another reason not to come back. I should add a few caveats. The niche doesn't actually have to be a specific subject—it can be an attitude or a writing style if it's done really well. You can also be your own niche if, for example, you're an author with fans who just want to come and get to know you better.
  3. **Provide valuable content**. If you make obvious mistakes, or don't add anything to the existing online conversation, or only cover things in a superficial manner, people will see through you and won't come back. Of course, what counts as valuable depends on your niche. People might come to you for your humor, in which case you darn well better be funny every time. Me? I think I do OK at this. I don't generally post about something unless I feel I have something interesting to say about it.
  4. **Be personal**. Visitors love to see the person behind the curtain. They like the personal touch, to feel as if they know you. Emotion and humor reels them in.  Me? I share personal posts every now and then, but not often. I don't feel totally comfortable putting myself out there. Plus, I don't feel like this rule describes me as a reader—it's not so important to me.
  5. **Reply to comments**. The vast majority of blog readers don't comment. But when they do, you should acknowledge that you're listening. Otherwise, they won't have much motivation to comment again. Me? I've been doing pretty well at this. I love it when people comment on my blog, so I go out of my way to try to respond to each comment personally.

So, all of this has me thinking about what I could do better. Obviously, I could post more often. But time is limited, and I really want to finish that darn novel one of these days. When I'm closer to publishing my book, then I'll try to put a bit more effort into the blog. I could also try to narrow my focus a bit. Right now my only regular readers are a few family members and a select few friends I've met online (mostly writers). Focusing more closely on one subject and covering it more thoroughly would increase my chances of having visitors stick around. On the other hand, if I ever do publish my book and get readers coming to the site, they'll be coming for me, the author, not the expert on whatever.

Having put some thought into these things, I think my blog is about what it needs to be right now. I hope you keep coming back.

How are you doing with your own blog?
