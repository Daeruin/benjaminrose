---
from_wordpress: true
comments_id: 1282
title: "Primalcraft: Thirst, Twigs, Branches, and Stone Scrapers"
date: 2017-08-20T22:31:52-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1282
permalink: /post/primalcraft-thirst-twigs-branches-stone-scrapers/
categories:
  - Minecraft modding
---
Last time I posted, my plan was to test out my entire mod, one feature at a time. I've checked in 18 different updates to my mod since then. Most of the changes are little bugs, like my new loose dirt block breaking instantly when it shouldn't, or campfires failing to go out in the rain. I think I've caught most of those, but honestly I haven't made it through everything yet. I keep getting distracted by more interesting things. And I'm not sure I'll finish a comprehensive review.

**New Thirst Bar**

One of the most interesting things I did was update the thirst bar from a simple blue bar to some nice little water droplets that appear right over the hunger bar. It was kind of fun to create the droplet textures and figure out the math formula for getting them to fill up a half droplet at a time (similar to how the hunger bar works). I'm pleased with the results.

![Thirst bar in Primalcraft](/assets/images/primalcraft_thirstGui.png){: width="500px" height="133px"}

**Twigs and Branches**

I have always been bugged by the ubiquitous Minecraft stick. First, you punch trees to get logs. Then you somehow combine logs to get planks. And then you use planks to get sticks. Whaaaa? And they're these perfectly straight little rods—yeah, it fits the Minecraft aesthetic, but it's one of those little things that bothers me. On top of that, it seems ridiculous to make a sword handle out of a stick.

![Wooden shaft in Primalcraft](/assets/images/primalcraft_recipeWoodenShaft.gif){: width="172px" height="172px"}

So in Primalcraft, there are no more sticks. Instead you get twigs and branches directly from trees by breaking leaf blocks. The outer leaf blocks yield twigs, while leaf blocks right next to the tree trunk yield branches. You'll need a hatchet to get the branches. **Twigs** are used in fire making, as kindling, and can also be used in basket weaving. **Branches** can be trimmed with a stone scraper or stone knife to get wooden shafts. **Shafts**, in turn, can be used as fire sticks, tool handles, digging sticks, cudgels, torches, and wooden pegs.

&nbsp;

**Stone Scrapers**

Stone scrapers are also new. I've been wanting a reason to include them in Primalcraft for some time, because they were a very common tool for our Stone Age ancestors. But it seemed like knives and hatchets were already easy enough to make, and I didn't want an extra tool for no reason. The problem is that I wanted tool handles to be interesting and realistic, and it seemed like it should take a tool to make them. Yet all the tools already require shafts.

Enter the stone scraper. It's a simple tool made from stone flakes (just like other tool heads), but it doesn't require a handle. You use the stone scraper to break or shave unneeded parts from a branch, and voila—you have a shaft.

Later, I'm going to add more usefulness to stone scrapers in leather working.

&nbsp;
