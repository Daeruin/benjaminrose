---
from_wordpress: true
comments_id: 975
title: Studying the German Longsword Techniques at True Edge Academy
date: 2012-07-18T17:35:01-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=975
permalink: /post/studying-the-german-longsword-techniques-at-true-edge-academy/
categories:
  - medieval combat
---
The past two weeks, I attended a class on the medieval art of the German longsword.

The past decade or so has seen an explosion in the area of Western Martial Arts called Historical European Martial Arts, or HEMA. These arts are based on a number of fighting manuals from the Middle Ages that have been discovered and translated. Most of them were written in German, and practitioners still use the original German terms for most of the techniques. The fighting manuals reveal a rich martial tradition that didn't survive the advancements in firearm technology and the passing of time—unlike like the Eastern Martial Arts—but the tradition is now being revived.

The school I attended is called the True Edge Academy, and it has three branches in Salt Lake City and Utah Valley. They base their curriculum on the Lichtenauer tradition. Lichtenauer was a fencing master whose many students went on to pen their own fighting manuals, and this tradition pretty much defines the art of the German longsword. The True Edge Academy meets once or twice a week for about two hours to practice German longsword techniques.

{%
  include figure.html
  image-name="hemaSpring2010.jpg"
  width="500"
  height="395"
  alt="Members of the True Edge at a local Renaissance festival"
  caption="Members of the True Edge at a local Renaissance festival"
%}


I've been building up to this for a long time. I've always dreamed of wielding a real sword, but it never seemed possible. As a kid, I gazed longingly at the few swords on display at a local knife shop, but they were always far too expensive for me. I considered trying out fencing, but it just wasn't the same—somehow, the cheesy white costumes and wimpy little foils just didn't measure up.

My first introduction to the idea that you could actually learn how to wield a real sword came when I discovered The Riddle of Steel, a roleplaying game whose combat system was modeled on Historical European Martial Arts. It was even endorsed by the Association for Renaissance Martial Arts, a group of people who study HEMA. Since discovering The Riddle of Steel, I've been hanging out in discussion forums (like [TROSfans.com](http://www.trosfans.com/forum/ "The Riddle of Steel Fan Community") and [CodexMartialis.com](http://www.codexmartialis.com/index.php "Codex Martialis")) where I've met people who actually practice HEMA and even compete in tournaments. I still didn't think there was anything like that in my area, until someone pointed me to a [database of groups that practice HEMA](http://www.communitywalk.com/user/view/81443 "HEMA Alliance Group Finder")—and there was the [True Edge Academy](http://trueedgeacademy.com/provo.html "True Edge Academy"), practically in my backyard.

It took me a while to finally go to a class. Besides simple scheduling difficulties, there's the fact that it costs money—only $20 a month for membership, which really isn't that much, but around $300 for all the equipment you need, including a practice sword. So that held me back for a few months. I finally justified the cost by chalking it up as healthy exercise. On top of those issues, I've got a good load of social anxiety to overcome when I need to meet new people. After emailing the instructor and building myself up to it for a few weeks, I finally got in the car and went to the park where they meet. I walked up to the little grassy area where they were practicing, but I let my nervousness get the better of me and actually turned around and went home before talking to anyone. Yeah, I'm certifiable. What can I say?

After a few more weeks, I finally got mad at myself for being such a social wuss and forced myself to go.

And I loved it. Even my second class, when the instructor made me spar for the first time. I felt like a total idiot, not knowing what the heck I was doing, but there were moments in the intensity of the fight that I lost track of my self-consciousness, lost track of time, and truly entered the melee. It was euphoric. I was actually holding a sword in my hand and fighting. Okay, it wasn't a REAL sword. It was a plastic practice sword. But still. It was long and heavy, and I was hitting someone with it.

![Duelists](/assets/images/hemaDuelists.png){: width="300px" height="208px"}

But I'm getting ahead of myself. The first lesson was, of course, the parts of the sword, followed by some debunking of stupid stuff people learn from games and movies. I already knew most of those from hanging out in the right online forums for a few years. Then we went over footwork, practicing the basic steps, then the five guards (most online sources I've seen talk about four, so I'm not sure where the fifth one comes from exactly). They taught me how to combine the steps and guards by stepping across the field simultaneously switching between guards. Then came the various cuts aiming at different parts of the body. Finally they showed me the technique called "abnemen," which involved stepping, blocking, and cutting in a single motion. I pretty much sucked and had to do it really slow the whole time.

The second lesson was spent reviewing the guards, then focusing on a few different cutting and blocking techniques, ending with my first sparring session.

The day after the first lesson, I tried practicing the steps and guards at home, but honestly couldn't remember most of them. It doesn't help that all the names are in German, so even if I remembered the position, I couldn't necessarily remember what it was called, and vice versa.

For those who are interested, here is a great introduction to the primary guards used for German longsword. This guy has great technique (as far as I can tell, being a mere beginner).

<iframe src="https://www.youtube.com/embed/UBatfvEixoA?rel=0" width="480" height="360" frameborder="0"></iframe>

And below is the best set of introductory videos to the instruction of German longsword that I've been able to find. The folks at Drei Wunder in Washington have a different training approach than my instructor, but it gives you an idea for the kind of thing the class does. I'm not embedding the videos, because this post is already pretty long. The introduction is just them talking about their training techniques, and it's fairly long. You can probably skip it, but it does explain some of the terminology they use in further videos.

[Introduction](http://youtu.be/wkTiDjwkapw): Explanation of training techniques and terminology

[Longsword Drill 1](http://youtu.be/gh4J8VeEI7U): Sword anatomy, footwork, timing, and distance using the Zornhau cut

[Longsword Drill 2](http://youtu.be/0XoApP-H-xo): Defense using Ochs and Pflug

[Longsword Drill 3](http://youtu.be/6vv96X5XvWM): The Master Cuts

[Longsword Drill 4](http://youtu.be/ONjXqJFSt4g): Hangen defensive technique versus the Zornhau cut

[Longsword Drill 5](http://youtu.be/n71eoQ1DJPI): The Bind, or meeting of the swords

So there you go. I'll post more on this in the future, hopefully some pictures of my equipment when I get it and maybe even some shots of me in action.
