---
from_wordpress: true
comments_id: 705
title: "Review of Dream Theater's New Album: A Dramatic Turn of Events"
date: 2011-09-15T23:16:52-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=705
permalink: /post/review-of-dream-theaters-new-album-a-dramatic-turn-of-events/
categories:
  - music
---
![Dream Theater group photo](/assets/images/dreamTheaterGroupPhoto.jpg)

Although I've been a huge fan of Dream Theater ever since Images and Words came out back in 1992, I have been a little disappointed with their past couple of albums. It seemed to me that they were just treading water, or retreading the same ground they've covered multiple times before. There was very little that felt original or interesting to me, with one or two exceptions ([The Dark Eternal Night](https://www.youtube.com/watch?v=sO3D_-xQFog)). Their last album, Black Clouds and Silver Linings, was completely forgettable. I started to worry that my tastes had shifted to leave the new Dream Theater behind. It seemed pretty clear to me that Dream Theater had stopped innovating.

Then founding member and drummer Mike Portnoy shocked everyone and abruptly left the band. The remaining members hired the amazing [Mike Mangini](https://en.wikipedia.org/wiki/Mike_Mangini), and my hopes for something fresh from Dream Theater were renewed. Around the same time, it was announced that they were writing a new album, and I was filled with both hope and trepidation. The band had said that without Portnoy, they were undergoing "a musical change, re-evaluating and restructuring 'who we are and what we do'." (Quote from Wikipedia.)

Their first single from the new album was, pardon the pun, singularly underwhelming. [On The Backs of Angels](https://www.youtube.com/watch?v=oasnbzEMV08) was yet another slow-tempo rock song with nothing new to offer. One-minute samples of several other songs were released and didn't convince me anything else interesting was in the works.

The new album was released in full this past Tuesday, and thanks to the glory of Spotify and MOG (which has higher quality music) I was able to listen to the entire album on release day without having to travel to a store or wait for shipping.

I'm sure you're breathlessly awaiting my evaluation of the album. I suppose if you've made it this far into this post, I might as well indulge you. :)

I have been pleasantly surprised. Although there's nothing dramatically different here from what Dream Theater is known for, there is a subtle but persistent shift in the mood and atmosphere of this album from previous efforts. The overall tone is definitely a little more reflective and hopeful than their past four or so albums, while still containing a liberal dose of crunchy, heavy metal goodness and blazing instrumentation. If I had to compare it to another Dream Theater album, it would be Images and Words, which is still among my favorites even nearly 20 years later. Yet this album is definitely its own beast. Keyboardist Jordan Rudess has branched out and done some much more interesting things than in past albums, adding some electronica, latin, and symphonic elements to some of the songs. It seems that he spends a little more time at the piano rather than his synthesizer, as well, which is a refreshing change for me.

I know I have several friends who, like me, have felt a little let down by Dream Theater in recent years. You might want to give this new album a try. Hop onto Grooveshark.com or get yourself a free Spotify account and give it a few listens, all the way through if possible, and let the album speak to you on its own terms. Don't expect a dramatic departure from Dream Theater's signature sound, but give it a chance. It just might bring you back into the fold.

Or if you're still skeptical, listen to this new one, Bridges in the Sky. It has a rather odd intro, but it's one of the most interesting songs on the album, with the strange deep-voice dude, cool symphonic overlays, brutal riffs, and even your standard crazy Dream Theater blazing instrumental breakout session near the two thirds mark. Check it out.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yeg7Ycyp-hk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
