---
from_wordpress: true
comments_id: 247
title: Warbreaker
date: 2010-09-02T16:39:22-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=247
permalink: /post/warbreaker/
categories:
  - fiction
---
A few months back, I made a pact with a reader to give Warbreaker by Brandon Sanderson a second try if she would give Servant of a Dark God by John Brown a second try. I should have posted my review a long time ago, but I'm just a big old bloated procrastinator.

One of my problems with Warbreaker was trying to overcome my initial bias. I had already read the first six chapters or so, and it didn't grab me. I probably would have still finished it back then, but I was reading it in a Word document downloaded for free from Sanderson's Web site, and I really didn't enjoy reading on the computer. With a book that felt mediocre to me, it wasn't worth it. Having already given up once, on my second try I found myself continually being more negative and critical than I probably would have otherwise. I'm usually pretty forgiving once I've decided to give a book a try, but it was harder this time.

Warbreaker was still worth reading. Sanderson always finds a way to surprise and delight, and this book was no exception. He created a number of likable and sympathetic characters, notably Lightsong and Vasher. It's interesting to me that online fans seem to like those two characters more than the two primary protagonists, Siri and Vivenna. I think the problem with those two characters is that they were just a little too bland, where Lightsong and Vasher were much more dynamic and conflicted. In the end, they really stole the show, and considering the small amount of screen time Vasher gets, that's really saying something.

I do have a number of things I didn't like about the book. Sanderson made a big deal about how controlled and calm Vivenna was, but she never really acted that way. Of course we find out later why, but for a long time I couldn't get over the supposed contradiction. And then when it finally started to make sense, her turnaround felt flat to me. The banter of Denth and Tonk Fah, the mercenaries, also grated on my nerves—but unlike Vivenna's character arc, theirs really worked for me late in the book. This was one of the good surprises.

I was not fond of the magic system. Although it was enjoyable to watch it develop and see the characters use it in interesting ways—as it always is with Sanderson—something about it rang hollow for me. Magical power coming from color? In order to animate objects? It just doesn't make sense. It feels too contrived. I suppose it's not that much different from the magic system in Mistborn—metabolizing metals to gain strength, attract or repel metals, or control emotion. But it still seems different to me. Sorry if I can't articulate why. Maybe it has to do with the name Sanderson gave the system: BioChroma. The capitalization is way too anachronistic for me.

In the end, the best part of the story was Lightsong's character arc, and that's what finally made it worthwhile for me. So despite some problems and reservations, I'm still glad I read it. There's only one more book of Sanderson's I haven't read (Elantris), so maybe it will be the same. I'll have to pick it up sometime.

Oh, wait. There's also The Way of Kings, which just came out. It'll probably take me months to read that monster, if I even get around to it at all.