---
from_wordpress: true
comments_id: 883
title: The Epic Fantasy Avengers Lineup
date: 2012-05-11T20:00:22-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=883
permalink: /post/the-fantasy-avengers-lineup/
categories:
  - fiction
  - 'movies &amp; TV'
---
This idea is just too hard to resist. I literally can't help myself.

Tor.com has come up with a list of fantasy characters who they think would comprise a fictional, [epic fantasy Avengers team](http://www.tor.com/blogs/2012/05/whos-in-the-epic-fantasy-avengers). Naturally, I think they got it horribly wrong. And most of the commenters got it even more horribly wrong.

The spirit of the exercise is to assemble a team of individuals who represent the actual members of the Avengers from the movie. Tor.com, and the many commenters, picked too many characters who don't actually have anything in common with the character they are supposed to correlate with. Just because you like Martin's A Song of Ice and Fire, or the Malazan Book of the Fallen, or Rothfuss's Name of the Wind, or whatever, doesn't mean one of those characters belongs on the team. Plus, it ruins the exercise to list too many or too few members. That's just dumb.

Of course, you can only pick characters from books you've read, so my list will have its own idiosyncrasies. Unlike Tor.com's misnamed team, I'm actually going to limit my choices to epic fantasy—no modern fantasy, no games or movies, and no mythology.

So, here is my own personal list:

#  The Epic Fantasy Avengers

**Captain America: Aragorn** (_The Lord of the Rings_ by JRR Tolkien)

Aragorn is the perfect replacement for Captain America, the leader and patriotic super-soldier. He takes up the role with some reluctance, but he stands for what's good and right in the world. Aragorn lacks the shield, but he has a sweet magic sword and he protects those in need. Tor.com's choice was King Arthur, but as a historical figure, however epic he may be, he's out of the running for me.

![Aragorn](/assets/images/epicFantasyAvengers_aragorn.jpg)
![Captain America](/assets/images/epicFantasyAvengers_captainAmerica.jpeg)

**Iron Man: Dalinar Kholin** (_The Way of Kings_ by Brandon Sanderson)

For Iron Man, the armored, flying "genius billionaire playboy philanthropist." The problem with picking a replacement for Iron Man is the armor. Dalinar's not a perfect fit, but he's about as close as you're going to get. He's got a full suit of magically powered plate armor that's virtually impenetrable and lets him make gigantic leaps. That's kinda like flying, right? He's also rich (uncle to the king) and philanthropic (he gives away his freaking Shardblade to a commoner!). There really was no other choice, here.

![Dalinar Kholin](/assets/images/epicFantasyAvengers_dalinarKholin.jpeg)
![Iron Man](/assets/images/epicFantasyAvengers_ironMan.jpg)

Dalinar art by [Caios Santos](https://caiosantosart.tumblr.com/post/185109606906/dalinar-kholin-the-blackthorn-from-brandon).

**The Hulk: Logen Ninefingers** (_The First Law Trilogy_ by Joe Abercrombie)

He fills the Hulk's spot. Logen's not a scientist or even particularly smart, but he's a somewhat normal, likeable guy who occasionally turns into an unstoppable killing machine. Perhaps the most obvious choice would have been Dr. Jekyll/Mr. Hyde, but unfortunately he's not an epic fantasy character. Someone suggested Fezzik from The Princess Bride. Wha . . . ? He's big, sure, but he's like a big fuzzy teddy bear. He's about as far removed from a psychotic rampager as you can get. Sadly, it's tough to find a good picture of Logen Ninefingers. He deserves a lot more attention.

![Logen Ninefingers](/assets/images/epicFantasyAvengers_logenNinefingers.jpg)
![The Hulk](/assets/images/epicFantasyAvengers_hulk.jpg)

The picture I chose for Logen Ninefingers is actually called _Chivalry: Medieval Warfare_ by [KypcaHT](https://www.deviantart.com/kypcaht/art/Chivalry-medievil-warfare-335997322).

**Thor: Perrin Aybara** (_The Wheel of Time_ by Robert Jordan)

Thor, the demi-god with a magic hammer, was a tough one to replace. On the one hand, Thor's role is essentially just a generic damage dealer, but on the other hand, he has some fairly specific traits, like the hammer. In the end, Perrin won out for me largely because of the magic hammer (as of Towers of Midnight, which I actually have yet to read) but also because of the strength and the beard. Perrin's no demi-god, but he does have the whole ta'veren thing on his side. Another choice I considered was Conan the Barbarian. Alas, he lacks the magic. Conan is like anti-magic. I even temporarily considered Aragorn for this role, but I needed him more as a leader.

![Perrin Aybara](/assets/images/13-The-Towers-of-Midnight-eBook-Raymond-Swanland.jpg){: height="230px"}
![Thor](/assets/images/epicFantasyAvengers_thor.png)

Perrin art by [Raymond Swanland](https://raymondswanland.com/) (it comes from the e-book cover for _The Towers of Midnight_).

**Black Widow: Vin** (_Mistborn_ by Brandon Sanderson)

Vin replaces Black Widow, the superspy assassin. When burning pewter, Vin is super strong and fast. Burning steel and pushing on a bunch of coins basically functions as a firearm. As with the others, Vin's not a perfect fit, but epic fantasy has an unfortunate lack of femme fatale characters. But personally, I prefer a female character who can take out entire fortresses full of baddies without having to play the sex object.

![Vin](https://static.wixstatic.com/media/c3f6a5_abb676553635495ca7cde794db518f67~mv2.jpg/v1/fill/w_1000,h_666,al_c,q_85,usm_0.66_1.00_0.01/c3f6a5_abb676553635495ca7cde794db518f67~mv2.webp){: height="230px"}
![Black Widow](/assets/images/epicFantasyAvengers_blackWidow.jpg)

Vin art by [Steve Argyle](https://www.steveargyle.com/product-page/vin-in-the-mists-20x30-print?art_id=358&cmd=gallery_ext). Other great Vin art: [Elizabeth Peiro](https://www.reddit.com/r/Mistborn/comments/e6honb/vin_fan_art/), [Evan Monteiro](https://www.writeups.org/wp-content/uploads/Vin-Mistborn-trilogy-Brandon-Sanderson-c.jpg), [x-tive](https://www.zedge.net/wallpaper/52d7879d-4172-46cc-bc7d-f54eb78ab41f).

**Hawkeye: Legolas** (_The Lord of the Rings_ by JRR Tolkien)

He fills the role of Hawkeye, the specialist archer, and beats out Robin Hood because, well, Robin Hood isn't magic. The only other super-archer I can think of would be Birgitte Silverbow from The Wheel of Time, but the obvious problem with her is gender. Sorry, Birgitte. There just aren't that many awesome archers out there.

![Legolas](/assets/images/epicFantasyAvengers_legolas.jpg)
![Hawkeye](/assets/images/epicFantasyAvengers_hawkeye.jpg)

**Nick Fury: Gandalf** (_The Lord of the Rings_ by JRR Tolkien)

Originally I was going to try not to pick two characters from the same book, but really, who better than Gandalf to play Nick Fury, the mentor, manipulator, and motivator behind the scenes? Besides, if any book deserves to get more than one character, it would be The Lord of the Rings. Nick Fury is actually a hard one to cast, since he has two incarnations in the comic books—the original white Fury and the more recent [black Fury that was actually modeled after Samuel L. Jackson](http://upload.wikimedia.org/wikipedia/en/e/e8/UltimateNickFury.jpg), who also plays him in the movie. Plus, Fury is more than just a mentor/leader—he's a pretty hard-core gun-toting soldier. Picking Gandalf is kind of like splitting things 50/50. He's basically a white Fury emphasizing the mentor/leader aspects of his character from the movie. This role has a few other serious contenders, prime among them Moiraine (wrong gender) and Allanon (not as likeable as Gandalf). If nothing else, Gandalf wins because of the whole smoking thing.

![Gandalf](/assets/images/epicFantasyAvengers_gandalf.jpg)
![Nick Fury](/assets/images/epicFantasyAvengers_nickFury.jpg)

So there you go. Who would you pick? Did I miss anyone obvious?
