---
from_wordpress: true
comments_id: 774
title: "LTUE Notes: The Principles of Suspense"
date: 2012-02-15T20:20:24-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=774
permalink: /post/ltue-notes-the-principles-of-suspense/
categories:
  - writing and publishing
tags:
  - LTUE
  - suspense
---
Today's LTUE notes come from the panel "The Principles of Suspense." The panelists were James Dashner, Berin Stephens, Kathleen Dalton-Woodbury, Clint Johnson, and J. Scott Savage. This panel was kind of all over the place, and my notes are correspondingly chaotic. Apologies beforehand.

Suspense is when a someone's walking down the street, and you show a banana peel on the sidewalk in front of them. Surprise is when you don't show the banana peel. With suspense, the reader knows something the character does not, something the reader is waiting for.

The characters need to be likeable for the suspense to work. Otherwise the reader doesn't care, and it's just not suspenseful anymore.

The goal of the book's first sentence is to get you to read the second sentence. The goal of the first page is to get you to read the second page. Suspense is one of the primary ways of accomplishing these goals.

Suspense is: if you screw up you're going to die. Well . . . suspense is not action. Suspense is about consequences. _The Help_ was full of suspense, but it wasn't life-or-death.

One way to increase suspense is to change the nature of the suspense that's happening. Example: Imagine a teenage girl at school. A popular boy approaches her and says "Can I have your number?" She gives it to him and goes home. What is she feeling? What is the reader feeling? Suspense! Will he call, will he call, will he call??? He doesn't. The next day she goes to school and one of her friends says, "Did you hear about that new government program where you can enter someone's phone number and it tells you all about them?" You can learn their address, family, schools they went to, etc. What's the girl and the reader feeling now? Suspense of a different kind. So the next day at school, the boy approaches her again. More suspense. What is he going to say? He says: "Hey, aren't you so-and-so's sister? She went here a few years ago, right?" The suspense changes again.

Three principles for causing suspense:

  * **Isolation** (Harry Potter—orphan, newbie at school, popular yet isolated)
  * **Disorientation** (Harry Potter going to Hogwarts for the first time, not knowing what's going on)
  * **Misdirection** (Example: You're in the jungle and you know there's a tiger. You see the bushes move and a flash of color—it's the tiger! You get your gun ready, waiting for it to jump out—and a bunny comes hopping out. Whew! Then you hear a roar right behind you.)

More tools for causing suspense:

  * A choice between two good things, neither one is 100% good. What will you do?
  * Incidental, small clues dropped into the narrative showing that something bad is happening or about to happen—especially if the characters don't know what's going on.
  * The "time bomb" technique. Basically, there's a ticking clock, a deadline, before something awful is going to happen.

Some types of suspense are very hard to do in first person, so be aware of the drawbacks.

If your character doesn't know something, or misses a clue, or does something the reader knows is dumb, you have to justify it within the story. Make sure you show the character being confused, distracted, angry, or whatever.

Two things will kill suspense: say too much or say too little. The most common mistake is the second. Novice writers try to shroud what's happening in too much mystery and end up making the reader confused. Big mistake.
