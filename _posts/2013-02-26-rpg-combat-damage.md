---
from_wordpress: true
comments_id: 1097
title: "RPG Combat: Damage"
date: 2013-02-26T22:24:13-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1097
permalink: /post/rpg-combat-damage/
categories:
  - gaming
---
I have some pretty strong ideas on how damage ought to be handled in a roleplaying game. But rather than complaining about how boring and unrealistic hit points are, I'll simply present my current thinking on how I'm going to implement damage in my own game.

I am an unabashed violence monger (well, I might be slightly abashed depending on who I'm talking to), and I love the thrill of a good fight scene. I love knowing all the details of a strike in combat. When it's my own character getting hit, I want to know exactly what happened to him so that I can imagine how he's going to deal with it and where the scar will be. When it's my character dealing out the hurt, I want to know exactly what he did so that I can picture how awesome he is. I want to be able to picture it all in my mind as if it were a movie.

But I also know that every increase in detail comes with a corresponding increase in how long it takes to play out the combat, and that often detracts from the excitement. In order to keep combat fast-paced and exciting, I've decided that the result of an attack roll should always translate directly into damage. There should be no extra rolling. The degree by which you succeed on the attack roll determines exactly how much damage you will do.

In my opinion, every strike that actually hits ought to have a concrete and dramatic impact on the fight. No successful blow should come without consequences. To keep things from getting bogged down, I envision four basic levels of damage that matter in the thick of a fight:

![Arwen with scratch on cheek](/assets/images/arwen-scratch-on-cheek.jpg){: .image-wrap-right width="500"}
  1. **Glancing Blows**. These are scratches and minor bruises, the type of stuff you don't even feel in the rush of combat. They're like the scratch that appears on Arwen's cheek in _The Fellowship of the Ring_ movie when she's fleeing the Ringwraiths. It's just enough to let you know that what you're doing is dangerous. The fact that you took a scratch instead of getting killed serves to remind you of your badassness and your potentially imminent death at the same time.
  2. **Minor Wounds**. These are strikes that cause enough pain and damage to impair you, but won't necessarily end the fight. They'll probably cause a fairly high but momentary penalty due to the shock of getting your flesh torn up, then fade into a minor penalty after that. We don't need to know the specifics; whether it's a torn ligament, a damaged muscle, or a severed vein doesn't really matter because those details don't necessarily have a direct impact on the fight. All you know is that you got hit, and it hurts. Your doctor will figure out exactly what's damaged and how to fix you up later. For now, you've got to fight through the pain and take out your opponent soon or else your pain penalties will eventually result in your demise. Minor wounds will comprise a fairly large range of possible results but will all have fairly similar and fairly low penalties.
![I am starting to question my choice of attire](/assets/images/iAmStartingToQuestionMyChoiceOfAttire.jpg){: .image-wrap-right width="500"}
  3. **Critical Wounds**. This kind of wound doesn't kill right away all on its own, but it's essentially a fight finisher. A severed hand, a stab to the gut, or a shattered femur will put you into instant shock so that even if you keep hold of your weapon, you're not likely to be able to do much with it. If your opponent is merciful and pauses to let you recover from the initial shock, you've probably got just one difficult shot left to strike back. Otherwise, you've lost, although you might live to fight another day if you have a good healer nearby.
  4. **Death Blows**. Some blows spell instant or near-instant death—decapitation, crushed skull, pierced lung. These are the kind of possibility that should keep you afraid of every combat you decide to enter. Unless you vastly outmatch your opponent, you'd better tread carefully. What might have been a glancing blow last time could just as easily have been a death blow if you hadn't chosen your defense with care.

When appropriate, the exact results of the blow will be looked up on a wound chart. Glancing blows are incidental and don't need to be looked up at all. The GM or the player can decide where the little scar will be, if they care. Minor wounds only need to be looked up after the fight is over, when the healer is examining the wound. The specifics only matter for the purposes of recovery. Critical wounds and death blows are both fight finishers and should be looked up right away unless the receiving character is unimportant cannon fodder. Since death blows mean that you're dealing with a death scene, it ought to be imbued with an appropriate level of detail and drama. We need to know exactly what happened.

Wound charts should be detailed and specific to the type of attack being made (slashing, impaling, or crushing). Wounds are also dependent on the location being struck. In my system you will always choose the body part you're aiming for, and the charts will need to reflect that. Such charts can be found in the excellent book _[Trauma](http://www.radicalapproach.co.uk/trauma/)_ by Claus Bernich or in the appendices to _The Riddle of Steel_ (identical charts have been reused in the TROS derivative _[Blade of the Iron Throne](http://www.ironthronepublishing.com/)_).

In my system, each wound is unique and gets recorded on your character sheet separately. Wounds will have three primary characteristics: (1) a pain rating that gives a penalty to all actions involving the wounded location; (2) a blood loss rating that can result in death after a certain amount of time if left untreated; (3) unique effects that depend on the individual location (blindness, internal bleeding, concussion, loss of function). I have been thinking a lot about the role of healers in the game, as well (if I even have them). Detailed wounds could enable healing magic that's more involved than simply healing hit points and could make things a lot more fun and interesting for people who want to play healers.

Since attack location is important, it also becomes important to keep track of what kind of armor you are wearing. A steel breastplate won't protect you against attacks to the leg. This kind of detail can add a little extra work, but most of it occurs beforehand and is recorded on the character sheet before the fight begins. But keeping track of armor also adds an extra dimension of strategy to a fight, since you can target your enemy's weak spots, and you have to be extra careful to protect your own.

What's your opinion?
