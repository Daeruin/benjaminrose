---
from_wordpress: true
comments_id: 957
title: Cover for Witch Born by Amber Argyle
date: 2012-07-02T12:58:02-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=957
permalink: /post/cover-for-witch-born-by-amber-argyle/
categories:
  - art
  - fiction
---
A while back, I blogged about the book _Witch Song_ by [Amber Argyle](http://amberargyle.blogspot.com/ "Amber Argyle"), an author I met at the local convention [LTUE](http://www.ltue.org/ "Life, the Universe, and Everything") here in Utah. I was very impressed by the cover for _Witch Song_. If you missed that post, [go check it out and read about _Witch Song_](/post/witch-song-by-amber-argyle/ "Witch Song by Amber Argyle").

Today, Amber revealed the cover art for _Witch Born_, the sequel to _Witch Song_. As much as I loved the cover for the first book, this one is better. It's incredible. I probably won't be writing books that need this style of cover, but I hope that one day I get an artist who can portray something that depicts the essence of my own story with this much beauty.

Here's the cover:

![Cover Art for Witch Born by Amber Argyle](https://3.bp.blogspot.com/-PwEY40QLZTQ/T_HoDcq_MpI/AAAAAAAAAh4/yxi_uPaQ9fM/s1600/Witch_Song_Frontcover.jpg "Cover Art for Witch Born by Amber Argyle"){: width="497"}
