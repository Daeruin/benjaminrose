---
from_wordpress: true
comments_id: 31
title: Cooperative character creation
date: 2007-08-11T12:22:58-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=31
permalink: /post/cooperative-character-creation/
categories:
  - gaming
---
Role playing games I've played tend to have a particular problem that I've been pondering. Role playing games are games about cooperative story telling; in most games, each person controls a character in the story. The problem is how to ensure that each character fits into the story you want to tell. If you don't place boundaries on character creation, the players can end up with a set of characters that just don't fit into the same story.

Some game designers deal with the problem from the outset by making the setting very specific and tailoring character creation so you can't create a character who doesn't fit in. In other games, it's left up to the game master to lay out the setting and guide the players into making appropriate characters. Both of these methods can work, but only if the players buy into the setting and the kind of story being told.

So, in my pondering, I started to wonder if there was another way around this problem. What I came up with is the idea of cooperative character creation.

With cooperative character creation, all the players would sit around the table and create their characters at the same time. In the process, each player will have a chance to come up with some way of tying their character to another player's character. These ties would probably need to be predefined to some extent, and it would require both players to agree. There would be have to be multiple opportunities to establish ties so that by the time they're done, the characters form a group that is tightly woven together. This would give every character a reason to be with the others and would provide multiple story possibilities that the players should enjoy, because the players are the ones who invented them.

Most character ties would be some kind of relationship: family member, friend, employer, slave, debtor, liege, partner, maybe even enemy. Creating ties would provide some other benefits, too, perhaps bonuses to attributes or other characteristics. Some ties would provide more benefits than others.

Of course, this presupposes a setting has been provided, which puts that burden back on either the game designer or the game master. But it does solve the related problem of character creation.

So, those of you who are into role playing games, would you be interested in trying this idea out? Are there some problems I haven't thought of?