---
from_wordpress: true
comments_id: 590
title: "Speed Archery: Fast and Deadly"
date: 2011-05-26T21:08:27-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=590
permalink: /post/speed-archery-fast-and-deadly/
categories:
  - gaming
  - medieval combat
---
This is one of the cooler things I've seen. If you like archery at all, you'll like this, but if you've ever played an archer in a role playing game and been frustrated at how long you have to wait in between shots, you'll _love_ this. This girl is shooting arrows at an average of one arrow every 1.7 seconds. (Yes, I sat there and timed it.)

<iframe src="https://www.youtube.com/embed/1o9RGnujlkI" width="480" height="390" frameborder="0"></iframe>

At first I wondered how accurate she could possibly be when shooting that fast. In the first few sets, when you can't see the target yet, it looks like some of the arrows must be going pretty far off the mark. But then in the last set, when the camera is looking over her shoulder, you can see how accurate she really is. There's a little target hanging down in the center of what looks like maybe a bed sheet. It's pretty much a bullseye and makes a loud, echoing _thwack_ when the arrows hit it.

She hits it three times out of twelve in the last set. And that's when she's walking and shooting at the same time! Sure, some of the arrows hit around the edges of the sheet, too, but still—I wouldn't want to be the poor sap on the other end of her arrows.

Also notice the cool reverse grip she uses.

Here's another interesting archery technique that lets you shoot even faster. These guys grab two arrows at once, nock and shoot one while still holding the other in the same hand, then quickly nock and shoot the second one.

<iframe src="https://www.youtube.com/embed/k_VUW6_SpmQ" width="480" height="390" frameborder="0"></iframe>

This kind of stuff makes me want to pick up archery.

**Update**: You might want to check out my other archery posts:

  * [Part One](/post/speed-archery-fast-and-deadly/ "Speed Archery: Fast and Deadly") — You're reading it now.
  * [Part Two](/post/archery-speed-shooting-techniques-part-2/ "Archery: Speed Shooting Techniques, Part 2") — Two videos of Lars Andersen demonstrating his incredibly fast shooting and one of Lajos Kassai showing ancient Hun archery
  * [Part Three](/post/fast-archery-techniques-part-3-the-crossbow/ "Fast Archery Techniques, Part 3: The Crossbow") — Methods for quickly spanning or cocking a crossbow
  * [Part Four](/post/speed-archery-techniques-part-4/ "Speed Archery Techniques, Part 4") — More videos showing some speculation on Native American archery, recreations of ancient Turkish techniques, and more fast shooting from Russia.
