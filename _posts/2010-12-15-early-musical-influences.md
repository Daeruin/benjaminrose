---
from_wordpress: true
comments_id: 305
title: Early Musical Influences
date: 2010-12-15T22:30:37-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=305
permalink: /post/early-musical-influences/
categories:
  - 'life &amp; stuff'
  - music
---
Based on the music I listen to and talk about these days, you probably wouldn't guess what my early musical influences were. Of course, most children tend to like only what they get exposure to. They like what their parents and friends like. The younger you go, the more pronounced that influence is. I suppose it's the same with me, but many of those early influences persist, and I still love some of that music. Not all, but some. So here are some of my early influences, if you're willing to indulge me a bit. I couldn't get the spacing how I wanted it, but my commentary comes below each song.

**Weird Al Yankovic**

This is one of the earliest songs I remember liking. Maybe this is the root of my sometimes violent and/or sick sense of humor. In fifth grade my friend and I did a lip sync of this song for my class, pointing to the girl I liked every time it says "you." Good times. Don't quit now, there's better to come.

<iframe width="560" height="315" src="https://www.youtube.com/embed/yWhpk-8QLFQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**John Denver**

This is one of my very earliest influences, thanks to my parents. I have never liked country music (in fact I practically loathe it), but there are several John Denver songs that are simply immortal, that any true music lover will appreciate in one way or another. This is one. It brings back memories of long road trips and family reunions in the Abajo Mountains of southeastern Utah.

<iframe width="560" height="315" src="https://www.youtube.com/embed/R15vi55cJmk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**The King's Singers**

<iframe width="560" height="315" src="https://www.youtube.com/embed/Y0PLClBlE1Y" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Another influence from my parents, this a cappella group probably had a greater influence on me than any other music group next to Rush. This was my first exposure to the Beatles—I don't think I heard an original Beatles song until I was in high school. I don't know how that happened, but anyway I got to know the Beatles through the King's Singers. I couldn't limit these guys to just one song, so here's another one.

<iframe width="560" height="315" src="https://www.youtube.com/embed/J3LduXNeicY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

I come from a very musical family of six siblings, but I was the first of only two who took choir in high school, and I continued on in college as long as I could. I joined a relaxed a cappella/barbershop group for several years and have continued to sing in church choirs throughout my life. This influence is most strongly seen in my resistance to growling vocals and screams despite loving most other aspects of the heavy metal genre. I love nothing better than a clear, strong, trained vocal performance.

**Edvard Grieg, In the Hall of the Mountain King**

When I was little, my dad used to record mixes of classical music and give us weekly quizzes on them. We had to name the song and the composer based on a 30-second clip of each song. For each answer we got right, we got a dollar. That was our allowance. This song is the one I remember the most from those days. It's totally epic in every way despite being a mere two-and-a-half minutes long (the entire Peer Gynt suite that it's part of is epically long, though). This song fed my appetite for fantasy and drama in my formative years. Plus it prominently features two instruments that I later ended up playing in high school and college: the bassoon (check them out at 0:16) and the cello (right after the bassoon).

<iframe width="560" height="315" src="https://www.youtube.com/embed/kLp_Hh6DKWc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Enya**

That's right, Enya. I make no apologies. Enya is the epitome of fantasy. The music transports you into another place and time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/rGwUpsyDJTk" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

**Rush**

Rush was my soft intro into rock, progressive rock, progressive metal, and eventually straight heavy metal. If you really don't know about the incredible phenomenon of Rush, you should check out the documentary about them that recently came out, Beyond the Lighted Stage. The biggest influence they had on me was probably their creative, thoughtful, and sometimes fantastical lyrics.

I've chosen a few of their lesser-known songs that had a big emotional impact on me as a teenager. No greatest hits here. This is all about me, not them! These all come from the glory days of the '80s. You'll be able to tell. Don't mock it. Just ride the wave of nostalgia.

I listened to this song over and over as a teenager. Angst, anyone? This song was very therapeutic to me.

<iframe width="560" height="315" src="https://www.youtube.com/embed/1RADjjMzxEo" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

This one is a science-fiction flavored song about a car. I've never been into cars, but this is one of the greatest driving songs ever (possibly eclipsed by Ozzy's Crazy Train, depending on your mood), and the lyrics transported me into another place and time.

<iframe width="560" height="315" src="https://www.youtube.com/embed/PjjNvjURS-s" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

Okay, this is the last one, I promise. How could I do appropriate tribute to a band as diverse as Rush without including at least three songs? A band that's given us four decades of mind-blowing talent, emotional highs and lows, dark fantasy and insightful reality? I could go on and on and on . . . and on . . . and on about them, but I won't. Rush is a love em' or hate 'em kind of band. If you don't love them by instinct, nothing I can say will convince you.

<iframe width="560" height="315" src="https://www.youtube.com/embed/mNn3zI2bCHw" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

That about brings you up to 1993 in my musical life, when I discovered Dream Theater, and music was never the same for me again. Thanks for indulging me.
