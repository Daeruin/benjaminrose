---
from_wordpress: true
comments_id: 12
title: 'On "On Dragons"'
date: 2007-02-17T11:27:00-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=12
permalink: /post/on-on-dragons/
categories:
  - writing and publishing
---
I wanted to say a few things about my last post, "On Dragons."

First, a few questions.

1. Did you get the sense that the historian had some special status? Or did he maybe just seem pompous or nuts?
2. Did the style of the letter turn you off? Was it annoying to slog through the salutation and genealogy?
3. Did the abrupt beginning and ending work? If I were to expand this into a short story, I'd obviously want to add more detail. But as a mere snippet—a vignette, I guess you'd call it—did the abruptness work? Would it perhaps have added to the drama or suspense if I had described in a little more detail the process of lighting the tent on fire, so the reader comes to a slower realization of what Muomno is doing? What if I had actually shown Muomno entering the tent and killing the historian?

Comments requested and welcome.

Also, I just made a few minor edits. In the sentence talking about the historian's calligraphy, I removed the phrase "of the type employed" because it seemed excessively wordy. In the part of the letter giving Kumet's ancestry, I changed "then" to "and" since the three names aren't a list of three generations; they're a list of the man's father and two grandfathers, then his mother and two grandmothers. Did you notice anything else that could be improved?
