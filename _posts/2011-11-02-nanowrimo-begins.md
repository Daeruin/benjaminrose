---
from_wordpress: true
comments_id: 742
title: NaNoWriMo Begins
date: 2011-11-02T10:02:43-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=742
permalink: /post/nanowrimo-begins/
categories:
  - writing and publishing
---
As I [mentioned](/post/the-nano-post/) in my post a few days ago, I have been planning to participate in [NaNoWriMo](http://www.nanowrimo.org/en/about/whatisnano) this year by trying to write an entire 50,000 word novel in one month starting November 1st, or at least 50,000 words of the beginning of a novel. That requires 1,667 words per day, on average. My original plan was to complete an entire outline using the principles of story structure as explained by the estimable Larry Brooks of [Storyfix.com](http://storyfix.com). I had been following a [series of posts](http://storyfix.com/nail-your-nanowrmo) he was doing on his blog specifically to help NaNo participants get their novels planned out and ready for November.

Unfortunately, a big work deadline had me working overtime the past two weeks, and I only finished about one third of my outline. And with a brother's wedding and Thanksgiving coming up this month, I just wasn't sure I could handle it. Without an outline, I felt powerless and directionless. I suck at improvisation and always have. I need a plan to work inside of.

So November 1st arrived and I wasn't ready. I finally finished my big work project that afternoon, then ate dinner and put the kids to bed. Nine o'clock PM arrived. It was do-or-die time. I had to decide. To NaNo or not to NaNo?

I decided to NaNo. Despite how unprepared I felt. I would just have to write and continue to fill out my outline as I went.

Even after deciding to take the plunge, it took a bit of time to get started. I opened my outline and fiddled with it a bit before realizing that I was running out of time. If I was going to meet the necessary word count for the day, I needed to get going. I opened a blank document. I thought about how to start the scene I was going to write. I couldn't think of anything that I liked. Then I checked Facebook. I thought about the scene a bit more. Then I opened up Google and started researching a question that came up while I was thinking. When I realized what I was doing, I closed Firefox and got back to my still-blank document. Nothing came to me. Then I got up and wandered into the family room where my wife was watching TV. Luckily, my guilt soon overcame me and I returned to the computer.

Finally after an hour of this hemming and hawing, I harkened back to the advice of Larry Brooks. I remembered the beat sheet, the outline of scenes, each with a mission to forward the plot, each with a context to guide its shape and purpose. I realized I needed to return to the basics. What's the mission of this first scene? I glanced at my incomplete outline again, then wrote down a few goals for the scene. Plot goals, needed characterization, a few possible settings. No longer facing a blank page, I tentatively typed out a first sentence using one of the possible settings. Halfway into the second sentence, I realized it wasn't going to work if I was going to achieve the plot goals. I erased it and started again.

This time it worked a little better. Pretty soon I had a good head of steam built up. I could see where this scene was headed, how it was going to accomplish the goals I had written down. Whenever I came to a point where I wasn't sure what to write next, I glanced up at my scene goals. Almost every time, something immediately popped into my head. What could this character say next? Well, the scene goals say she has to demonstrate her loneliness. Oh, I know . . .

And so it went until I was chugging along like a freight train that couldn't stop. Midnight came and went. I checked my word count. 1,500 words. I kept going. 1,667 words. My daily word count had been met, but I wasn't finished yet. This scene wasn't done. I kept typing. 2,235 words. One o'clock AM arrived. Finally I typed the last sentence. All my scene goals had been accomplished.

What a rush! I actually did it. For the first time in over three years since I first decided I was going to write a novel, I actually finished an entire chapter. It sounds pathetic but awesome at the same time. No more procrastination for me. I'm in it to win, this time. I might crash and burn, I might embarrass myself, but I'm not thinking that far ahead. I'm taking it day by day. Fill in the outline. Churn out the words. Repeat. Repeat. Repeat.
