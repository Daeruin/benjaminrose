---
from_wordpress: true
comments_id: 413
title: The Joy of Fantastic Maps
date: 2011-04-12T16:30:42-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=413
permalink: /post/the-joy-of-fantastic-maps/
categories:
  - art
  - fiction
  - writing and publishing
---
Fans of epic fantasy love their maps. Fans of role playing games love their maps. Lots of other people love maps, too. I've been fascinated with them ever since I was a kid. My favorite country was Italy, because it was shaped like a boot. I think that is part of what shaped my love for both fantasy and maps—the idea that something real could be so fantastic at the same time.

Lots of fantasy fans expect—demand, even—that if they're going to buy a fantasy book, it had better have a map. It just comes with the territory. I've spent a long time considering how the map for my own fantasy book should look. Some of the most respected works of fantasy literature feature extreme settings and climates. Earthsea, set in a world that is mostly water strewn with a plethora of islands. Dune, set on a desert planet. One of my more recent favorites is Mistborn, set in a place where ash falls from the sky on a daily basis and mist dominates the night.

So I've been looking around the web, trying to find some great maps to serve as inspiration, and I wanted to share some of my findings.

First I give you [Donjon's fractal world generator](http://donjon.bin.sh/world/). I spent hours with this little piece of software! It uses fractal algorithms to generate a randomized map of a planet. You get to specify what percentage of the world is water and polar ice cap. You can then display the new world in different views by specifying the style of the art, the rotation of the planet, and the type of map projection—square, mercator, isohedral, animated globe, and so forth. Most of my favorites have been Earthsea-type planets with lots of ocean and scattered islands.

[![Random World Map](/assets/images/donjonFractalWorldExample3.gif){: width="498px" height="248px"}](/assets/images/donjonFractalWorldExample3.gif)

In my wanderings, I ran across the world of [Khoras](http://www.khoras.net/), a wonderfully detailed fantasy world built for role playing games. The creator of Khoras has made some beautiful maps using Photoshop, and he's described the process in detail for those who want to try some of his techniques. Unfortunately I can't link to the map or the description directly due to the way the site is set up, but you can reach them easily from the home page by clicking on the gigantic MAP button in the sidebar.

{%
  include figure.html
  image-url="https://www.khoras.net/Khoras/Maps/Version%204%20-%202018/Full%20Size%20Maps/2018%20KHORAS%20MAP%20-%20Final%20Flattened%20Map%20-%202500.jpg"
  link="http://www.khoras.net/"
  width="498"
  height="249"
  caption="Khoras"
%}


How do you feel about maps? Do you have any shining examples you'd like to share?
