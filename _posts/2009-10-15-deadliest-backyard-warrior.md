---
from_wordpress: true
comments_id: 132
title: Deadliest Backyard Warrior
date: 2009-10-15T20:53:29-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=132
permalink: /post/deadliest-backyard-warrior/
categories:
  - medieval combat
  - 'movies &amp; TV'
---
For those of you who followed [Ing's Deadliest Warrior posts](http://www.dallenrose.com/?p=238) and some of the discussions we had about it, and especially for Ing, here's a hilarious spoof.

"Inhuman!"

<iframe src="https://www.youtube.com/embed/iTxFM9khgfA?rel=0" width="480" height="360" frameborder="0"></iframe>
