---
from_wordpress: true
comments_id: 76
title: Fantasy languages
date: 2008-05-06T23:12:08-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=76
permalink: /post/fantasy-languages/
categories:
  - writing and publishing
---
Tolkien is famous for his invention of several imaginary languages. The amount of detail and genuine linguistic principle he put into these languages is practically insane. One of the primary reasons he created Middle-Earth and chronicled its history in The Silmarillion and other works was to provide somewhere for his invented languages to be spoken. Tolkien often presented his writings as translations rather than inventions, and the linguistic element was essential to that fiction.

That represents one extreme in how you might go about creating a realistic world for your own fantasy story. Most current authors take a rather different approach to language in fantasy. Just use regular English. At most, you might want to use an occasional invented word for some animal or drink that doesn't exist in our world. Oh, and you want your character names to sound somewhat consistent. You don't want to have two brothers named Flavius and Henry without explaining the cultural disparity that's obvious in those two names.

As a linguist myself, I lean towards Tolkien's end of the spectrum. I've even invented a couple of languages, or at least I've gone as far as coming up with their phonetics (the sounds they have available) and a general idea of their phonology (how you combine the sounds to create syllables). In my head, I know more or less how they ought to sound. My current work in progress features a vaguely Viking-like culture whose language, naturally enough, resembles our own Germanic languages, mostly Old English. I've also got a somewhat Asian culture whose language I've tried to model after ancient Egyptian.

So far, this manifests itself only in the names of places and characters, but there may be call to invent a word or two for other things. As crazy as it probably is to go creating my own languages, I'm not going to go Tolkien and use them to write poetry or anything. But I really get a lot of enjoyment out of creating names that really sound authentic and have some linguistic rationale behind them. So you have Weathulf, Ecstan, Hute, and Katla all from one area, and Suhtan, Nomet, Asmet, and Suke from another. I think that adds a really cool dimension to my story.

How deeply do you think about your character names? I worry that I may be going too far by making my names too inaccessible to the modern reader. And then I go a step further by using far away cultures as models, too. Readers and writers of fantasy tend to rely on medieval Europe as their bridge into fantasy worlds, and for good reason. It takes a lot of work to take someone into a culture that's radically different from their own. It can even get in the way of the story if you're not careful. What do you think?