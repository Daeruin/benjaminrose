---
from_wordpress: true
comments_id: 335
title: Interview with Literary Agent Evan Marshall
date: 2011-02-02T23:26:46-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=335
permalink: /post/interview-with-literary-agent-evan-marshall/
categories:
  - writing and publishing
---
I stumbled upon this [interview with literary agent Evan Marshall](http://astorybookworld.blogspot.com/2011/01/interview-with-literary-agent-evan.html) and thought it had some very interesting and positive information. You should check it out.