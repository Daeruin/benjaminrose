---
from_wordpress: true
comments_id: 2651
title: Remembering Neil Peart
date: 2020-01-11T16:43:28-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=2651
permalink: /post/remembering-neil-peart/
categories:
  - music
---
I just learned that Neil Peart, drummer and lyricist for the band Rush, died from brain cancer this week. The depth to which his death has affected me was surprising at first. But as I thought about it, I realized I've probably listened to thousands of hours of his lyrics and music throughout my life.

Rush was the first band I fell in love with as a teenager, so I have a strong emotional connection to their music. They are the band that got me into rock music, and specifically into progressive rock, which has had a huge impact on my musical life—and my musical life is a big part of me.

I have never been a drummer, nor been particularly attuned to percussion. I don't always notice what the drums are doing. I have to take others' words for it that he was one of the best rock drummers of all time. Still, Peart's drum solos are legendary and always awe-inspiring.

For me, it was always Peart's lyrics that affected me the most. Peart has had the distinction of being named both the best and the worst rock lyricist of all time. Peart's early lyrics, in the seventies and early eighties, were heavily inspired by fantasy, science fiction, and mythology—songs like Rivendell, The Necromancer, 2112, The Trees, and Cygnus X-1. As a huge fantasy geek, these songs really appealed to me. In the mid-eighties, he stopped writing fantasy lyrics and moved more into social, emotional, and humanitarian themes—songs like Red Sector A (about the Holocaust), Subdivisions (about the mundanity of living in the suburbs), and Losing It (about growing old). In the nineties and onward, he also started including more religious ideas in his lyrics, with songs like Faithless. The diversity of songs spoke to me in many different circumstances throughout my life.

To honor him for a moment, here's Rush's song Afterimage. Peart wrote the lyrics after the death of a friend. It seems fitting.

"Suddenly, you were gone

From all the lives you left your mark upon . . .

Tried to believe but you know it's no good

This is something that just can't be understood . . .

I feel your presence

I remember

I feel the way you would

This just can't be understood"

<iframe width="560" height="315" src="https://www.youtube.com/embed/OJuFlaHKLoc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
