---
from_wordpress: true
comments_id: 88
title: Some General Floggings
date: 2008-06-14T20:24:04-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=88
permalink: /post/some-general-floggings/
categories:
  - word counters union
---
I am the only person on the WCB Web site with a word count posted for June 14th. So here's a general flogging for every single one of the Word Count Buddies.

Also, I've flogged several of you multiple days in a row. I feel like the repetitious floggings are starting to lose their sting. Kind of like when kids grow out of fearing a spanking. So for those of you who have been slacking multiple days in a row (you know who you are!), I'm just going to strap you up to a flogging machine. That should cover things for a while. I'll let you know when I'm back.