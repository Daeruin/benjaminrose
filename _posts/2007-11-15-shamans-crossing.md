---
from_wordpress: true
comments_id: 44
title: "Shaman's Crossing"
date: 2007-11-15T22:35:54-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=44
permalink: /post/shamans-crossing/
categories:
  - fiction
---
It's been a couple of months since I finished reading this book, and I'm only now finding time to write about it. [_Shaman's Crossing_](http://www.amazon.com/Shamans-Crossing-Book-Soldier-Trilogy/dp/0060758287/ref=pd_sim_b) is the first book in [Robin Hobb's](http://en.wikipedia.org/wiki/Robin_Hobb) latest trilogy, Soldier Son. The second book, [_Forest Mage_](http://www.amazon.com/Forest-Mage-Soldier-Trilogy-Book/dp/B000OCZEPK/ref=pd_sim_b), was published last year, and the third installment, [_Renegade's Magic_](http://www.amazon.com/Renegades-Magic-Three-Soldier-Trilogy/dp/0060757647/ref=pd_lpo_k2_dp_k2a_3_txt?pf_rd_p=304485601&pf_rd_s=lpo-top-stripe-2&pf_rd_t=201&pf_rd_i=0060757620&pf_rd_m=ATVPDKIKX0DER&pf_rd_r=17YAZ0K9R32GR8FP94E6), is due out in January of 2008.

_Shaman's Crossing_ is the story of Nevare Burvelle, a second son in a country where one's occupation is decided by the order of birth. As a second son, Nevare is a soldier son, destined to an army career. (First sons inherit their father's profession and estate, and third sons are dedicated to the church.) However, his life becomes complicated when he comes into contact with the magic of the Specks, a "savage" people living on the borders of Nevare's country. The book details Nevare's childhood and education at the Cavalla Academy.

I found _Shaman's Crossing_ an engrossing read. My wife will attest to my foolish decisions to stay up extremely late reading for days in a row while I suffered with sleep deprivation at work. If you read any of the reviews posted on the link I provided to Amazon.com, you'll find that the book caused mixed reactions from Hobb fans. Anyone who has read Robin Hobb's books knows that she tends towards darker themes and slow but excellent plotting. _Shaman's Crossing_ is even slower than Hobb's other books, and so some people felt disappointed. You should realize if you pick up this book that it is not an epic fantasy story overflowing with huge battles and flashy magic. It is more like a study of culture and character set in a fantastic world (the setting is actually a post-colonial-type world). As always, Hobb's characterization is masterful. Her characters are complex, flawed, and realistic, and they will draw you into the story.

So, I highly recommend this book, but with a word of caution to those who aren't sure they would like the slow pace. If you have read and enjoyed any of Hobb's other books, you are likely to enjoy this one, too.

If you have never read anything by Robin Hobb, you should go out and buy her first book, [_Assassin's Apprentice_](http://www.amazon.com/Assassins-Apprentice-Farseer-Trilogy-Book/dp/055357339X/ref=pd_sim_b), right now. _Assassin's Apprentice_ is the first book of her Farseer Trilogy, and it's definitely among the best books I have ever read. It caught my eye way back in 1996 when it first came out, but I didn't pick it up because of the ridiculous character names in the book's blurb: King Shrewd, Prince Chivalry, Prince Verity, and so forth. I wish I had given the book a chance, though. The names turn out to have a legitimate reason behind them (and of course Hobb is partly playing off of our childhood fairy tale character Prince Charming by giving his name a darker, more realistic twist).

Later, when Hobb published her Liveship Trader series (a trilogy that is loosely connected to the Farseer Trilogy), I was again tempted but eventually decided not to pick up the book. My regretful decision was again based on a feeling that there was something ridiculous about the book. This time it was the idea of sentient ships—the figurehead can move and speak—and I just couldn't imagine how the book could not be silly. I was so wrong. If possible, I enjoyed the Liveship Trader trilogy even more than the Farseer Trilogy. Hobb has a talent for turning tired fantasy tropes into something richer, darker, and more satisfying than just about anything else out there.

If you're looking for something good to read, Hobb's books will soon number 12, and that should keep you busy for quite some time. I hope you'll enjoy them as much as I have.
