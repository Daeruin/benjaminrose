---
from_wordpress: true
comments_id: 131
title: "Renegade's Magic by Robin Hobb"
date: 2009-10-01T20:53:07-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=131
permalink: /post/renegades-magic-by-robin-hobb/
categories:
  - fiction
---
I finished reading [Renegade's Magic](http://www.amazon.com/Renegades-Magic-Soldier-Trilogy-Book/dp/0060757647) by [Robin Hobb](http://www.robinhobb.com/) last night, and I thought I'd post a review for all y'all. Now, I've actually read a number of good books in the past several months, and I've been meaning to post some book reviews for some time now. Some of those books have been hella good reads. But it took Robin Hobb to finally get my fingers tapping the keyboard.

Renegade's Magic is the third book in Hobb's [Soldier Son](http://en.wikipedia.org/wiki/Soldier_Son_Trilogy) trilogy, which started with Shaman's Crossing and continued with Forest Mage.

To give you an idea of how I felt about this book, I think I had just finished chapter three when I leaned over to my wife and said "Nobody should be able to write this good. It's not fair. I just read a chapter about a fat guy who sits around listening to some people arguing over who gets to be the one to feed him, and then they feed him, and it was so good I couldn't stop."

Of course, there was more to the chapter than that. I'm trying not to give any horrible spoilers here, so I'll just say that the fat guy had just used up a vast amount of magic, and he was on the verge of death. In this world, the wild and nomadic Specks revere their Great Ones because of their ability to wield magic. By eating certain foods, they feed their magic abilities and become very large. The larger the Great One, the more respected he or she is. And the person who gets to feed the Great One gets some of that respect, too.

And who but Robin Hobb would dare to write a story where the magic-users are gigantic fat people? And then (spoiler alert) turn the protagonist of the story, a dashing young cavalry officer, into one of those gigantic fat people? And who but Robin Hobb could actually pull it off and keep you reading?

I think I've related my Robin Hobb story here on the blog before, but I'll give it another go. I had seen Hobb's Assassin's Apprentice book on the bookstore shelf before and thought it looked interesting. It even had a cover painting by my favorite fantasy illustrator, Michael Whelan. But I had also seen her Liveship Trader's series there, too, and it just seemed so ridiculous that I couldn't bring myself to give either series a try. I mean, seriously, sentient boats? What the . . . ? How could that possibly be any good? But let me tell you, those books rocketed Hobb right to the top of my list of favorite authors.

Not everybody is as happy as I am with Hobb right now. Renegade's Magic is book three of her Soldier Son trilogy, and [it hasn't been universally well received](http://www.amazon.com/Renegades-Magic-Soldier-Trilogy-Book/product-reviews/0060757647/ref=cm_cr_dp_all_summary?ie=UTF8&showViewpoints=1&sortBy=bySubmissionDateDescending). I think there are a number of factors. First, the pacing of the books is a bit slower than usual, even for Hobb. Her books are a lot more character focused than many fantasy authors, and she always spends a fair bit of time dwelling on the characters' thoughts. Second, the trilogy's setting is completely new and unrelated to her previous three trilogies, and it's not very fantasy-like. It's a post-colonial world with all the accompanying technology, including rifles and even the beginnings of electricity. I'm not much on those types of settings myself—I prefer medieval settings—and if this had been the first book of Hobb's I'd run into, I'm sure I would have passed it over.

But that just highlights my point yet again. You have to try Hobb out. You just have to do it, and you'll discover the joy, too.

Oh, and if you're wondering what the heck kind of book review this is that doesn't even give a brief synopsis of the book—that's what all the hyperlinks are for! Anyway, why do you need a synopsis? Just go out and buy the books already!