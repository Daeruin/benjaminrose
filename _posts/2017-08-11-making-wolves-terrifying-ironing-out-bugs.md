---
from_wordpress: true
comments_id: 1272
title: "Primalcraft: Making wolves terrifying - ironing out the bugs"
date: 2017-08-11T22:53:36-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=1272
permalink: /post/making-wolves-terrifying-ironing-out-bugs/
categories:
  - Minecraft modding
---


{%
  include figure.html
  image-name="primalcraft_terrifyingWolves.png"
  alt="Angry wolves circling the fire"
  caption="Angry wolves circling the fire"
%}


In Primalcraft, I've changed wolf behavior so they're aggressive toward players at night. But I also wanted to provide some protection against them, even in the early game when you don't have shelter or armor. I thought it would be really cool if they were afraid of fire, so I implemented a modification of skeleton behavior to cause wolves to avoid firelight at night. It's pretty awesome when you end up with a small circle of firelight surrounded by growling, red-eyed wolves staring at you menacingly. Don't let that fire go out!

Unfortunately, it wasn't _quite_ working as I had hoped. When they accidentally ended up too close to the firelight, such as when you placed a torch near them, my wolves would try to get away, just like I wanted—but they seemed to be seeking out blocks that were too bright, so once they got there they would immediately move again due to the brightness. They were zipping around like crazy things, only reaching the darkness by accident, it seemed. This is the last thing that was still bugging me about my wolf behavior.

Strangely, the algorithm that helps skeletons find appropriate shelter just picks a set of random blocks within 10 blocks n/s/e/w of their current position, and 6 blocks up or down. This causes the algorithm to sometimes pick blocks that are underground, which have a light level of 0 (pure darkness) by default. That's what was causing my wolves to zip around like crazy. The algorithm was finding a block that seemed to be in pure darkness, but it was actually underground. When the wolf reached that spot (or as close to it as it could get), it would find that it was still in the light, and the whole cycle would repeat again. I'm still not sure why skeletons don't act this way in vanilla Minecraft.

I managed to solve the problem tonight. After picking my random block, I just use a vanilla Minecraft function to check if the block has a collision bounding box. If so, it must be a block that a wolf can't occupy. So then I start checking all the blocks above it to see if I can find a legitimate space (a block without a collision box) for the wolf to go. I only check up 3 blocks, to keep processing to a minimum. If I get that far and still haven't found a good spot, I let the algorithm move on and try to select another random block.

With this improvement, I think my wolves are good to go. By the way, I've also increased their speed such that they will catch up to you even when sprinting. Wolves are quite terrifying in Primalcraft. I've also increased their chances of spawning in every biome. You better make sure you have a good fire going before it gets dark, or you'll get eaten alive!
