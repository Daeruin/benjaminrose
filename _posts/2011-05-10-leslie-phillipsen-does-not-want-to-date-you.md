---
from_wordpress: true
comments_id: 549
title: Leslie Phillipsen Does Not Want to Date You
date: 2011-05-10T22:09:34-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=549
permalink: /post/leslie-phillipsen-does-not-want-to-date-you/
categories:
  - gaming
  - metablogging
  - music
tags:
  - leslie phillipsen
  - roll a d6
---

{%
  include figure.html
  image-url="https://www.denverpost.com/wp-content/uploads/2016/05/20110510__connorp1.jpg?w=500"
  width="500"
  height="348"
  alt="Connor Anderson, Aaron Mull, Zac Smith, and Leslie Phillipsen"
  caption="Connor Anderson, Aaron Mull, Zac Smith, and Leslie Phillipsen"
  link="https://www.denverpost.com/2011/05/10/greeley-man-has-dungeons-dragons-hit-online-with-roll-a-d6/"
%}

Let me break some news to all you poor, hopeful RPG geeks out there: Leslie Phillipsen does not want to date you. She has a deep dark secret. How do I know this? It's simple, really.

I'm in the habit of checking my site statistics every few days, and after [posting the video](/post/roll-a-d6-music-video-featuring-hot-rpg-chick-and-her-goofy-pals/ "Roll a d6 – Music Video with Hot RPG Chick and Her Nerdy RPG Pals"), I noticed a few odd search terms that had been leading people to my blog. One was "who is the roll a d6 chick." I looked at that, and an evil idea sparked in my mind. Obviously, someone wanted to know who she was. Heck, _I_ wanted to know who she was.

So I went to Google and searched for "who is the roll a d6 chick." After a few minutes, I found a MySpace account with her picture, and even better, her name was in the video's credits on YouTube. Then I edited my post to add her name, along with the other actors in the video. And then I waited.

The next day, traffic to my blog doubled. And it all came from one search term: "Leslie Phillipsen." Traffic has remained at double its level for the past three days. (Not that my traffic is amazingly high even when doubled, but still.)

So now I was really curious. I wondered how much attention poor Leslie was actually getting. Naturally, I went to Google and searched for "Leslie Phillipsen." Lo and behold, there sits my blog post, right beneath Leslie's Facebook, Twitter, and MySpace accounts.

But here's the gist of the whole story. You'll also find on that page of Google results a [story from the Greeley Tribune](http://www.greeleytribune.com/article/20110509/NEWS/705099915/1002&parentprofile=1) that reveals a deep dark secret: Connor Anderson, the guy who wrote and filmed the video, has only played Dungeons and Dragons a couple of times.

**And his friends—including Leslie Phillipsen—are just actors along for the ride.**

I hate to be the one to break the news to you. Leslie Phillipsen is not a wizard. She won't call lightning from the sky. And I seriously doubt she wants to play D&D with you, or date anyone who plays D&D. My guess is she already has a boyfriend, or at least enough good-looking guys willing to take her out on a date, give her chocolate, and not ask her to play D&D.

Sorry, guys. Keep fantasizing. 'Cause that's all you're gonna get.

**Update 13/2/2012:** I've posted a follow-up to this post: [Leslie Phillipsen DOES Want to Date You](/post/leslie-phillipsen-does-want-to-date-you/ "Leslie Phillipsen DOES Want to Date You!")!
