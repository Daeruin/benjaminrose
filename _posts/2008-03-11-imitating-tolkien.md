---
from_wordpress: true
comments_id: 56
title: Imitating Tolkien
date: 2008-03-11T20:25:30-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/blogs/noesis/?p=56
permalink: /post/imitating-tolkien/
categories:
  - writing and publishing
---
I ran across a link to a very well-written article discussing why fantasy writers should or should not imitate Tolkien. The link was posted by a fellow named Matt (aka Deliverator) as part of a discussion on [The Riddle of Steel fan forums](http://trosfans.com/forum/viewtopic.php?f=28&t=72). We were talking about different styles of roleplaying, and naturally Tolkien came up as an example upon which to base a roleplaying game story.

Sarah Monette, the article's author, makes some very astute and well-spoken points. She expresses thoughts that have sat half-formed in my brain for years now. I think you'll really enjoy reading this article. I did.

[The article.](http://www.reflectionsedge.com/archives/mar2005/dtw_sm.html)