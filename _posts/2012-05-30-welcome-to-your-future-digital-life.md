---
from_wordpress: true
comments_id: 912
title: Welcome to Your Future Digital Life
date: 2012-05-30T15:14:03-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=912
permalink: /post/welcome-to-your-future-digital-life/
categories:
  - 'movies &amp; TV'
  - software
---
This is one of those times where I post something without saying much about it. The video pretty much illustrates it all. Hilarious and horrifying.

<iframe src="https://www.youtube.com/embed/IFe9wiDfb0E?rel=0" width="500" height="315" frameborder="0" allowfullscreen="allowfullscreen"></iframe>
