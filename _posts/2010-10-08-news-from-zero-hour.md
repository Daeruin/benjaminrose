---
from_wordpress: true
comments_id: 264
title: News from Zero Hour
date: 2010-10-08T10:34:32-08:00
author: Ben
layout: article
guid: http://www.benjaminrose.com/?p=264
permalink: /post/news-from-zero-hour/
categories:
  - music
---
After hearing nothing for a long while after Zero Hour released their last album, we finally get something new. Founding members Jasun and Troy Tipton are getting back together with Zero Hour's original vocalist for a new project called Cynthesis. I got this message from Zero Hour's Facebook page today:

> If you haven't already, please check out Cynthesis.
>
> Cynthesis consists of:
> Troy Tipton - Bass and backing Vocals
> Erik Rosvold - Lead and backing Vocals
> Jasun Tipton - Guitars, Keys and backing Vocals
> Sean Flanegan - Drums
>
> <http://www.facebook.com/pages/Cynthesis/151408508214843>
>
> <http://www.myspace.com/cynthesisband>

Check out the MySpace link and listen the music sample. It sounds like it's going to be more on the progressive side than metal.
